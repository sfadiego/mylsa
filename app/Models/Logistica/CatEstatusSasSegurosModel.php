<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatEstatusSasSegurosModel extends Modelo  
{
    protected $table = 'cat_estatus_sas_seguros';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ESTATUS
    ];
}
