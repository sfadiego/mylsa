<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class FaltantesModel extends Modelo
{
    protected $table = 'faltantes_bodyshop';
    const ID = 'id';
    const FOLIO = 'folio';
    const ID_UNIDAD = 'id_unidad';
    const ID_COLOR = 'id_color';
    const SERIE = 'serie';
    const OBSERVACIONES = 'observaciones';
    const SOLICITUD = 'solicitud';
    const RECIBOS = 'recibos';
    const FECHA_ENTREGA = 'fecha_entrega';

    protected $fillable = [
        self::FOLIO,
        self::ID_UNIDAD,
        self::ID_COLOR,
        self::SERIE,
        self::OBSERVACIONES,
        self::SOLICITUD,
        self::RECIBOS,
        self::FECHA_ENTREGA
    ];
}
