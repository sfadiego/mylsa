<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class LogisticaModel extends Modelo
{
    protected $table = 'logistica';
    const ID = 'id';
    const FECHA_ENTREGA_CLIENTE = 'fecha_entrega_cliente';
    const HORA_ENTREGA_CLIENTE = 'hora_entrega_cliente';
    const ECO = 'eco';
    const ID_UNIDAD = 'id_unidad';
    const ID_COLOR = 'id_color';
    const SERIE = 'serie';
    const ID_CLIENTE = 'id_cliente';
    const ID_VENDEDOR = 'id_vendedor';
    const TIPO_OPERACION = 'tipo_operacion';
    const IVA_DESGLOSADO = 'iva_desglosado';
    const TOMA = 'toma';
    const FECHA_PROMESA_VENDEDOR = 'fecha_promesa_vendedor';
    const HORA_PROMESA_VENDEDOR = 'hora_promesa_vendedor';
    const UBICACION = 'ubicacion';
    const REPROGRAMACION = 'reprogramacion';
    const SEGURO = 'seguro';
    const PERMISO_PLACAS = 'permiso_placas'; //0 PERMISO 1 PLACAS
    const ACCESORIOS = 'accesorios';
    const SERVICIO = 'servicio';
    const FECHA_PROGRAMACION = 'fecha_programacion';
    const HORA_PROGRAMACION = 'hora_programacion';
    const ID_MODO = 'id_modo';
    const ID_CARRIL = 'id_carril';
    protected $fillable = [
        self::FECHA_ENTREGA_CLIENTE,
        self::HORA_ENTREGA_CLIENTE,
        self::ECO,
        self::ID_UNIDAD,
        self::ID_COLOR,
        self::SERIE,
        self::ID_CLIENTE,
        self::ID_VENDEDOR,
        self::TIPO_OPERACION,
        self::IVA_DESGLOSADO,
        self::TOMA,
        self::FECHA_PROMESA_VENDEDOR,
        self::HORA_PROMESA_VENDEDOR,
        self::UBICACION,
        self::REPROGRAMACION,
        self::SEGURO,
        self::PERMISO_PLACAS,
        self::ACCESORIOS,
        self::SERVICIO,
        self::FECHA_PROGRAMACION,
        self::HORA_PROGRAMACION,
        self::ID_MODO,
        self::ID_CARRIL
    ];
}
