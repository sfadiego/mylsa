<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class DeTablasSitemasTempModel extends ModeloNominas
{
    protected $table = 'de_TablasSitemasTemp';
     
    
    const ID = "id";
    const id_Origen = "id_Origen";
    const Valor_1 = "Valor_1";
    const Valor_2 = "Valor_2";
    const Valor_3 = "Valor_3";
   
    protected $fillable = [
        self::id_Origen,
        self::Valor_1,
        self::Valor_2,
        self::Valor_3
    ];
}