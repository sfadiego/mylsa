<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTurnosImssModel extends ModeloNominas
{
    protected $table = 'ca_TurnosImss';
     
    
    const ID = "id_TurnosImss";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}