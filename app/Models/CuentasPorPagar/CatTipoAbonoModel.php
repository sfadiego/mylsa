<?php

namespace App\Models\CuentasPorPagar;

use App\Models\Core\Modelo;

class CatTipoAbonoModel extends Modelo
{
    protected $table = 'cat_tipo_abono';
    const ID = "id";
    const NOMBRE = 'nombre';

    const ENGANCHE = 1;
    const ABONO = 2;
    const UN_SOLO_PAGO = 3;

    protected $fillable = [
        self::NOMBRE
    ];
}
