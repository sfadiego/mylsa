<?php

namespace App\Models\CuentasPorPagar;

use App\Models\Core\Modelo;

class CuentasPorPagarModel extends Modelo
{
    protected $table = 'cuentas_por_pagar';
    const ID = "id";
    const FOLIO_ID = "folio_id";
    const PROVEEDOR_ID = "proveedor_id";
    const CONCEPTO = "concepto";
    const TIPO_FORMA_PAGO_ID = "tipo_forma_pago_id";
    const TIPO_PAGO_ID = "tipo_pago_id";
    const PLAZO_CREDITO_ID = "plazo_credito_id";
    const IMPORTE = "importe";
    const TOTAL = "total";
    const COMENTARIOS = "comentarios";
    const ENGANCHE = "enganche";
    const TASA_INTERES = "tasa_interes";
    const INTERESES = "intereses";
    const ESTATUS_CUENTA_ID = "estatus_cuenta_id";
    const FECHA = "fecha";

    protected $fillable = [
        self::FOLIO_ID,
        self::PROVEEDOR_ID,
        self::ESTATUS_CUENTA_ID,
        self::CONCEPTO,
        self::TIPO_FORMA_PAGO_ID,
        self::TIPO_PAGO_ID,
        self::PLAZO_CREDITO_ID,
        self::IMPORTE,
        self::TOTAL,
        self::COMENTARIOS,
        self::ENGANCHE,
        self::TASA_INTERES,
        self::INTERESES,
        self::FECHA,
    ];
}
