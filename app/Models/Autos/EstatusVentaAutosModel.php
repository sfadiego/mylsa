<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class EstatusVentaAutosModel extends Modelo
{
    protected $table = 'estatus_venta_autos';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_PRE_PEDIDO = 1;
    const ESTATUS_FINALIZADA = 2;
    const PAGADA = 3;
    const ESTATUS_CANCELADA = 4;
    const ESTATUS_DEVOLUCION = 5;

    protected $fillable = [
        self::NOMBRE
    ];
}
