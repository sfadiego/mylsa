<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DetalleRemisionModel extends Modelo
{
    protected $table = 'detalle_remision';
    const ID = "id";
    const REMISIONID = "remisionId";
    const PUERTAS = "puertas";
    const CILINDROS = "cilindros";
    const TRANSMISION = "transmision";
    const COMBUSTIBLE_ID = "cumbustibleId";
    const CAPACIDAD = "capacidad";
    const COLORINTID = "colorIntId";
    const COLOREXTID = "colorExtId";

    protected $fillable = [
        self::REMISIONID,
        self::PUERTAS,
        self::CILINDROS,
        self::TRANSMISION,
        self::COMBUSTIBLE_ID,
        self::CAPACIDAD,
        self::COLORINTID,
        self::COLOREXTID,
    ];
}
