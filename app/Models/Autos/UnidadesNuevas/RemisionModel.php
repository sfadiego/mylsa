<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;

class RemisionModel extends Modelo
{
    protected $table = 'remisiones';
    const ID = "id";
    const LINEA_ID = 'lineaId';
    const CATALOGO_FORDID = 'catalogoFordId';
    const CARLINEID = 'carLineId';
    const CATREALID = 'catRealId';
    const ECONOMICO = 'economico';
    const DESCUENTO_MAX = 'des_max';
    const TENENCIA = 'tenencia';
    const SEG_COB_AMPLIA = 'segCobAmplia';
    const PLAN_GANE = 'planGane';
    const ORDEN_REPORTE = 'ordenReporte';
    const UNIDAD_IMPORTAA = 'unidadImportada';
    const TIPO_AUTO = 'tipoAuto';
    const CLAVE_ISAN = 'claveIsan';
    const CTA_MENUDO = 'ctaMenudeo';
    const CTA_FLOTILLA = 'ctaFlotilla';
    const CTA_CONAUTO = 'ctaConauto';
    const CTA_INTERCAMBIO = 'ctaIntercambio';
    const CTA_PLLENO = 'ctaPlleno';
    const CTA_INVENTARIO = 'ctaInventario';
    const VTA_MENUDEO = 'ventaMenudeo';
    const VTA_FLOTILLA = 'vtaFlotilla';
    const VTA_CONAUTO = 'vtaConauto';
    const VTA_INTERCAMBIO = 'vtaIntercambio';
    const VTA_PLLENO = 'ventaPlleno';
    const USERID = 'userId';
    const C_SUBTOTAL = 'cSubtotal';
    const V_SUBTOTAL = 'vSubtotal';
    const C_IVA = 'cIva';
    const V_IVA = 'vIva';
    const C_PLISTA = 'cPlista';
    const V_PLISTA = 'vPlista';
    const INV = 'inv';
    const FECHA_PEDIMENTO = 'fechaPedimento';
    const PEDIMENTO = 'pedimento';
    const FECHA_REMISION = 'fechaRemsion';
    const SERIE = 'serie';
    const MOTOR = 'motor';
    const INTERCAMBIO = 'intercambio';
    const PROVEEDOR_ID = 'proveedorId';
   


    protected $fillable = [
        self::LINEA_ID,
        self::CATALOGO_FORDID,
        self::CARLINEID,
        self::CATREALID,
        self::ECONOMICO,
        self::DESCUENTO_MAX,
        self::TENENCIA,
        self::SEG_COB_AMPLIA,
        self::PLAN_GANE,
        self::ORDEN_REPORTE,
        self::UNIDAD_IMPORTAA,
        self::TIPO_AUTO,
        self::CLAVE_ISAN,
        self::CTA_MENUDO,
        self::CTA_FLOTILLA,
        self::CTA_CONAUTO,
        self::CTA_INTERCAMBIO,
        self::CTA_PLLENO,
        self::CTA_INVENTARIO,
        self::VTA_MENUDEO,
        self::VTA_FLOTILLA,
        self::VTA_CONAUTO,
        self::VTA_INTERCAMBIO,
        self::VTA_PLLENO,
        self::USERID,
        self::C_SUBTOTAL,
        self::V_SUBTOTAL,
        self::C_IVA,
        self::V_IVA,
        self::C_PLISTA,
        self::V_PLISTA,
        self::INV,
        self::FECHA_PEDIMENTO,
        self::PEDIMENTO,
        self::FECHA_REMISION,
        self::SERIE,
        self::MOTOR,
        self::INTERCAMBIO,
        self::PROVEEDOR_ID,
    ];
}
