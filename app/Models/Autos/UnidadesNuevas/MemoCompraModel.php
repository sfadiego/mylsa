<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MemoCompraModel extends Modelo
{
    protected $table = 'memo_compra';
    const ID = 'id';
    const REMISION_ID = 'remisionId';
    const ORIGEN_ID = 'origenId';
    const MES = 'mes';
    const NO_PRODUCTO = 'noProducto';
    const MEMO = 'memo';
    const COSTO_REMISION = 'costoRemision';
    const COMPRAR = 'comprar';
    const FECHA = 'fecha';
    const MES_COMPRA = 'mesCompra';
    const FP = 'fp';
    const DIAS = 'dias';

    protected $fillable = [
        self::REMISION_ID,
        self::ORIGEN_ID,
        self::MES,
        self::NO_PRODUCTO,
        self::MEMO,
        self::COSTO_REMISION,
        self::COMPRAR,
        self::FECHA,
        self::MES_COMPRA,
        self::FP,
        self::DIAS,
    ];
}
