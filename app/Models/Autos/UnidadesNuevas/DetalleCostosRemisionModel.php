<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DetalleCostosRemisionModel extends Modelo
{
    protected $table = 'detalle_costos_remision';
    const ID = "id";
    const REMISIONID = "remisionId";
    const C_VALOR_UNIDAD = "c_valor_unidad";
    const V_VALOR_UNIDAD = "v_valor_unidad";
    const C_EQUIPO_BASE = "c_equipo_base";
    const V_EQUIPO_BASE = "v_equipo_base";
    const C_TOTAL_BASE = "c_total_base";
    const V_TOTAL_BASE = "v_total_base";
    const C_OTROS_GASTOS = "c_otros_gastos";
    const V_OTROS_GASTOS = "v_otros_gastos";
    const C_DEDUCCION_FORD = "c_deduccion_ford";
    const V_DEDUCCION_FORD = "v_deduccion_ford";
    const C_SEG_TRASLADO = "c_seg_traslado";
    const V_SEG_TRASLADO = "v_seg_traslado";
    const C_GASTOS_TRASLADO = "c_gastos_traslado";
    const V_GASTOS_TRASLADO = "v_gastos_traslado";
    const C_GASTOS_ACOND = "c_gastos_acond";
    const V_GASTOS_ACOND = "v_gastos_acond";
    const C_IMP_IMPORT = "c_imp_import";
    const V_IMP_IMPORT = "v_imp_import";
    const C_FLETES_EXT = "c_fletes_ext";
    const V_FLETES_EXT = "v_fletes_ext";
    const C_ISAN = "c_isan";
    const V_ISAN = "v_isan";
    const AP_FONDO_PUB = "ap_fondo_pub";
    const CUOTA_AMDA = "cuota_amda";
    const CUOTA_COPARMEX = "cuota_coparmex";
    const CUOTA_ASO_FORD = "cuota_aso_ford";
    

    protected $fillable = [
        self::REMISIONID,
        self::C_VALOR_UNIDAD,
        self::V_VALOR_UNIDAD,
        self::C_EQUIPO_BASE,
        self::V_EQUIPO_BASE,
        self::C_TOTAL_BASE,
        self::V_TOTAL_BASE,
        self::C_OTROS_GASTOS,
        self::V_OTROS_GASTOS,
        self::C_DEDUCCION_FORD,
        self::V_DEDUCCION_FORD,
        self::C_SEG_TRASLADO,
        self::V_SEG_TRASLADO,
        self::C_GASTOS_TRASLADO,
        self::V_GASTOS_TRASLADO,
        self::C_GASTOS_ACOND,
        self::V_GASTOS_ACOND,
        self::C_IMP_IMPORT,
        self::V_IMP_IMPORT,
        self::C_FLETES_EXT,
        self::V_FLETES_EXT,
        self::C_ISAN,
        self::V_ISAN,
        self::AP_FONDO_PUB,
        self::CUOTA_AMDA,
        self::CUOTA_COPARMEX,
        self::CUOTA_ASO_FORD,
    ];
}
