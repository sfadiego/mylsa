<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class FacturacionModel extends Modelo
{
    protected $table = 'facturacion_unidades_nuevas';
    const ID = 'id';
    const REMISION_ID = 'remisionId';
    const PROCEDENCIA = 'procedencia';
    const HOLOGRAMA = 'holograma';
    const VENDEDOR = 'vendedor';
    const ADUANA = 'aduana';
    const REPUVE = 'repuve';

    protected $fillable = [
        self::REMISION_ID,
        self::PROCEDENCIA,
        self::HOLOGRAMA,
        self::VENDEDOR,
        self::ADUANA,
        self::REPUVE,
    ];
}
