<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
class ListPreciosUNModel extends Modelo
{
    protected $table = 'lista_precios_un';
    const ID = "id";
    const CAT = 'cat';
    const CLAVE_VEHICULAR = 'clave_vehicular';
    const DESCRIPCION = 'descripcion';
    const PRECIO_LISTA = 'precio_lista';
    const PRECIO_CLIENTE = 'precio_cliente';
    const BONO_FC = 'bono_fc';
    const PRECIO_BONO = 'precio_bono';
    const PRECIO_ACTIVO_ID = 'precio_activo_id';

    protected $fillable = [
        self::CAT,
        self::DESCRIPCION,
        self::CLAVE_VEHICULAR,
        self::PRECIO_LISTA,
        self::PRECIO_CLIENTE,
        self::BONO_FC,
        self::PRECIO_BONO,
        self::PRECIO_ACTIVO_ID,
    ];
}
