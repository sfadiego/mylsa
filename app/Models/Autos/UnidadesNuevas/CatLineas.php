<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatLineas extends Modelo
{
    protected $table = 'cat_lineas';
    const ID = "id";
    const MODELO = 'modelo';
    const DESCRIPCION = 'descripcion';
    const LINEA = 'linea';

    protected $fillable = [
        self::MODELO,
        self::DESCRIPCION,
        self::LINEA,
    ];
}
