<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class DocumentosVentaModel extends Modelo
{
    protected $table = 'documentos_venta';
    const ID = "id";
    const ID_VENTA_UNIDAD = "id_venta_unidad";
    const RUTA_ARCHIVO = "ruta_archivo";
    const NOMBRE_ARCHIVO = "nombre_archivo";
    const ID_TIPO_DOCUMENTO = "id_tipo_documento";

    const DIRECTORIO_DOCUMENTOS = 'unidades/documentos';
    
    protected $fillable = [
        self::NOMBRE_ARCHIVO,
        self::RUTA_ARCHIVO,
        self::ID_VENTA_UNIDAD,
        self::ID_TIPO_DOCUMENTO
    ];
}
