<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class InformacionDocumentosVentaModel extends  Modelo
{
    protected $table = 'informacion_documentos_venta';
    const ID = "id";
    const TIPO_VENTA = "tipo_venta";
    const ID_VENTA_UNIDAD = "id_venta_unidad";
    const FECHA_FACTURA = "fecha_factura";
    const FECHA_ULTIMO_INGRESO = "fecha_ultimo_ingreso";
    const FECHA_CONTRATO = "fecha_contrato";
    const FECHA_CARTA_CONAUTO = "fecha_carta_conauto";
    const FECHA_ORDEN_COMPRA = "fecha_orden_compra";
    const FECHA_VENTA_SMART = "fecha_venta_smart";
    const FECHA_INGRESO_SMART = "fecha_ingreso_smart";
    const FIRMA_GERENTE = 'firma_gerente';
    const FIRMA_OTROS = 'firma_otros';
    
    protected $fillable = [
        self::ID_VENTA_UNIDAD,
        self::TIPO_VENTA,
        self::FECHA_FACTURA,
        self::FECHA_ULTIMO_INGRESO,
        self::FECHA_CONTRATO,
        self::FECHA_CARTA_CONAUTO,
        self::FECHA_ORDEN_COMPRA,
        self::FECHA_VENTA_SMART,
        self::FECHA_INGRESO_SMART,
        self::FIRMA_GERENTE,
        self::FIRMA_OTROS
    ];
}
