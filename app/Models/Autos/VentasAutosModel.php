<?php

namespace App\Models\Autos;

use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;
use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;

class VentasAutosModel extends Modelo
{
    protected $table = 'ventas_autos';
    const ID = "id";
    const ID_UNIDAD = "id_unidad";
    const ID_FOLIO = "id_folio";
    const ID_CLIENTE = "id_cliente";
    const ID_ESTATUS = 'id_estatus';
    const ID_TIPO_AUTO = 'id_tipo_auto'; //nuevo - semi nuevo
    const MONEDA = "moneda";
    const ID_PLAN_CREDITO = "id_plan_credito";
    const VENDEDOR_ID = "vendedor_id";
    const SERVICIO_CITA_ID = "servicio_cita_id";

    const FECHA_REGRESO = "fecha_regreso";
    const NO_ECONOMICO = "no_economico";
    const NO_SERIE = "no_serie";
    const ID_ASESOR = "id_asesor";
    const FIRMA_CLIENTE = "firma_cliene";
    const FIRMA_ASESOR = "firma_asesor";


    const FECHA_INICIO = "fecha_fin";
    const FECHA_FIN = "fecha_inicio";

    const ID_VENTA_AUTO = 'id_venta_auto';
    const IMPUESTO_ISAN = 'impuesto_isan';
    const IVA = 'iva';
    const DESCUENTO = 'descuento';

    protected $fillable = [
        self::ID_UNIDAD,
        self::ID_FOLIO,
        self::VENDEDOR_ID,
        self::ID_CLIENTE,
        self::ID_ESTATUS,
        self::ID_TIPO_AUTO,
        self::MONEDA,
        self::SERVICIO_CITA_ID,
        self::NO_ECONOMICO,
        self::NO_SERIE,
        self::ID_ASESOR,
        self::FIRMA_CLIENTE,
        self::FIRMA_ASESOR,
        self::IMPUESTO_ISAN,
        self::IVA,
        self::DESCUENTO
    ];



    public function folio()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID, self::ID_FOLIO);
    }

    public function unidad()
    {
        return $this->hasOne(UnidadesModel::class, UnidadesModel::ID, self::ID_UNIDAD);
    }

    public function cliente()
    {
        return $this->hasOne(ClientesModel::class, ClientesModel::ID, self::ID_CLIENTE);
    }

    public function seguridad()
    {
        return $this->hasOne(SeguridadModel::class, SeguridadModel::ID, self::ID_UNIDAD);
    }

    public function ahorro_combustible()
    {
        return $this->hasOne(AhorroCombustibleModel::class, AhorroCombustibleModel::ID_VENTA_AUTO, self::ID);
    }

    public function comentarios()
    {
        return $this->hasOne(ComentariosModel::class, ComentariosModel::ID_VENTA_AUTO, self::ID);
    }

    public function confort()
    {
        return $this->hasOne(ConfortModel::class, ConfortModel::ID_VENTA_AUTO, self::ID);
    }

    public function desempeno()
    {
        return $this->hasOne(DesempenoModel::class, DesempenoModel::ID_VENTA_AUTO, self::ID);
    }

    public function documentos()
    {
        return $this->hasOne(DocumentosModel::class, DocumentosModel::ID_VENTA_AUTO, self::ID);
    }

    public function especiales()
    {
        return $this->hasOne(EspecialesModel::class, EspecialesModel::ID_VENTA_AUTO, self::ID);
    }

    public function extras()
    {
        return $this->hasOne(ExtrasModel::class, ExtrasModel::ID_VENTA_AUTO, self::ID);
    }

    public function funcionamiento_vehiculo()
    {
        return $this->hasOne(FuncionamientoVehiculoModel::class, FuncionamientoVehiculoModel::ID_VENTA_AUTO, self::ID);
    }

    public function iluminacion()
    {
        return $this->hasOne(IluminacionModel::class, IluminacionModel::ID_VENTA_AUTO, self::ID);
    }

    public function otras_tecnologias()
    {
        return $this->hasOne(OtrasTecnologiasModel::class, OtrasTecnologiasModel::ID_VENTA_AUTO, self::ID);
    }

    public function seguridad_pasiva()
    {
        return $this->hasOne(SeguridadPasivaModel::class, SeguridadPasivaModel::ID_VENTA_AUTO, self::ID);
    }

    public function vehiculo_hibrido()
    {
        return $this->hasOne(VehiculoHibridoModel::class, VehiculoHibridoModel::ID_VENTA_AUTO, self::ID);
    }
}
