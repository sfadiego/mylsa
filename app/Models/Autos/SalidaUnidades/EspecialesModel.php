<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class EspecialesModel extends Modelo
{
    protected $table = 'especiales';
    const ID = "id";
    const PEPS = "peps";
    const APERTIRA_CAJUELA = "apertura_cajuela";
    const ENCENDIDO_REMOTO = "encendido_remoto";
    const TECHO_PANORAMICO = "techo_panoramico";
    const AIRE_ACONDICIONADO = "aire_acondicionado";
    const EATC = "eatc";
    const CONSOLA_ENFRIAMIENTO = "consola_enfriamiento";
    const VOLANTE_AJUSTE_ALTURA = "volante_ajuste_altura";
    const VOLANTE_CALEFACTADO = "volante_calefactado";
    const ESTRIBOS_ELECTRICOS = "estribos_electricos";
    const APERTURA_CAJA_CARGA = "apertura_caja_carga";
    const LIMPIAPARABRISAS = "limpiaparabrisas";
    const ESPEJO_ELECTRICO = "espejo_electrico";
    const ESPEJOS_ABATIBLES = "espejo_abatible";
    const ID_VENTA_AUTO = 'id_venta_auto';
    
    protected $fillable = [
        self::PEPS,
        self::APERTIRA_CAJUELA,
        self::ENCENDIDO_REMOTO,
        self::TECHO_PANORAMICO,
        self::AIRE_ACONDICIONADO,
        self::EATC,
        self::CONSOLA_ENFRIAMIENTO,
        self::VOLANTE_AJUSTE_ALTURA,
        self::VOLANTE_CALEFACTADO,
        self::ESTRIBOS_ELECTRICOS,
        self::APERTURA_CAJA_CARGA,
        self::LIMPIAPARABRISAS,
        self::ESPEJO_ELECTRICO,
        self::ESPEJOS_ABATIBLES,
        self::ID_VENTA_AUTO
    ];
}
