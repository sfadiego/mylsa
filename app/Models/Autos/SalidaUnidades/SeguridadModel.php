<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class SeguridadModel extends Modelo
{
    protected $table = 'seguridad';
    const ID = "id";
    const SISTEMA_FRENADO = "sistema_frenado";
    const CONTROL_TRACCION = "control_traccion";
    const ESC = "esc";
    const RSC = "rsc";
    const CONTROL_TORQUE_CURVAS = "control_torque_curvas";
    const CONTROL_CURVAS = "control_curvas";
    const CONTROL_BALANCE_REMOLQUE = "control_balance_remolque";
    const FRENOS_ELECTRONICOS = "frenos_electronicos";
    const CONTROL_ELECTRONICO_DES = "control_electronico_des";
    const ID_VENTA_AUTO = 'id_venta_auto';
    
    protected $fillable = [
        self::SISTEMA_FRENADO,
        self::CONTROL_TRACCION,
        self::ESC,
        self::RSC,
        self::CONTROL_TORQUE_CURVAS,
        self::CONTROL_CURVAS,
        self::CONTROL_BALANCE_REMOLQUE,
        self::FRENOS_ELECTRONICOS,
        self::CONTROL_ELECTRONICO_DES,
        self::ID_VENTA_AUTO
    ];
}
