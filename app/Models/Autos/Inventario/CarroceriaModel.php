<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CarroceriaModel extends Modelo
{
    protected $table = 'inventario_carroceria';
    const ID = "id";
    const CARROCERIA_IMG = "carroceria_img";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::CARROCERIA_IMG,
        self::ID_INVENTARIO
    ];
}
