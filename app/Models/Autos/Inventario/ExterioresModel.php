<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class ExterioresModel extends Modelo
{
    protected $table = 'inventario_exteriores';
    const ID = "id";
    const TAPONES_RUEDA = "tapones_rueda";
    const GOMA_LIMPIADORES = "goma_limpiadores";
    const ANTENA = "antena";
    const TAPON_GASOLINA = "tapon_gasolina";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::TAPONES_RUEDA,
        self::GOMA_LIMPIADORES,
        self::ANTENA,
        self::ID_INVENTARIO,
        self::TAPON_GASOLINA
    ];
}
