<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DocumentacionModel extends Modelo
{
    protected $table = 'inventario_documentacion';
    const ID = "id";
    const POLIZA_GARANTIA = "poliza_garantia";
    const SEGURO_RINES = "seguro_rines_documentacion";
    const CERTIFICADO_VERIFICACION = "certificado_verificacion";
    const TARJETA_CIRCULACION = "tarjeta_circulacion";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::POLIZA_GARANTIA,
        self::SEGURO_RINES,
        self::CERTIFICADO_VERIFICACION,
        self::TARJETA_CIRCULACION,
        self::ID_INVENTARIO
    ];
}
