<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class InventarioModel extends Modelo
{
    protected $table = 'inventario_unidad';
    const ID = "id";
    const DIAS_HABILES = "dias_habiles";
    const FIRMA_ELABORA = "firma_elabora";
    const FECHA_ELABORACION = "fecha_elaboracion";
    const FECHA_ACEPTACION = "fecha_aceptacion";
    const FIRMA_CONSUMIDOR = "firma_consumidor";
    const ID_UNIDAD = 'id_unidad';
    
    protected $fillable = [
        self::DIAS_HABILES,
        self::FIRMA_ELABORA,
        self::FECHA_ELABORACION,
        self::FECHA_ACEPTACION,
        self::FIRMA_CONSUMIDOR,
        self::ID_UNIDAD
    ];

    public function cajuela()
    {
        return $this->hasOne(CajuelaModel::class, CajuelaModel::ID, self::ID);
    }

    public function carroceria()
    {
        return $this->hasOne(CarroceriaModel::class, CarroceriaModel::ID, self::ID);
    }

    public function documentacion()
    {
        return $this->hasOne(DocumentacionModel::class, DocumentacionModel::ID, self::ID);
    }

    public function exteriores()
    {
        return $this->hasOne(ExterioresModel::class, ExterioresModel::ID, self::ID);
    }

    public function gasolina()
    {
        return $this->hasOne(GasolinaModel::class, GasolinaModel::ID, self::ID);
    }

    public function interiores()
    {
        return $this->hasOne(InterioresModel::class, InterioresModel::ID, self::ID);
    }

}
