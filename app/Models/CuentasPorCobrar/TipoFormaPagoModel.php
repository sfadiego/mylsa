<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class TipoFormaPagoModel extends Modelo
{
    protected $table = 'tipo_forma_pago';
    const ID = "id";
    const DESCRIPCION = "descripcion";

    const FORMA_CONTADO = 1;
    const FORMA_CREDITO = 2;

    protected $fillable = [
        self::DESCRIPCION,
    ];
}
