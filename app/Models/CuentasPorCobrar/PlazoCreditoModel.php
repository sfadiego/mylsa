<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class PlazoCreditoModel extends Modelo
{
    protected $table = 'plazos_credito';
    const ID = "id";
    const CANTIDAD_MES = "cantidad_mes";
    const NOMBRE = "nombre";

    const UNA_EXHIBICION = 14;
    
    protected $fillable = [
        self::CANTIDAD_MES,
        self::NOMBRE,
    ];
}
