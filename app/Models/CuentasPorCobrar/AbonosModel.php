<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class AbonosModel extends Modelo
{
    protected $table = 'abonos_por_cobrar';
    const ID = "id";
    const CUENTA_POR_COBRAR_ID = "cuenta_por_cobrar_id";
    const TIPO_ABONO_ID = "tipo_abono_id";
    const TOTAL_ABONO = "total_abono";
    const FECHA_VENCIMIENTO = 'fecha_vencimiento';
    const TOTAL_PAGO = "total_pago";
    const FECHA_PAGO = 'fecha_pago';
    const TIPO_PAGO_ID = "tipo_pago_id";
    const DIAS_MORATORIOS = 'dias_moratorios';
    const ESTATUS_ABONO_ID = 'estatus_abono_id';
    const FECHA_ACTUAL = 'fecha_actual';
    const MONTO_MORATORIO = 'monto_moratorio';
    const CFDI_ID = 'cfdi_id';
    const POLIZA_ID = 'poliza_id';
    const CAJA_ID = 'caja_id';

    protected $fillable = [
        self::CUENTA_POR_COBRAR_ID,
        self::TIPO_ABONO_ID,
        self::TOTAL_ABONO,
        self::TOTAL_PAGO,
        self::TIPO_PAGO_ID,
        self::FECHA_PAGO,
        self::FECHA_VENCIMIENTO,
        self::ESTATUS_ABONO_ID,
        self::MONTO_MORATORIO,
        self::CFDI_ID,
        self::CAJA_ID,
        self::POLIZA_ID
    ];
}
