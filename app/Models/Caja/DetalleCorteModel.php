<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class DetalleCorteModel extends Modelo
{
    protected $table = 'detalle_corte_caja';
    
    const ID = "id";
    const CORTE_CAJA_ID = 'corte_caja_id';
    const TIPO_PAGO_ID = 'tipo_pago_id';
    const TOTAL_CANTIDAD_CAJA = 'total_cantidad_caja';
    const TOTAL_CANTIDAD_REPORTADA = 'total_cantidad_reportada';
    const FALTANTES = 'faltantes';
    const SOBRANTES = 'sobrantes';

    protected $fillable = [
        self::CORTE_CAJA_ID,
        self::TIPO_PAGO_ID,
        self::TOTAL_CANTIDAD_CAJA,
        self::TOTAL_CANTIDAD_REPORTADA,
        self::FALTANTES,
        self::SOBRANTES
    ];
}
