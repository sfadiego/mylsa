<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class CatalogoCajaModel extends Modelo
{
    protected $table = 'catalogo_caja';
    
    const ID = "id";
    const NOMBRE = 'nombre';

    protected $fillable = [
        self::NOMBRE
    ];
}
