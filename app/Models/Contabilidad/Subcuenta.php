<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class Subcuenta extends Modelo
{
    protected $table = 'subcuenta';
    const ID = "id";
    const NO_CUENTA = "no_cuenta";
    const NOMBRE_CUENTA = "nombre_cuenta";
    const ID_CUENTA = "id_cuenta";
    


    protected $fillable = [
        self::NO_CUENTA,
        self::NOMBRE_CUENTA,
        self::ID_CUENTA,
    ];
}
