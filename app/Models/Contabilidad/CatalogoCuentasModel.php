<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class CatalogoCuentasModel extends Modelo
{
    protected $table = 'catalogo_cuentas';
    const ID = "id";
    const NO_CUENTA = "no_cuenta";
    const NOMBRE_CUENTA = "nombre_cuenta";
    const CVE_AUXILIAR = "cve_auxiliar";
    const TIPO_CUENTA = "tipo_cuenta";
    const CODIGO = "codigo";
    const NATURALEZA = "naturaleza";
    const ID_MOVIMIENTO = "id_movimiento";

    const CAT_VENTAS = 1;
    const CAT_TRASPASO = 2;
    const CAT_COMPRAS = 3;
    const CAT_DEVOLUCION_PROVEEDOR = 4;
    const CAT_DEVOLUCION_VENTA = 5;
    const CAT_VENTA_AUTOS_NUEVOS = 6;
    const CAT_VENTA_AUTOS_SEMINUEVOS = 7;
    const ORDEN_SERVICIO = 8;
    const ABONO_BANORTE = 16;

    const CARGO = 2;
    const ABONO = 1;

    const CUENTA_VENTA_REFACCIONES_ACCESORIOS_SJR = 866;
    const CUENTA_MANO_OBRA_TALLER_SJR = 894;
    const CUENTA_CLIENTE_SERVICIO = 43;
    const CUENTA_IVA = 130;
    const CUENTA_BANORTE = 16;
    const CUENTA_IVA_COBRADO = 410;
    const CUENTA_CLIENTE_AUTOS_NUEVOS = 40;
    const CUENTA_ISAN_POR_PAGAR = 404;
    const CUENTA_IVA_POR_ACREDITAR = 81;
    const CUENTA_BANCOS = 21;
    const CUENTA_MENUDEO_REFACCIONES = 858;
    

    protected $fillable = [
        self::NO_CUENTA,
        self::NOMBRE_CUENTA,
        self::CVE_AUXILIAR,
        self::TIPO_CUENTA,
        self::CODIGO,
        self::NATURALEZA,
        self::ID_MOVIMIENTO
    ];
}
