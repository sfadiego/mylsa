<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatEstatusPlanPisoModel extends Modelo
{
    protected $table = 'cat_estatus_plan_piso';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ID,
        self::ESTATUS
    ];
}
