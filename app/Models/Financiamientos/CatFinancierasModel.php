<?php

namespace App\Models\Financiamientos;
use App\Models\Core\Modelo;
class CatFinancierasModel extends Modelo    
{
    protected $table = 'cat_financieras';
    const ID = 'id';
    const FINANCIERA = 'financiera';

    protected $fillable = [
        self::ID,
        self::FINANCIERA
    ];
}
