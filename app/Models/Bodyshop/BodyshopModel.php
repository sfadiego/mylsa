<?php

namespace App\Models\Bodyshop;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class BodyshopModel extends Modelo
{
    protected $table = 'bodyshop';
    const ID = 'id';
    const ID_PROYECTO = 'id_cliente';
    const DESCRIPCION = 'descripcion';
    const NUMERO_SINIESTRO = 'numero_siniestro';
    const NUMERO_POLIZA = 'numero_poliza';
    const ID_ANIO = 'id_anio';
    const PLACAS = 'placas';
    const ID_COLOR = 'id_color';
    const ID_MODELO = 'id_modelo';
    const SERIE = 'serie';
    const CITA = 'cita';
    const ID_ASESOR = 'id_asesor';
    const FECHA_CITA = 'fecha_cita';
    const HORA_CITA = 'hora_cita';
    const ID_STATUS = 'id_status';
    const FECHA_INICIO = 'fecha_inicio';
    const FECHA_FIN = 'fecha_fin';
    const TIPO_GOLPE = 'tipo_golpe';
    const COMENTARIOS = 'comentarios';
    

    protected $fillable = [
        self::ID_PROYECTO,
        self::DESCRIPCION,
        self::NUMERO_SINIESTRO,
        self::NUMERO_POLIZA,
        self::ID_ANIO,
        self::PLACAS,
        self::ID_COLOR,
        self::ID_MODELO,
        self::SERIE,
        self::CITA,
        self::ID_ASESOR,
        self::FECHA_CITA,
        self::HORA_CITA,
        self::ID_STATUS,
        self::FECHA_INICIO,
        self::FECHA_FIN,
        self::TIPO_GOLPE,
        self::COMENTARIOS,
    ];
}
