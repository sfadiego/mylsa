<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class RequisicionesModel extends Modelo
{
    protected $table = 'requisicion';
    const ID = "id";
    const FOLIO = "folio";
    const REQUISICION = "requisicion";
    const CLASE_REQUISICION = "clase_requisicion";
    const CLIENTE_ID = 'cliente_id';
    const CONSECUTIVO = 'consecutivo';

    const PRODUCTO_ID = 'producto_id';
    const VENDEDOR_ID = 'vendedor_id';

    protected $fillable = [
        self::FOLIO,
        self::CLASE_REQUISICION,
        self::REQUISICION,
        self::PRODUCTO_ID,
        self::CLIENTE_ID,
        self::CONSECUTIVO,
        self::VENDEDOR_ID
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class, ProductosModel::ID);
    }

    public function vendedor()
    {
        return $this->belongsTo(VendedorModel::class, VendedorModel::ID);
    }

    public function clientes()
    {
        return $this->belongsTo(ClientesModel::class, ClientesModel::ID);
    }
}
