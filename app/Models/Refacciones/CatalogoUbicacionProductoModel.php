<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoUbicacionProductoModel extends Modelo
{
    protected $table = 'catalogo_ubicaciones_producto';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
