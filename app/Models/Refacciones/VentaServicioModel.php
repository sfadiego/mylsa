<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class VentaServicioModel extends Modelo
{
    protected $table = 'venta_servicio';
    const ID = "id";
    const VENTA_ID = 'venta_id';
    const NOMBRE_SERVICIO = 'nombre_servicio';
    const NO_IDENTIFICACION = 'no_identificacion';
    const CANTIDAD = 'cantidad';
    const PRECIO = 'precio';
    const COSTO_MO = 'costo_mo';
    const IVA = 'iva';

    protected $fillable = [
        self::VENTA_ID,
        self::NOMBRE_SERVICIO,
        self::PRECIO,
        self::NO_IDENTIFICACION,
        self::CANTIDAD,
        self::COSTO_MO,
        self::IVA
    ];
}
