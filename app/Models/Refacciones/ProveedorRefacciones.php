<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ProveedorRefacciones extends Modelo
{
    protected $table = 'proveedor_refacciones';
    const ID = "id";
    const PROVEEDOR_NOMBRE = "proveedor_nombre";
    const APELLIDO_MATERNO = "apellido_materno";
    const APELLIDO_PATERNO = "apellido_paterno";

    const PROVEEDOR_RFC = "proveedor_rfc";
    const PROVEEDOR_CALLE = "proveedor_calle";
    const PROVEEDOR_NUMERO = "proveedor_numero";
    const PROVEEDOR_COLONIA = "proveedor_colonia";
    const PROVEEDOR_ESTADO = "proveedor_estado";
    const PROVEEDOR_LOCALIDAD = "localidad";
    const CLAVE_IDENTIFICADOR = "clave_identificador";
    const RAZON_SOCIAL = "razon_social";
    const PROVEEDOR_PAIS = "proveedor_pais";
    const CORREO = "correo";
    const CORREO_SECUNDARIO = "correo_secundario";
    const TELEFONO_SECUNDARIO = "telefono_secundario";
    const TELEFONO = "telefono";
    const NOTAS = "notas";
    const LIMITE_CREDITO = 'limite_credito';
    const SALDO = 'saldo';
    const CODIGO_POSTAL = 'codigo_postal';
    const TIPO_REGISTRO = 'tipo_registro';
    
    const CFDI_ID = 'cfdi_id';
    const FORMA_PAGO_ID = 'forma_pago_id';
    const METODO_PAGO_ID = 'metodo_pago_id';


    protected $fillable = [
        self::PROVEEDOR_NOMBRE,
        self::PROVEEDOR_RFC,
        self::PROVEEDOR_CALLE,
        self::PROVEEDOR_NUMERO,
        self::PROVEEDOR_COLONIA,
        self::PROVEEDOR_ESTADO,
        self::PROVEEDOR_LOCALIDAD,
        self::CLAVE_IDENTIFICADOR,
        self::RAZON_SOCIAL,
        self::APELLIDO_MATERNO,
        self::APELLIDO_PATERNO,
        self::PROVEEDOR_PAIS,
        self::CORREO,
        self::CORREO_SECUNDARIO,
        self::TELEFONO_SECUNDARIO,
        self::TELEFONO,
        self::NOTAS,
        self::CFDI_ID,
        self::METODO_PAGO_ID,
        self::LIMITE_CREDITO,
        self::SALDO,
        self::CODIGO_POSTAL,
        self::FORMA_PAGO_ID,
        self::TIPO_REGISTRO
    ];
}
