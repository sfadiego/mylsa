<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ProductosModel extends Modelo
{
    protected $table = 'producto';
    const ID = "id";
    const CANTIDAD = 'cantidad';
    const DESCRIPCION = "descripcion";
    const UNIDAD = "unidad";
    const INVENTARIABLE = "inventariable";
    const EXISTENCIA = "existencia";
    const PRECIO_ID = "precio_id";
    const PRECIO_FACTURA = "precio_factura";
    const VALOR_UNITARIO = "valor_unitario";

    const BASICO = 'basico';
    const PREFIJO = 'prefijo';
    const SUFIJO = 'sufijo';
    const PESO = 'peso';

    const CLAVE_PROD_SERV_FACTURA = "clave_prod_serv";
    const CLAVE_UNIDAD_FACTURA = "clave_unidad";
    const NO_IDENTIFICACION_FACTURA = "no_identificacion";
    const FACTURA_ID = 'factura_id';
    const TALLER_ID = 'taller_id';
    const ALMACEN_ID = "almacen_id";
    const UBICACION_PRODUCTO_ID = "ubicacion_producto_id";
    const ACTIVO = "activo";
    const FECHA_RANGO_INICIO = "fecha_rango_inicio";
    const FECHA_RANGO_FIN = "fecha_rango_fin";

    const REL_PRECIO = 'rel_precio';
    const REL_PRODUCTO_ALMACEN = 'producto_almacen';
    const REL_PRODUCTO_ORDEN_COMPRA = 'producto_orden_compra';
    const REL_TALLER = 'taller';
    const REL_UBICACION = 'ubicacion';
    const REL_DESGLOSE_PRODUCTO = 'desglose_producto';

    protected $fillable = [
        self::DESCRIPCION,
        self::UNIDAD,
        self::INVENTARIABLE,
        self::EXISTENCIA,
        self::CANTIDAD,
        self::PRECIO_FACTURA,
        self::VALOR_UNITARIO,
        self::FACTURA_ID,
        self::TALLER_ID,
        self::PRECIO_ID,
        self::ACTIVO,
        self::CLAVE_PROD_SERV_FACTURA,
        self::CLAVE_UNIDAD_FACTURA,
        self::NO_IDENTIFICACION_FACTURA,
        self::ALMACEN_ID,
        self::UBICACION_PRODUCTO_ID,
        self::BASICO,
        self::PREFIJO,
        self::SUFIJO,
        self::PESO
    ];

    public function requisicion()
    {
        return $this->hasMany(RequisicionesModel::class);
    }

    public function rel_precio()
    {
        return $this->belongsTo(Precios::class, self::PRECIO_ID);
    }

    public function taller()
    {
        return $this->belongsTo(Talleres::class);
    }

    public function ubicacion()
    {
        return $this->hasOne(CatalogoUbicacionProductoModel::class, CatalogoUbicacionProductoModel::ID, self::UBICACION_PRODUCTO_ID);
    }

    public function almacen()
    {
        return $this->belongsTo(Almacenes::class);
    }

    public function desglose_producto()
    {
        return $this->hasOne(StockProductosModel::class, StockProductosModel::PRODUCTO_ID, self::ID);
    }

    public function producto_almacen()
    {
        return $this->hasMany(ProductoAlmacenModel::class, ProductoAlmacenModel::PRODUCTO_ID, self::ID);
    }

    public function producto_orden_compra()
    {
        return $this->hasMany(ListaProductosOrdenCompraModel::class, ListaProductosOrdenCompraModel::PRODUCTO_ID, self::ID);
    }
}
