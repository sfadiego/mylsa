<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ClientesModel extends Modelo 
{
    protected $table = 'clientes';
    const ID = "id";
    const NUMERO_CLIENTE = 'numero_cliente';
    const TIPO_REGISTRO = 'tipo_registro';
    const NOMBRE = 'nombre';
    const APELLIDO_MATERNO = 'apellido_paterno';
    const APELLIDO_PATERNO = 'apellido_materno';
    const REGIMEN_FISCAL = 'regimen_fiscal';
    const NOMBRE_EMPRESA = 'nombre_empresa';
    const RFC = 'rfc';
    const DIRECCION = 'direccion';
    const NUMERO_INT = 'numero_int';
    const NUMERO_EXT = 'numero_ext';
    const COLONIA = 'colonia';
    const MUNICIPIO = 'municipio';
    const ESTADO = 'estado';
    const PAIS = 'pais';
    const CODIGO_POSTAL = 'codigo_postal';
    const TELEFONO = 'telefono';
    const TELEFONO_2 = 'telefono_secundario';
    const TELEFONO_3 = 'telefono_oficina';
    const FLOTILLERO = 'flotillero';
    const CORREO_ELECTRONICO = 'correo_electronico';
    const CORREO_ELECTRONICO_2 = 'correo_electronico_secundario';
    const FECHA_NACIMIENTO = 'fecha_nacimiento';
    const CFDI_ID = 'cfdi_id';
    const METODO_PAGO_ID = 'metodo_pago_id';
    const FORMA_PAGO = 'forma_pago';
    const SALDO = 'saldo';
    const LIMITE_CREDITO = 'limite_credito';
    const NOTAS = 'notas';
    const ES_CLIENTE = 'es_cliente';
    const APLICA_CREDITO = 'aplica_credito';

    protected $fillable = [
        self::NOMBRE,
        self::NUMERO_CLIENTE,
        self::TIPO_REGISTRO,
        self::NOMBRE,
        self::APELLIDO_MATERNO,
        self::APELLIDO_PATERNO,
        self::REGIMEN_FISCAL,
        self::NOMBRE_EMPRESA,
        self::RFC,
        self::DIRECCION,
        self::NUMERO_INT,
        self::NUMERO_EXT,
        self::COLONIA,
        self::MUNICIPIO,
        self::ESTADO,
        self::PAIS,
        self::CODIGO_POSTAL,
        self::TELEFONO,
        self::TELEFONO_2,
        self::TELEFONO_3,
        self::FLOTILLERO,
        self::CORREO_ELECTRONICO,
        self::CORREO_ELECTRONICO_2,
        self::FECHA_NACIMIENTO,
        self::CFDI_ID,
        self::METODO_PAGO_ID,
        self::FORMA_PAGO,
        self::SALDO,
        self::LIMITE_CREDITO,
        self::NOTAS,
        self::ES_CLIENTE,
        self::APLICA_CREDITO
    ];
}
