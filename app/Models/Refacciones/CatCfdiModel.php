<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatCfdiModel extends Modelo
{
    protected $table = 'catalogo_cfdi';
    const ID = "id";
    const CLAVE = "clave";
    const DESCRIPCION = "descripcion";
    const PERSONA_FISICA = "persona_fisica";
    const PERSONA_MORAL = "persona_moral";

    protected $fillable = [
        self::CLAVE,
        self::DESCRIPCION,
        self::PERSONA_FISICA,
        self::PERSONA_MORAL,
    ];
}
