<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoMotivoCancelacion extends Modelo
{
    protected $table = 'catalogo_motivo_cancelacion';
    const ID = "id";
    const NOMBRE = "nombre";    
    
    protected $fillable = [
        self::NOMBRE
    ];
}
