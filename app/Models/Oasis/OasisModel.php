<?php

namespace App\Models\Oasis;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class OasisModel extends Modelo
{
    protected $table = 'oasis';
     
    
    const ID = "id";
    const FECHA = "fecha";
    const VIN = "vin";
    const SERIE_CORTA = "serie_corta";
    const ID_UNIDAD = "id_unidad";
    const ID_COLOR = "id_color";
    const ID_ANIO = "id_anio";
    const QR = "qr";
    const ID_STATUS = "id_status";
    const ID_USUARIO = "id_usuario";
    const ID_UBICACION = "id_ubicacion";
    const NO_ECONOMICO = "no_economico";
    const ID_UBICACION_LLAVES = "id_ubicacion_llaves";

    const DESCUENTO_MAXIMO = 'descuento_maximo';
    const TENENCIA = 'tenencia';
    const SEGURO_COB_AMPLIA = 'seguro_cob_amplia';
    const PLAN_GANE = 'plan_gane';
    const ORDEN_REPORTE = 'orden_reporte';
    const UNIDAD_IMPORTADA = 'unidad_importada';
    const TIPO_AUTO = 'tipo_auto'; //Auto, Camión, Camioneta
    const COSTO_VALOR_UNIDAD = 'costo_valor_unidad';
    const VENTA_VALOR_UNIDAD = 'venta_valor_unidad';
    const COSTO_EQUIPO_BASE = 'costo_equipo_base';
    const VENTA_EQUIPO_BASE = 'venta_equipo_base';
    const COSTO_TOTAL_BASE = 'costo_total_base';
    const VENTA_TOTAL_BASE = 'venta_total_base';
    const COSTO_OTROS_GASTOS = 'costo_otros_gastos';
    const VENTA_OTROS_GASTOS = 'venta_otros_gastos';
    const COSTO_DEDUCCIONES_FORD = 'costo_deducciones_ford';
    const VENTA_DEDUCCIONES_FORD = 'venta_deducciones_ford';
    const COSTO_SEGUROS_TRASLADOS = 'costo_seguros_traslados';
    const VENTA_SEGUROS_TRASLADOS = 'venta_seguros_traslados';
    const COSTO_GASTOS_TRASLADOS = 'costo_gastos_traslados';
    const VENTA_GASTOS_TRASLADOS = 'venta_gastos_traslados';
    const COSTO_GASTOS_ACOND = 'costo_gastos_acond';
    const VENTA_GASTOS_ACOND = 'venta_gastos_acond';
    const COSTO_IMP_IMP = 'costo_imp_imp';
    const VENTA_IMP_IMP = 'venta_imp_imp';
    const COSTO_FLETES_EXT = 'costo_fletes_ext';
    const VENTA_FLETES_EXT = 'venta_fletes_ext';
    const COSTO_ISAN = 'costo_isan';
    const VENTA_ISAN = 'venta_isan';
    const COSTO_SUBTOTAL = 'costo_subtotal';
    const VENTA_SUBTOTAL = 'venta_subtotal';
    const COSTO_IVA = 'costo_iva';
    const VENTA_IVA = 'venta_iva';
    const CLAVE_ISAN = 'clave_isan';

    protected $fillable = [
        self::FECHA,
        self::VIN,
        self::VIN,
        self::SERIE_CORTA,
        self::ID_UNIDAD,
        self::ID_COLOR,
        self::ID_ANIO,
        self::QR,
        self::ID_STATUS,
        self::ID_USUARIO,
        self::ID_UBICACION,
        self::NO_ECONOMICO,
        self::ID_UBICACION_LLAVES,

        self::DESCUENTO_MAXIMO,
        self::TENENCIA,
        self::SEGURO_COB_AMPLIA,
        self::PLAN_GANE,
        self::ORDEN_REPORTE,
        self::UNIDAD_IMPORTADA,
        self::TIPO_AUTO,
        self::COSTO_VALOR_UNIDAD,
        self::VENTA_VALOR_UNIDAD,
        self::COSTO_EQUIPO_BASE,
        self::VENTA_EQUIPO_BASE,
        self::COSTO_TOTAL_BASE,
        self::VENTA_TOTAL_BASE,
        self::COSTO_OTROS_GASTOS,
        self::VENTA_OTROS_GASTOS,
        self::COSTO_DEDUCCIONES_FORD,
        self::VENTA_DEDUCCIONES_FORD,
        self::COSTO_SEGUROS_TRASLADOS,
        self::VENTA_SEGUROS_TRASLADOS,
        self::COSTO_GASTOS_TRASLADOS,
        self::VENTA_GASTOS_TRASLADOS,
        self::COSTO_GASTOS_ACOND,
        self::VENTA_GASTOS_ACOND,
        self::COSTO_IMP_IMP,
        self::VENTA_IMP_IMP,
        self::COSTO_FLETES_EXT,
        self::VENTA_FLETES_EXT,
        self::COSTO_ISAN,
        self::VENTA_ISAN,
        self::COSTO_SUBTOTAL,
        self::VENTA_SUBTOTAL,
        self::COSTO_IVA,
        self::VENTA_IVA,
        self::CLAVE_ISAN,

    ];
}
