<?php

namespace App\Models\Oasis;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatEstatusOasisModel extends Modelo
{
    protected $table = 'catalogo_estatus_oasis';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ID,
        self::ESTATUS
    ];
}
