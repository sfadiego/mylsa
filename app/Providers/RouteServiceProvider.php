<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapApiRoutesFinanzas();
        $this->mapWebRoutes();
        $this->mapApiRoutesAutos();
        $this->mapApiRoutesPolizas();
        $this->mapApiRoutesKardex();
        $this->mapaPolizasContabilidad();
        $this->mapApiRoutesCxc();
        $this->mapApiRoutesCxp();
        $this->mapApiRoutesNomina();
        $this->mapApiRoutesTelemarketing();
        $this->mapApiRoutesFinanciamientos();
        $this->mapApiRoutesLogistica();
        $this->mapApiRoutesCaja();
        $this->mapApiRoutesBodyshop();
        $this->mapApiRoutesOasis();
        $this->mapApiRoutesUnidadesNuevas();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    protected function mapApiRoutesFinanzas()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/finanzas.route.api.php'));
    }

    protected function mapApiRoutesKardex()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/kardex.route.api.php'));
    }

    

    protected function mapApiRoutesAutos()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/autos.route.api.php'));
    }

    protected function mapApiRoutesCxc()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/cxc.route.api.php'));
    }

    protected function mapApiRoutesCxp()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/cxp.route.api.php'));
    }

    protected function mapApiRoutesPolizas()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/polizas.route.api.php'));
    }

    protected function mapaPolizasContabilidad()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/polizascontabilidad.route.api.php'));
    }

    protected function mapApiRoutesNomina()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/nominas.route.api.php'));
    }
    protected function mapApiRoutesTelemarketing()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/telemarketing.route.php'));
    }
    protected function mapApiRoutesFinanciamientos()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/financiamientos.route.php'));
    }
    protected function mapApiRoutesLogistica()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/logistica.route.php'));
    }
    protected function mapApiRoutesBodyshop()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/bodyshop.route.php'));
    }
    protected function mapApiRoutesCaja()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/caja.route.api.php'));
    }
    protected function mapApiRoutesOasis()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/oasis.route.php'));
    }
    protected function mapApiRoutesUnidadesNuevas()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/unidadesNuevas.route.php'));
    }
}
