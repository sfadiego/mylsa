<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuSubmenuModel;

class ServicioSubmenus extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuSubmenuModel();
        $this->recurso = 'submenus';
    }

    public function getReglasGuardar()
    {
        return [
            MenuSubmenuModel::NOMBRE => 'required',
            MenuSubmenuModel::SECCION_ID => 'required|exists:menu_secciones,id',
            MenuSubmenuModel::VISIBLE => 'nullable'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuSubmenuModel::NOMBRE => 'required',
            MenuSubmenuModel::SECCION_ID => 'required|exists:menu_secciones,id',
            MenuSubmenuModel::VISIBLE => 'nullable'
        ];
    }

    public function getReglasSubmenu()
    {
        return [
            MenuSubmenuModel::SECCION_ID => 'required|exists:menu_secciones,id'
        ];
    }

    public function getSubmenus($parametros)
    {
        return $this->modelo
            ->where(MenuSubmenuModel::SECCION_ID, '=', $parametros[MenuSubmenuModel::SECCION_ID])
            ->get();
    }

    public function subMenuBySeccion($parametros)
    {
        $query = $this->modelo->select(
            'menu_submenu.id as submenu_id',
            'menu_submenu.nombre_submenu',
            'menu_submenu.visible',
            'menu_secciones.nombre as seccion_nombre',
            'menu_secciones.id as seccion_id'
        );

        $query->join('menu_secciones', 'menu_submenu.seccion_id', '=', 'menu_secciones.id');

        if (isset($parametros[MenuSubmenuModel::SECCION_ID])) {
            $query->where(MenuSubmenuModel::SECCION_ID, '=', $parametros[MenuSubmenuModel::SECCION_ID]);
        }

        return  $query->get();
    }

    public function subMenuBySeccionAndPermisos($parametros)
    {
        $query = $this->modelo->select(
            'menu_submenu.id as submenu_id',
            'menu_submenu.nombre_submenu',
            'menu_submenu.visible',
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre'
        );

        $query->join('menu_secciones', 'menu_submenu.seccion_id', '=', 'menu_secciones.id');
        // $query->join('menu_rol_vista', 'menu_secciones.id', '=', 'menu_rol_vista.seccion_id');
        $query->join('menu_usuario_vista', function ($query) {
            $query->on('menu_secciones.id', '=', 'menu_usuario_vista.seccion_id')
                ->on('menu_usuario_vista.submenu_id', '=', 'menu_submenu.id');
        });

        if (isset($parametros[MenuSubmenuModel::SECCION_ID])) {
            // $query->where(MenuSubmenuModel::SECCION_ID, '=', $parametros[MenuSubmenuModel::SECCION_ID]);
            $query->where('menu_submenu.seccion_id', '=', $parametros[MenuSubmenuModel::SECCION_ID]);
        }

        return  $query->groupBy(
            'menu_submenu.id',
            'menu_submenu.nombre_submenu',
            'menu_submenu.visible',
            'menu_secciones.id',
            'menu_secciones.nombre'
        )->get();
    }
}
