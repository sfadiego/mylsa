<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuRolesVistasModel;
use App\Models\Usuarios\MenuVistasModel;

class ServiciomenuRolesVistas extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuRolesVistasModel();
        $this->menuVistasModel = new MenuVistasModel();
        $this->recurso = 'roles_vistas';
    }

    public function getReglasGuardar()
    {
        return [
            MenuRolesVistasModel::ROL_ID => 'required|exists:roles,id',
            MenuRolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuRolesVistasModel::ROL_ID => 'required|exists:roles,id',
            MenuRolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasMenuVistaRoles()
    {
        return [
            MenuRolesVistasModel::ROL_ID => 'required|exists:roles,id',
            MenuRolesVistasModel::VISTA_ID => 'nullable|array',
            MenuRolesVistasModel::SECCION_ID => 'required',
            MenuRolesVistasModel::SUBMENU_ID => 'required',
        ];
    }

    public function getReglasVistaByRol()
    {
        return [
            MenuRolesVistasModel::ROL_ID => 'required|exists:roles,id'
        ];
    }

    //TODO: revisar consultas
    //TODO: duplicar con usuarios
    public function vistasByRoles($parametros)
    {
        $query = $this->menuVistasModel->select(
            'menu_vistas.nombre',
            'roles.rol',
            'roles.id as rol_id',
            'menu_vistas.id as vista_id'
        );
        $query->join('menu_rol_vista', 'menu_rol_vista.vista_id', '=', 'menu_vistas.id');
        $query->join('roles', 'menu_rol_vista.rol_id', '=', 'roles.id');

        if (isset($parametros[MenuRolesVistasModel::ROL_ID])) {
            $query->where(MenuRolesVistasModel::ROL_ID, '=', $parametros[MenuRolesVistasModel::ROL_ID]);
        }

        return  $query->get();
    }

    //TODO: duplicar con usuarios
    public function returnArrayIdsVistaRoles($parametros)
    {
        $data = $this->vistasByRoles($parametros);
        $array_menu = [];
        foreach ($data as $key => $vista) {
            array_push($array_menu, $vista->vista_id);
        }

        return $array_menu;
    }

    public function storeVistasRoles($parametros)
    {
        $vistas = $parametros[MenuRolesVistasModel::VISTA_ID];
        $this->modelo
            ->where(MenuRolesVistasModel::ROL_ID, '=',  $parametros[MenuRolesVistasModel::ROL_ID])
            ->where(MenuRolesVistasModel::SECCION_ID, '=',  $parametros[MenuRolesVistasModel::SECCION_ID])
            ->where(MenuRolesVistasModel::SUBMENU_ID, '=',  $parametros[MenuRolesVistasModel::SUBMENU_ID])
            ->delete();

        if(isset($vistas)){
            foreach ($vistas as $key => $item) {
                $this->crear([
                    MenuRolesVistasModel::ROL_ID => $parametros[MenuRolesVistasModel::ROL_ID],
                    MenuRolesVistasModel::VISTA_ID => $item,
                    MenuRolesVistasModel::SUBMENU_ID => $parametros[MenuRolesVistasModel::SUBMENU_ID],
                    MenuRolesVistasModel::SECCION_ID => $parametros[MenuRolesVistasModel::SECCION_ID]
                ]);
            }
        }
    }
}
