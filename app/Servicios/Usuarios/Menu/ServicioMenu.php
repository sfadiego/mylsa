<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\UsuariosModulosModel;
use App\Servicios\Usuarios\Menu\ServicioUsuariosModulos;

class ServicioMenu extends ServicioDB
{
    private $permisos;
    public function __construct()
    {
        $this->servicioModulos = new ServicioModulos();
        $this->servicioRoles = new ServicioRoles();
        $this->servicioUsuariosModulos = new ServicioUsuariosModulos();
        $this->servicioSeccionesMenu = new ServicioSeccionesMenu();
        $this->servicioSubmenus = new ServicioSubmenus();
        $this->serviciomenuVistas = new ServiciomenuVistas();
        $this->servicioMenuUsuariosVistas = new ServicioMenuUsuariosVistas();
        $this->recurso = 'menu';
    }

    public function getReglasMenu()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id'
        ];
    }



    public function getReglasGuardar()
    {
        return [];
    }

    public function getReglasUpdate()
    {
        return [];
    }

    public function setPermisos($array_permisos)
    {
        return $this->permisos = $array_permisos;
    }

    public function getmenu($parametros)
    {
        $modulos_usuarios = $this->servicioUsuariosModulos->usuariosAndModulos($parametros);
        $permisos = $this->servicioMenuUsuariosVistas->returnArrayIdsVistaUsuarios($parametros);
        $this->setPermisos($permisos);
        $array_menu = [];

        foreach ($modulos_usuarios as $key => $modulo) {
            $item = new \stdClass;
            $item->modulo_id = $modulo->modulo_id;
            $item->modulo = $modulo->nombre_modulo;
            $item->icono = $modulo->icono;
            $item->orden = $modulo->orden;
            $item->contenido_seccion = $this->getmenuSeccion([
                MenuSeccionesModel::MODULO_ID => $modulo->modulo_id
            ]);

            array_push($array_menu, $item);
        }

        return $array_menu;
    }


    public function getmenuSeccion($parametros)
    {
        if (count($this->permisos) > 0) {
            $secciones = $this->servicioSeccionesMenu->seccionesByModulosPermisos([
                MenuSeccionesModel::MODULO_ID => $parametros[MenuSeccionesModel::MODULO_ID]
            ]);
        } else {
            $secciones = $this->servicioSeccionesMenu->seccionesByModulos([
                MenuSeccionesModel::MODULO_ID => $parametros[MenuSeccionesModel::MODULO_ID]
            ]);
        }
        // dd($secciones->toArray());
        $array_menu = [];
        foreach ($secciones as $key => $seccion) {
            $item = new \stdClass;
            $item->seccion_id = $seccion->seccion_id;
            $item->seccion_nombre = $seccion->seccion_nombre;
            $item->modulo_id = $seccion->modulo_id;

            $submenus = $this->getSubmenus([
                MenuSubmenuModel::SECCION_ID => $seccion->seccion_id
            ]);

            $item->contenido_submenus = $submenus;

            array_push($array_menu, $item);
        }

        return $array_menu;
    }

    public function getSubmenus($parametros)
    {

        if (count($this->permisos) > 0) {
            $submenus = $this->servicioSubmenus->subMenuBySeccionAndPermisos([
                MenuSubmenuModel::SECCION_ID => $parametros[MenuSubmenuModel::SECCION_ID]
            ]);
        } else {
            $submenus = $this->servicioSubmenus->subMenuBySeccion([
                MenuSubmenuModel::SECCION_ID => $parametros[MenuSubmenuModel::SECCION_ID]
            ]);
        }

        $array_menu = [];
        foreach ($submenus as $key => $submenu) {
            $item = new \stdClass;
            $item->submenu_id = $submenu->submenu_id;
            $item->nombre_submenu = $submenu->nombre_submenu;
            $item->seccion_nombre = $submenu->seccion_nombre;
            $item->seccion_id = $submenu->seccion_id;
            $item->visible = $submenu->visible;

            $vistas = $this->getVistas([
                MenuVistasModel::SUBMENU_ID => $submenu->submenu_id
            ]);

            $item->vistas = $vistas;
            array_push($array_menu, $item);
        }
        return $array_menu;
    }

    public function getVistas($parametros)
    {
        $vistas = $this->serviciomenuVistas->vistasBySubmenuId([
            MenuVistasModel::SUBMENU_ID => $parametros[MenuVistasModel::SUBMENU_ID],

        ]);

        $array_menu = [];
        foreach ($vistas as $key => $vista) {
            $item = new \stdClass;
            $item->vista_id = $vista->vista_id;
            $item->nombre_vista = $vista->nombre_vista;
            $item->link = $vista->link;
            $item->controlador = $vista->controlador;
            $item->submenu_id = $vista->submenu_id;
            $item->nombre_submenu = $vista->nombre_submenu;
            $item->modulo = $vista->modulo;
            if (count($this->permisos) > 0) {
                if (in_array($vista->vista_id, $this->permisos)) {
                    array_push($array_menu, $item);
                }
            } else {
                array_push($array_menu, $item);
            }
        }

        return $array_menu;
    }
}
