<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatUdiModel;

class ServicioCatUdi extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo udi';
        $this->modelo = new CatUdiModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatUdiModel::UDI => 'required',
            CatUdiModel::VALOR => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatUdiModel::UDI => 'required',
            CatUdiModel::VALOR => 'required'
        ];
    }
}
