<?php

namespace App\Servicios\Autos\Inventario;

use App\Models\Autos\Inventario\CajuelaModel;
use App\Models\Autos\Inventario\CarroceriaModel;
use App\Models\Autos\Inventario\DocumentacionModel;
use App\Models\Autos\Inventario\ExterioresModel;
use App\Models\Autos\Inventario\GasolinaModel;
use App\Models\Autos\Inventario\InterioresModel;
use App\Models\Autos\Inventario\InventarioModel;
use App\Servicios\Core\ServicioDB;

class ServicioInventario extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'inventario unidades';
        $this->modelo = new InventarioModel();
        $this->cajuelaModel = new CajuelaModel();
        $this->carroceriaModel = new CarroceriaModel();
        $this->documentacionModel = new DocumentacionModel();
        $this->exterioresModel = new ExterioresModel();
        $this->gasolinaModel = new GasolinaModel();
        $this->interioresModel = new InterioresModel();
    }

    public function getReglasGuardar()
    {
        return [
            InventarioModel::DIAS_HABILES => 'required',
            InventarioModel::FIRMA_ELABORA => 'required',
            InventarioModel::FECHA_ELABORACION => 'required',
            InventarioModel::FECHA_ACEPTACION => 'required',
            InventarioModel::FIRMA_CONSUMIDOR => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            InventarioModel::DIAS_HABILES => 'nullable',
            InventarioModel::FIRMA_ELABORA => 'nullable',
            InventarioModel::FECHA_ELABORACION => 'nullable',
            InventarioModel::FECHA_ACEPTACION => 'nullable',
            InventarioModel::FIRMA_CONSUMIDOR => 'nullable'
        ];
    }

    public function reglasStoreformulario()
    {
        return [
            InventarioModel::DIAS_HABILES => 'required',
            InventarioModel::FIRMA_ELABORA => 'required',
            InventarioModel::FECHA_ELABORACION => 'required',
            InventarioModel::FECHA_ACEPTACION => 'required',
            InventarioModel::FIRMA_CONSUMIDOR => 'required',
            InventarioModel::ID_UNIDAD => 'required|exists:unidades,id',
            CajuelaModel::HERRAMIENTA => 'required',
            CajuelaModel::GATO_LLAVE => 'required',
            CajuelaModel::REFLEJANTES => 'required',
            CajuelaModel::CABLES => 'required',
            CajuelaModel::EXTINTOR => 'required',
            CajuelaModel::LLANTA_REFACCION => 'required',
            CarroceriaModel::CARROCERIA_IMG => 'nullable',
            DocumentacionModel::POLIZA_GARANTIA => 'required',
            DocumentacionModel::SEGURO_RINES => 'required',
            DocumentacionModel::CERTIFICADO_VERIFICACION => 'required',
            DocumentacionModel::TARJETA_CIRCULACION => 'required',
            GasolinaModel::NIVEL_GASOLINA => 'required',
            GasolinaModel::CUALES => 'required',
            GasolinaModel::ARTICULOS_PERSONALES => 'required',
            GasolinaModel::REPORTE_ALGO_MAS => 'required',
            InterioresModel::LLAVERO => 'required',
            InterioresModel::SEGURO_RINES => 'required',
            InterioresModel::INDICADORES_ACTIVADOS => 'nullable',
            InterioresModel::INDICADORES => 'required',
            InterioresModel::ROCIADOR => 'required',
            InterioresModel::CLAXON => 'required',
            InterioresModel::LUCES_DEL => 'required',
            InterioresModel::LUCES_TRAS => 'required',
            InterioresModel::LUCES_STOP => 'required',
            InterioresModel::RADIO => 'required',
            InterioresModel::PANTALLAS => 'required',
            InterioresModel::AC => 'required',
            InterioresModel::ENCENDEDOR => 'required',
            InterioresModel::VIDRIOS => 'required',
            InterioresModel::ESPEJOS => 'required',
            InterioresModel::SEGUROS_ELECTRICOS => 'required',
            InterioresModel::DISCO_COMPACTO => 'required',
            InterioresModel::ASIENTO_VESTIDURA => 'required',
            InterioresModel::TAPETES => 'required',
            ExterioresModel::TAPONES_RUEDA => 'required',
            ExterioresModel::GOMA_LIMPIADORES => 'required',
            ExterioresModel::ANTENA => 'required',
            ExterioresModel::TAPON_GASOLINA => 'required'
            
        ];
    }

    public function storeForumlario($parametros)
    {

        $modelo = $this->modelo->updateOrCreate([InventarioModel::ID_UNIDAD => $parametros[InventarioModel::ID_UNIDAD]],$parametros);
        $this->cajuelaModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        $this->carroceriaModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        $this->documentacionModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        $this->exterioresModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        $this->gasolinaModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        $this->interioresModel->updateOrCreate([InterioresModel::ID_INVENTARIO => $modelo->id], $parametros);
        return $modelo;
    }

    public function getallformdata($id_formulario)
    {
        return $this->modelo
            ->where(InventarioModel::ID, $id_formulario)
            ->with(
                [
                    'cajuela',
                    'carroceria',
                    'documentacion',
                    'exteriores',
                    'gasolina',
                    'interiores'
                ]
            )->get();
    }
}
