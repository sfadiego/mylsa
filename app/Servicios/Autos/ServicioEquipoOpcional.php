<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\EquipoOpcionalModel;
use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Support\Facades\DB;

class ServicioEquipoOpcional extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'equipo opcional';
        $this->modelo = new EquipoOpcionalModel();
        $this->servicioVentaAutos = new ServicioVentaAutos();
    }

    public function getReglasGuardar()
    {
        return [
            EquipoOpcionalModel::ID_VENTA_AUTO => 'required|exists:ventas_autos,id',
            EquipoOpcionalModel::PRODUCTO_ID => 'required|exists:producto,id',
            EquipoOpcionalModel::CANTIDAD => 'required|numeric',
            EquipoOpcionalModel::PRECIO => 'required|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EquipoOpcionalModel::ID_VENTA_AUTO => 'required|exists:ventas_autos,id',
            EquipoOpcionalModel::PRODUCTO_ID => 'required|exists:producto,id',
            EquipoOpcionalModel::CANTIDAD => 'required|numeric',
            EquipoOpcionalModel::PRECIO => 'required|numeric',
        ];
    }

    public function getByPreVentaId($preventa_id)
    {
        return $this->modelo->select(
            EquipoOpcionalModel::getTableName() . '.id as item_id',
            EquipoOpcionalModel::getTableName() . '.producto_id',
            EquipoOpcionalModel::getTableName() . '.cantidad',
            EquipoOpcionalModel::getTableName() . '.precio',
            EquipoOpcionalModel::getTableName() . '.total',
            ProductosModel::getTableName() . '.no_identificacion',
            ProductosModel::getTableName() . '.clave_unidad',
            ProductosModel::getTableName() . '.descripcion',
            ProductosModel::getTableName() . '.unidad',
            ProductosModel::getTableName() . '.valor_unitario'
        )->join(
            ProductosModel::getTableName(),
            EquipoOpcionalModel::getTableName() . '.producto_id',
            '=',
            ProductosModel::getTableName() . '.id'
        )->where(EquipoOpcionalModel::getTableName().'.id_venta_auto', $preventa_id)->get();
    }

    public function store($parametros)
    {
        $producto = $this->existeProducto($parametros[EquipoOpcionalModel::ID_VENTA_AUTO], $parametros[EquipoOpcionalModel::PRODUCTO_ID]);
        if (count($producto) > 0) {
            $total_productos = $producto[0][EquipoOpcionalModel::CANTIDAD] + $parametros[EquipoOpcionalModel::CANTIDAD];
            $total = $total_productos * $parametros[EquipoOpcionalModel::PRECIO];
            $data[EquipoOpcionalModel::PRODUCTO_ID] = $parametros[EquipoOpcionalModel::PRODUCTO_ID];
            $data[EquipoOpcionalModel::ID_VENTA_AUTO] = $parametros[EquipoOpcionalModel::ID_VENTA_AUTO];
            $data[EquipoOpcionalModel::PRECIO] = $parametros[EquipoOpcionalModel::PRECIO];
            $data[EquipoOpcionalModel::CANTIDAD] = $total_productos;
            $data[EquipoOpcionalModel::TOTAL] = $total;
            return DB::table($this->modelo->getTableName())
                ->where(EquipoOpcionalModel::ID, $producto[0][EquipoOpcionalModel::ID])
                ->update($data);
        } else {
            $data[EquipoOpcionalModel::PRODUCTO_ID] = $parametros[EquipoOpcionalModel::PRODUCTO_ID];
            $data[EquipoOpcionalModel::ID_VENTA_AUTO] = $parametros[EquipoOpcionalModel::ID_VENTA_AUTO];
            $data[EquipoOpcionalModel::PRECIO] = $parametros[EquipoOpcionalModel::PRECIO];
            $data[EquipoOpcionalModel::CANTIDAD] = $parametros[EquipoOpcionalModel::CANTIDAD];
            $data[EquipoOpcionalModel::TOTAL] = $parametros[EquipoOpcionalModel::CANTIDAD] * $parametros[EquipoOpcionalModel::PRECIO];
            return $this->modelo->create($data);
        }
    }

    public function existeProducto($preventa_id, $pructo_id)
    {
        return $this->modelo
            ->where(EquipoOpcionalModel::ID_VENTA_AUTO, $preventa_id)
            ->where(EquipoOpcionalModel::PRODUCTO_ID, $pructo_id)
            ->get()->toArray();
    }

    public function getVentaConEquipoOpcional()
    {
        $data = [];
        $data['id_estatus_venta'] = EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO;
        $data['venta_auto'] = true;
        $data['servicio_cita_id'] = true;
        return $this->servicioVentaAutos->getVentaAutos($data);
        
    }
}
