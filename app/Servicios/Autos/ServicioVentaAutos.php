<?php

namespace App\Servicios\Autos;

use App\Models\Autos\CatModelosModel;
use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Servicios\CuentasPorCobrar\ServicioAbono;

class ServicioVentaAutos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'venta auto';
        $this->modelo = new VentasAutosModel();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioCuentasxCobrar = new ServicioCuentaPorCobrar();
        $this->servicio_abonos = new ServicioAbono();
    }

    public function getReglasGuardar()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'required|numeric',
            VentasAutosModel::ID_CLIENTE => 'required|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'required|numeric',
            VentasAutosModel::ID_TIPO_AUTO => 'required|numeric',
            VentasAutosModel::VENDEDOR_ID => 'required|numeric',
            CuentasPorCobrarModel::TOTAL => 'required',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            VentasAutosModel::MONEDA => 'required',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            VentasAutosModel::IMPUESTO_ISAN => 'required|numeric',
            VentasAutosModel::DESCUENTO => 'nullable|numeric',
            VentasAutosModel::IVA => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'nullable|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_FOLIO => 'nullable|numeric',
            VentasAutosModel::ID_CLIENTE => 'nullable|numeric',
            VentasAutosModel::VENDEDOR_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'nullable|numeric',
            CuentasPorCobrarModel::TOTAL => 'nullable',
            VentasAutosModel::ID_TIPO_AUTO => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            VentasAutosModel::MONEDA => 'nullable',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            VentasAutosModel::IMPUESTO_ISAN => 'nullable|numeric',
            VentasAutosModel::DESCUENTO => 'nullable|numeric',
            VentasAutosModel::IVA => 'nullable|numeric'
        ];
    }

    public function getReglasFiltroPrepedido()
    {
        return [
            VentasAutosModel::FECHA_INICIO => 'date',
            VentasAutosModel::FECHA_FIN => 'date',
        ];
    }

    public function store($parametros)
    {
        $folio = $this->servicioFolios->generarFolio(CatalogoCuentasModel::CAT_VENTA_AUTOS_NUEVOS);
        $parametros[VentasAutosModel::ID_FOLIO] = $folio->id;
        $importe = $parametros[CuentasPorCobrarModel::TOTAL];
        if ($parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID] == TipoFormaPagoModel::FORMA_CREDITO) {

            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $intereses = ($tasa_interes * ($importe / 100));
            $total_pagar = $importe + $intereses;
            $parametros[CuentasPorCobrarModel::TOTAL] = $total_pagar;
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = $intereses;
        } else {
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::TOTAL] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = 0;
        }

        $this->store_cxc($parametros);
        $venta = $this->modelo->create($parametros);
        $venta['folio'] = $folio->folio;
        
        return $venta;
    }

    public function store_cxc($parametros)
    {

        $cuentaxcobrar =  $this->servicioCuentasxCobrar->crear([
            CuentasPorCobrarModel::FOLIO_ID => $parametros[VentasAutosModel::ID_FOLIO],
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[VentasAutosModel::ID_CLIENTE],
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_PROCESO,
            CuentasPorCobrarModel::CONCEPTO => "VENTA_AUTO_NUEVO",
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::TOTAL => $parametros[CuentasPorCobrarModel::TOTAL],
            CuentasPorCobrarModel::ENGANCHE => $parametros[CuentasPorCobrarModel::ENGANCHE],
            CuentasPorCobrarModel::TASA_INTERES => $parametros[CuentasPorCobrarModel::TASA_INTERES],
            CuentasPorCobrarModel::INTERESES => $parametros[CuentasPorCobrarModel::INTERESES],
            CuentasPorCobrarModel::IMPORTE => $parametros[CuentasPorCobrarModel::IMPORTE],
            CuentasPorCobrarModel::COMENTARIOS => "",
            CuentasPorCobrarModel::FECHA => date('Y-m-d')
        ]);

        $this->servicio_abonos->crearAbonosByCuentaId($cuentaxcobrar->id);
        return $cuentaxcobrar;
    }

    public function getPrepedidos($parametros)
    {
        $data = [];
        $data['id_estatus_venta'] = EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO;
        $data['id_estado'] = UnidadesModel::TIPO_NUEVO;
        return $this->getVentaAutos(array_merge($data, $parametros));
    }

    public function getPrepedidosSeminuevos($parametros)
    {
        $data = [];
        $data['id_estatus_venta'] = EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO;
        $data['id_estado'] = UnidadesModel::TIPO_SEMI_NUEVO;
        return $this->getVentaAutos(array_merge($data, $parametros));
    }

    public function getVentaAutos($parametros)
    {
        $tabla_venta = $this->modelo->getTable();

        $query = $this->modelo->select(
            'ventas_autos.id',
            'ventas_autos.moneda',
            'ventas_autos.servicio_cita_id',
            'unidades.id as id_unidad',
            'unidades.unidad_descripcion',
            'unidades.motor',
            'unidades.transmision',
            'unidades.combustible',
            'unidades.precio_costo',
            'unidades.precio_venta',
            'unidades.no_serie',
            'unidades.placas',
            'unidades.numero_cilindros',
            'unidades.id_estado',
            'catalogo_marcas.id as marca_id',
            'catalogo_marcas.nombre as nombre_marca',

            'catalogo_modelos.id as modelo_id',
            'catalogo_modelos.nombre as nombre_modelo',

            'catalogo_colores.id as color_id',
            'catalogo_colores.nombre as nombre_color',

            'catalogo_anio.id as id_anio',
            'catalogo_anio.nombre as nombre_anio',

            'clientes.id as cliente_id',
            'clientes.nombre_empresa as nombre_empresa',
            'clientes.nombre as nombre_cliente',
            'clientes.apellido_paterno as cliente_apellido_paterno',
            'clientes.apellido_materno as cliente_apellido_materno',
            'clientes.rfc as cliente_rfc',
            'clientes.telefono',
            'clientes.correo_electronico',
            'clientes.rfc as cliente_rfc',
            'cuentas_por_cobrar.id as cxc_id',
            'cuentas_por_cobrar.concepto',
            'cuentas_por_cobrar.tipo_forma_pago_id',
            'cuentas_por_cobrar.tipo_pago_id',
            'cuentas_por_cobrar.plazo_credito_id',
            'cuentas_por_cobrar.enganche',
            'cuentas_por_cobrar.total',
            'cuentas_por_cobrar.tasa_interes',
            'usuarios.id as id_vendedor',
            'usuarios.nombre as nombre_vendedor',
            'usuarios.apellido_paterno as apellido_paterno_vendedor',
            'usuarios.apellido_materno as apellido_materno_vendedor',
            'estatus_venta_autos.nombre as estatus_venta',
            'ventas_autos.created_at'

        )->from($tabla_venta);
        $query->join('unidades', 'unidades.id', '=', 'ventas_autos.id_unidad');
        $query->join('clientes', 'clientes.id', '=', 'ventas_autos.id_cliente');
        $query->join('cuentas_por_cobrar', 'cuentas_por_cobrar.folio_id', '=', 'ventas_autos.id_folio');
        $query->join('usuarios', 'usuarios.id', '=', 'ventas_autos.vendedor_id');
        $query->join('estatus_venta_autos', 'estatus_venta_autos.id', '=', 'ventas_autos.id_estatus');
        $query->leftJoin('folios', 'folios.id', '=', 'ventas_autos.id_folio');

        $query->join('catalogo_marcas', 'catalogo_marcas.id', '=', 'unidades.marca_id');
        $query->join('catalogo_anio', 'catalogo_anio.id', '=', 'unidades.anio_id');
        $query->join('catalogo_modelos', 'catalogo_modelos.id', '=', 'unidades.modelo_id');
        $query->join('catalogo_colores', 'catalogo_colores.id', '=', 'unidades.color_id');

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->join('equipo_opcional_unidad', 'equipo_opcional_unidad.id_venta_auto', '=', 'ventas_autos.id');
        }

        if (isset($parametros['servicio_cita_id']) && $parametros['servicio_cita_id']) {
            $query->whereNull('ventas_autos.servicio_cita_id');
        }

        if (isset($parametros['id_venta'])) {
            $query->where('ventas_autos.id', $parametros['id_venta']);
        }

        if (isset($parametros['id_estado'])) {
            $query->where('unidades.id_estado', $parametros['id_estado']);
        }

        if (isset($parametros['id_estatus_venta'])) {
            $query->where('ventas_autos.id_estatus', $parametros['id_estatus_venta']);
        }

        if (isset($parametros['fecha_inicio']) && !isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
        }

        if (!isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['id_folio'])) {
            $query->where('ventas_autos.id_folio', $parametros['id_folio']);
        }

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->groupBy(
                'ventas_autos.id',
                'ventas_autos.moneda',
                'unidades.id',
                'unidades.unidad_descripcion',
                'unidades.motor',
                'unidades.transmision',
                'unidades.combustible',
                'unidades.precio_costo',
                'unidades.precio_venta',
                'unidades.numero_cilindros',
                'unidades.no_serie',
                'unidades.placas',
                'catalogo_marcas.nombre',
                'catalogo_modelos.nombre',
                'catalogo_colores.nombre',
                'catalogo_anio.nombre',
                'clientes.nombre_empresa',
                'clientes.nombre',
                'clientes.apellido_paterno',
                'clientes.apellido_materno',

                'clientes.rfc',
                'clientes.telefono',
                'clientes.correo_electronico',
                'cuentas_por_cobrar.concepto',
                'cuentas_por_cobrar.tipo_forma_pago_id',
                'cuentas_por_cobrar.tipo_pago_id',
                'cuentas_por_cobrar.plazo_credito_id',
                'cuentas_por_cobrar.enganche',
                'cuentas_por_cobrar.total',
                'cuentas_por_cobrar.tasa_interes',
                'usuarios.nombre',
                'usuarios.apellido_paterno',
                'usuarios.apellido_materno',
                'estatus_venta_autos.nombre',

                'ventas_autos.servicio_cita_id',
                'ventas_autos.created_at',

                'unidades.id_estado',
                'catalogo_marcas.id',
                'catalogo_modelos.id',
                'catalogo_colores.id',
                'catalogo_anio.id',
                'clientes.id',
                'cuentas_por_cobrar.id',
                'usuarios.id',
                'estatus_venta_autos.nombre'
            );
        }

        $query->limit(200);
        return $query->get();
    }

    public function getOne($id)
    {
        return $this->getVentaAutos(['id_venta' => $id]);
    }

    public function getPreventaById($id)
    {
        return $this->getVentaAutos([
            'id_venta' => $id,
            'id_estado' => UnidadesModel::TIPO_NUEVO,
            'id_estatus_venta' => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO
        ]);
    }

    public function getPreventaSeminuevoById($id)
    {
        return $this->getVentaAutos([
            'id_venta' => $id,
            'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO,
            'id_estatus_venta' => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO
        ]);
    }

    public function getReglasformatosalida()
    {
        return [
            VentasAutosModel::ID_VENTA_AUTO => 'required|exists:ventas_autos,id',

            VentasAutosModel::FIRMA_ASESOR => 'nullable',
            VentasAutosModel::FIRMA_CLIENTE => 'nullable',

            FuncionamientoVehiculoModel::ASISTENTE_ARRANQUE => 'required',
            FuncionamientoVehiculoModel::PRESERVACION_CARRIL => 'required',
            FuncionamientoVehiculoModel::SENSORES_DEL_LAT => 'required',
            FuncionamientoVehiculoModel::BLIS => 'required',
            FuncionamientoVehiculoModel::ALERTA_TRAFICO_CRUZADO => 'required',
            FuncionamientoVehiculoModel::ASISTENCIA_ACTIVA_ESTACI => 'required',
            FuncionamientoVehiculoModel::CAMARA_REVERSA => 'required',
            FuncionamientoVehiculoModel::CAMARA_FRONTAL => 'required',
            FuncionamientoVehiculoModel::CAMARA_360_GRADOS => 'required',
            FuncionamientoVehiculoModel::SITEMA_MON_PLL_TMS => 'required',
            AhorroCombustibleModel::IVCT => 'required',
            AhorroCombustibleModel::TIVCT => 'required',

            ComentariosModel::EXPLICO_ASISTENCIA_FORD => 'required',
            ComentariosModel::EXPLICO_FORD_PROTECT => 'required',
            ComentariosModel::ACCESORIOS_PERSONALIZAR => 'required',
            ComentariosModel::PRUEBA_MANEJO => 'required',
            ComentariosModel::INFORMACION_MANTENIMIENTO => 'required',
            ComentariosModel::ASESOR_SERVICIO => 'required',

            ConfortModel::ACCIONAMIENTO_MANUAL => 'required',
            ConfortModel::ACCIONAMIENTO_ELECTRICO => 'required',
            ConfortModel::ASIENTO_DELANTERO_MASAJE => 'required',
            ConfortModel::ASIENTO_CALEFACCION => 'required',
            ConfortModel::EASY_POWER_FOLD => 'required',
            ConfortModel::MEMORIA_ASIENTOS => 'required',

            DesempenoModel::MOTORES => 'required',
            DesempenoModel::CLUTCH => 'required',
            DesempenoModel::CAJA_CAMBIOS => 'required',
            DesempenoModel::ARBOL_TRANSMISIONES => 'required',
            DesempenoModel::DIFERENCIAL => 'required',
            DesempenoModel::DIFERENCIAL_TRACCION => 'required',
            DesempenoModel::CAJA_REDUCTORA => 'required',
            DesempenoModel::CAJA_TRANSFERENCIA => 'required',
            DesempenoModel::SISTEMA_DINAMICO => 'required',
            DesempenoModel::DIRECCION => 'required',
            DesempenoModel::SUSPENSION => 'required',
            DesempenoModel::TOMA_PODER => 'required',

            DocumentosModel::CARTA_FACTURA => 'required',
            DocumentosModel::POLIZA_GARANTIA => 'required',
            DocumentosModel::POLIZA_SEGURO => 'required',
            DocumentosModel::TENENCIA => 'required',
            DocumentosModel::HERRAMIENTAS => 'required',

            EspecialesModel::PEPS => 'required',
            EspecialesModel::APERTIRA_CAJUELA => 'required',
            EspecialesModel::ENCENDIDO_REMOTO => 'required',
            EspecialesModel::TECHO_PANORAMICO => 'required',
            EspecialesModel::AIRE_ACONDICIONADO => 'required',
            EspecialesModel::EATC => 'required',
            EspecialesModel::CONSOLA_ENFRIAMIENTO => 'required',
            EspecialesModel::VOLANTE_AJUSTE_ALTURA => 'required',
            EspecialesModel::VOLANTE_CALEFACTADO => 'required',
            EspecialesModel::ESTRIBOS_ELECTRICOS => 'required',
            EspecialesModel::APERTURA_CAJA_CARGA => 'required',
            EspecialesModel::LIMPIAPARABRISAS => 'required',
            EspecialesModel::ESPEJO_ELECTRICO => 'required',
            EspecialesModel::ESPEJOS_ABATIBLES => 'required',

            ExtrasModel::CONECTIVIDAD => 'required',
            ExtrasModel::SYNC => 'required',
            ExtrasModel::MYFORD_TOUCH => 'required',
            ExtrasModel::MYFORD_TOUCH_NAVEGACION => 'required',
            ExtrasModel::SYNC_FORDPASS => 'required',
            ExtrasModel::APPLE_CARPLAY => 'required',
            ExtrasModel::ANDROID_AUTO => 'required',
            ExtrasModel::CONSOLA_SMARTPHONE => 'required',
            ExtrasModel::AUDIO_SHAKER_PRO => 'required',
            ExtrasModel::RADIO_HD => 'required',
            ExtrasModel::EQUIPO_SONY => 'required',
            ExtrasModel::PUERTOS_USB => 'required',
            ExtrasModel::BLUETOOH => 'required',
            ExtrasModel::WIFI => 'required',
            ExtrasModel::TARJETA_SD => 'required',
            ExtrasModel::INVERSOR_CORRIENTE => 'required',
            ExtrasModel::ESPEJO_ELECTROCROMATICO => 'required',

            IluminacionModel::ILUMINACION_AMBIENTAL => 'required',
            IluminacionModel::SISTEMA_ILUMINACION => 'required',
            IluminacionModel::FAROS_BIXENON => 'required',
            IluminacionModel::HID => 'required',
            IluminacionModel::LED => 'required',

            OtrasTecnologiasModel::EASY_FUEL => 'required',
            OtrasTecnologiasModel::TRACK_APPS => 'required',
            OtrasTecnologiasModel::AUTO_START_STOP => 'required',
            OtrasTecnologiasModel::SISTEMA_CTR_TERRENO => 'required',

            SeguridadModel::SISTEMA_FRENADO => 'required',
            SeguridadModel::CONTROL_TRACCION => 'required',
            SeguridadModel::ESC => 'required',
            SeguridadModel::RSC => 'required',
            SeguridadModel::CONTROL_TORQUE_CURVAS => 'required',
            SeguridadModel::CONTROL_CURVAS => 'required',
            SeguridadModel::CONTROL_BALANCE_REMOLQUE => 'required',
            SeguridadModel::FRENOS_ELECTRONICOS => 'required',
            SeguridadModel::CONTROL_ELECTRONICO_DES => 'required',

            SeguridadPasivaModel::CINTURON_SEGURIDAD => 'required',
            SeguridadPasivaModel::BOLSAS_AIRE => 'required',
            SeguridadPasivaModel::CHASIS_CARROCERIA => 'required',
            SeguridadPasivaModel::CARROSERIA_HIDROFORMA => 'required',
            SeguridadPasivaModel::CRISTALES => 'required',
            SeguridadPasivaModel::CABECERAS => 'required',
            SeguridadPasivaModel::SISTEMA_ALERTA => 'required',
            SeguridadPasivaModel::TECLADO_PUERTA => 'required',
            SeguridadPasivaModel::ALARMA_VOLUMETRICA => 'required',
            SeguridadPasivaModel::ALARMA_PERIMETRAL => 'required',
            SeguridadPasivaModel::MYKEY => 'required',

            VehiculoHibridoModel::VEHICULO_HIBRIDO => 'required',
            VehiculoHibridoModel::MOTOR_GASOLINA => 'required',
            VehiculoHibridoModel::TIPS_MANEJO => 'required',
            VehiculoHibridoModel::TRANSMISION_ECVT => 'required'
        ];
    }

    public function guardarFormatosalida($parametros)
    {
        $this->modeloAhorroCombustible = new AhorroCombustibleModel();
        $this->modeloComentarios = new ComentariosModel();
        $this->modeloConfort = new ConfortModel();
        $this->modeloDesempeno = new DesempenoModel();
        $this->modeloDocumentos = new DocumentosModel();
        $this->modeloEspeciales = new EspecialesModel();
        $this->modeloExtras = new ExtrasModel();
        $this->modeloFuncionamientoVehiculo = new FuncionamientoVehiculoModel();
        $this->modeloIluminacion = new IluminacionModel();
        $this->modeloOtrasTecnologias = new OtrasTecnologiasModel();
        $this->modeloSeguridad = new SeguridadModel();
        $this->modeloSeguridadPasiva = new SeguridadPasivaModel();
        $this->modeloVehiculoHibrido = new VehiculoHibridoModel();

        $this->modelo->updateOrCreate([VentasAutosModel::ID => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);

        $this->modeloAhorroCombustible->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloComentarios->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloConfort->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloDesempeno->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloDocumentos->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloEspeciales->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloExtras->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);

        $this->modeloFuncionamientoVehiculo->updateOrCreate([FuncionamientoVehiculoModel::ID_VENTA_AUTO => $parametros[FuncionamientoVehiculoModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloIluminacion->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloOtrasTecnologias->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloSeguridad->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloSeguridadPasiva->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloVehiculoHibrido->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
    }



    public function getdataformatosalida($id_preventa)
    {
        return  $this->modelo->with([
            'seguridad',
            'ahorro_combustible',
            'comentarios',
            'confort',
            'desempeno',
            'documentos',
            'especiales',
            'extras',
            'funcionamiento_vehiculo',
            'iluminacion',
            'otras_tecnologias',
            'seguridad_pasiva',
            'vehiculo_hibrido',
            'cliente',
            'unidad'
        ])->where(VentasAutosModel::ID, $id_preventa)
            ->get();
    }
}
