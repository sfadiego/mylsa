<?php

namespace App\Servicios\Autos;

use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Servicios\Core\ServicioDB;

class ServicioInformacionDocumentosVenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'documentacion';
        $this->modelo = new InformacionDocumentosVentaModel();
    }

    public function getReglasGuardar()
    {
        return [
            InformacionDocumentosVentaModel::ID_VENTA_UNIDAD => 'required|exists:ventas_autos,id',
            InformacionDocumentosVentaModel::TIPO_VENTA => 'nullable',
            InformacionDocumentosVentaModel::FECHA_FACTURA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CONTRATO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_VENTA_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_INGRESO_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FIRMA_GERENTE => 'nullable',
            InformacionDocumentosVentaModel::FIRMA_OTROS => 'nullable'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            InformacionDocumentosVentaModel::ID_VENTA_UNIDAD => 'nullable|exists:ventas_autos,id',
            InformacionDocumentosVentaModel::TIPO_VENTA => 'required',
            InformacionDocumentosVentaModel::FECHA_FACTURA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CONTRATO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_VENTA_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_INGRESO_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FIRMA_GERENTE => 'nullable',
            InformacionDocumentosVentaModel::FIRMA_OTROS => 'nullable'
        ];
    }

    public function getByIdunidad($id)
    {
        return $this->modelo
            ->where(InformacionDocumentosVentaModel::ID_VENTA_UNIDAD, $id)
            ->first();
    }
}
