<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use Illuminate\Support\Facades\DB;

class ServicioUnidades extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'unidades';
        $this->modelo = new UnidadesModel();
    }

    public function getReglasGuardar()
    {
        return [
            UnidadesModel::MARCA_ID => 'required',
            UnidadesModel::MODELO_ID => 'required',
            UnidadesModel::ANIO_ID => 'required',
            UnidadesModel::COLOR_ID => 'required',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::COMBUSTIBLE => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::PRECIO_COSTO => 'required',
            UnidadesModel::PRECIO_VENTA => 'required',
            UnidadesModel::ID_ESTADO => 'required',
            UnidadesModel::KILOMETRAJE => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::NO_SERIE => 'required|digits:17',
            UnidadesModel::PLACAS => 'required',
            UnidadesModel::ID_UBICACION => 'required',
            UnidadesModel::ID_UBICACION_LLAVES => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required',
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required',
            UnidadesModel::N_MOTOR => 'required',
            UnidadesModel::N_PRODUCCION => 'required',
            UnidadesModel::N_REMISION => 'required',
            UnidadesModel::N_FOLIO_REMISION => 'required',
            UnidadesModel::PDTO => 'required',
            UnidadesModel::FOLIO_PEDIDO => 'required',
            UnidadesModel::VESTIDURA_ID => 'required',
            UnidadesModel::UNIDAD_DESCRIPCION => 'required',
            UnidadesModel::ID_CUENTA => 'required'

        ];
    }
    public function getReglasUpdate()
    {
        return [
            UnidadesModel::MARCA_ID => 'nullable',
            UnidadesModel::MODELO_ID => 'nullable',
            UnidadesModel::ANIO_ID => 'nullable',
            UnidadesModel::COLOR_ID => 'nullable',
            UnidadesModel::MOTOR => 'nullable',
            UnidadesModel::TRANSMISION => 'nullable',
            UnidadesModel::COMBUSTIBLE => 'nullable',
            UnidadesModel::VIN => 'nullable',
            UnidadesModel::PRECIO_COSTO => 'nullable',
            UnidadesModel::PRECIO_VENTA => 'nullable',
            UnidadesModel::ID_ESTADO => 'nullable',
            UnidadesModel::KILOMETRAJE => 'nullable',
            UnidadesModel::NUMERO_CILINDROS => 'nullable',
            UnidadesModel::CAPACIDAD => 'nullable',
            UnidadesModel::NUMERO_PUERTAS => 'nullable',
            UnidadesModel::SERIE_CORTA => 'nullable',
            UnidadesModel::NO_SERIE => 'nullable|digits:17',
            UnidadesModel::PLACAS => 'nullable',
            UnidadesModel::ID_UBICACION => 'nullable',
            UnidadesModel::ID_UBICACION_LLAVES => 'nullable',
            UnidadesModel::NUMERO_ECONOMICO => 'nullable',
            UnidadesModel::USUARIO_RECIBE => 'nullable',
            UnidadesModel::FECHA_RECEPCION => 'nullable',
            UnidadesModel::N_MOTOR => 'nullable',
            UnidadesModel::N_PRODUCCION => 'nullable',
            UnidadesModel::N_REMISION => 'nullable',
            UnidadesModel::N_FOLIO_REMISION => 'nullable',
            UnidadesModel::PDTO => 'nullable',
            UnidadesModel::FOLIO_PEDIDO => 'nullable',
            UnidadesModel::VESTIDURA_ID => 'nullable',
            UnidadesModel::UNIDAD_DESCRIPCION => 'nullable',
            UnidadesModel::ID_CUENTA => 'required'
        ];
    }

    public function getUnidades($parametros)
    {
        $tabla_unidades = $this->modelo->getTable();

        $query = $this->modelo->select(
            'unidades.id',
            'unidades.unidad_descripcion',

            'catalogo_marcas.id as marca_id',
            'catalogo_marcas.nombre as nombre_marca',

            'catalogo_modelos.id as modelo_id',
            'catalogo_modelos.nombre as nombre_modelo',

            'catalogo_colores.id as color_id',
            'catalogo_colores.nombre as nombre_color',

            'catalogo_anio.id as id_anio',
            'catalogo_anio.nombre as nombre_anio',

            'unidades.motor',
            'unidades.transmision',
            'unidades.combustible',
            'unidades.precio_costo',
            'unidades.vin',
            'unidades.precio_venta',
            'unidades.numero_cilindros',
            'unidades.id_estado',
            'unidades.kilometraje',
            'unidades.capacidad',
            'unidades.numero_puertas',
            'unidades.serie_corta',
            'unidades.no_serie',
            'unidades.placas',
            'unidades.id_ubicacion',
            'unidades.id_ubicacion_llaves',
            'unidades.numero_economico',
            'unidades.usuario_recibe',
            'unidades.fecha_recepcion',
            'unidades.n_produccion',
            'unidades.n_remision',
            'unidades.n_folio_remision',
            'unidades.pdto',
            'unidades.folio_pedido',
            'unidades.vestidura_id',
            'unidades.n_motor',
            'unidades.id_cuenta',
            'unidades_costos.valor_unidad',
            'unidades_costos.valor_unidad_venta',
            'unidades_costos.otros_gastos',
            'unidades_costos.equipo_base_costo',
            'unidades_costos.equipo_base_venta',
            'unidades_costos.gastos_acondi',
            'unidades_costos.seg_traslado_costo',
            'unidades_costos.seg_traslado_venta',
            'unidades_costos.ap_fondos_pub',
            'unidades_costos.impuesto_import_costo',
            'unidades_costos.impuesto_import_venta',
            'unidades_costos.ap_prog_civil',
            'unidades_costos.fletes_import_costo',
            'unidades_costos.fletes_import_venta',
            'unidades_costos.cuota_amda',
            'unidades_costos.gastos_traslado_costo',
            'unidades_costos.gastos_traslado_venta',
            'unidades_costos.cuota_coparmex',
            'unidades_costos.deduccion_ford_costo',
            'unidades_costos.deduccion_ford_venta',
            'unidades_costos.cuota_asociacion_ford',
            'unidades_costos.bonificacion_costo',
            'unidades_costos.bonificacion_venta',
            'unidades_costos.subtota_costo',
            'unidades_costos.subtota_venta',
            'unidades_costos.iva_costo',
            'unidades_costos.iva_venta',
            'unidades_costos.total_costo',
            'unidades_costos.total_venta'
        )->from($tabla_unidades);
        $query->join('unidades_costos', 'unidades_costos.unidad_id', '=', 'unidades.id');
        $query->join('catalogo_marcas', 'catalogo_marcas.id', '=', 'unidades.marca_id');
        $query->join('catalogo_anio', 'catalogo_anio.id', '=', 'unidades.anio_id');
        $query->join('catalogo_modelos', 'catalogo_modelos.id', '=', 'unidades.modelo_id');
        $query->join('catalogo_colores', 'catalogo_colores.id', '=', 'unidades.color_id');

        if (isset($parametros['id_unidad'])) {
            $query->where('unidades.id', $parametros['id_unidad']);
        }

        if (isset($parametros['id_estado'])) {
            $query->where('unidades.id_estado', $parametros['id_estado']);
        }

        $query->limit(200);
        return $query->get();
    }

    public function getunidadesNuevas()
    {
        return $this->getUnidades([
            'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getunidadesSemiNuevas()
    {
        return $this->getUnidades([
            'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function getUnidadSeminuevaById($id)
    {
        return $this->getUnidades([
            'id' => $id,
            'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function getUnidadById($id)
    {
        return $this->getUnidades([
            'id_unidad' => $id,
            'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getTotalUnidades($parametros)
    {
        $tabla_unidades = $this->modelo->getTable();
        $query = $this->modelo->select(
            DB::raw('sum(' . $tabla_unidades . '.precio_costo) as total_precio_costo'),
            DB::raw('sum(' . $tabla_unidades . '.precio_venta) as total_precio_venta')
        )->from($tabla_unidades);

        if (isset($parametros['id_estado'])) {
            $query->where('unidades.id_estado', $parametros['id_estado']);
        }
        return $query->get();
    }

    public function getTotalUnidadesNuevas()
    {
        return $this->getTotalUnidades([
            'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getTotalUnidadesSemiNuevas()
    {
        return $this->getTotalUnidades([
            'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }
}
