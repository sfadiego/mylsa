<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\CatModelosModel;

class ServicioCatModelos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'modelos';
        $this->modelo = new CatModelosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatModelosModel::NOMBRE => 'required',
            CatModelosModel::CLAVE => 'required',
            CatModelosModel::ID_MARCA => 'required',
            CatModelosModel::TIEMPO_LAVADO => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatModelosModel::NOMBRE => 'required',
            CatModelosModel::CLAVE => 'required',
            CatModelosModel::ID_MARCA => 'required',
            CatModelosModel::TIEMPO_LAVADO => 'nullable'
        ];
    }

    public function getReglasBusquedaModelo()
    {
        return [
            CatModelosModel::NOMBRE => 'required'
        ];
    }

    public function searchIdModelo($request)
    {
        $data = $this->modelo->where(CatModelosModel::NOMBRE,$request->get(CatModelosModel::NOMBRE))
            ->select(CatModelosModel::ID)->limit(1)->get();
        
        return $data;
    }
}
