<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Servicios\Core\ServicioDB;

class ServicioCatCombustible extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo combustible';
        $this->modelo = new CatCombustible();
    }

    public function getReglasGuardar()
    {
        return [
            CatCombustible::DESCRIPCION => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatCombustible::DESCRIPCION => 'required'
        ];
    }
}
