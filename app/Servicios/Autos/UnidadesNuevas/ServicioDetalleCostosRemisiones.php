<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Servicios\Core\ServicioDB;

class ServicioDetalleCostosRemisiones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'detalle costos remision';
        $this->modelo = new DetalleCostosRemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            DetalleCostosRemisionModel::REMISIONID => 'required',
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::V_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::V_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::V_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::C_OTROS_GASTOS => 'required',
            DetalleCostosRemisionModel::V_OTROS_GASTOS => 'required',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::V_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::V_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::V_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_ACOND => 'required',
            DetalleCostosRemisionModel::V_GASTOS_ACOND => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::V_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::V_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::C_ISAN => 'required',
            DetalleCostosRemisionModel::V_ISAN => 'required',
            DetalleCostosRemisionModel::AP_FONDO_PUB => 'required',
            DetalleCostosRemisionModel::CUOTA_AMDA => 'required',
            DetalleCostosRemisionModel::CUOTA_COPARMEX => 'required',
            DetalleCostosRemisionModel::CUOTA_ASO_FORD => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DetalleCostosRemisionModel::REMISIONID => 'required',
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::V_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::V_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::V_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::C_OTROS_GASTOS => 'required',
            DetalleCostosRemisionModel::V_OTROS_GASTOS => 'required',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::V_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::V_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::V_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_ACOND => 'required',
            DetalleCostosRemisionModel::V_GASTOS_ACOND => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::V_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::V_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::C_ISAN => 'required',
            DetalleCostosRemisionModel::V_ISAN => 'required',
            DetalleCostosRemisionModel::AP_FONDO_PUB => 'required',
            DetalleCostosRemisionModel::CUOTA_AMDA => 'required',
            DetalleCostosRemisionModel::CUOTA_COPARMEX => 'required',
            DetalleCostosRemisionModel::CUOTA_ASO_FORD => 'required',
        ];
    }
}
