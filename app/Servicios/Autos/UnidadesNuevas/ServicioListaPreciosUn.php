<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\ListPreciosUNModel;
use App\Servicios\Core\ServicioDB;

class ServicioListaPreciosUn extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo líneas';
        $this->modelo = new ListPreciosUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            ListPreciosUNModel::CAT => 'required',
            ListPreciosUNModel::CLAVE_VEHICULAR => 'required',
            ListPreciosUNModel::DESCRIPCION => 'required',
            ListPreciosUNModel::PRECIO_LISTA => 'required',
            ListPreciosUNModel::PRECIO_CLIENTE => 'required',
            ListPreciosUNModel::BONO_FC => 'required',
            ListPreciosUNModel::PRECIO_BONO => 'required',
            ListPreciosUNModel::PRECIO_ACTIVO_ID => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ListPreciosUNModel::CAT => 'required',
            ListPreciosUNModel::CLAVE_VEHICULAR => 'required',
            ListPreciosUNModel::DESCRIPCION => 'required',
            ListPreciosUNModel::PRECIO_LISTA => 'required',
            ListPreciosUNModel::PRECIO_CLIENTE => 'required',
            ListPreciosUNModel::BONO_FC => 'required',
            ListPreciosUNModel::PRECIO_BONO => 'required',
            ListPreciosUNModel::PRECIO_ACTIVO_ID => 'required',
        ];
    }
}
