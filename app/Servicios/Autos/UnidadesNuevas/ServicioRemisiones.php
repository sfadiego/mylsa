<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Servicios\Core\ServicioDB;

class ServicioRemisiones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'proveedores unidades nuevas';
        $this->modelo = new RemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            RemisionModel::LINEA_ID => 'required',
            RemisionModel::CATALOGO_FORDID => 'required',
            RemisionModel::CARLINEID => 'required',
            RemisionModel::CATREALID => 'required',
            RemisionModel::ECONOMICO => 'required',
            RemisionModel::DESCUENTO_MAX => 'required',
            RemisionModel::TENENCIA => 'required',
            RemisionModel::SEG_COB_AMPLIA => 'required',
            RemisionModel::PLAN_GANE => 'required',
            RemisionModel::ORDEN_REPORTE => 'required',
            RemisionModel::UNIDAD_IMPORTAA => 'required',
            RemisionModel::TIPO_AUTO => 'required',
            RemisionModel::CLAVE_ISAN => 'required',
            RemisionModel::CTA_MENUDO => 'required',
            RemisionModel::CTA_FLOTILLA => 'required',
            RemisionModel::CTA_CONAUTO => 'required',
            RemisionModel::CTA_INTERCAMBIO => 'required',
            RemisionModel::CTA_PLLENO => 'required',
            RemisionModel::CTA_INVENTARIO => 'required',
            RemisionModel::VTA_MENUDEO => 'required',
            RemisionModel::VTA_FLOTILLA => 'required',
            RemisionModel::VTA_CONAUTO => 'required',
            RemisionModel::VTA_INTERCAMBIO => 'required',
            RemisionModel::VTA_PLLENO => 'required',
            RemisionModel::USERID => 'required',
            RemisionModel::C_SUBTOTAL => 'required',
            RemisionModel::V_SUBTOTAL => 'required',
            RemisionModel::C_IVA => 'required',
            RemisionModel::V_IVA => 'required',
            RemisionModel::C_PLISTA => 'required',
            RemisionModel::V_PLISTA => 'required',
            RemisionModel::INV => 'required',
            RemisionModel::FECHA_PEDIMENTO => 'required',
            RemisionModel::PEDIMENTO => 'required',
            RemisionModel::FECHA_REMISION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::MOTOR => 'required',
            RemisionModel::INTERCAMBIO => 'required',
            RemisionModel::PROVEEDOR_ID => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RemisionModel::LINEA_ID => 'required',
            RemisionModel::CATALOGO_FORDID => 'required',
            RemisionModel::CARLINEID => 'required',
            RemisionModel::CATREALID => 'required',
            RemisionModel::ECONOMICO => 'required',
            RemisionModel::DESCUENTO_MAX => 'required',
            RemisionModel::TENENCIA => 'required',
            RemisionModel::SEG_COB_AMPLIA => 'required',
            RemisionModel::PLAN_GANE => 'required',
            RemisionModel::ORDEN_REPORTE => 'required',
            RemisionModel::UNIDAD_IMPORTAA => 'required',
            RemisionModel::TIPO_AUTO => 'required',
            RemisionModel::CLAVE_ISAN => 'required',
            RemisionModel::CTA_MENUDO => 'required',
            RemisionModel::CTA_FLOTILLA => 'required',
            RemisionModel::CTA_CONAUTO => 'required',
            RemisionModel::CTA_INTERCAMBIO => 'required',
            RemisionModel::CTA_PLLENO => 'required',
            RemisionModel::CTA_INVENTARIO => 'required',
            RemisionModel::VTA_MENUDEO => 'required',
            RemisionModel::VTA_FLOTILLA => 'required',
            RemisionModel::VTA_CONAUTO => 'required',
            RemisionModel::VTA_INTERCAMBIO => 'required',
            RemisionModel::VTA_PLLENO => 'required',
            RemisionModel::USERID => 'required',
            RemisionModel::C_SUBTOTAL => 'required',
            RemisionModel::V_SUBTOTAL => 'required',
            RemisionModel::C_IVA => 'required',
            RemisionModel::V_IVA => 'required',
            RemisionModel::C_PLISTA => 'required',
            RemisionModel::V_PLISTA => 'required',
            RemisionModel::INV => 'required',
            RemisionModel::FECHA_PEDIMENTO => 'required',
            RemisionModel::PEDIMENTO => 'required',
            RemisionModel::FECHA_REMISION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::MOTOR => 'required',
            RemisionModel::INTERCAMBIO => 'required',
            RemisionModel::PROVEEDOR_ID => 'required',
        ];
    }
}
