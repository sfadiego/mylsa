<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\FacturacionModel;
use App\Servicios\Core\ServicioDB;

class ServicioFacturacion extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'facturacion unidades nuevas';
        $this->modelo = new FacturacionModel();
    }

    public function getReglasGuardar()
    {
        return [
            FacturacionModel::REMISION_ID => 'required',
            FacturacionModel::PROCEDENCIA => 'required',
            FacturacionModel::HOLOGRAMA => 'required',
            FacturacionModel::VENDEDOR => 'required',
            FacturacionModel::ADUANA => 'required',
            FacturacionModel::REPUVE => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            FacturacionModel::REMISION_ID => 'required',
            FacturacionModel::PROCEDENCIA => 'required',
            FacturacionModel::HOLOGRAMA => 'required',
            FacturacionModel::VENDEDOR => 'required',
            FacturacionModel::ADUANA => 'required',
            FacturacionModel::REPUVE => 'required',
        ];
    }
}
