<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\ListPreciosUNModel;
use App\Servicios\Core\ServicioDB;

class ServicioDetalleRemision extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'detalle remisión';
        $this->modelo = new ListPreciosUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            DetalleRemisionModel::REMISIONID => 'required',
            DetalleRemisionModel::PUERTAS => 'required',
            DetalleRemisionModel::CILINDROS => 'required',
            DetalleRemisionModel::TRANSMISION => 'required',
            DetalleRemisionModel::COMBUSTIBLE_ID => 'required',
            DetalleRemisionModel::CAPACIDAD => 'required',
            DetalleRemisionModel::COLORINTID => 'required',
            DetalleRemisionModel::COLOREXTID => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DetalleRemisionModel::REMISIONID => 'required',
            DetalleRemisionModel::PUERTAS => 'required',
            DetalleRemisionModel::CILINDROS => 'required',
            DetalleRemisionModel::TRANSMISION => 'required',
            DetalleRemisionModel::COMBUSTIBLE_ID => 'required',
            DetalleRemisionModel::CAPACIDAD => 'required',
            DetalleRemisionModel::COLORINTID => 'required',
            DetalleRemisionModel::COLOREXTID => 'required',
        ];
    }
}
