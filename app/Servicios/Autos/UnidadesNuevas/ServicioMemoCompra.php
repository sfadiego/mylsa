<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\MemoCompraModel;
use App\Servicios\Core\ServicioDB;

class ServicioMemoCompra extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'memo compra';
        $this->modelo = new MemoCompraModel();
    }

    public function getReglasGuardar()
    {
        return [
            MemoCompraModel::REMISION_ID => 'required',
            MemoCompraModel::ORIGEN_ID => 'required',
            MemoCompraModel::MES => 'required',
            MemoCompraModel::NO_PRODUCTO => 'required',
            MemoCompraModel::MEMO => 'required',
            MemoCompraModel::COSTO_REMISION => 'required',
            MemoCompraModel::COMPRAR => 'required',
            MemoCompraModel::FECHA => 'required',
            MemoCompraModel::MES_COMPRA => 'required',
            MemoCompraModel::FP => 'required',
            MemoCompraModel::DIAS => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            MemoCompraModel::REMISION_ID => 'required',
            MemoCompraModel::ORIGEN_ID => 'required',
            MemoCompraModel::MES => 'required',
            MemoCompraModel::NO_PRODUCTO => 'required',
            MemoCompraModel::MEMO => 'required',
            MemoCompraModel::COSTO_REMISION => 'required',
            MemoCompraModel::COMPRAR => 'required',
            MemoCompraModel::FECHA => 'required',
            MemoCompraModel::MES_COMPRA => 'required',
            MemoCompraModel::FP => 'required',
            MemoCompraModel::DIAS => 'required',
        ];
    }
}
