<?php

namespace App\Servicios\Autos;

use App\Models\Autos\DocumentosVentaModel;
use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Models\Autos\UnidadesModel;
use App\Servicios\Core\ServicioDB;

class ServicioDocumentosVenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'documento';
        $this->modelo = new DocumentosVentaModel();
    }

    public function getReglasGuardar()
    {
        return [
            DocumentosVentaModel::NOMBRE_ARCHIVO => 'required',
            DocumentosVentaModel::ID_VENTA_UNIDAD => 'required|exists:unidades,id',
            DocumentosVentaModel::ID_TIPO_DOCUMENTO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DocumentosVentaModel::NOMBRE_ARCHIVO => 'required',
            DocumentosVentaModel::ID_VENTA_UNIDAD => 'required|exists:unidades,id',
            DocumentosVentaModel::ID_TIPO_DOCUMENTO => 'required'
        ];
    }

    public function validarArchivo($parametros)
    {
        return $this->modelo
            ->where(DocumentosVentaModel::ID_VENTA_UNIDAD, $parametros[DocumentosVentaModel::ID_VENTA_UNIDAD])
            ->where(DocumentosVentaModel::ID_TIPO_DOCUMENTO, $parametros[DocumentosVentaModel::ID_TIPO_DOCUMENTO])
            ->count();
    }

    public function getIdRow($id_unidad, $id_tipo_documento)
    {
        return $this->modelo
            ->where(DocumentosVentaModel::ID_VENTA_UNIDAD, $id_unidad)
            ->where(DocumentosVentaModel::ID_TIPO_DOCUMENTO, $id_tipo_documento)
            ->first();
    }

    public function getArchivosbyIdunidad($id)
    {
        $modelo =  $this->modelo
            ->where(DocumentosVentaModel::ID_VENTA_UNIDAD, $id)
            ->get();

        $new_array = [];

        if (count($modelo) > 0) {
            foreach ($modelo as $key => $value) {
                $new_array[$value->id_tipo_documento] = $value;
            }
        }

        return $new_array;
    }
}
