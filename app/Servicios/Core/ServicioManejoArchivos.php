<?php

namespace App\Servicios\Core;

use Illuminate\Support\Facades\Storage;

class ServicioManejoArchivos
{
    public function __construct()
    {
        $almacenamiento = 'local';
        $this->storage = Storage::disk($almacenamiento);
    }

    public function upload($file, $directorio, $fileName = null)
    {
        if ($file && $directorio) {
            return $this->storage->putFileAs($directorio, $file, !$fileName ? $file->getClientOriginalName() : $fileName);
        }
    }

    public function uploadConNombre($file, $directorio, $name)
    {
        if ($file && $directorio) {
            return $this->storage->putFileAs($directorio, $file, $name);
        }
    }

    public function view($ruta)
    {
        if ($ruta) {
            return $this->storage->get($ruta);
        }
    }

    public function download($ruta, $fileName = '', array $headers = [])
    {
        if ($ruta) {
            return $this->storage->download($ruta, $fileName, $headers);
        }
    }

    public function fileExist($fileUrl)
    {
        return Storage::disk('local')->exists($fileUrl);
    }

    public function allFiles($directory)
    {
        if ($directory) {
            return Storage::allFiles($directory);
        }
    }

    public function setFileName($fileObject)
    {
        $extension = "." . $fileObject->getClientOriginalExtension();
        return uniqid() . "_" . date('Y-m-d') . $extension;
    }

    public function setDirectory($directorio)
    {
        if ($this->fileExist($directorio)) {
            return $directorio;
        }

        Storage::makeDirectory($directorio);
        return $directorio;
    }

    public const DIRECTORIO_UPLOADS = 'uploads';
}
