<?php

namespace App\Servicios\Oasis;

use App\Models\Oasis\CatEstatusOasisModel;
use App\Servicios\Core\ServicioDB;

class ServicioEstatusOasis extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus Oasis';
        $this->modelo = new CatEstatusOasisModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusOasisModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusOasisModel::ESTATUS => 'required'
        ];
    }
}


