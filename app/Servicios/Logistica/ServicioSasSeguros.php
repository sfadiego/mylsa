<?php

namespace App\Servicios\Logistica;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use App\Models\Logistica\SasSeguroModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Usuarios\User;

class ServicioSasSeguros extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'daños bodyshop';
        $this->modelo = new SasSeguroModel();
    }

    public function getReglasGuardar()
    {
        return [
            SasSeguroModel::RECLAMO => 'required',
            SasSeguroModel::TIPO => 'required',
            SasSeguroModel::FECHA_ALTA => 'required',
            SasSeguroModel::ID_UNIDAD => 'required',
            SasSeguroModel::ID_COLOR => 'required',
            SasSeguroModel::ID_ANIO => 'required',
            SasSeguroModel::SERIE => 'required|digits:17',
            SasSeguroModel::DANIO => 'required',
            SasSeguroModel::ID_ESTATUS => 'required',
            SasSeguroModel::FINALIZADO => 'required',
            SasSeguroModel::ID_ASESOR_VENTA => 'required',
            SasSeguroModel::FECHA_ENTREGA_ASESOR => 'required',
            SasSeguroModel::OBSERVACIONES => 'nullable',
            SasSeguroModel::FOTOS => 'nullable',
            SasSeguroModel::DOCUMENTOS => 'nullable',
            SasSeguroModel::PRESUPUESTOS => 'nullable',
            SasSeguroModel::RECUPERADO => 'nullable',
            SasSeguroModel::COBRADO => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            SasSeguroModel::RECLAMO => 'required',
            SasSeguroModel::TIPO => 'required',
            SasSeguroModel::FECHA_ALTA => 'required',
            SasSeguroModel::ID_UNIDAD => 'required',
            SasSeguroModel::ID_COLOR => 'required',
            SasSeguroModel::ID_ANIO => 'required',
            SasSeguroModel::SERIE => 'required|digits:17',
            SasSeguroModel::DANIO => 'required',
            SasSeguroModel::ID_ESTATUS => 'required',
            SasSeguroModel::FINALIZADO => 'required',
            SasSeguroModel::ID_ASESOR_VENTA => 'required',
            SasSeguroModel::FECHA_ENTREGA_ASESOR => 'required',
            SasSeguroModel::OBSERVACIONES => 'nullable',
            SasSeguroModel::FOTOS => 'nullable',
            SasSeguroModel::DOCUMENTOS => 'nullable',
            SasSeguroModel::PRESUPUESTOS => 'nullable',
            SasSeguroModel::RECUPERADO => 'nullable',
            SasSeguroModel::COBRADO => 'nullable'
        ];
    }
    public function getSasSeguros($parametros)
    {
        return $this->modelo
            ->select(
                SasSeguroModel::getTableName() . '.*',
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::NOMBRE.' as color',
                CatEstatusDaniosBodyshopModel::getTableName() . '.' . CatEstatusDaniosBodyshopModel::ESTATUS,
                UnidadesModel::getTableName() . '.' . UnidadesModel::UNIDAD_DESCRIPCION.' as unidad',
                CatalogoAnioModel::getTableName() . '.' . CatalogoAnioModel::NOMBRE.' as anio',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO
            )
            ->join(
                CatalogoColoresModel::getTableName(),
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::ID,
                '=',
                SasSeguroModel::getTableName() . '.' . SasSeguroModel::ID_COLOR
            )
            ->join(
                CatEstatusDaniosBodyshopModel::getTableName(),
                CatEstatusDaniosBodyshopModel::getTableName() . '.' . CatEstatusDaniosBodyshopModel::ID,
                '=',
                SasSeguroModel::getTableName() . '.' . SasSeguroModel::ID_ESTATUS
            )
            ->join(
                UnidadesModel::getTableName(),
                UnidadesModel::getTableName() . '.' . UnidadesModel::ID,
                '=',
                SasSeguroModel::getTableName() . '.' . SasSeguroModel::ID_UNIDAD
            )
            ->join(
                CatalogoAnioModel::getTableName(),
                CatalogoAnioModel::getTableName() . '.' . CatalogoAnioModel::ID,
                '=',
                SasSeguroModel::getTableName() . '.' . SasSeguroModel::ID_ANIO
            )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                SasSeguroModel::getTableName() . '.' . SasSeguroModel::ID_ASESOR_VENTA
            )
            ->get();
    }
}
