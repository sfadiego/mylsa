<?php

namespace App\Servicios\Logistica;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use App\Models\Logistica\CatModosModel;
use App\Models\Logistica\LogisticaModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;

class ServicioLogistica extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'logística';
        $this->modelo = new logisticaModel();
    }

    public function getReglasGuardar()
    {
        return [
            logisticaModel::FECHA_ENTREGA_CLIENTE => 'required',
            logisticaModel::HORA_ENTREGA_CLIENTE => 'required',
            logisticaModel::ECO => 'required',
            logisticaModel::ID_UNIDAD => 'required',
            logisticaModel::ID_COLOR => 'required',
            logisticaModel::SERIE => 'required',
            logisticaModel::ID_CLIENTE => 'required',
            logisticaModel::ID_VENDEDOR => 'required',
            logisticaModel::TIPO_OPERACION => 'required',
            logisticaModel::IVA_DESGLOSADO => 'required',
            logisticaModel::TOMA => 'required',
            logisticaModel::FECHA_PROMESA_VENDEDOR => 'required',
            logisticaModel::HORA_PROMESA_VENDEDOR => 'required',
            logisticaModel::UBICACION => 'required',
            logisticaModel::REPROGRAMACION => 'required',
            logisticaModel::SEGURO => 'required',
            logisticaModel::PERMISO_PLACAS => 'required',
            logisticaModel::ACCESORIOS => 'required',
            logisticaModel::SERVICIO => 'required',
            logisticaModel::FECHA_PROGRAMACION => 'required',
            logisticaModel::HORA_PROGRAMACION => 'required',
            logisticaModel::ID_MODO => 'required',
            logisticaModel::ID_CARRIL => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            logisticaModel::FECHA_ENTREGA_CLIENTE => 'required',
            logisticaModel::HORA_ENTREGA_CLIENTE => 'required',
            logisticaModel::ECO => 'required',
            logisticaModel::ID_UNIDAD => 'required',
            logisticaModel::ID_COLOR => 'required',
            logisticaModel::SERIE => 'required',
            logisticaModel::ID_CLIENTE => 'required',
            logisticaModel::ID_VENDEDOR => 'required',
            logisticaModel::TIPO_OPERACION => 'required',
            logisticaModel::IVA_DESGLOSADO => 'required',
            logisticaModel::TOMA => 'required',
            logisticaModel::FECHA_PROMESA_VENDEDOR => 'required',
            logisticaModel::HORA_PROMESA_VENDEDOR => 'required',
            logisticaModel::UBICACION => 'required',
            logisticaModel::REPROGRAMACION => 'required',
            logisticaModel::SEGURO => 'required',
            logisticaModel::PERMISO_PLACAS => 'required',
            logisticaModel::ACCESORIOS => 'required',
            logisticaModel::SERVICIO => 'required',
            logisticaModel::FECHA_PROGRAMACION => 'required',
            logisticaModel::HORA_PROGRAMACION => 'required',
            logisticaModel::ID_MODO => 'required',
            logisticaModel::ID_CARRIL => 'required'
        ];
    }
    public function getLogistica($parametros)
    {
        $query = $this->modelo
            ->select(
                logisticaModel::getTableName() . '.*',
                UnidadesModel::getTableName() . '.' . UnidadesModel::UNIDAD_DESCRIPCION . ' as unidad',
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::NOMBRE . ' as color',
                catModosModel::getTableName() . '.' . catModosModel::MODO,
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO . ' as ap_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO . ' as am_cliente',
                User::getTableName() . '.' . User::NOMBRE . ' as nombre_vendedor',
                User::getTableName() . '.' . User::APELLIDO_PATERNO . ' as ap_vendedor',
                User::getTableName() . '.' . User::APELLIDO_MATERNO . ' as am_vendedor'
            )
            ->join(
                UnidadesModel::getTableName(),
                UnidadesModel::getTableName() . '.' . UnidadesModel::ID,
                '=',
                logisticaModel::getTableName() . '.' . logisticaModel::ID_UNIDAD
            )
            ->join(
                CatalogoColoresModel::getTableName(),
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::ID,
                '=',
                logisticaModel::getTableName() . '.' . logisticaModel::ID_COLOR
            )
            ->join(
                catModosModel::getTableName(),
                catModosModel::getTableName() . '.' . catModosModel::ID,
                '=',
                logisticaModel::getTableName() . '.' . logisticaModel::ID_MODO
            )
            ->join(
                ClientesModel::getTableName(),
                ClientesModel::getTableName() . '.' . ClientesModel::ID,
                '=',
                logisticaModel::ID_CLIENTE
            )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                logisticaModel::ID_VENDEDOR
            );
            if(isset($parametros[logisticaModel::FECHA_ENTREGA_CLIENTE])){
                $query->where(
                    logisticaModel::FECHA_ENTREGA_CLIENTE,$parametros[logisticaModel::FECHA_ENTREGA_CLIENTE]
                );
            }
            if(isset($parametros[logisticaModel::ID_CARRIL])){
                $query->where(
                    logisticaModel::ID_CARRIL,$parametros[logisticaModel::ID_CARRIL]
                );
            }
            if(isset($parametros[logisticaModel::HORA_ENTREGA_CLIENTE])){
                $query->where(
                    logisticaModel::HORA_ENTREGA_CLIENTE,$parametros[logisticaModel::HORA_ENTREGA_CLIENTE]
                );
            }
            if(isset($parametros[logisticaModel::ID])){
                $query->where(
                    logisticaModel::getTableName() .'.'.logisticaModel::ID,$parametros[logisticaModel::ID]
                );
            }
         return $query->get();
    }
    public function getHorarios($parametros){
        return $this->getLogistica($parametros);
    }
    //Obtener los horarios que tiene el asesor ocupados
    public function getHorariosOcupadosCarril($parametros){
        $horarios = $this->modelo->where(logisticaModel::ID_CARRIL,$parametros[logisticaModel::ID_CARRIL])
                                ->where(logisticaModel::FECHA_ENTREGA_CLIENTE,$parametros[logisticaModel::FECHA_ENTREGA_CLIENTE])
                                ->select(logisticaModel::HORA_ENTREGA_CLIENTE)
                                ->get();
        
    	$array_horarios = array();
    	foreach ($horarios as $key => $value) {
    		$array_horarios[] = $value[logisticaModel::HORA_ENTREGA_CLIENTE];
    	}
    	return $array_horarios;
    }
    //Función para saber si un horario de un asesor en un día específico está ocupado
    public function validarReservacion($parametros){
        return $this->modelo->where(logisticaModel:: FECHA_ENTREGA_CLIENTE,$parametros[logisticaModel:: FECHA_ENTREGA_CLIENTE])
                    ->where(logisticaModel:: HORA_ENTREGA_CLIENTE,$parametros[logisticaModel:: HORA_ENTREGA_CLIENTE])
                    ->where(logisticaModel::ID,'!=',$parametros[logisticaModel::ID])
                    ->get();
    }
}
