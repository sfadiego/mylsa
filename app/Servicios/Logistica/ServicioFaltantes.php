<?php
namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use App\Models\Logistica\FaltantesModel;
use App\Models\Refacciones\CatalogoColoresModel;

class ServicioFaltantes extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'faltantes bodyshop';
        $this->modelo = new FaltantesModel();
    }

    public function getReglasGuardar()
    {
        return [
            FaltantesModel::FOLIO => 'required',
            FaltantesModel::ID_UNIDAD => 'required',
            FaltantesModel::ID_COLOR => 'required',
            FaltantesModel::SERIE => 'required|digits:17',
            FaltantesModel::OBSERVACIONES => 'required',
            FaltantesModel::SOLICITUD => 'required',
            FaltantesModel::RECIBOS => 'required',
            FaltantesModel::FECHA_ENTREGA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            FaltantesModel::FOLIO => 'required',
            FaltantesModel::ID_UNIDAD => 'required',
            FaltantesModel::ID_COLOR => 'required',
            FaltantesModel::SERIE => 'required|digits:17',
            FaltantesModel::OBSERVACIONES => 'required',
            FaltantesModel::SOLICITUD => 'required',
            FaltantesModel::RECIBOS => 'required',
            FaltantesModel::FECHA_ENTREGA => 'required'
        ];
    }
    public function getFaltantes($parametros)
    {
        return $this->modelo
            ->select(
                FaltantesModel::getTableName() . '.*',
                UnidadesModel::getTableName() . '.' . UnidadesModel::UNIDAD_DESCRIPCION.' as unidad',
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::NOMBRE.' as color'
            )
            ->join(
                UnidadesModel::getTableName(),
                UnidadesModel::getTableName() . '.' . UnidadesModel::ID,
                '=',
                FaltantesModel::getTableName() . '.' . FaltantesModel::ID_UNIDAD
            )
            ->join(
                CatalogoColoresModel::getTableName(),
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::ID,
                '=',
                FaltantesModel::getTableName() . '.' . FaltantesModel::ID_COLOR
            )
            
            ->get();
    }
}


