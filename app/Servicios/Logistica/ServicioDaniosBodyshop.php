<?php

namespace App\Servicios\Logistica;

use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatAgenciasModel;
use App\Models\Logistica\CatEstatusAdmvoModel;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use App\Models\Logistica\DaniosBodyshopModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Models\Telemarketing\CatalogoOrigenesModel;
use TablaCatOrigen;

class ServicioDaniosBodyshop extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'daños bodyshop';
        $this->modelo = new DaniosBodyshopModel();
    }

    public function getReglasGuardar()
    {
        return [
            DaniosBodyshopModel::FECHA_ARRIBO => 'required',
            DaniosBodyshopModel::ID_ESTATUS_ADMVO => 'required',
            DaniosBodyshopModel::ID_ASESOR => 'required',
            DaniosBodyshopModel::SERIE => 'required|digits:17',
            DaniosBodyshopModel::ID_COLOR => 'required',
            DaniosBodyshopModel::DANIOS => 'required',
            DaniosBodyshopModel::ID_UBICACION_LLAVES => 'required',
            DaniosBodyshopModel::DESCRIPCION => 'required',
            DaniosBodyshopModel::FALTANTES => 'required',
            DaniosBodyshopModel::ID_ESTATUS => 'required',
            DaniosBodyshopModel::FECHA_INGRESO => 'required',
            DaniosBodyshopModel::FECHA_ENTREGA => 'required',
            DaniosBodyshopModel::OBSERVACIONES => 'required',
            DaniosBodyshopModel::ID_AGENCIA => 'required',
            DaniosBodyshopModel::ID_ORIGEN => 'required',
            DaniosBodyshopModel::HORA_RECEPCION => 'required',
            DaniosBodyshopModel::CARGO_A => 'required',
            DaniosBodyshopModel::COBRO_PROVEEDORES => 'required',
            DaniosBodyshopModel::MEDIO_TRANSPORTACION => 'required',
            DaniosBodyshopModel::ID_AREA_REPARACION => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DaniosBodyshopModel::FECHA_ARRIBO => 'required',
            DaniosBodyshopModel::ID_ESTATUS_ADMVO => 'required',
            DaniosBodyshopModel::ID_ASESOR => 'required',
            DaniosBodyshopModel::SERIE => 'required',
            DaniosBodyshopModel::ID_COLOR => 'required',
            DaniosBodyshopModel::DANIOS => 'required',
            DaniosBodyshopModel::ID_UBICACION_LLAVES => 'required',
            DaniosBodyshopModel::DESCRIPCION => 'required',
            DaniosBodyshopModel::FALTANTES => 'required',
            DaniosBodyshopModel::ID_ESTATUS => 'required',
            DaniosBodyshopModel::FECHA_INGRESO => 'required',
            DaniosBodyshopModel::FECHA_ENTREGA => 'required',
            DaniosBodyshopModel::OBSERVACIONES => 'required',
            DaniosBodyshopModel::ID_AGENCIA => 'required',
            DaniosBodyshopModel::ID_ORIGEN => 'required',
            DaniosBodyshopModel::HORA_RECEPCION => 'required',
            DaniosBodyshopModel::CARGO_A => 'required',
            DaniosBodyshopModel::COBRO_PROVEEDORES => 'required',
            DaniosBodyshopModel::MEDIO_TRANSPORTACION => 'required',
            DaniosBodyshopModel::ID_AREA_REPARACION => 'required'
        ];
    }
    public function getDanios($parametros)
    {
        return $this->modelo
            ->select(
                DaniosBodyshopModel::getTableName() . '.*',
                UbicacionLLavesModel::getTableName() . '.' . UbicacionLLavesModel::NOMBRE.' as ubicacion_llaves',
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::NOMBRE.' as color',
                CatEstatusAdmvoModel::getTableName() . '.' . CatEstatusAdmvoModel::ESTATUS.' as estatus_admvo',
                CatEstatusDaniosBodyshopModel::getTableName() . '.' . CatEstatusDaniosBodyshopModel::ESTATUS.' as estatus_danio',
                CatAgenciasModel::getTableName() . '.' . CatAgenciasModel::AGENCIA,
                CatalogoOrigenesModel::getTableName() . '.' . CatalogoOrigenesModel::ORIGEN
            )
            ->join(
                UbicacionLLavesModel::getTableName(),
                UbicacionLLavesModel::getTableName() . '.' . UbicacionLLavesModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_UBICACION_LLAVES
            )
            ->join(
                CatalogoColoresModel::getTableName(),
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_COLOR
            )
            ->join(
                CatEstatusAdmvoModel::getTableName(),
                CatEstatusAdmvoModel::getTableName() . '.' . CatEstatusAdmvoModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_ESTATUS_ADMVO
            )
            ->join(
                CatEstatusDaniosBodyshopModel::getTableName(),
                CatEstatusDaniosBodyshopModel::getTableName() . '.' . CatEstatusDaniosBodyshopModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_ESTATUS
            )
            ->join(
                CatAgenciasModel::getTableName(),
                CatAgenciasModel::getTableName() . '.' . CatAgenciasModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_AGENCIA
            )
            ->join(
                CatalogoOrigenesModel::getTableName(),
                CatalogoOrigenesModel::getTableName() . '.' . CatalogoOrigenesModel::ID,
                '=',
                DaniosBodyshopModel::getTableName() . '.' . DaniosBodyshopModel::ID_ORIGEN
            )
            ->get();
    }
}
