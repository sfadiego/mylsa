<?php

namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;

class ServicioCatEstatusDaniosBS extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus daños bodyshop';
        $this->modelo = new CatEstatusDaniosBodyshopModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusDaniosBodyshopModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusDaniosBodyshopModel::ESTATUS => 'required'
        ];
    }
}


