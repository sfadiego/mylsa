<?php

namespace App\Servicios\Polizas;

use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Servicios\Refacciones\ServicioCurl;

class ServicioMovimientos
{
    public function __construct()
    {
        $this->servicioCurl = new ServicioCurl;
        $this->cuentasPorCobrarModel = new CuentasPorCobrarModel();
        $this->cuentasPorPagarModel = new CuentasPorPagarModel();
        $this->ventasModel = new VentasRealizadasModel();
        $this->ventasAutosModel = new VentasAutosModel();
    }

    public function getTipoCuentaxCobrar($cuenta_por_cobrar_id)
    {
        return $this->queryTipocuentasxCobrar($cuenta_por_cobrar_id);
    }

    public function getVentaAutoxFolio($folio_id)
    {

        $tabla_ventas_autos = VentasAutosModel::getTableName();
        $tabla_unidades = UnidadesModel::getTableName();
        $query = $this->ventasAutosModel->select(
            $tabla_ventas_autos . '.*',
            $tabla_unidades . '.' . UnidadesModel::PRECIO_COSTO,
            $tabla_unidades . '.' . UnidadesModel::PRECIO_VENTA,
            $tabla_unidades . '.' . UnidadesModel::ID_CUENTA
        )
            ->where($tabla_ventas_autos . '.' . VentasAutosModel::ID_FOLIO,  '=',  $folio_id)
            ->join($tabla_unidades, $tabla_unidades . '.' . UnidadesModel::ID, '=', $tabla_ventas_autos . '.' . VentasAutosModel::ID_UNIDAD)
            ->get();
        return $query->first();
    }

    public function getVentaServicioxFolio($folio_id)
    {
        $tabla_ventas = VentasRealizadasModel::getTableName();
        $tabla_venta_servicio = VentaServicioModel::getTableName();
        $query = $this->ventasModel
            ->select(
                $tabla_ventas . '.' . VentasRealizadasModel::ID,
                $tabla_venta_servicio . '.' . VentaServicioModel::PRECIO,
                $tabla_venta_servicio . '.' . VentaServicioModel::IVA,
                $tabla_venta_servicio . '.' . VentaServicioModel::COSTO_MO
            )
            ->join($tabla_venta_servicio, $tabla_venta_servicio . '.' . VentaServicioModel::VENTA_ID, '=', $tabla_ventas . '.' . VentasRealizadasModel::ID)
            ->where($tabla_ventas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $folio_id);
        return $query->first();
    }

    public function getTipoCuentaxPagar($cxp_id)
    {
        return $this->queryTipocuentasxPagar($cxp_id);
    }

    public function queryTipocuentasxCobrar($cuenta_id)
    {
        $tabla_cxc = CuentasPorCobrarModel::getTableName();
        $tabla_folios = FoliosModel::getTableName();
        $tabla_cuentas = CatalogoCuentasModel::getTableName();
        $query = $this->cuentasPorCobrarModel
            ->select(
                $tabla_cxc . '.' . CuentasPorCobrarModel::ID . ' as id_cxc',
                $tabla_cxc . '.' . CuentasPorCobrarModel::FOLIO_ID,
                $tabla_folios . '.' . FoliosModel::FOLIO,
                $tabla_folios . '.' . FoliosModel::TIPO_CUENTA_ID,
                $tabla_cuentas . '.' . CatalogoCuentasModel::NO_CUENTA,
                $tabla_cuentas . '.' . CatalogoCuentasModel::ID_MOVIMIENTO,
                $tabla_cxc . '.' . CuentasPorCobrarModel::CONCEPTO,
                $tabla_cuentas . '.' . CatalogoCuentasModel::NOMBRE_CUENTA
            )
            ->join($tabla_folios, $tabla_folios . '.' . FoliosModel::ID, '=', $tabla_cxc . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->join($tabla_cuentas, $tabla_cuentas . '.' . CatalogoCuentasModel::ID, '=', $tabla_folios . '.' . FoliosModel::TIPO_CUENTA_ID)
            ->where($tabla_cxc . '.' . CuentasPorCobrarModel::ID, '=', $cuenta_id);

        return $query->first();
    }

    public function queryTipocuentasxPagar($cuenta_id)
    {
        $tabla_cxp = CuentasPorPagarModel::getTableName();
        $tabla_folios = FoliosModel::getTableName();
        $tabla_cuentas = CatalogoCuentasModel::getTableName();
        $query = $this->cuentasPorPagarModel
            ->select(
                $tabla_cxp . '.' . CuentasPorPagarModel::ID . ' as id_cxp',
                $tabla_cxp . '.' . CuentasPorPagarModel::FOLIO_ID,
                $tabla_cxp . '.' . CuentasPorPagarModel::CONCEPTO,
                $tabla_folios . '.' . FoliosModel::FOLIO,
                $tabla_folios . '.' . FoliosModel::TIPO_CUENTA_ID,
                $tabla_cuentas . '.' . CatalogoCuentasModel::NO_CUENTA,
                $tabla_cuentas . '.' . CatalogoCuentasModel::ID_MOVIMIENTO,
                $tabla_cuentas . '.' . CatalogoCuentasModel::NOMBRE_CUENTA
            )
            ->join($tabla_folios, $tabla_folios . '.' . FoliosModel::ID, '=', $tabla_cxp . '.' . CuentasPorPagarModel::FOLIO_ID)
            ->join($tabla_cuentas, $tabla_cuentas . '.' . CatalogoCuentasModel::ID, '=', $tabla_folios . '.' . FoliosModel::TIPO_CUENTA_ID)
            ->where($tabla_cxp . '.' . CuentasPorPagarModel::ID, '=', $cuenta_id);

        return $query->first();
    }
}
