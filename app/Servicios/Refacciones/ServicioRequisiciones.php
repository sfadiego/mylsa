<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\RequisicionesModel;
use App\Models\Refacciones\VendedorModel;

class ServicioRequisiciones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'requisicion';
        $this->modelo = new RequisicionesModel();
    }

    public function getByIdRequisicion($id)
    {
        return $this->modelo->with([
            ProductosModel::getTableName(),
            ClientesModel::getTableName(),
            VendedorModel::getTableName()
        ])->find($id);
    }

    public function getReglasGuardar()
    {
        return [
            RequisicionesModel::FOLIO => 'required',
            RequisicionesModel::REQUISICION => 'required',
            RequisicionesModel::CLASE_REQUISICION => 'required',
            RequisicionesModel::CLIENTE_ID => 'required|exists:clientes,id',
            RequisicionesModel::PRODUCTO_ID => 'required|exists:producto,id',
            RequisicionesModel::VENDEDOR_ID => 'required|exists:vendedor,id',
            RequisicionesModel::CONSECUTIVO => 'integer',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RequisicionesModel::FOLIO => 'required',
            RequisicionesModel::REQUISICION => 'required',
            RequisicionesModel::CLASE_REQUISICION => 'required',
            RequisicionesModel::CLIENTE_ID => 'required',
            RequisicionesModel::PRODUCTO_ID => 'required|exists:producto,id',
            RequisicionesModel::VENDEDOR_ID => 'required|exists:vendedor,id',
            RequisicionesModel::CONSECUTIVO => 'integer',

        ];
    }
}
