<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Facturas\ServicioFacturacion;
use App\Servicios\Facturas\ServicioXmlFactura;
use App\Models\Autos\HistoricoProductosServicioModel;
use App\Models\Refacciones\Almacenes as RefaccionesAlmacenes;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ListaProductosOrdenCompraModel as ProductoOrdenCompra;
use App\Models\Refacciones\ProductosModel as ProductosModel;
use App\Models\Refacciones\Talleres;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\EstatusFacturaModel;
use App\Models\Refacciones\ReFacturaProductoModel;
use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\TraspasoProductoAlmacenModel;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class ServicioProductos extends ServicioDB
{
    private $factura_id;
    private $tipo_factura;
    private $orden_compra_id;

    public function __construct()
    {
        $this->recurso = 'producto';
        $this->modelo = new ProductosModel();
        $this->modeloOrdenCompraProducto = new ProductoOrdenCompra();
        $this->modeloReFacturaProducto = new ReFacturaProductoModel();
        $this->modeloStockProductos = new StockProductosModel();
        $this->modeloVentaProducto = new VentaProductoModel();
        $this->modeloTraspasoProducto = new TraspasoProductoAlmacenModel();
        $this->servicioFacturacion = new ServicioFacturacion();
        $this->servicioProductoAlmacen = new ServicioProductoAlmacen();
        $this->servicioConsultaStock = new ServicioConsultaStock();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioXml = new ServicioXmlFactura();
        $this->servicioValidaPedidoProducto = new ServicioValidaPedidoProducto();
    }

    public function storeproductos($request)
    {
        $create_producto =  parent::store($request);

        $params = [
            StockProductosModel::PRODUCTO_ID => $create_producto->id,
            StockProductosModel::CANTIDAD_ACTUAL => $request->get('cantidad'),
            StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => $request->get('cantidad'),
        ];

        return $this->modeloStockProductos->create($params);
    }
    public function setTipoFactura($tipo, $id_elemento = '')
    {
        $this->tipo_factura = $tipo;
        if (ServicioFacturacion::FACTURA_ORDEN_COMPRA == $tipo) {
            $this->orden_compra_id = $id_elemento;
        }
    }

    public function getAllProductos($parametros)
    {

        $producto_id = isset($parametros['producto_id'])
            && $parametros['producto_id']
            ? $parametros['producto_id'] : null;

        $consulta = $this->modelo
            ->with([
                Talleres::getTableName(),
                RefaccionesAlmacenes::getTableName(),
                ProductosModel::REL_UBICACION,
                ProductosModel::REL_PRECIO,
                ProductosModel::REL_PRODUCTO_ALMACEN => function ($query) {
                    $query->with([
                        ProductoAlmacenModel::REL_ALMACEN => function ($query2) {
                            $query2->select(Almacenes::ID, Almacenes::NOMBRE);
                        }
                    ])->select(
                        ProductosModel::ALMACEN_ID,
                        ProductoAlmacenModel::PRODUCTO_ID,
                        ProductoAlmacenModel::NO_IDENTIFICACION,

                        DB::raw('sum(' . ProductoAlmacenModel::CANTIDAD . ') as cantidad')

                    )->groupBy(
                        ProductosModel::ALMACEN_ID,
                        ProductoAlmacenModel::PRODUCTO_ID,
                        ProductoAlmacenModel::NO_IDENTIFICACION
                    );
                }
            ]);
        if ($producto_id) {
            $consulta->where(ProductosModel::getTableName() . '.' . ProductosModel::ID, $producto_id);
        }
        $consulta->where(ProductosModel::EXISTENCIA, true);
        return $consulta->get();
    }

    public function getproductodataByid($id)
    {
        return $this->modelo->with([
            Talleres::getTableName(),
            RefaccionesAlmacenes::getTableName(),
            ProductosModel::REL_PRECIO,
            ProductosModel::REL_UBICACION,
            ProductosModel::REL_PRODUCTO_ALMACEN => function ($query) {
                $query->with([
                    ProductoAlmacenModel::REL_ALMACEN => function ($query2) {
                        $query2->select(Almacenes::ID, Almacenes::NOMBRE);
                    }
                ])->select(
                    ProductosModel::ID,
                    ProductosModel::ALMACEN_ID,
                    ProductoAlmacenModel::PRODUCTO_ID,
                    ProductoAlmacenModel::NO_IDENTIFICACION,
                    ProductoAlmacenModel::CANTIDAD
                );
            }
        ])->find($id);
    }

    public function updateproductos(Request $request, $id)
    {
        ParametrosHttpValidador::validar($request, $this->getReglasUpdate());
        $productos_update = parent::update($request, $id);
        if ($productos_update) {
            $modelo = $this->modeloStockProductos->where(StockProductosModel::PRODUCTO_ID, $id)->first();
            $modelo->cantidad_actual = $request->get('cantidad');
            $modelo->cantidad_almacen_primario = $request->get('cantidad');
            return $modelo->save();
        }
    }

    public function existeProducto($request)
    {
        if ($request instanceof Request) {
            $query = $this->modelo->select(
                ProductosModel::getTableName() . '.*'
            );

            if (isset($request[ucwords(ProductosModel::DESCRIPCION)])) {
                $query->where(ProductosModel::DESCRIPCION, '=', $request->get(ProductosModel::DESCRIPCION));
            }

            if (isset($request[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
                $query->where(ProductosModel::NO_IDENTIFICACION_FACTURA, '=', $request->get(ProductosModel::NO_IDENTIFICACION_FACTURA));
            }

            return $query->get();
        }
        
        $query = $this->modelo->select(
            ProductosModel::getTableName() . '.*'
        );

        
        if (isset($request[ucwords(ProductosModel::NO_IDENTIFICACION_FACTURA)])) {
            $query->where(ProductosModel::NO_IDENTIFICACION_FACTURA, '=', $request[ucwords(ProductosModel::NO_IDENTIFICACION_FACTURA)]);
        }else if (isset($request[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            $query->where(ProductosModel::NO_IDENTIFICACION_FACTURA, '=', $request[ProductosModel::NO_IDENTIFICACION_FACTURA]);
        }

        if (isset($request[ucwords(ProductosModel::DESCRIPCION)])) {

            $query->where(ProductosModel::DESCRIPCION, '=', $request[ucwords(ProductosModel::DESCRIPCION)]);
        }else if (isset($request[ProductosModel::DESCRIPCION])) {
            $query->where(ProductosModel::DESCRIPCION, '=', $request[ProductosModel::DESCRIPCION]);
        }
        
        if (isset($request['producto_id'])) {
            $query->where(ProductosModel::ID, '=', $request['producto_id']);
        }
        // dd($query->dd());
        // return ['sql'=>$query->toSql()];

        return $query->get();
    }

    public function validarExistenciaProducto($parametros)
    {
        $query = $this->modelo->select(
            ProductosModel::getTableName() . '.' . ProductosModel::ID . ' as producto_id',
            ProductosModel::getTableName() . '.' . ProductosModel::VALOR_UNITARIO,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA . ' as no_identificacion_dms',
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
            HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID  . ' as producto_servicio_id',
            HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::NO_IDENTIFICACION  . ' as no_identificacion_servicio'
        );

        $query->join(
            HistoricoProductosServicioModel::getTableName(),
            HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::PRODUCTO_ID,
            '=',
            ProductosModel::getTableName() . '.' . ProductosModel::ID
        );

        if (isset($parametros[ucwords(ProductosModel::NO_IDENTIFICACION_FACTURA)])) {
            $query->where(HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::NO_IDENTIFICACION, '=', $parametros[ucwords(ProductosModel::NO_IDENTIFICACION_FACTURA)]);
        }

        if (isset($parametros[ucwords(ProductosModel::DESCRIPCION)])) {
            $query->Orwhere(ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION, 'like', strtoupper($parametros[ucwords(ProductosModel::DESCRIPCION)]) . '%'); //validar mayuscula y camel case
        }

        return $query->get();
    }

    public function getXmlData($path_factura)
    {
        $real_path = realpath('../storage/app/' . $path_factura);
        return $this->servicioFacturacion->getFacturaxml($real_path);
    }

    public function guardarDataFacturaProducto($parametros)
    {

        $xml_object = $this->getXmlData($parametros['xml_path']);
        $proveedor_id = isset($parametros['proveedor_id']) ? $parametros['proveedor_id'] : 0;
        $this->guardarConceptos($xml_object->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto'), $proveedor_id);
        return $this->servicioFacturacion->getinformacionFactura($this->getFacturaId());
    }

    public function guardarConceptos($data, $proveedor_id = 0)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $key => $item) {
                
                $producto = $this->existeProducto([
                    ProductosModel::NO_IDENTIFICACION_FACTURA => $this->servicioXml->validateAndCastItem($item['NoIdentificacion'])
                ]);
                $data = [
                    ProductosModel::DESCRIPCION => $this->servicioXml->validateAndCastItem($item['Descripcion']),
                    ProductosModel::CLAVE_UNIDAD_FACTURA => $this->servicioXml->validateAndCastItem($item['ClaveUnidad']),
                    ProductosModel::NO_IDENTIFICACION_FACTURA => $this->servicioXml->validateAndCastItem($item['NoIdentificacion']),
                    ProductosModel::CLAVE_PROD_SERV_FACTURA => $this->servicioXml->validateAndCastItem($item['ClaveProdServ']),
                    ProductosModel::FACTURA_ID => $this->getFacturaId(),
                    ProductosModel::UNIDAD => $this->servicioXml->validateAndCastItem($item['Unidad']),
                    ProductosModel::PRECIO_FACTURA => $this->servicioXml->validateAndCastItem($item['Importe']),
                    ProductosModel::VALOR_UNITARIO => $this->servicioXml->validateAndCastItem($item['ValorUnitario']),
                    ProductosModel::CANTIDAD => $this->servicioXml->validateAndCastItem($item['Cantidad']),
                    ProductosModel::PRECIO_ID => Precios::DEFAULT_PRECIO,
                    ProductosModel::TALLER_ID => Talleres::DEFAUL_TALLER,
                    ProductosModel::ALMACEN_ID => RefaccionesAlmacenes::DEFAULT_ALMACEN
                ];
                if (count($producto) > 0) {
                    $elemento = $producto->toArray();
                    $cantidad_actual = $elemento[0][ProductosModel::CANTIDAD];
                    $total = $this->servicioXml->validateAndCastItem($item['Cantidad']) + $cantidad_actual;
                    $data[ProductosModel::UBICACION_PRODUCTO_ID] = $elemento[0][ProductosModel::UBICACION_PRODUCTO_ID];
                    $data[ProductosModel::CANTIDAD] = $total;
                    
                    if ($proveedor_id != 0) {
                        $existe_pedido = $this->servicioValidaPedidoProducto->validarProductoPedido([
                            'producto_id' => $producto[0][ProductosModel::ID],
                            'proveedor_id' => $proveedor_id
                        ]);

                        if ($existe_pedido) {

                            $updatepedido = [
                                'cantidad_factura' => $this->servicioXml->validateAndCastItem($item['Cantidad']),
                                'cantidad_cargada' => $existe_pedido->cantidad_cargada,
                                'producto_pedido_id' => $existe_pedido->id,
                                'cantidad_solicitada' => $existe_pedido->cantidad_solicitada
                            ];
                            $this->servicioValidaPedidoProducto->updatepedido($updatepedido);
                            $this->servicioValidaPedidoProducto->pedidofinalizado();
                        } else {
                            DB::rollback();
                            throw new ParametroHttpInvalidoException([
                                'msg' => "No hay pedidos para este producto/proveedor."
                            ]);
                        }
                    }

                    DB::table($this->modelo->getTableName())
                        ->where(ProductosModel::ID, $producto[0][ProductosModel::ID])
                        ->update($data);
                    $this->modeloOrdenCompraProducto->create([
                        ProductoOrdenCompra::PRODUCTO_ID => $producto[0][ProductosModel::ID],
                        ProductoOrdenCompra::TOTAL => $this->servicioXml->validateAndCastItem($item['Importe']),
                        ProductoOrdenCompra::CANTIDAD => $this->servicioXml->validateAndCastItem($item['Cantidad']),
                        ProductoOrdenCompra::PRECIO => $data[ProductosModel::VALOR_UNITARIO],
                        ProductoOrdenCompra::ORDEN_COMPRA_ID => $this->orden_compra_id,
                    ]);

                    $this->modeloReFacturaProducto->create([
                        ReFacturaProductoModel::FACTURA_ID => $this->getFacturaId(),
                        ReFacturaProductoModel::PRODUCTO_ID => $producto[0][ProductosModel::ID],
                        ReFacturaProductoModel::ESTATUS_FACTURA_ID => EstatusFacturaModel::ESTATUS_CREADA,
                        ReFacturaProductoModel::USER_ID =>  Auth::user() ? Auth::user()->id : 1
                    ]);

                    DB::table(ProductoAlmacenModel::getTableName())
                        ->where(ProductoAlmacenModel::PRODUCTO_ID, $producto[0][ProductosModel::ID])
                        ->update([ProductoAlmacenModel::CANTIDAD => $total]);
                    // $this->servicioProductoAlmacen->massUpdateWhereId(
                    //     ProductoAlmacenModel::PRODUCTO_ID,
                    //     $producto[0][ProductosModel::ID],
                    //     [ProductoAlmacenModel::CANTIDAD => $total]
                    // );

                } else {
                    $producto = $this->modelo->create($data);
                    $this->relacionarFacturaMovimiento($producto);
                    $this->servicioProductoAlmacen->crear([
                        ProductoAlmacenModel::ALMACEN_ID => Talleres::DEFAUL_TALLER,
                        ProductoAlmacenModel::PRODUCTO_ID => $producto->id,
                        ProductoAlmacenModel::NO_IDENTIFICACION => $data[ProductosModel::NO_IDENTIFICACION_FACTURA],
                        ProductoAlmacenModel::CANTIDAD => $data[ProductosModel::CANTIDAD],
                    ]);

                    $this->modeloReFacturaProducto->create([
                        ReFacturaProductoModel::FACTURA_ID => $this->getFacturaId(),
                        ReFacturaProductoModel::PRODUCTO_ID => $producto->id,
                        ReFacturaProductoModel::ESTATUS_FACTURA_ID => EstatusFacturaModel::ESTATUS_CREADA,
                        ReFacturaProductoModel::USER_ID =>  Auth::user() ? Auth::user()->id : 1,
                    ]);
                }
            }
            DB::commit();
        } catch (ParametrosHttpValidador $e) {
            Log::error($e);
            DB::rollback();
            // throw new MylsaException(__(static::$E0013_ERROR_REGISTERING_RESOURCE, ['recurso' => $this->getRecurso()]));
        }
    }

    public function relacionarFacturaMovimiento($producto)
    {
        // dd($producto->valor_unitario, $producto->precio_factura, $producto->cantidad, $producto->id);
        // dd($this->orden_compra_id, $this->tipo_factura);
        switch ($this->tipo_factura) {
            case ServicioFacturacion::FACTURA_ORDEN_COMPRA:
                $this->modeloOrdenCompraProducto->create([
                    ProductoOrdenCompra::PRODUCTO_ID => $producto->id,
                    ProductoOrdenCompra::TOTAL => $producto->precio_factura,
                    ProductoOrdenCompra::CANTIDAD => $producto->cantidad,
                    ProductoOrdenCompra::PRECIO => $producto->valor_unitario,
                    ProductoOrdenCompra::ORDEN_COMPRA_ID => $this->orden_compra_id,
                ]);
                break;

            default:
                break;
        }
    }

    public function getReglasGuardar()
    {
        return [
            ProductosModel::DESCRIPCION => 'required',
            ProductosModel::UNIDAD => 'required|string',
            ProductosModel::PREFIJO => 'nullable|string',
            ProductosModel::SUFIJO => 'nullable|string',
            ProductosModel::BASICO => 'nullable|string',
            ProductosModel::VALOR_UNITARIO => 'required',
            ProductosModel::INVENTARIABLE => 'required|integer',
            ProductosModel::EXISTENCIA => 'integer',
            ProductosModel::TALLER_ID => 'required|exists:taller,id',
            ProductosModel::ALMACEN_ID => 'required|exists:almacen,id',
            ProductosModel::PRECIO_ID => 'nullable|exists:precio,id',
            ProductosModel::PRECIO_FACTURA => 'required|integer',
            ProductosModel::ACTIVO => 'integer',
            ProductosModel::CANTIDAD => 'nullable|integer',
            ProductosModel::CLAVE_PROD_SERV_FACTURA => '',
            ProductosModel::CLAVE_UNIDAD_FACTURA => '',
            ProductosModel::NO_IDENTIFICACION_FACTURA => 'required',
            ProductosModel::UBICACION_PRODUCTO_ID => 'nullable|exists:catalogo_ubicaciones_producto,id'
        ];
    }

    public function getReglasAlmacen()
    {
        return [
            ProductosModel::ALMACEN_ID => 'nullable|exists:almacen,id',
        ];
    }

    public function updateProductoAlmacen($id, $data)
    {
        return  $this->modelo->where(ProductosModel::ID, $id)->update($data);
    }

    public function setFacturaId($factura_id)
    {
        $this->factura_id = $factura_id;
    }

    public function getFacturaId()
    {
        return $this->factura_id;
    }

    public function getReglasUpdate()
    {
        return [
            ProductosModel::DESCRIPCION => 'string',
            ProductosModel::UNIDAD => 'required|string',
            ProductosModel::PREFIJO => 'nullable|string',
            ProductosModel::SUFIJO => 'nullable|string',
            ProductosModel::BASICO => 'nullable|string',
            ProductosModel::VALOR_UNITARIO => 'required',
            ProductosModel::INVENTARIABLE => 'integer',
            ProductosModel::EXISTENCIA => 'integer',
            ProductosModel::PRECIO_ID => 'required|exists:precio,id',
            ProductosModel::TALLER_ID => 'required|exists:taller,id',
            ProductosModel::PRECIO_FACTURA => 'integer',
            ProductosModel::ACTIVO => 'integer',
            ProductosModel::ALMACEN_ID => 'required|exists:almacen,id',
            ProductosModel::CANTIDAD => 'required',
            ProductosModel::CLAVE_PROD_SERV_FACTURA => '',
            ProductosModel::CLAVE_UNIDAD_FACTURA => '',
            ProductosModel::NO_IDENTIFICACION_FACTURA => 'required',
            ProductosModel::UBICACION_PRODUCTO_ID => 'nullable|exists:catalogo_ubicaciones_producto,id'
        ];
    }

    public function getReglasBusquedaPiezas()
    {
        return [
            ProductosModel::DESCRIPCION => 'nullable|string',
            ProductosModel::NO_IDENTIFICACION_FACTURA => 'nullable|string',
        ];
    }

    public function getReglasUpdateUbicacionProducto()
    {
        return [
            ProductosModel::UBICACION_PRODUCTO_ID => 'required|exists:catalogo_ubicaciones_producto,id'
        ];
    }

    public function searchProductos($data)
    {
        if (isset($data[ProductosModel::DESCRIPCION]) && empty($data[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            return $this->modelo
                ->where(ProductosModel::DESCRIPCION, 'like', '%' . strtoupper($data[ProductosModel::DESCRIPCION]) . '%')
                ->get();
        }

        if (isset($data[ProductosModel::NO_IDENTIFICACION_FACTURA]) && empty($data[ProductosModel::DESCRIPCION])) {
            return $this->modelo
                ->where(ProductosModel::NO_IDENTIFICACION_FACTURA, 'like', '%' . $data[ProductosModel::NO_IDENTIFICACION_FACTURA] . '%')
                ->get();
        }

        if (isset($data[ProductosModel::NO_IDENTIFICACION_FACTURA]) && isset($data[ProductosModel::DESCRIPCION])) {
            return $this->modelo
                ->where(ProductosModel::NO_IDENTIFICACION_FACTURA, 'like', '%' . $data[ProductosModel::NO_IDENTIFICACION_FACTURA] . '%')
                ->where(ProductosModel::DESCRIPCION, 'like', '%' . '%' . strtoupper($data[ProductosModel::DESCRIPCION]) . '%')
                ->get();
        }

        if (empty($data[ProductosModel::NO_IDENTIFICACION_FACTURA]) && empty($data[ProductosModel::DESCRIPCION])) {
            throw new Exception(__(static::$I0006_REQUIRED_PARAMETERS), 400);
        }
    }

    public function getReglasDescontarAumentar()
    {
        return [
            ProductosModel::NO_IDENTIFICACION_FACTURA => 'required|exists:producto,no_identificacion',
            ProductosModel::CANTIDAD => 'required|numeric'
        ];
    }

    //TODO: agregar cardex
    public function descontarProductos($data)
    {
        $productoId = $data[ProductosModel::ID];
        $cantidad_descontar = $data[ProductosModel::CANTIDAD];
        $producto = $this->modelo->where(ProductosModel::ID, $productoId)->first();
        if (isset($producto) && $producto) {
            if ($cantidad_descontar > $producto->cantidad) {
                throw new ParametroHttpInvalidoException([
                    'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $productoId])
                ]);
            } else {
                return $this->massUpdateWhereId(ProductosModel::ID, $producto->id, [
                    ProductosModel::CANTIDAD => $producto->cantidad - $cantidad_descontar
                ]);
            }
        } else {
            throw new ParametroHttpInvalidoException([
                'producto' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => $productoId])
            ]);
        }
    }


    public function aumentarProductos($data)
    {
        $producto_no_identificacion = $data[ProductosModel::NO_IDENTIFICACION_FACTURA];
        $cantidad_sumar = $data[ProductosModel::CANTIDAD];
        $producto = $this->modelo->where(ProductosModel::NO_IDENTIFICACION_FACTURA, $producto_no_identificacion)->first();

        return $this->massUpdateWhereId(ProductosModel::ID, $producto->id, [
            ProductosModel::CANTIDAD => $producto->cantidad + $cantidad_sumar
        ]);
    }

    public function getReglasValidarCantidad()
    {
        return [
            ProductosModel::CANTIDAD => 'required|numeric',
            ProductosModel::ALMACEN_ID => 'required|numeric',
            ProductoAlmacenModel::PRODUCTO_ID => 'required|exists:producto,id'
        ];
    }

    public function validarRestarCantidadProducto($data)
    {
        $producto = $this->servicioProductoAlmacen->getCantidadAlmByProducto($data);
        if ($data[ProductosModel::CANTIDAD] > $producto->cantidad) {
            throw new ParametroHttpInvalidoException([
                'cantidad' => [self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN]
            ]);
        }

        return $producto;
    }

    public function getVentasProductoId($params)
    {
        $query = DB::table('producto')
            ->join('factura', 'producto.factura_id', '=', 'factura.id')
            ->join('venta_producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->join('ventas', 'venta_producto.venta_id', '=', 'ventas.id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->join('clientes', 'clientes.id', '=', 'ventas.cliente_id');
        $query->leftJoin('devolucion_venta', 'devolucion_venta.venta_id', '=', 'ventas.id');
        $query->select(
            'factura.folio AS folioFactura',
            'producto.factura_id',
            'producto.valor_unitario',
            'venta_producto.cantidad as cantidad',
            'estatus_venta.nombre as estatus',
            'estatus_venta.id as estatus_id',
            're_ventas_estatus.created_at as fecha',
            'clientes.nombre as cliente',
            'ventas.folio_id as folio',
            'devolucion_venta.folio_id as folio_devolucion'
        );
        $query->where('venta_producto.producto_id', $params[ProductosModel::ID]);
        if (isset($params[VentasRealizadasModel::CLIENTE_ID]) && $params[VentasRealizadasModel::CLIENTE_ID]) {
            $query->where(VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::CLIENTE_ID, $params[VentasRealizadasModel::CLIENTE_ID]);
        }
        $query->whereIn('re_ventas_estatus.estatus_ventas_id', [
            EstatusVentaModel::ESTATUS_FINALIZADA,
            EstatusVentaModel::ESTATUS_DEVOLUCION
        ]);
        $query->whereBetween('ventas.created_at', [$params[ProductosModel::FECHA_RANGO_INICIO] . ' 00:00:00', $params[ProductosModel::FECHA_RANGO_FIN] . ' 23:59:59']);
        $query->orderBy('re_ventas_estatus.created_at', 'asc');
        return $query->get();
    }

    public function getComprasProductoId($params)
    {
        $consulta = DB::table('producto')
            ->join('factura', 'producto.factura_id', '=', 'factura.id')
            ->join('productos_orden_compra', 'productos_orden_compra.producto_id', '=', 'producto.id')
            ->join('orden_compra', 'orden_compra.id', '=', 'productos_orden_compra.orden_compra_id')
            ->join('proveedor_refacciones', 'orden_compra.proveedor_id', '=', 'proveedor_refacciones.id')
            ->join('re_compras_estatus', 're_compras_estatus.orden_compra_id', '=', 'orden_compra.id')
            ->join('estatus_compra', 'estatus_compra.id', '=', 're_compras_estatus.estatus_compra_id');
        $consulta->leftJoin('devolucion_proveedor', 'devolucion_proveedor.orden_compra_id', '=', 'orden_compra.id');
        $consulta->select(
            'factura.folio AS folioFactura',
            'producto.factura_id',
            'producto.valor_unitario',
            'productos_orden_compra.cantidad as cantidad',
            'estatus_compra.nombre as estatus',
            'estatus_compra.id as estatus_id',
            're_compras_estatus.created_at as fecha',
            'proveedor_refacciones.proveedor_nombre as cliente',
            'orden_compra.folio_id as folio',
            'devolucion_proveedor.folio_id as folio_devolucion'
        )
            ->where('producto_id', $params[ProductosModel::ID])
            // ->where('re_compras_estatus.activo', true)
            ->whereIn('re_compras_estatus.estatus_compra_id', [
                EstatusCompra::ESTATUS_FINALIZADA,
                EstatusCompra::ESTATUS_DEVOLUCION
            ])
            ->whereBetween('orden_compra.created_at', [$params[ProductosModel::FECHA_RANGO_INICIO] . ' 00:00:00', $params[ProductosModel::FECHA_RANGO_FIN] . ' 23:59:59'])
            ->orderBy('re_compras_estatus.created_at', 'asc');
        return $consulta->get();
    }

    public function stockProductos($respuesta)
    {
        $consulta = DB::table(ProductosModel::getTableName());
        $consulta->select(
            ProductosModel::getTableName() . '.*',
            StockProductosModel::getTableName() . '.*',
            ProductosModel::getTableName() . '.id',
            CatalogoUbicacionProductoModel::getTableName() . '.' . CatalogoUbicacionProductoModel::NOMBRE . ' as ubicacionProducto'
        );
        $consulta->leftJoin('stock_productos', 'stock_productos.producto_id', '=', 'producto.id');
        $consulta->join('catalogo_ubicaciones_producto', 'catalogo_ubicaciones_producto.id', '=', 'producto.ubicacion_producto_id');
        //$consulta->where('stock_productos.cantidad_actual', '>=', 1);

        if (isset($respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]) && $respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]) {
            $consulta->where(
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                '=',
                $respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]
            );
        }

        if (isset($respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]) && $respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]) {
            $consulta->where(
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                '=',
                $respuesta[ProductosModel::NO_IDENTIFICACION_FACTURA]
            );
        }

        if (isset($respuesta[ProductosModel::DESCRIPCION]) && $respuesta[ProductosModel::DESCRIPCION]) {
            $consulta->where(
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
                'like',
                strtoupper($respuesta[ProductosModel::DESCRIPCION]) . '%'
            );
        }

        if (isset($respuesta[StockProductosModel::PRODUCTO_ID]) && $respuesta[StockProductosModel::PRODUCTO_ID]) {
            $consulta->where(
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                $respuesta[StockProductosModel::PRODUCTO_ID]
            );
        }
        $consulta->limit(30);
        $query = $consulta->get();

        if (isset($respuesta['venta_id']) && $respuesta['venta_id']) {
            return $this->productosMenosVentasByVentaId($query, $respuesta['venta_id']);
        }

        if (isset($respuesta['traspaso_id']) && $respuesta['traspaso_id']) {
            return $this->productosMenosTraspasosByTraspasoId($query, $respuesta['traspaso_id']);
        }

        return $query;
    }

    private function productosMenosVentasByVentaId($consulta, $venta_id)
    {
        $listado = [];
        $ventas = $this->modeloVentaProducto->where(VentaProductoModel::VENTA_ID, $venta_id)->get();

        foreach ($consulta as $producto) {
            foreach ($ventas as $venta) {
                if ($producto->id == $venta['producto_id']) {
                    $producto->cantidad_actual =  $producto->cantidad_actual - $venta['cantidad'];
                    $producto->cantidad_almacen_primario =  $producto->cantidad_almacen_primario - $venta['cantidad'];
                }
            }
            array_push($listado, $producto);
        }
        return $listado;
    }

    private function productosMenosTraspasosByTraspasoId($consulta, $traspaso_id)
    {
        $listado = [];
        $consulta_productos = $consulta->toArray();
        $traspasos = $this->modeloTraspasoProducto->where(TraspasoProductoAlmacenModel::ID, $traspaso_id)->get()->toArray();
        foreach ($consulta_productos as $producto) {
            foreach ($traspasos as $traspaso) {
                if ($producto->id == $traspaso['producto_id']) {
                    if ($traspaso['almacen_origen_id'] == 1) {
                        $producto->cantidad_almacen_primario = $producto->cantidad_almacen_primario - $traspaso['cantidad'];
                        $producto->cantidad_almacen_secundario = $producto->cantidad_almacen_secundario + $traspaso['cantidad'];
                    } else {
                        $producto->cantidad_almacen_primario = $producto->cantidad_almacen_primario + $traspaso['cantidad'];
                        $producto->cantidad_almacen_secundario = $producto->cantidad_almacen_secundario - $traspaso['cantidad'];
                    }
                }
            }
            array_push($listado, $producto);
        }
        return $listado;
    }

    public function stockByProductoId($producto_id)
    {
        return $this->modelo
            ->with([
                ProductosModel::REL_TALLER,
                ProductosModel::REL_DESGLOSE_PRODUCTO,
                ProductosModel::REL_PRECIO,
                ProductosModel::REL_UBICACION
            ])
            ->find($producto_id);
    }

    public function stockProductos_back($respuesta)
    {
        $productos = $this->getAllProductos($respuesta);
        $listado = [];
        foreach ($productos as $producto) {
            $respuesta['producto_id'] = $producto->id;
            $compras = $this->servicioConsultaStock->getComprasStock($respuesta);
            $ventas = $this->servicioConsultaStock->getVentasProductoByIdProducto($respuesta);
            $producto['ventas'] = $ventas;
            $producto['compras'] = (array) $compras;

            if ($compras) {
                foreach ($producto['producto_almacen'] as $key2 => $producto_almacen) {
                    $sumVentas = 0;
                    $producto['producto_almacen'][$key2]['stock_almacen'] = $producto_almacen['cantidad'];
                    if (isset($ventas) && count($ventas) >= 1) {
                        $sumVentaAlmacen = 0;
                        foreach ($ventas as $venta) {
                            $sumVentas += (int) $venta['total_cantidad_venta'];
                            if ($producto_almacen['almacen_id'] == $venta['almacen_id']) {
                                $sumVentaAlmacen += (int) $venta['total_cantidad_venta'];
                                $producto['producto_almacen'][$key2]['stock_almacen'] = $producto_almacen['cantidad']
                                    - $sumVentaAlmacen;
                            }
                        }
                    }
                }
                $producto['stockActual'] = $producto['compras']['total_cantidad'] - $sumVentas;
                $producto['totalCompras'] = (int) $producto['compras']['total_cantidad'];
                $producto['totalVentas'] = $sumVentas;

                array_push($listado, $producto);
            }
        }
        return $listado;
    }
}
