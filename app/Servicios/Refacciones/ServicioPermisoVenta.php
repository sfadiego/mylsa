<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\permisoVentaModel;

class ServicioPermisoVenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'permiso venta';
        $this->modelo = new permisoVentaModel();
    }

    public function getReglasGuardar()
    {
        return [
            permisoVentaModel::USUARIO_ID => 'required',
            permisoVentaModel::VENTA_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            permisoVentaModel::USUARIO_ID => 'required',
            permisoVentaModel::VENTA_ID => 'required'
        ];
    }

    public function guardarVentaPermiso($parametros)
    {
        $this->crear([
            permisoVentaModel::USUARIO_ID => $parametros[permisoVentaModel::USUARIO_ID],
            permisoVentaModel::VENTA_ID => $parametros[permisoVentaModel::VENTA_ID]
        ]);
    }
}
