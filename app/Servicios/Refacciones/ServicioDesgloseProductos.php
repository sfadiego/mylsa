<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\DevolucionAlmacenModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioConsultaStock;
use App\Servicios\Refacciones\ServicioInventarioProductos;
use Illuminate\Support\Facades\DB;

class ServicioDesgloseProductos extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'stock productos';
        $this->modelo = new StockProductosModel();
        $this->modeloProductoModel = new ProductosModel();
        $this->modeloDevolucionAlmacenModel = new DevolucionAlmacenModel();
        $this->servicioConsultaStock = new ServicioConsultaStock();
        $this->servicioInventarioProductos = new ServicioInventarioProductos();
        
    }

    public function getReglasValidaProducto()
    {
        return [
            ProductosModel::ID => 'nullable|exists:producto,id',
        ];
    }

    public function getReglasGuardar()
    {
        return [
            StockProductosModel::PRODUCTO_ID => 'required|exists:producto,id',
            StockProductosModel::CANTIDAD_ACTUAL => 'required|numeric',
            StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => 'nullable|numeric',
            StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO => 'nullable|numeric',
            StockProductosModel::CANTIDAD_COMPRAS => 'required|numeric',
            StockProductosModel::CANTIDAD_VENTAS => 'nullable|numeric',
            StockProductosModel::TOTAL_PRECIO_COMPRAS => 'required|numeric',
            StockProductosModel::TOTAL_PRECIO_VENTAS => 'nullable|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            StockProductosModel::PRODUCTO_ID => 'required|exists:producto,id',
            StockProductosModel::CANTIDAD_ACTUAL => 'required|numeric',
            StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => 'nullable|numeric',
            StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO => 'nullable|numeric',
            StockProductosModel::CANTIDAD_COMPRAS => 'required|numeric',
            StockProductosModel::CANTIDAD_VENTAS => 'nullable|numeric',
            StockProductosModel::TOTAL_PRECIO_COMPRAS => 'required|numeric',
            StockProductosModel::TOTAL_PRECIO_VENTAS => 'nullable|numeric',
        ];
    }

    public function getStockByProducto($respuesta){
        ParametrosHttpValidador::validar_array([
            ProductosModel::ID => $respuesta['producto_id'] 
        ], $this->getReglasValidaProducto());
        
        return $this->modelo->where(StockProductosModel::PRODUCTO_ID, $respuesta['producto_id'])->first();
    }
    public function actualizaStockByProducto($respuesta)
    {
        ParametrosHttpValidador::validar_array([
            ProductosModel::ID => $respuesta['producto_id'] 
        ], $this->getReglasValidaProducto());
        $producto_id = $respuesta['producto_id'];
        $data = [
            'compra_realizada' => true,
            'producto_id' => $producto_id
        ];
        $compras = $this->servicioConsultaStock->getComprasStock($data);
        $ventas_realizadas = $this->servicioConsultaStock->getVentasTotalesProductoId($data);
        
        $devolucion_almacen_primario = $this->getCantidadDevolucionAlmacen($producto_id, 1);
        $devolucion_almacen_secundario = $this->getCantidadDevolucionAlmacen($producto_id, 2);

        $suma_devoluciones_almacenes = $devolucion_almacen_primario + $devolucion_almacen_secundario;

        $almacen_primario = $this->getCantidadAlmacen($producto_id, 1) - $devolucion_almacen_primario;
        $almacen_secundario = $this->getCantidadAlmacen($producto_id, 2) - $devolucion_almacen_secundario;

        // Son la suma de todas las orden compra del producto que esten en estatus realizada o factura pagada
        $producto[StockProductosModel::PRODUCTO_ID] = $producto_id;
        $producto[StockProductosModel::CANTIDAD_COMPRAS] = isset($compras) ? $compras->total_cantidad  : null;
        // Son la suma de todas las ventas del producto que esten en estatus realizada o factura pagada
        $producto[StockProductosModel::CANTIDAD_VENTAS] = $ventas_realizadas ? $ventas_realizadas->total_cantidad_venta : null;
        // Es una resta de lo que existe actualmente en inventario del producto
        $inventariofisico = $this->servicioInventarioProductos->getByProductoId($producto_id);
        $faltantes = isset($inventariofisico) && isset($inventariofisico->totalFaltantes) ? $inventariofisico->totalFaltantes : null;
        $sobrantes = isset($inventariofisico) && isset($inventariofisico->totalSobrantes) ? $inventariofisico->totalSobrantes : null;
        
        $cantidad_actual_inventario = ($producto[StockProductosModel::CANTIDAD_COMPRAS] + $sobrantes) - ($producto[StockProductosModel::CANTIDAD_VENTAS] + $suma_devoluciones_almacenes + $faltantes);
        $producto[StockProductosModel::CANTIDAD_ACTUAL] = $cantidad_actual_inventario;
        //Es la operación del inventario actual + la suma de lo que hay en almacen primario - los traspasos hecho al almacen secundario
        $producto[StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO] = $cantidad_actual_inventario + $almacen_primario - $almacen_secundario;
        $producto[StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO] = $almacen_secundario;
        // Suma de traspasos realizados al almacen secundario - los traspasos hecho del secundario al primario        
        $producto[StockProductosModel::TOTAL_PRECIO_COMPRAS] = isset($compras) ? $compras->total_precio  : null;
        $producto[StockProductosModel::TOTAL_PRECIO_VENTAS] = isset($ventas_realizadas) ? $ventas_realizadas->total_precio_venta  : null;
            
        $data = StockProductosModel::firstOrNew([StockProductosModel::PRODUCTO_ID => $producto_id] );
        $data->fill($producto);

        return $this->guardarModelo($data);
       
    }

    private function getCantidadAlmacen($producto_id, $almacen) {
        $query = $this->servicioConsultaStock->sumaProductosTraspasadosAlmacen($producto_id, $almacen);
        return $query ? $query->total_cantidad : null;
    }

    private function getCantidadDevolucionAlmacen($producto_id, $almacen_id) {
        $query =  $this->modeloDevolucionAlmacenModel
                    ->select(DB::raw('sum('.DevolucionAlmacenModel::CANTIDAD.') as total'))
                    ->where(DevolucionAlmacenModel::PRODUCTO_ID, $producto_id)
                    ->where(DevolucionAlmacenModel::ALMACEN_ID, $almacen_id)
                    ->first();
        return $query ? $query->total_cantidad : null;

    }
}
