<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class ServicioPedidoSugerido extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->modelo = new VentaProductoModel();
        $this->modeloProductos = new ProductosModel();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioManejoArchivos = new ServicioManejoArchivos();
        $this->servicioProductoPedidos = new ServicioProductoPedidos();
    }

    public function queryVentasPedidoSugerido($parametros)
    {
        $fecha_actual = date('Y-m-d');

        $data = $this->modelo->select(
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::TOTAL_VENTA,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ' as fecha_venta',
            ProductosModel::getTableName() . '.' . ProductosModel::VALOR_UNITARIO,
            ProductosModel::getTableName() . '.' . ProductosModel::PESO,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION . ' as nombre_producto',
            DB::raw('sum(' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CANTIDAD . ') as cantidad'),
            DB::raw('EXTRACT(YEAR FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ') as venta_anio'),
            DB::raw('EXTRACT(MONTH FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ') as venta_mes')
        )->join(
            ProductosModel::getTableName(),
            ProductosModel::getTableName() . '.' . ProductosModel::ID,
            '=',
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
        )
            ->join(
                ReVentasEstatusModel::getTableName(),
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::VENTA_ID,
                '=',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::ID
            )
            ->where(
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                EstatusVentaModel::ESTATUS_FINALIZADA
            )
            ->where(ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ACTIVO, TRUE);

        if (isset($parametros['sugerido_rapido']) && $parametros['sugerido_rapido']) {
            $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 5 month"));
            $data->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                $fecha_inicial . ' 00:00:00',
                $fecha_actual . ' 23:59:59'
            ]);
        }

        if (isset($parametros['sugerido_lento']) && $parametros['sugerido_lento']) {
            $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 5 month"));
            $data->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                $fecha_inicial . ' 00:00:00',
                $fecha_actual . ' 23:59:59'
            ]);
        }

        if (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            $data->where(ProductosModel::NO_IDENTIFICACION_FACTURA, $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]);
        }

        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $data->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }

        if (isset($parametros['numero_mes'])) {
            $data->where(
                DB::raw("MONTH(" . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ")"),
                $parametros['numero_mes']
            );
        }

        $data->groupBy(
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::TOTAL_VENTA,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
            ProductosModel::getTableName() . '.' . ProductosModel::ID,
            DB::raw('EXTRACT(YEAR FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ')'),
            DB::raw('EXTRACT(MONTH FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ')')
        );

        $data->orderBy('cantidad', 'asc');

        return $data->get();
    }

    public function getAllTipoProductosVenta()
    {
        $query = $this->modelo
            ->select(
                DB::raw("DISTINCT(" . VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID . ")"),
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION . ' as nombre_producto',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            )->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                '=',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            );

        return $query->get();
    }

    public function pedidosugeridorapido($item_id = '')
    {
        if ($item_id != '') {
            $final_array = [];
            $producto_data = $this->servicioProductos->getById($item_id);

            $producto_venta = $this->queryVentasPedidoSugerido([
                'sugerido_rapido' => true,
                VentaProductoModel::PRODUCTO_ID => $producto_data->id
            ])->toArray();

            $promedio_producto = $this->promedioProducto($producto_venta);

            if ($promedio_producto != null && $promedio_producto > 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto_data->no_identificacion,
                    'nombre_producto' => $producto_data->descripcion,
                    'producto_id' => $producto_data->id,
                    'promedio' => $promedio_producto,
                ]);
            }
            return $final_array;
        }

        $tipos_producto = $this->getAllTipoProductosVenta()->toArray();
        $final_array = [];
        foreach ($tipos_producto as $key => $producto) {
            $producto_id = $producto['producto_id'];
            $producto_venta = $this->queryVentasPedidoSugerido([
                'sugerido_rapido' => true,
                VentaProductoModel::PRODUCTO_ID => $producto_id
            ])->toArray();

            $promedio_producto = $this->promedioProducto($producto_venta);
            if ($promedio_producto != null && $promedio_producto > 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto['no_identificacion'],
                    'nombre_producto' => $producto['nombre_producto'],
                    'producto_id' => $producto['producto_id'],
                    'promedio' => $promedio_producto,
                ]);
            }
        }

        return $final_array;
    }

    public function promedioProducto($data_array)
    {

        $array_length = count($data_array);

        if ($array_length >= 4) {

            $total = 0;
            $venta_array = [];
            foreach ($data_array as $key => $item) {

                $total = $item['cantidad'] + $total;
                $venta_array[$item['venta_mes']]['cantidad'] = $item['cantidad'];
            }


            return $total / 5;
        }

        return 0;
    }

    public function promedioLento($data_array)
    {
        $array_length = count($data_array);
        if ($array_length > 0 && $array_length <= 3) {
            $total = 0;
            $venta_array = [];

            foreach ($data_array as $key => $item) {
                $total = $item['cantidad'] + $total;
                $venta_array[$item['venta_mes']]['cantidad'] = $item['cantidad'];
            }

            return round($total / 5);
        } else {
            return 0;
        }
    }


    public function calcularSugeridoLento($item_id = '')
    {
        if ($item_id != '') {
            $final_array = [];
            $producto_data = $this->servicioProductos->getById($item_id);
            $producto_venta = $this->queryVentasPedidoSugerido([
                VentaProductoModel::PRODUCTO_ID => $producto_data->id,
                'sugerido_lento' => true
            ])->toArray();

            $promedio_producto_lento = $this->promedioLento($producto_venta);
            if ($promedio_producto_lento != 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto_data->no_identificacion,
                    'nombre_producto' => $producto_data->descripcion,
                    'producto_id' => $producto_data->id,
                    'promedio' => $promedio_producto_lento
                ]);
            }

            return $final_array;
        }

        $tipos_producto = $this->getAllTipoProductosVenta()->toArray();
        $final_array = [];
        foreach ($tipos_producto as $key => $producto) {
            $producto_id = $producto['producto_id'];
            $producto_venta = $this->queryVentasPedidoSugerido([
                VentaProductoModel::PRODUCTO_ID => $producto_id,
                'sugerido_lento' => true
            ])->toArray();

            $promedio_producto_lento = $this->promedioLento($producto_venta);
            if ($promedio_producto_lento != 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto['no_identificacion'],
                    'nombre_producto' => $producto['nombre_producto'],
                    'producto_id' => $producto['producto_id'],
                    'promedio' => $promedio_producto_lento
                ]);
            }
        }

        return $final_array;
    }


    public function calcularSugeridoObsoleto($item_id = '')
    {
        $parametros = [];
        $parametros['obsoleto'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function buildQueryCategoriaC($parametros)
    {
        $query = $this->modeloProductos
            ->select(
                ProductosModel::getTableName() . '.' . ProductosModel::ID . ' as producto_id',
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION . ' as nombre_producto',
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA
            );
        $query->whereNotIn(ProductosModel::getTableName() . '.' . ProductosModel::ID, function ($query) use ($parametros) {
            $query->select(
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            )
                ->from(VentaProductoModel::getTableName());

            $fecha_actual = date('Y-m-d');
            if (isset($parametros['obsoleto']) && $parametros['obsoleto']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 1 year"));
                $query->whereBetween(
                    VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT,
                    [
                        $fecha_inicial . ' 00:00:00',
                        $fecha_actual . ' 23:59:59'
                    ]
                );
            }

            if (isset($parametros['estancado']) && $parametros['estancado']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 1 year"));
                $periodo = date("Y-m-d", strtotime($fecha_inicial . '+ 5 month'));
                $periodo_undia = date("Y-m-d", strtotime($periodo . '+ 1 day'));

                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $periodo_undia . ' 23:59:59'
                ]);
            }

            if (isset($parametros['potencial_obsoleto']) && $parametros['potencial_obsoleto']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 91 day"));

                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $fecha_actual . ' 23:59:59'
                ]);
            }

            if (isset($parametros['inactivo']) && $parametros['inactivo']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 3 month"));
                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $fecha_actual . ' 23:59:59'
                ]);
            }
        });


        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $query->where(ProductosModel::getTableName() . '.' . ProductosModel::ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }

        return $query->get();
    }

    public function calcularSugeridoInactivo($item_id = '')
    {
        $parametros = [];
        $parametros['inactivo'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }

        return $this->buildQueryCategoriaC($parametros);
    }

    public function calcularSugeridoPotencialObsoleto($item_id = '')
    {
        $parametros = [];
        $parametros['potencial_obsoleto'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function calcularSugeridoEstancado($item_id = '')
    {
        $parametros = [];
        $parametros['estancado'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function validarEstancado($data)
    {

        $new_data = $data[0];
        $cantidad = $new_data['cantidad'];
        return round($cantidad / 11);
    }

    public function categorizaporproducto($producto_id)
    {
        return [
            'rapido' => $this->pedidosugeridorapido($producto_id),
            'lento' => $this->calcularSugeridoLento($producto_id),
            'inactivo' => $this->calcularSugeridoInactivo($producto_id),
            'potencial_obsoleto' => $this->calcularSugeridoPotencialObsoleto($producto_id),
            // 'estancado' => $this->calcularSugeridoEstancado($producto_id),
            'obsoleto' => $this->calcularSugeridoObsoleto($producto_id)
        ];
    }

    public function dataproductopedidosugerido()
    {
        $datos_pedido_sugerido = $this->queryVentasPedidoSugerido([])->toArray();
        $datos_pedido = $this->servicioProductoPedidos->productospedido()->toArray();

        $merge_datos =  array_merge($datos_pedido, $datos_pedido_sugerido);
        $new_productos = [];
        $key_temp = [];
        $key_productos = [];

        foreach ($merge_datos as $item) {

            $proveedor_id = isset($item['proveedor_id']) ? $item['proveedor_id'] : "";
            $test_item = $item['producto_id'] . '_' . $proveedor_id;

            if (!in_array($test_item, $key_temp) && $proveedor_id !== "") {
                $key_productos[] = $item['producto_id'];
                $key_temp[] = $test_item;//$item['producto_id'] . '_' . $proveedor_id;
                $new_productos[] = $item;
            } else if (!in_array($item['producto_id'], $key_productos) && $proveedor_id == "") {
                $key_productos[] = $item['producto_id'];
                $new_productos[] = $item;
            }
        }

        return $new_productos;

        // return [
            // $new_productos,
            // $key_productos,
            // $key_temp
        // ];
    }

    public function generarlayoutpedido()
    {

        $data_productos = $this->dataproductopedidosugerido();
        $file_name = self::PEDIDO_SUGERIDO_FOLDER . DIRECTORY_SEPARATOR .  "embarque" . date('Y-m-d') . ".DAT";

        $this->servicioManejoArchivos->setDirectory(self::PEDIDO_SUGERIDO_FOLDER);
        $storage = Storage::disk('local');
        if (empty($data_productos)) {
            throw new ParametroHttpInvalidoException([
                'msg' => 'No hay productos en el pedido'
            ]);
        }
        foreach ($data_productos as $key => $item) {

            if (empty($item['id_enviodetalle'])) {
                throw new ParametroHttpInvalidoException([
                    'msg' => 'No se ha indicado el detalle de envio'
                ]);
            }

            if (isset($item['ma_pedido_id'])) {
                $row1 = $this->getRow1($item);
                $row2 = $this->getRow2($item);
                $row3 = $this->getRow3($item);
                $row4 = $this->getRow4($item);
                $row5 = $this->getRow5($item);
                $storage->prepend($file_name, $row5);
                $storage->prepend($file_name, $row4);
                $storage->prepend($file_name, $row3);
                $storage->prepend($file_name, $row2);
                $storage->prepend($file_name, $row1);
            }
        }

        return $file_name;
    }

    public function getRow1($data)
    {
        $tipo = '1';
        $razon_social = $this->getspaces($data['proveedor_nombre'], 40);
        $proveedor_rfc = $this->getspaces($data['proveedor_rfc'], 15);
        $pedido_id = $this->getspaces($data['ma_pedido_id'], 8);
        return $tipo . $razon_social . ' ' . date('d') . ' ' . date('m') . ' ' . date('Y') . ' ' . $proveedor_rfc . ' ' . $pedido_id;
    }

    public function getRow2($data)
    {
        $tipo = '2';
        $calle = $data['proveedor_calle'] . ' #' . $data['proveedor_numero'] . ', ' . $data['proveedor_colonia'] . ' ' . $data['localidad'];
        $estado =  $data['proveedor_estado'];
        $tipo_pago =  $data['tipo_pago'];

        return $tipo . $calle . ' ' . $estado . ' ' . $tipo_pago . '    ';
    }

    public function getRow3($data)
    {
        $tipo = '3';
        $ciudad = "San Juan del Río, Queretaro.";
        $pais =  "Mexico";
        $codigo_postal  =  "76800";
        $tipo_pedido = $data['tipo_pedido'];
        return $tipo . $ciudad . ' ' . $pais . ' ' . $codigo_postal . '    ' . $tipo_pedido;
    }

    public function getRow4($data)
    {
        $tipo = '4';
        $via_envio = $this->getspaces($data['via_envio'], 25);
        $dia_envio = $this->getspaces($data['dia_envio'], 9);
        $ruta = $this->getspaces($data['ruta'], 5);
        $lineas = $this->getspaces($data['lineas'], 4);
        $total_piezas = $this->getspaces($data['total_piezas'], 8);
        $peso_total = $this->getspaces($data['peso_total'], 6);
        $numero_remision = $this->getspaces($data['numero_remision'], 6);
        return $tipo . $via_envio . ' ' . $dia_envio . ' ' . $ruta . ' ' . $lineas . ' ' . $total_piezas . ' ' . $peso_total . ' ' . $numero_remision . '  ';
    }

    public function getRow5($data)
    {
        $tipo = '5';
        $prefijo = $this->getspaces($data['prefijo'], 8);
        $basico = $this->getspaces($data['basico'], 8);
        $sufijo = $this->getspaces($data['sufijo'], 6);
        $cantidad = $this->getspaces($data['cantidad_solicitada'], 7);
        $remision = $this->getspaces($data['ma_pedido_id'], 7);
        $aviso = $this->getspaces("", 2); // revisar
        $peso = $this->getspaces($data['peso'], 3);
        $precio_unitario = $this->getspaces($data['valor_unitario'], 8);
        $descripcion = $this->getspaces($data['descripcion'], 9);
        return $tipo . ' ' . $prefijo . ' ' . $basico . ' ' . $sufijo . ' ' . $prefijo . ' ' . $cantidad . ' ' . $remision . ' ' . $aviso . ' ' . $peso . ' ' . $precio_unitario . ' ' . $descripcion . '     ';
    }

    public function getspaces($texto, $cantidad)
    {
        $original_text = strlen($texto);
        if ($original_text < $cantidad) {
            return $texto . str_repeat(' ', $cantidad - $original_text);
        } else if ($original_text >= $cantidad) {
            return $texto . str_repeat(' ', abs($original_text - $cantidad));
        } else if ($original_text == $cantidad) {
            return $texto . str_repeat(' ', $cantidad);
        }
    }

    const PEDIDO_SUGERIDO_FOLDER = "embarque";
}
