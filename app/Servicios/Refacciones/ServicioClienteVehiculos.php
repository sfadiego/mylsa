<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ClienteVehiculosModel;
use App\Models\Refacciones\ClaveClienteModel;
use App\Models\Refacciones\ClientesModel;

class ServicioClienteVehiculos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'vehiculos';
        $this->modelo = new ClienteVehiculosModel();
        $this->modeloClientes = new ClientesModel();
        $this->modeloClave = new ClaveClienteModel();
    
        $this->servicioClave = new ServicioClaveCliente();
        $this->servicioCatalogoClave = new ServicioCatalogoClaveCliente();
        $this->servicioCatContactos = new ServicioContacto();
    }

    public function getReglasGuardar()
    {
        return [
            ClienteVehiculosModel::UNIDAD_DESCRIPCION => 'required',
            ClienteVehiculosModel::ULTIMO_KILOMETRAJE => 'required',
            ClienteVehiculosModel::MARCA_ID => 'required',
            ClienteVehiculosModel::MODELO_ID => 'required',
            ClienteVehiculosModel::ANIO_ID => 'required',
            ClienteVehiculosModel::COLOR_ID => 'required',
            ClienteVehiculosModel::CATALOGO_ID => 'required',
            ClienteVehiculosModel::N_MOTOR => 'nullable',
            ClienteVehiculosModel::MOTOR => 'nullable',
            ClienteVehiculosModel::TRANSMISION => 'required',
            ClienteVehiculosModel::COMBUSTIBLE => 'nullable',
            ClienteVehiculosModel::VIN => 'required',
            ClienteVehiculosModel::SERIE_CORTA => 'required',
            ClienteVehiculosModel::PLACAS => 'required',
            ClienteVehiculosModel::NUMERO_CILINDROS => 'nullable',
            ClienteVehiculosModel::CAPACIDAD => 'nullable',
            ClienteVehiculosModel::NUMERO_PUERTAS => 'nullable',
            ClienteVehiculosModel::NOTAS => 'nullable',
            ClienteVehiculosModel::CLIENTE_ID => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            ClienteVehiculosModel::UNIDAD_DESCRIPCION => 'required',
            ClienteVehiculosModel::ULTIMO_KILOMETRAJE => 'required',
            ClienteVehiculosModel::MARCA_ID => 'required',
            ClienteVehiculosModel::MODELO_ID => 'required',
            ClienteVehiculosModel::ANIO_ID => 'required',
            ClienteVehiculosModel::COLOR_ID => 'required',
            ClienteVehiculosModel::CATALOGO_ID => 'required',
            ClienteVehiculosModel::N_MOTOR => 'nullable',
            ClienteVehiculosModel::MOTOR => 'nullable',
            ClienteVehiculosModel::TRANSMISION => 'required',
            ClienteVehiculosModel::COMBUSTIBLE => 'nullable',
            ClienteVehiculosModel::VIN => 'required',
            ClienteVehiculosModel::SERIE_CORTA => 'required',
            ClienteVehiculosModel::PLACAS => 'required',
            ClienteVehiculosModel::NUMERO_CILINDROS => 'nullable',
            ClienteVehiculosModel::CAPACIDAD => 'nullable',
            ClienteVehiculosModel::NUMERO_PUERTAS => 'nullable',
            ClienteVehiculosModel::NOTAS => 'nullable',
            ClienteVehiculosModel::CLIENTE_ID => 'required'
        ];
    }

    public function vehiculoByClienteId($cliente_id)
    {
        $consulta = $this->modelo->where(ClienteVehiculosModel::getTableName() . '.' . ClienteVehiculosModel::CLIENTE_ID, $cliente_id)->limit(200);
        
        return $consulta->get();
    }

    public function getReglasBusquedaSerie()
    {
         return [
            ClienteVehiculosModel::PLACAS  => 'nullable'
        ];
    }

    public function searchVehiculoCliente($parametros)
    {
        $query = $this->modelo->select(ClienteVehiculosModel::CLIENTE_ID)
            ->orWhere(ClienteVehiculosModel::VIN,'=', $parametros[ClienteVehiculosModel::PLACAS])
            ->orWhere(ClienteVehiculosModel::SERIE_CORTA,'=', $parametros[ClienteVehiculosModel::PLACAS])
            ->orWhere(ClienteVehiculosModel::PLACAS,'=', $parametros[ClienteVehiculosModel::PLACAS])
            ->first();

        // dd($query->toArray(), $query->cliente_id);
        if(!is_null($query)){
            $data = $this->modeloClientes->where(ClientesModel::ID, '=', $query->cliente_id)->get();
            foreach ($data as $key => $item) {
                $data[$key]['contactos'] = isset($item[ClientesModel::ID]) ? $this->servicioCatContactos->contactosByClienteId($item[ClientesModel::ID]) : [];
    
                if (count($data[$key]['contactos']) == 0) {
                    $data[$key]['contactos'] = array(
                        'contacto_nombre' => $item[ClientesModel::NOMBRE],
                        'contacto_apellido_paterno' => $item[ClientesModel::APELLIDO_MATERNO],
                        'contacto_apellido_materno' => $item[ClientesModel::APELLIDO_PATERNO],
                        'contacto_telefono' => $item[ClientesModel::TELEFONO],
                        'contacto_correo' => $item[ClientesModel::CORREO_ELECTRONICO]
                    );
                }
    
                $data[$key]["vehiculo"] = isset($item[ClientesModel::ID]) ?  $this->getVehiculosCliente($item[ClientesModel::ID]) : [];
            }
    
            return $data;
        }
        return [];
    } 

    public function getVehiculosCliente($id_cliente = 0)
    {
        $vehiculo_info = $this->modelo->select(
            'cat_vehiculos_cliente.kilometraje',
            'catalogo_anio.nombre AS anio',
            'catalogo_colores.nombre AS color',
            'catalogo_marcas.nombre AS marca',
            'catalogo_autos.nombre AS modelo',
            'catalogo_colores.nombre AS modelo',
            'cat_vehiculos_cliente.n_motor',
            'cat_vehiculos_cliente.motor',
            'cat_vehiculos_cliente.transmision',
            'cat_vehiculos_cliente.vin',
            'cat_vehiculos_cliente.placas',
            'cat_vehiculos_cliente.numero_cilindros',
            'cat_vehiculos_cliente.marca_id',
            'cat_vehiculos_cliente.anio_id',
            'cat_vehiculos_cliente.color_id',
            'cat_vehiculos_cliente.catalogo_id AS modelo_id'
        )

        ->join('catalogo_marcas', 'catalogo_marcas.id', '=', 'cat_vehiculos_cliente.marca_id')
        ->join('catalogo_anio', 'catalogo_anio.id', '=', 'cat_vehiculos_cliente.anio_id')
        ->join('catalogo_colores', 'catalogo_colores.id', '=', 'cat_vehiculos_cliente.color_id')
        ->join('catalogo_autos', 'catalogo_autos.id', '=', 'cat_vehiculos_cliente.catalogo_id')

        ->where('cat_vehiculos_cliente.cliente_id', '=', $id_cliente)
        ->get();

        return $vehiculo_info;
    }

    public function getInfoVehiculosCliente($id_cliente = 0)
    {
        $query = $this->modelo->select(
            'clientes.numero_cliente',
            'clientes.id'
        )
        ->join('clientes', 'clientes.id', '=', 'cat_vehiculos_cliente.cliente_id')
        ->where('cat_vehiculos_cliente.cliente_id', '=', $id_cliente)
        ->get();

        return $query; 
    }
}
