<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Core\ServicioDB;
use Illuminate\Support\Facades\DB;

class ServicioVentasServicios extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas servicio';
        $this->modelo = new VentaServicioModel();
    }

    public function getReglasGuardar()
    {
        return [
            VentaServicioModel::VENTA_ID => 'required|exists:ventas,id',
            VentaServicioModel::NOMBRE_SERVICIO => 'required',
            VentaServicioModel::PRECIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentaServicioModel::VENTA_ID => 'nullable|exists:ventas,id',
            VentaServicioModel::NOMBRE_SERVICIO => 'nullable',
            VentaServicioModel::PRECIO => 'nullable'
        ];
    }

    public function createUpdateServicioVenta($parametros)
    {
        return $this->modelo->updateOrInsert($parametros);
    }

    public function totalservicio($venta_id)
    {
        $ventas_servicio = $this->modelo->getTable();
        $query = $this->modelo->select(
            DB::raw('SUM(' . $ventas_servicio . '.' . VentaServicioModel::PRECIO . ') as total_costo')
        )->from($ventas_servicio)
            ->where($ventas_servicio . '.' . VentaServicioModel::VENTA_ID, $venta_id);

        return $query->count() > 0 ? $query->first()->total_costo : 0;
    }

    ##test
    public function ventaservicio($venta_id)
    {
        $tabla_venta_servicio = VentaServicioModel::getTableName();
        $tabla_venta = VentasRealizadasModel::getTableName();
        $tabla_re_venta_estatus = ReVentasEstatusModel::getTableName();
        return DB::table($tabla_venta)
            ->join($tabla_venta_servicio, $tabla_venta_servicio . '.' . VentaServicioModel::VENTA_ID, '=',  $tabla_venta . '.' . VentasRealizadasModel::ID)
            ->join($tabla_re_venta_estatus, $tabla_re_venta_estatus . '.' . ReVentasEstatusModel::VENTA_ID, '=',  $tabla_venta . '.' . VentasRealizadasModel::ID)
            ->where($tabla_re_venta_estatus . '.' . ReVentasEstatusModel::ACTIVO, true)
            ->where($tabla_venta . '.' . VentasRealizadasModel::ID, $venta_id)
            ->select(
                $tabla_venta_servicio . '.' . VentaServicioModel::ID,
                $tabla_venta_servicio . '.' . VentaServicioModel::VENTA_ID,
                $tabla_venta_servicio . '.' . VentaServicioModel::NO_IDENTIFICACION,
                $tabla_venta_servicio . '.' . VentaServicioModel::NOMBRE_SERVICIO . ' as descripcion_producto',
                $tabla_venta_servicio . '.' . VentaServicioModel::PRECIO . ' as valor_unitario',
                $tabla_venta_servicio . '.' . VentaServicioModel::CANTIDAD,
                $tabla_venta_servicio . '.' . VentaServicioModel::COSTO_MO,
                $tabla_venta_servicio . '.' . VentaServicioModel::IVA,
                $tabla_re_venta_estatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                DB::raw($tabla_venta_servicio . '.' . VentaServicioModel::CANTIDAD . ' * ' . $tabla_venta_servicio . '.' . VentaServicioModel::PRECIO . ' as venta_total')
            )
            ->get();
    }
}
