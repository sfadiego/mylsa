<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Autos\ServicioHistoricoProductosServicio;
use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\permisoVentaModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\ReVentasVentanillaTallerModel;
use App\Servicios\Refacciones\ServicioCurl;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\VentaServicioModel;
use Carbon\Carbon;

class ServicioVentasRealizadas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->servicioFolio = new ServicioFolios();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->servicioReVentaVentanillaTaller = new ServicioReVentaVentanillaTaller();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioProductos = new ServicioProductos();
        $this->modelo = new VentasRealizadasModel();
        $this->modeloEstatusCompra = new EstatusCompra();
        $this->modeloCliente = new ClientesModel();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
        $this->modeloFolio = new FoliosModel();
        $this->servicioCuentaxCobrar = new ServicioCuentaPorCobrar();
        $this->servicio_abonos = new ServicioAbono();
        $this->servicioHistoricoProductosServicio = new ServicioHistoricoProductosServicio();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioVentasServicios = new ServicioVentasServicios();
    }

    public function getReglasGuardar()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::CLIENTE_ID => 'exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric'
        ];
    }

    public function getReglasGuardarSinFolio()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::REL_PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentaProductoModel::VALOR_UNITARIO => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric|exists:cat_tipo_precio,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric'
        ];
    }

    public function getReglasFinalizaVentaContado()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id'
        ];
    }

    public function getReglasFinalizaVentaCredito()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'required|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required'
        ];
    }
    public function getReglasVentasAgrupadasMes()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::CANTIDAD_MESES => 'nullable|numeric',
            VentasRealizadasModel::YEAR => 'required|numeric'

        ];
    }

    public function registrarVenta($parametros)
    {
        $folio = $this->servicioFolio->generarFolio(CatalogoCuentasModel::CAT_VENTAS);
        $parametros[VentasRealizadasModel::FOLIO_ID] = $folio->id;
        $parametros[VentasRealizadasModel::VENTA_TOTAL] = $parametros[VentaProductoModel::VALOR_UNITARIO] * $parametros[VentaProductoModel::CANTIDAD];
        $venta = $this->crear($parametros);
        if ($venta) {
            $re_estastus[ReVentasEstatusModel::VENTA_ID] = $venta->id;
            $re_estastus[ReVentasEstatusModel::ESTATUS_VENTA_ID] = EstatusVentaModel::ESTATUS_PROCESO;
            $this->servicioReVentasEstatus->storeReVentasEstatus($re_estastus);

            $parametros[VentasRealizadasModel::VENTA_ID] = $venta->id;
            $this->servicioVentaProducto->crear($parametros);
        }
        return isset($venta) && $venta ? $venta : null;
    }

    public function getReglasCrearVentaMpm()
    {
        return [
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric|exists:cat_tipo_precio,id',
            ReVentasVentanillaTallerModel::NUMERO_ORDEN => 'required|numeric|unique:' . VentasRealizadasModel::getTableName()
        ];
    }

    public function listaProductosTaller($numero_orden)
    {
        return $this->curlGetDataApiMpm($numero_orden);
    }

    public function crearVentaMpm($parametros)
    {
        #TODO: Verificar funcionalidad
        $existe_venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);
        if (!$existe_venta) {
            $folio = $this->servicioFolio->generarFolio(CatalogoCuentasModel::CAT_COMPRAS);
            $parametros[VentasRealizadasModel::FOLIO_ID] = $folio->id;
            $parametros[VentasRealizadasModel::VENTA_TOTAL] = 0;
            $parametros[VentasRealizadasModel::NUMERO_ORDEN];
            $venta = $this->crear($parametros);

            if (!$venta) {
                DB::rollback();
                return null;
            }
        } else {
            $folio = $this->servicioFolio->getById($existe_venta->folio_id);
            $venta = $existe_venta;
        }

        $re_estastus[ReVentasEstatusModel::VENTA_ID] = $venta->id;
        $re_estastus[ReVentasEstatusModel::ESTATUS_VENTA_ID] = EstatusVentaModel::ESTATUS_PROCESO;
        $this->servicioReVentasEstatus->storeReVentasEstatus($re_estastus);

        $productos_taller = $this->curlGetDataApiMpm($parametros[ReVentasVentanillaTallerModel::NUMERO_ORDEN]);
        $this->guardarDetalleProductoVentaMpm($productos_taller, [
            VentaProductoModel::PRECIO_ID =>  $parametros[VentaProductoModel::PRECIO_ID],
            VentaProductoModel::CLIENTE_ID => $parametros[VentaProductoModel::CLIENTE_ID],
            VentaProductoModel::FOLIO_ID => $folio->id,
            VentaProductoModel::VENTA_ID => $venta->id
        ]);

        $total_productos_venta = $this->servicioVentaProducto->totalVentaByFolio($folio->id);
        $totalservicio = $this->servicioVentasServicios->totalservicio($venta->id);
        $total_venta =  $total_productos_venta + $totalservicio;

        $this->massUpdateWhereId(VentasRealizadasModel::ID, $venta->id, [
            VentasRealizadasModel::VENTA_TOTAL => $total_venta
        ]);

        return $venta;
    }

    public function guardarDetalleProductoVentaMpm($data_array, $parametros_venta)
    {
        foreach ($data_array as $key => $item) {
            // dd($item);
            if ($item->tipo == "OPERACION") {
                $precio_total = (float)$item->precio_unitario + (float)$item->costo_mo;
                return $this->servicioVentasServicios->createUpdateServicioVenta([
                    VentaServicioModel::NOMBRE_SERVICIO => $item->descripcion,
                    VentaProductoModel::VENTA_ID =>  $parametros_venta[VentaProductoModel::VENTA_ID],
                    VentaServicioModel::PRECIO => $precio_total,
                    VentaServicioModel::NO_IDENTIFICACION => $item->num_pieza,
                    VentaServicioModel::CANTIDAD => (int) $item->cantidad,
                    VentaServicioModel::IVA => $item->iva,
                    VentaServicioModel::COSTO_MO =>  $item->costo_mo
                ]);
            } else {
                $existe_producto = $this->servicioProductos->validarExistenciaProducto([
                    'No_identificacion' => $item->num_pieza,
                    'Descripcion' => $item->descripcion
                ])->first();

                ##TODO: validar redondeo
                if (isset($existe_producto)) {
                    $productos_venta = $this->servicioVentaProducto->existeProducto([
                        VentaProductoModel::FOLIO_ID => $parametros_venta[VentaProductoModel::FOLIO_ID],
                        VentaProductoModel::VENTA_ID => $parametros_venta[VentaProductoModel::VENTA_ID],
                        VentaProductoModel::PRODUCTO_ID =>  $existe_producto->producto_id
                    ]);

                    #si no existe en la venta se agrega
                    if (empty($productos_venta)) {
                        ##TODO: validar precios mano de obra y sumar en venta total
                        // $precio_total = (float)$item->precio_unitario + (float)$item->costo_mo;

                        $parametros_venta[VentaProductoModel::PRODUCTO_ID] = $existe_producto->producto_id;
                        $parametros_venta[VentaProductoModel::VALOR_UNITARIO] = $existe_producto->valor_unitario;
                        $parametros_venta[VentaProductoModel::CANTIDAD] = round($item->cantidad);
                        $parametros_venta[VentaProductoModel::TOTAL_VENTA] = $existe_producto->valor_unitario * $item->cantidad;
                        $parametros_venta[VentaProductoModel::DESCONTADO_ALMACEN] = 1;
                        $this->servicioVentaProducto->crear($parametros_venta);
                    }
                } else {
                    DB::rollback();
                    throw new ParametroHttpInvalidoException([
                        'validacion_msg' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => ""])
                    ]);
                }
            }
        }
    }

    public function curlGetDataApiMpm($numero_orden)
    {
        //DMS - https://sohex.net/queretaro_citas_dms/citas/dms_panles_queretaro/conexion/Conexion_DMS/recuperar_operaciones
        //MPM - https://so-hex.com/cs/mpm/matriz_mpm/orden/mpm_matriz/api/Conexion_DMS/recuperar_operaciones

        $request = $this->servicioCurl->curlPost('https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion_DMS/recuperar_operaciones', [
            'id_cita' => $numero_orden
        ], true);


        if (empty($request)) {
            throw new ParametroHttpInvalidoException([
                'validacion_msg' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => ""])
            ]);
        }
        return $this->validarPiezasToMpm($request);
    }

    public function validarPiezasToMpm($request)
    {
        $new_array = [];
        $data_api =  json_decode($request);
        $validar = count($data_api) > 0 ? $data_api : [];

        foreach ($validar as $key => $value) {
            if ($value->num_pieza !== "") {
                $existe_producto = $this->servicioProductos->validarExistenciaProducto([
                    'No_identificacion' => $value->num_pieza,
                    'Descripcion' => $value->descripcion
                ]);

                $cantidad_stock = $this->servicioProductos->stockProductos([
                    // StockProductosModel::PRODUCTO_ID =>  $value->num_pieza
                    'no_identificacion' => $value->num_pieza
                ])->first();

                $item = new \stdClass;
                $item->id = $value->id;
                $item->producto_id = isset($cantidad_stock->id) ? $cantidad_stock->id : null;
                $item->se_registro = count($existe_producto) > 0 ? true : false;
                $item->cantidad = $value->cantidad;
                $item->cantidad_stock = isset($cantidad_stock->cantidad_actual) ? $cantidad_stock->cantidad_actual : 0;
                $item->descripcion = $value->descripcion;
                $item->num_pieza = $value->num_pieza;
                $item->existe = $value->existe;
                $item->precio_unitario = $value->precio_unitario;
                $item->total_horas = $value->total_horas;
                $item->costo_mo = $value->costo_mo;
                $item->iva = $value->costo_iva;
                $item->tipo = $value->tipo;
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function ventasByFolioId($folio_id)
    {
        return $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO,
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_ESTATUS . '.' . ReVentasEstatusModel::REL_ESTATUS,
            VentasRealizadasModel::REL_PRECIO,
            VentasRealizadasModel::REL_ALMACEN => function ($query) {
                $query->select(
                    Almacenes::ID,
                    Almacenes::NOMBRE,
                    Almacenes::CODIGO_ALMACEN
                );
            }
        ])->where(VentasRealizadasModel::FOLIO_ID, $folio_id)->get();
    }

    public function finalizarVenta($id, $request)
    {
        DB::beginTransaction();

        $modelo = $this->modelo->findOrFail($id);
        try {
            $data = $request->all();
            $modelo->fill($data);
            $modelo = $this->guardarModelo($modelo);
            $cxc = $this->procesarCuentasPorCobrar($request->all());
            DB::commit();
            return $cxc;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }
    }

    public function procesarCuentasPorCobrar($parametros)
    {
        $importe = $parametros[VentasRealizadasModel::VENTA_TOTAL];

        if ($parametros['tipo_forma_pago_id'] == 2) {
            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $plazo_credito = $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID];
            $enganche = $parametros[CuentasPorCobrarModel::ENGANCHE];
            $totalsinenganche = $importe - $enganche;
            $cantidad_abonos = $this->modelo_plazo_credito->find($plazo_credito)->cantidad_mes;
            $abono_mensual = ($totalsinenganche / $cantidad_abonos);
            $interes_mensual = ($abono_mensual * $tasa_interes) / 100;
            $intereses = $interes_mensual * $cantidad_abonos;
            $total = $importe + $intereses;
        } else {
            $total = $parametros[VentasRealizadasModel::VENTA_TOTAL];
            $intereses = 0;
        }
        $params = [
            CuentasPorCobrarModel::FOLIO_ID => $parametros[CuentasPorCobrarModel::FOLIO_ID],
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[CuentasPorCobrarModel::CLIENTE_ID],
            CuentasPorCobrarModel::CONCEPTO => $parametros[CuentasPorCobrarModel::CONCEPTO],
            CuentasPorCobrarModel::IMPORTE => $importe,
            CuentasPorCobrarModel::TOTAL => $total,
            CuentasPorCobrarModel::FECHA => date('Y-m-d'),
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::ENGANCHE => $parametros[CuentasPorCobrarModel::ENGANCHE],
            CuentasPorCobrarModel::TASA_INTERES => $parametros[CuentasPorCobrarModel::TASA_INTERES],
            CuentasPorCobrarModel::INTERESES => $intereses,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_PROCESO,
            CuentasPorCobrarModel::USUARIO_GESTOR_ID =>  $parametros[CuentasPorCobrarModel::USUARIO_GESTOR_ID]
        ];
        $data = $this->servicioCuentaxCobrar->crear($params);

        $abonos = $this->servicio_abonos->crearAbonosByCuentaId($data->id);
        if (!$abonos) {
            return false;
        }
        return $data;
    }

    public function getAll()
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO,
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {
                $query->with([
                    'estatus_venta'
                ]);
            }
        ])->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function reglasListaVentaByStatus()
    {
        return [
            VentasRealizadasModel::ESTATUS_COMPRA => 'required|array',
            VentasRealizadasModel::CLIENTE_ID => 'nullable'
        ];
    }

    //TODO: listado ventas realizadas, separar devoluciones
    public function getVentasByStatus($parametros)
    {

        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tabla_clientes = ClientesModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tabla_clientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tabla_clientes . '.' . ClientesModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'folios.folio',
                'estatus_venta.nombre as estatus_venta',
                'clientes.nombre as nombre_cliente',
                'clientes.id as cliente_id',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta'
            );

        if (isset($parametros[VentasRealizadasModel::CLIENTE_ID])) {
            $query->where($tabla_clientes . '.' . ClientesModel::ID, $parametros[VentasRealizadasModel::CLIENTE_ID]);
        }

        $query->where(ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableEstatusVenta . '.' . EstatusVentaModel::ID, $parametros[VentasRealizadasModel::ESTATUS_COMPRA]);
        return $query->get();
    }

    public function getById($id)
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO,
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {
                $query->with([
                    'estatus_venta'
                ]);
            }
        ])
            ->where(VentasRealizadasModel::ID, $id)
            ->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function updateStatusVenta($venta_id, $status_id)
    {
        $modelo = $this->modelo->find($venta_id);
        $modelo->estatus_id = $status_id;
        return $modelo->save();
    }

    public function getReglasBusqueda()
    {
        return [
            VentasRealizadasModel::FECHA_INICIO => 'required|date',
            VentasRealizadasModel::FECHA_FIN => 'required|date',
        ];
    }

    public function getReglasBusquedaTraspaso()
    {
        return [
            Traspasos::FECHA_INICIO => 'nullable|date',
            Traspasos::FECHA_FIN => 'nullable|date',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
        ];
    }

    public function calcularTotal($modelo)
    {
        $ventas =  $modelo;
        $total = 0;
        foreach ($ventas as $key => $item) {
            $total = $total + $item->venta_total;
        }

        return [
            'total_venta' => $total,
            'data' => $modelo
        ];
    }

    public function filtrarPorFechas($parametros)
    {
        return $this->modelo->with([
            'folio' => function ($query) {
                $query->select('id', 'folio');
            },
            'estatus' => function ($query) {
                $query->select('id', 'nombre');
            },
            'cliente' => function ($query) {
                $query->select('id', 'nombre_empresa', 'numero_cliente', 'rfc');
            },
            'detalle_venta' => function ($query) {
                $query->with([
                    'producto' => function ($query_prod) {
                        $query_prod->select('id', 'no_identificacion', 'descripcion', 'valor_unitario');
                    }
                ]);
            }
        ])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '>=', $parametros[VentasRealizadasModel::FECHA_INICIO])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '<=', $parametros[VentasRealizadasModel::FECHA_FIN])
            ->get();
    }

    public function getVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableProducto, $tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'ventas.almacen_id',
                'folios.folio',
                'venta_producto.cantidad',
                'venta_producto.venta_total as venta_total_producto',
                'venta_producto.valor_unitario',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.unidad',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta',
                $tableEstatusVenta . '.' . EstatusVentaModel::ID . ' as estatusId'
            );
        if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
            $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA))
            ->get();
        return [
            'data' => $query->get()
        ];
    }

    public function getTotalesVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                DB::raw('count(ventas.id) as total_ventas'),
                DB::raw('sum(venta_producto.cantidad) as total_cantidad'),
                DB::raw('sum(venta_producto.venta_total) as sum_venta_total'),
                DB::raw('sum(venta_producto.valor_unitario) as sum_valor_unitario')
            );
        if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
            $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA));
        return $query->first();
    }

    public function getBusquedaVentas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::ESTATUS_DEVOLUCION));
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    public function getAllVentas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableCxc = CuentasPorCobrarModel::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->leftJoin($tableCxc, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableCxc . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));

        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    public function getBusquedaVentasDevoluciones($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableDevolucionVenta = DevolucionVentasModel::getTableName();
        $query = $this->modelo
            ->join($tableDevolucionVenta, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableDevolucionVenta . '.' . DevolucionVentasModel::VENTA_ID)
            ->join($tableFolios, $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::ESTATUS_DEVOLUCION));
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    //TODO: Ventas
    public function reglasAutorizarPrecio()
    {
        return [
            permisoVentaModel::USUARIO_ID => 'required',
            // permisoVentaModel::ven =>'required',
        ];
    }

    public function finalizarVentaServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);
        if (empty($venta)) {
            throw new ParametroHttpInvalidoException([
                'msg' => "No se encontro la orden"
            ]);
        } else if (isset($venta) && $venta->orden_cerrada == 1) {
            throw new ParametroHttpInvalidoException([
                'msg' => "Orden cerrada"
            ]);
        }

        $total_venta = $this->servicioVentaProducto->totalVentaByFolio($venta->folio_id);
        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => true]);
        $modelo = $this->guardarModelo($modelo);
        $this->procesarCuentasPorCobrar([
            VentasRealizadasModel::VENTA_TOTAL => $total_venta,
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::FOLIO_ID => $venta->folio_id,
            CuentasPorCobrarModel::CLIENTE_ID => $venta->cliente_id,
            CuentasPorCobrarModel::CONCEPTO => "CIERRE DE ORDEN",
            CuentasPorCobrarModel::TIPO_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => PlazoCreditoModel::UNA_EXHIBICION,
            CuentasPorCobrarModel::ENGANCHE => null,
            CuentasPorCobrarModel::TASA_INTERES => null,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_LIQUIDADO
        ]);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_FINALIZADA
        ]);

        $this->buscarProductosYdescontarStockByFolio($venta->folio_id);
        DB::commit();
        return $venta;
    }

    public function abrirOrdenServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);

        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => false]);
        $modelo = $this->guardarModelo($modelo);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_PROCESO
        ]);
    }

    public function buscarProductosYdescontarStockByFolio($folio_id)
    {
        $productos_venta = $this->servicioVentaProducto->getDataByFolio($folio_id);
        #validar que si son los mismos pero cantidad diferente
        foreach ($productos_venta as $key => $item) {
            if (empty($item->descontado_almacen)) {
                #descontar
                $this->servicioDesgloseProductos->actualizaStockByProducto([
                    VentaProductoModel::PRODUCTO_ID => $item->producto_id
                ]);

                #actualizar que ya se desconto
                $this->servicioVentaProducto->massUpdateWhereId(VentaProductoModel::ID, $item->id, [
                    VentaProductoModel::DESCONTADO_ALMACEN => 1
                ]);
            }
        }
    }

    public function buscarPorNumeroOrden($numero_orden)
    {
        return $this->modelo->where(VentasRealizadasModel::NUMERO_ORDEN, $numero_orden)->first();
    }

    public function getYearVentasByProducto($params)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $fecha_actual = date('Y-m-d');
        //DB::connection()->enableQueryLog();
        $query = DB::table($tableVentas)
            ->join($tableVentaProducto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::VENTA_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->select(
                DB::raw('EXTRACT(YEAR FROM ' . $tableVentas . '.' . VentasRealizadasModel::CREATED_AT . ') as anio')
            );
        if (isset($params[VentaProductoModel::PRODUCTO_ID]) && $params[VentaProductoModel::PRODUCTO_ID]) {
            $query->where($tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, $params[VentaProductoModel::PRODUCTO_ID]);
        }
        if (isset($params[VentasRealizadasModel::YEAR]) && $params[VentasRealizadasModel::YEAR]) {
            $query->whereYear($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, $params[VentasRealizadasModel::YEAR]);
        }
        // if (isset($params[VentasRealizadasModel::CANTIDAD_MESES]) && $params[VentasRealizadasModel::CANTIDAD_MESES]) {
        //     $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
        //     $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
        //         $fecha_inicial . ' 00:00:00',
        //         $fecha_actual . ' 23:59:59'
        //     ]);
        // }

        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA));
        $query->groupBy(
            'producto_id',
            'anio'
        );
        return $query->get();
        //dd(DB::getQueryLog());
    }

    public function getVentasAgrupadasMes($params)
    {
        $where = '';
        $where_mes = '';

        $fecha_actual = date('Y-m-d');
        $mes_actual = Carbon::parse($fecha_actual)->format('m');
        $fecha_actual = date('Y-m-d H:i:s', strtotime($fecha_actual . ' +1 day'));

        if (isset($params[VentasRealizadasModel::CANTIDAD_MESES])) {

            if ($params[VentasRealizadasModel::CANTIDAD_MESES] > $mes_actual) {
                throw new ParametroHttpInvalidoException([
                    'msg' => "No puedes sobrepasar la cantidad máxima al mes inicial"
                ]);
            }

            $fecha_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = $mes_actual - $params[VentasRealizadasModel::CANTIDAD_MESES] + 1;
            $where_mes .= ($fecha_inicio) ? " where cat_meses.id between '{$mes_inicio}' and '{$mes_actual}'" : "";
            $where .= ($fecha_inicio) ? " and venta_producto.created_at between '{$fecha_inicio}' and '{$fecha_actual}'" : "";
        }
        return DB::select(
            DB::raw(
                'SELECT cat_meses.nombre as mes, T1.total_ventas
                FROM cat_meses
                LEFT JOIN(
                SELECT
                    SUM (venta_producto.cantidad) AS total_ventas,
                    EXTRACT (MONTH FROM ventas.created_at) AS mes
                FROM
                    "ventas"
                INNER JOIN "venta_producto" ON "ventas"."id" = "venta_producto"."venta_id"
                INNER JOIN "re_ventas_estatus" ON "re_ventas_estatus"."ventas_id" = "ventas"."id"
                WHERE
                    "venta_producto"."producto_id" = ' . $params[VentaProductoModel::PRODUCTO_ID] . '
                AND "re_ventas_estatus"."activo" = true
                AND "re_ventas_estatus"."estatus_ventas_id" IN (' . EstatusVentaModel::ESTATUS_FINALIZADA . ') AND EXTRACT (YEAR FROM venta_producto.created_at) = ' . $params[VentasRealizadasModel::YEAR] . '
                ' . $where . '
                GROUP BY
                    "producto_id",
                    "mes"
                ) T1 ON T1.mes::integer  = cat_meses.id::integer'
                    . $where_mes
            )
        );
    }
}
