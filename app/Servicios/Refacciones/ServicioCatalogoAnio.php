<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CatalogoAnioModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatalogoAnio extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'anio';
        $this->modelo = new CatalogoAnioModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoAnioModel::NOMBRE => 'required'
        ];
    }
    
    public function getReglasUpdate()
    {
        return [
            CatalogoAnioModel::NOMBRE => 'required'
        ];
    }

    public function getReglasBusquedaAnio()
    {
        return [
            CatalogoAnioModel::NOMBRE => 'required'
        ];
    }

    public function searchIdanio($request)
    {
        $data = $this->modelo->where(CatalogoAnioModel::NOMBRE,$request->get(CatalogoAnioModel::NOMBRE))
            ->select(CatalogoAnioModel::ID)->limit(1)->get();
        
        return $data;
    }
}
