<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ProveedorRefacciones;

class ServicioProveedor extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'proveedor';
        $this->modelo = new ProveedorRefacciones();
        $this->servicioCatalogoCfdi = new ServicioCatalogoCfdi();
        $this->servicioCatTipoPago = new ServicioCatTipoPago();
    }

    public function getReglasGuardar()
    {
        return [
            ProveedorRefacciones::PROVEEDOR_NOMBRE => 'required',
            ProveedorRefacciones::PROVEEDOR_RFC => 'required',
            ProveedorRefacciones::PROVEEDOR_CALLE => 'required',
            ProveedorRefacciones::PROVEEDOR_NUMERO => 'required',
            ProveedorRefacciones::PROVEEDOR_COLONIA => 'required',
            ProveedorRefacciones::PROVEEDOR_ESTADO => 'required',
            ProveedorRefacciones::CLAVE_IDENTIFICADOR => 'required',
            ProveedorRefacciones::RAZON_SOCIAL => 'required',
            ProveedorRefacciones::APELLIDO_MATERNO => 'nullable',
            ProveedorRefacciones::APELLIDO_PATERNO => 'nullable',
            ProveedorRefacciones::PROVEEDOR_PAIS => 'nullable',
            ProveedorRefacciones::CORREO => 'required|email',
            ProveedorRefacciones::CORREO_SECUNDARIO => 'nullable|email',
            ProveedorRefacciones::TELEFONO_SECUNDARIO => 'nullable',
            ProveedorRefacciones::TELEFONO => 'required',
            ProveedorRefacciones::NOTAS => 'nullable',
            ProveedorRefacciones::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ProveedorRefacciones::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ProveedorRefacciones::SALDO => 'nullable',
            ProveedorRefacciones::LIMITE_CREDITO => 'nullable',
            ProveedorRefacciones::TIPO_REGISTRO => 'nullable',
            ProveedorRefacciones::FORMA_PAGO_ID => 'nullable',
            ProveedorRefacciones::CODIGO_POSTAL => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ProveedorRefacciones::PROVEEDOR_NOMBRE => 'required',
            ProveedorRefacciones::PROVEEDOR_RFC => 'required',
            ProveedorRefacciones::PROVEEDOR_CALLE => 'required',
            ProveedorRefacciones::PROVEEDOR_NUMERO => 'required',
            ProveedorRefacciones::PROVEEDOR_COLONIA => 'required',
            ProveedorRefacciones::PROVEEDOR_ESTADO => 'required',
            ProveedorRefacciones::CLAVE_IDENTIFICADOR => 'required',
            ProveedorRefacciones::RAZON_SOCIAL => 'required',
            ProveedorRefacciones::APELLIDO_MATERNO => 'nullable',
            ProveedorRefacciones::APELLIDO_PATERNO => 'nullable',
            ProveedorRefacciones::PROVEEDOR_PAIS => 'nullable',
            ProveedorRefacciones::CORREO => 'required|email',
            ProveedorRefacciones::CORREO_SECUNDARIO => 'nullable|email',
            ProveedorRefacciones::TELEFONO_SECUNDARIO => 'nullable',
            ProveedorRefacciones::TELEFONO => 'required',
            ProveedorRefacciones::NOTAS => 'nullable',
            ProveedorRefacciones::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ProveedorRefacciones::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ProveedorRefacciones::SALDO => 'nullable',
            ProveedorRefacciones::LIMITE_CREDITO => 'nullable',
            ProveedorRefacciones::TIPO_REGISTRO => 'nullable',
            ProveedorRefacciones::FORMA_PAGO_ID => 'nullable',
            ProveedorRefacciones::CODIGO_POSTAL => 'nullable',
        ];
    }

    public function getAll()
    {
        $data = $this->buscarTodos();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ProveedorRefacciones::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ProveedorRefacciones::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ProveedorRefacciones::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ProveedorRefacciones::METODO_PAGO_ID]) : [];
        }
        return $data;
    }

    public function getOneProveedor($cliente_id)
    {
        $data = $this->modelo->where(ProveedorRefacciones::ID, $cliente_id)->get();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ProveedorRefacciones::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ProveedorRefacciones::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ProveedorRefacciones::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ProveedorRefacciones::METODO_PAGO_ID]) : [];
        }
        return $data;
    }
}
