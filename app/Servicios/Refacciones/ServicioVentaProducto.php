<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioPrecios;
use App\Servicios\Refacciones\ServicioProductoAlmacen;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Support\Facades\DB;

class ServicioVentaProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->modelo = new VentaProductoModel();
        $this->servicioFolio = new ServicioFolios();
        $this->servicioPrecios = new ServicioPrecios();
        $this->servicioClientes = new ServicioClientes();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioProductoAlmacen = new ServicioProductoAlmacen();
    }

    public function getReglasGuardar()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'required|exists:producto,id',
            VentaProductoModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentaProductoModel::FOLIO_ID => 'required',
            VentaProductoModel::VALOR_UNITARIO => 'required',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentaProductoModel::VENTA_ID => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'exists:producto,id',
            VentaProductoModel::PRECIO_ID => 'exists:precio,id',
            VentaProductoModel::CLIENTE_ID => 'exists:clientes,id',
            VentaProductoModel::FOLIO_ID => 'required|exists:folios,id',
            VentaProductoModel::VALOR_UNITARIO => 'nullable',
            VentaProductoModel::CANTIDAD => 'numeric',
            VentaProductoModel::VENTA_ID => 'required|numeric'
        ];
    }

    public function getDetalleVenta()
    {
        return $this->modelo->with([
            ProductosModel::getTableName(),
            Precios::getTableName(),
            ClientesModel::getTableName()
        ])->get();
    }

    public function geDetalleVentaById($id)
    {
        return DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->leftJoin('catalogo_ubicaciones_producto', 'catalogo_ubicaciones_producto.id', '=', 'producto.ubicacion_producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('clientes', 'clientes.id', '=', 'venta_producto.cliente_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->where('re_ventas_estatus.activo', true)
            ->where('venta_producto.venta_id', $id)
            ->select(
                'venta_producto.*',
                'producto.no_identificacion',
                'producto.descripcion as descripcion_producto',
                'producto.unidad as unidad',
                'precio.precio_publico',
                'estatus_venta.nombre as estatus_venta',
                'catalogo_ubicaciones_producto.nombre as ubicacion_producto',
                're_ventas_estatus.estatus_ventas_id'
            )
            ->get();
    }

    public function getDetalleVentaByFolio($folio_id)
    {

        return DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('clientes', 'clientes.id', '=', 'venta_producto.cliente_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->where('re_ventas_estatus.activo', true)
            ->where('venta_producto.folio_id', $folio_id)
            ->select(
                'venta_producto.*',
                'producto.no_identificacion',
                'producto.descripcion as descripcion_producto',
                'producto.unidad as unidad',
                'precio.precio_publico',
                'estatus_venta.nombre as estatus_venta',
                're_ventas_estatus.estatus_ventas_id'
            )
            ->get();
    }

    public function totalVentaByFolio($folio_id)
    {
        return $this->totalventa([
            'folio_id' => $folio_id
        ]);
    }

    public function totalVentaById($venta_id)
    {
        return $this->totalventa([
            'venta_id' => $venta_id
        ]);
    }

    public function totalventa($parametros)
    {
        $query = DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('clientes', 'clientes.id', '=', 'venta_producto.cliente_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->where('re_ventas_estatus.activo', true);

        if (isset($parametros['folio_id'])) {
            $query->where('venta_producto.folio_id', $parametros['folio_id']);
        }
        if (isset($parametros['venta_id'])) {
            $query->where('venta_producto.venta_id', $parametros['venta_id']);
        }

        $query->select(
            DB::raw('SUM(venta_producto.venta_total ) as venta_total')
        );
        
        $resultado = $query->first();
        return isset($resultado->venta_total) ? $resultado->venta_total : 0;
    }

    public function porcentajeProducto($descuento, $precio_producto, $cantidad)
    {
        $des = (int) $descuento / 100;
        $precio_total = (100 * $des) + $precio_producto;
        return $precio_total * $cantidad;
    }

    public function store($request)
    {
        $producto = $this->existeProducto([
            VentaProductoModel::FOLIO_ID => $request->folio_id,
            VentaProductoModel::PRODUCTO_ID => $request->producto_id,
            VentaProductoModel::PRECIO_ID => $request->precio_id
        ]);
        if (count($producto) > 0) {
            $total = $producto[0][VentaProductoModel::CANTIDAD] + $request->cantidad;
            $data[VentaProductoModel::PRODUCTO_ID] = $request->producto_id;
            $data[VentaProductoModel::PRECIO_ID] = $request->precio_id;
            $data[VentaProductoModel::FOLIO_ID] = $request->folio_id;
            $data[VentaProductoModel::CANTIDAD] = $total;
            $data[VentaProductoModel::TOTAL_VENTA] = $total * $request->valor_unitario;
            return DB::table($this->modelo->getTableName())
                ->where(VentaProductoModel::ID, $producto[0][VentaProductoModel::ID])
                ->update($data);
        } else {
            $parametros = $request->all();
            $parametros[VentaProductoModel::TOTAL_VENTA] = $request->cantidad * $request->valor_unitario;
            return $this->modelo->create($parametros);
        }
    }

    public function existeProducto($parametros)
    {
        $query = $this->modelo
            ->where(VentaProductoModel::FOLIO_ID, $parametros[VentaProductoModel::FOLIO_ID]);

        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $query->where(VentaProductoModel::PRODUCTO_ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }

        if (isset($parametros[VentaProductoModel::PRECIO_ID])) {
            $query->where(VentaProductoModel::PRECIO_ID, $parametros[VentaProductoModel::PRECIO_ID]);
        }

        if (isset($parametros[VentaProductoModel::VENTA_ID])) {
            $query->where(VentaProductoModel::VENTA_ID, $parametros[VentaProductoModel::VENTA_ID]);
        }

        return $query->get()->toArray();
    }

    public function descontarVenta($modelo)
    {
        foreach ($modelo as $key => $item) {
            $total_vendida = $item->cantidad_vendida;
            $this->servicioProductos->descontarProductos([
                ProductosModel::ID => $item->producto_id,
                ProductosModel::CANTIDAD => $total_vendida
            ]);

            $this->servicioProductoAlmacen->descontarAlmacen([
                ProductosModel::ID => $item->producto_id,
                ProductosModel::CANTIDAD => $total_vendida
            ]);
        }
    }

    public function getDataByFolio($folio_id)
    {
        $modelo = DB::table('venta_producto as vp')
            ->join('producto', 'vp.producto_id', '=', 'producto.id')
            ->select(
                'vp.id',
                'vp.producto_id',
                'vp.precio_id',
                'vp.cliente_id',
                'vp.folio_id',
                'vp.descontado_almacen',
                'vp.cantidad as cantidad_vendida',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.cantidad as producto_cantidad',
                'producto.valor_unitario'
            )->where(VentaProductoModel::FOLIO_ID, $folio_id)->get();

        return $modelo;
    }
}
