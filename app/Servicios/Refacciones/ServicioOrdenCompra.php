<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\DevolucionProveedorModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\EstatusFacturaModel;
use App\Models\Refacciones\ListaProductosOrdenCompraModel as ProductoOrdenCompraModel;
use App\Models\Refacciones\ReFacturaOrdenCompraModel;
use App\Models\Refacciones\ReComprasEstatusModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use Illuminate\Support\Facades\DB;

class ServicioOrdenCompra extends ServicioDB
{
	public function __construct()
	{
		$this->recurso = 'Orden compra';
		$this->servicioProducto = new ServicioProductos();
		$this->servicioListaProductosOrdenCompra = new ServicioListaProductosOrdenCompra();
		$this->modeloProveedor = new ProveedorRefacciones();
		$this->modeloTipopago = new CatTipoPagoModel();
		$this->modeloEstatusCompra = new EstatusCompra();
		$this->modelo = new OrdenCompraModel();
	}

	public function getReglasGuardar()
	{
		return [
			OrdenCompraModel::PROVEEDOR_ID => 'required|numeric',
			OrdenCompraModel::FACTURA_ID => 'required|numeric',
			OrdenCompraModel::FECHA_PAGO => 'required|date',
			OrdenCompraModel::FECHA => 'required|date',
			OrdenCompraModel::OBSERVACIONES => 'nullable',
			OrdenCompraModel::FACTURA => 'nullable',
			OrdenCompraModel::FACTURA_ID => 'nullable'

		];
	}
	public function getReglasUpdate()
	{
		return [
			OrdenCompraModel::PROVEEDOR_ID => 'nullable',
			OrdenCompraModel::FACTURA_ID => 'nullable',
			OrdenCompraModel::FECHA_PAGO => 'nullable',
			OrdenCompraModel::FECHA => 'nullable',
			OrdenCompraModel::OBSERVACIONES => 'nullable',
			OrdenCompraModel::FACTURA => 'nullable',
			OrdenCompraModel::FACTURA_ID => 'nullable'
		];
	}

	public function getReglasUploadxmlOrdenCompra()
	{
		return [
			OrdenCompraModel::PROVEEDOR_ID => 'required|numeric',
			OrdenCompraModel::FACTURA_ID => 'required|numeric',
			OrdenCompraModel::FECHA_PAGO => 'required|date',
			OrdenCompraModel::FECHA => 'required|date',
			OrdenCompraModel::OBSERVACIONES => 'nullable',
			OrdenCompraModel::FACTURA => 'nullable',
			OrdenCompraModel::FACTURA_ID => 'nullable',
			Factura::ARCHIVO_FACTURA => 'required|mimes:xml'
		];
	}

	public function getReglasBusquedaTraspaso()
	{
		return [
			Traspasos::FECHA_INICIO => 'nullable|date',
			Traspasos::FECHA_FIN => 'nullable|date',
			OrdenCompraModel::FOLIO_ID => 'nullable|numeric|exists:folios,id'
		];
	}

	public function crearOrdenCompra($parametros)
	{
		return $this->crear($parametros);
	}

	public function showById($id)
	{
		return $this->modelo
			->with([
				OrdenCompraModel::REL_PROVEEDOR,
				OrdenCompraModel::REL_FOLIOS,
				OrdenCompraModel::REL_DETALLE_COMPRA,
				OrdenCompraModel::REL_COMPRA_ESTATUS . '.' . ReComprasEstatusModel::REL_ESTATUS

			])->find($id);
	}

	public function listadoProductos($id)
	{
		$orden = $this->modelo->find($id);
		$orden['proveedor'] = $this->modeloProveedor->find($orden->proveedor_id);
		$orden['tipo_pago'] = $this->modeloTipopago->find($orden->tipo_pago_id);
		$orden['estatus_compra'] = $this->modeloEstatusCompra->find($orden->estatus_compra_id);
		$orden['productos'] = $this->servicioListaProductosOrdenCompra->relacionByOrdenCompra($id);
		return $orden;
	}

	public function listado()
	{
		return $this->getAllOrdenCompra([]);
	}

	public function getOrdenCompra($request)
	{
		return $this->getAllOrdenCompra($request->toArray());
	}

	public function getDevoluciones($request)
	{
		return $this->getOrdenCompraDevolucion($request->toArray());
	}

	// actualizar stock cuando no se sube la factura
	public function actualizarOrdenCompra($request, $id)
	{
		$roden_compra = $this->showById($id);
		foreach ($roden_compra->detalle_compra as $key => $item) {
			$producto = $this->servicioProducto->getById($item->producto_id);
			$stock = $producto->cantidad + $item->cantidad;
			$this->servicioProducto->massUpdateWhereId('id', $item->producto_id, [
				ProductosModel::CANTIDAD => $stock
			]);
		}

		return $this->massUpdateWhereId(OrdenCompraModel::ID, $id, $request->all());
	}


	public function getComprasByFechas($request)
	{
		$tableOrdenCompra = OrdenCompraModel::getTableName();
		$tableProducto = ProductosModel::getTableName();
		$tableProductosOrdenCompra = ProductoOrdenCompraModel::getTableName();
		$tableReOrdenCompraEstatus = ReComprasEstatusModel::getTableName();
		$tableEstatusCompra = EstatusCompra::getTableName();
		$query = DB::table($tableOrdenCompra)
			->join($tableProductosOrdenCompra, $tableProductosOrdenCompra . '.' . ProductoOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableProducto, $tableProducto . '.' . ProductoOrdenCompraModel::ID, '=', $tableProductosOrdenCompra . '.' . ProductoOrdenCompraModel::PRODUCTO_ID)
			->join($tableReOrdenCompraEstatus, $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableEstatusCompra, $tableEstatusCompra . '.' . EstatusCompra::ID, '=', $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID)
			->select(
				'orden_compra.id',
				'orden_compra.created_at',
				'productos_orden_compra.producto_id',
				'productos_orden_compra.precio',
				'productos_orden_compra.cantidad',
				'productos_orden_compra.total',
				'producto.descripcion',
				'producto.no_identificacion',
				'producto.clave_unidad',
				'producto.clave_prod_serv',
				'producto.unidad',
				$tableEstatusCompra . '.' . EstatusCompra::NOMBRE . ' as estatusCompra',
				$tableEstatusCompra . '.' . EstatusCompra::ID . ' as estatusId'
			);

		if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
			$query->whereBetween('orden_compra.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
		}
		if ($request->get(OrdenCompraModel::FOLIO_ID)) {
			$query->where($tableOrdenCompra . '.' . OrdenCompraModel::FOLIO_ID, $request->get(OrdenCompraModel::FOLIO_ID));
		}
		$query->whereNotIn($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, array(EstatusCompra::ESTATUS_PROCESO));
		// ->where($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true)
		$query->get();

		return [
			'data' => $query->get()
		];
	}

	public function getTotalesComprasByFechas($request)
	{
		$tableOrdenCompra = OrdenCompraModel::getTableName();
		$tableProducto = ProductosModel::getTableName();
		$tableProductosOrdenCompra = ProductoOrdenCompraModel::getTableName();
		$tableReOrdenCompraEstatus = ReComprasEstatusModel::getTableName();
		$tableEstatusCompra = EstatusCompra::getTableName();
		$query = DB::table($tableOrdenCompra)
			->join($tableProductosOrdenCompra, $tableProductosOrdenCompra . '.' . ProductoOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableProducto, $tableProducto . '.' . ProductoOrdenCompraModel::ID, '=', $tableProductosOrdenCompra . '.' . ProductoOrdenCompraModel::PRODUCTO_ID)
			->join($tableReOrdenCompraEstatus, $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableEstatusCompra, $tableEstatusCompra . '.' . EstatusCompra::ID, '=', $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID)
			->select(
				DB::raw('count(DISTINCT(orden_compra.id)) as total_compras'),
				DB::raw('sum(productos_orden_compra.cantidad) as total_cantidad'),
				DB::raw('sum(productos_orden_compra.precio) as sum_valor_unitario'),
				DB::raw('sum(productos_orden_compra.total) as sum_compra_total')
			);

		if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
			$query->whereBetween('orden_compra.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
		}
		if ($request->get(OrdenCompraModel::FOLIO_ID)) {
			$query->where($tableOrdenCompra . '.' . OrdenCompraModel::FOLIO_ID, $request->get(OrdenCompraModel::FOLIO_ID));
		}
		$query->whereNotIn($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, array(EstatusCompra::ESTATUS_PROCESO, EstatusCompra::ESTATUS_DEVOLUCION, EstatusCompra::ESTATUS_CANCELADA));
		// ->where($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true)
		return $query->first();
	}

	public function getRecurso()
	{
		return $this->recurso;
	}

	public function listaFacturaOrdenCompra()
	{
		$tableReFacturaOrdenCompra = ReFacturaOrdenCompraModel::getTableName();
		$tabla_factura = Factura::getTableName();
		$tabla_estatus_factura = EstatusFacturaModel::getTableName();
		return DB::table($tabla_factura)
			->leftJoin($tableReFacturaOrdenCompra, $tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tabla_factura . '.' . Factura::ID)
			->leftJoin($tabla_estatus_factura, $tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID, '=', $tabla_estatus_factura . '.' . EstatusFacturaModel::ID)
			->select(
				$tabla_factura . '.' . Factura::ID,
				$tabla_factura . '.' . Factura::FOLIO,
				$tabla_factura . '.' . Factura::CREATED_AT . ' as fecha',
				$tabla_factura . '.' . Factura::TOTAL,
				$tabla_factura . '.' . Factura::SUB_TOTAL,
				$tabla_factura . '.' . Factura::UUID,
				$tabla_factura . '.' . Factura::RUTA_XML_FACTURA,
				$tabla_factura . '.' . Factura::FILE_NAME,
				$tabla_estatus_factura . '.' . EstatusFacturaModel::NOMBRE . ' as estatus_factura',
				$tabla_estatus_factura . '.' . EstatusFacturaModel::ID . ' as estatus_id',
				$tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ID . ' as orden_compra_id'
			)
			->get();
	}

	public function getOrdenCompraByFolio($folio_id)
	{
		return $this->modelo->where(OrdenCompraModel::FOLIO_ID, $folio_id)->get();
	}

	public function getAllOrdenCompra($parametros)
	{
		$tableOrdenCompra = OrdenCompraModel::getTableName();
		$tableProveedor = ProveedorRefacciones::getTableName();
		$tableTipoPago = CatTipoPagoModel::getTableName();
		$tableFolios = FoliosModel::getTableName();
		$tableReFacturaOrdenCompra = ReFacturaOrdenCompraModel::getTableName();
		$tableReOrdenCompraEstatus = ReComprasEstatusModel::getTableName();
		$tableEstatusCompra = EstatusCompra::getTableName();
		$tableFactura = Factura::getTableName();
		$tableCuentasPorPagar = CuentasPorPagarModel::getTableName();

		$consulta = DB::table($tableOrdenCompra)
			->leftJoin($tableProveedor, $tableProveedor . '.' . ProveedorRefacciones::ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::PROVEEDOR_ID)
			->join($tableReFacturaOrdenCompra, $tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableFactura, $tableFactura . '.' . Factura::ID, '=', $tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::FACTURA_ID)
			->join($tableFolios, $tableFolios . '.' . FoliosModel::ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::FOLIO_ID)
			->join($tableReOrdenCompraEstatus, $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableEstatusCompra, $tableEstatusCompra . '.' . EstatusCompra::ID, '=', $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID)
			->leftJoin($tableCuentasPorPagar, $tableCuentasPorPagar . '.' . CuentasPorPagarModel::FOLIO_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::FOLIO_ID)
			->leftJoin($tableTipoPago, $tableTipoPago . '.' . CatTipoPagoModel::ID, '=', $tableCuentasPorPagar . '.' . CuentasPorPagarModel::TIPO_PAGO_ID)
			->where($tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ACTIVO, true)
			->where($tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID, EstatusFacturaModel::ESTATUS_CREADA);
		if (isset($parametros[OrdenCompraModel::PROVEEDOR_ID]) && $parametros[OrdenCompraModel::PROVEEDOR_ID]) {
			$consulta->where($tableOrdenCompra . '.' . OrdenCompraModel::PROVEEDOR_ID, $parametros[OrdenCompraModel::PROVEEDOR_ID]);
		}
		if (isset($parametros[FoliosModel::FOLIO]) && $parametros[FoliosModel::FOLIO]) {
			//$consulta->whereRaw('LOWER('.$tableFolios . '.' . FoliosModel::FOLIO.') LIKE (?) ',["%{$parametros[FoliosModel::FOLIO]}%"]);
			$consulta->where($tableFolios . '.' . FoliosModel::FOLIO, 'ilike', $parametros[FoliosModel::FOLIO]);
		}
		if (isset($parametros[ReComprasEstatusModel::COMPRA_ID]) && $parametros[ReComprasEstatusModel::COMPRA_ID]) {
			$consulta->where($tableOrdenCompra . '.' . OrdenCompraModel::ID, $parametros[ReComprasEstatusModel::COMPRA_ID]);
			$consulta->whereIn($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, [EstatusCompra::ESTATUS_DEVOLUCION, EstatusCompra::ESTATUS_FINALIZADA]);
		} else {
			$consulta->whereIn($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, [EstatusCompra::ESTATUS_FINALIZADA, EstatusCompra::ESTATUS_PROCESO]);
		}
		$consulta->where($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true);
		$consulta->select(
			$tableOrdenCompra . '.' . OrdenCompraModel::ID,
			$tableProveedor . '.' . ProveedorRefacciones::PROVEEDOR_NUMERO,
			$tableProveedor . '.' . ProveedorRefacciones::ID . ' as proveedor_id',
			$tableProveedor . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE,
			$tableFolios . '.' . FoliosModel::FOLIO,
			$tableTipoPago . '.' . CatTipoPagoModel::NOMBRE . ' as tipoPago',
			$tableEstatusCompra . '.' . EstatusCompra::NOMBRE . ' as estatusCompra',
			$tableEstatusCompra . '.' . EstatusCompra::ID . ' as id_estatus_compra',
			$tableOrdenCompra . '.' . OrdenCompraModel::OBSERVACIONES,
			$tableFactura . '.' . Factura::FOLIO . ' as folioFactura',
			$tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::CREATED_AT
		);

		return $consulta->get()->toArray();
	}

	public function getOrdenCompraDevolucion($parametros)
	{
		$tableOrdenCompra = OrdenCompraModel::getTableName();
		$tableProveedor = ProveedorRefacciones::getTableName();
		$tableTipoPago = CatTipoPagoModel::getTableName();
		$tableFolios = FoliosModel::getTableName();
		$tableReFacturaOrdenCompra = ReFacturaOrdenCompraModel::getTableName();
		$tableReOrdenCompraEstatus = ReComprasEstatusModel::getTableName();
		$tableEstatusCompra = EstatusCompra::getTableName();
		$tableDevolucion = DevolucionProveedorModel::getTableName();

		$consulta = DB::table($tableOrdenCompra)
			->join($tableProveedor, $tableProveedor . '.' . ProveedorRefacciones::ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::PROVEEDOR_ID)
			->join($tableReFacturaOrdenCompra, $tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableReOrdenCompraEstatus, $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableEstatusCompra, $tableEstatusCompra . '.' . EstatusCompra::ID, '=', $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID);
		//->join($tableTipoPago, $tableTipoPago . '.' . CatTipoPagoModel::ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::TIPO_PAGO_ID);
		$consulta->join($tableDevolucion, $tableDevolucion . '.' . DevolucionProveedorModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID);
		$consulta->join($tableFolios, $tableFolios . '.' . FoliosModel::ID, '=', $tableDevolucion . '.' . DevolucionProveedorModel::FOLIO_ID);
		$consulta->where($tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ACTIVO, true);
		$consulta->where($tableReFacturaOrdenCompra . '.' . ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID, EstatusFacturaModel::ESTATUS_CREADA);
		if (isset($parametros[OrdenCompraModel::PROVEEDOR_ID]) && $parametros[OrdenCompraModel::PROVEEDOR_ID]) {
			$consulta->where($tableOrdenCompra . '.' . OrdenCompraModel::PROVEEDOR_ID, $parametros[OrdenCompraModel::PROVEEDOR_ID]);
		}
		if (isset($parametros[FoliosModel::FOLIO]) && $parametros[FoliosModel::FOLIO]) {
			//$consulta->whereRaw('LOWER('.$tableFolios . '.' . FoliosModel::FOLIO.') LIKE (?) ',["%{$parametros[FoliosModel::FOLIO]}%"]);
			$consulta->where($tableFolios . '.' . FoliosModel::FOLIO, 'ilike', $parametros[FoliosModel::FOLIO]);
		}
		$consulta->whereIn($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, [EstatusCompra::ESTATUS_DEVOLUCION]);
		$consulta->where($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true);
		if (isset($parametros[ReComprasEstatusModel::COMPRA_ID]) && $parametros[ReComprasEstatusModel::COMPRA_ID]) {
			$consulta->where($tableOrdenCompra . '.' . OrdenCompraModel::ID, $parametros[ReComprasEstatusModel::COMPRA_ID]);
		}
		$consulta->select(
			$tableOrdenCompra . '.' . OrdenCompraModel::ID,
			$tableProveedor . '.' . ProveedorRefacciones::ID . ' as proveedor_id',
			$tableProveedor . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE,
			$tableFolios . '.' . FoliosModel::FOLIO,
			//$tableTipoPago . '.' . CatTipoPagoModel::NOMBRE . ' as tipoPago',
			$tableEstatusCompra . '.' . EstatusCompra::NOMBRE . ' as estatusCompra',
			$tableEstatusCompra . '.' . EstatusCompra::ID . ' as id_estatus_compra',
			$tableOrdenCompra . '.' . OrdenCompraModel::OBSERVACIONES,
			$tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::CREATED_AT
		);

		return $consulta->get()->toArray();
	}

	function updateInventarioByOrdenCompra($orden_compra_id)
	{
		$tableOrdenCompra = OrdenCompraModel::getTableName();
		$tableProductosOrdenCompra = ProductoOrdenCompraModel::getTableName();
		$tableReOrdenCompraEstatus = ReComprasEstatusModel::getTableName();
		return $this->modelo
			->join($tableProductosOrdenCompra, $tableProductosOrdenCompra . '.' . ProductoOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->join($tableReOrdenCompraEstatus, $tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableOrdenCompra . '.' . OrdenCompraModel::ID)
			->select(
				'orden_compra.id',
				'productos_orden_compra.producto_id',
				'productos_orden_compra.cantidad',
				$tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID
			)
			->where($tableOrdenCompra . '.' . OrdenCompraModel::ID, $orden_compra_id)
			//->where($tableReOrdenCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID, EstatusCompra::ESTATUS_FINALIZADA)
			->get();
	}
}
