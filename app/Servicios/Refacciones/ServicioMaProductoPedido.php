<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\EnvioPedidoDetalleModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\MaProductoPedidoModel;
use App\Models\Refacciones\ProductosPedidosModel;

class ServicioMaProductoPedido extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Tipo de pago';
        $this->modelo = new MaProductoPedidoModel();
    }

    public function getReglasGuardar()
    {
        return [
            MaProductoPedidoModel::AUTORIZADO => 'nullable',
            MaProductoPedidoModel::ID_USUARIO_AUTORIZO => 'nullable'

        ];
    }
    public function getReglasUpdate()
    {
        return [
            MaProductoPedidoModel::AUTORIZADO => 'nullable',
            MaProductoPedidoModel::ID_USUARIO_AUTORIZO => 'nullable'
        ];
    }

    public function buscarTodos()
    {
        return $this->createQuery([]);
    }

    public function createQuery($parametros)
    {
        $query =  $this->modelo->select(
            MaProductoPedidoModel::getTableName() . '.*',
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::ID .' as id_detalle_envio',
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::NUMERO_REMISION
        )
            ->with(['rel_usuario' => function ($query) {
                return $query->select('id', 'nombre', 'apellido_paterno', 'apellido_materno');
            }])
            ->leftJoin(
                ProductosPedidosModel::getTableName(),
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID,
                MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID
            )
            ->leftJoin(
                EnvioPedidoDetalleModel::getTableName(),
                EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::PEDIDO_ID,
                MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID
            );
        $query->where(
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::AUTORIZADO,
            '=',
            false
        )->where(
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::FINALIZADO,
            '=',
            true
        );

        $query->orWhere(function ($query) use ($parametros) {
            $estatus_filtrado = isset($parametros[ProductosPedidosModel::ESTATUS_ID])
                && !is_null($parametros[ProductosPedidosModel::ESTATUS_ID])
                ? [$parametros[ProductosPedidosModel::ESTATUS_ID]]
                : [
                    ProductosPedidosModel::ESTATUS_PROCESO,
                    ProductosPedidosModel::ESTATUS_BACKORDER
                ];

            $query->whereIn(
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
                $estatus_filtrado
            )->orWhere(
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
                '=',
                null
            );
        });

        $query->groupBy(
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::ID
        );
        return $query->get();
    }


    public function validarActivoOrStore()
    {
        $validar = $this
            ->modelo
            ->where(MaProductoPedidoModel::AUTORIZADO, '=', 0)
            ->get();

        if (isset($validar) && count($validar) == 0) {
            return $this->crear([
                MaProductoPedidoModel::AUTORIZADO => 0
            ]);
        } else {
            throw new ParametroHttpInvalidoException([
                'msg' => 'Existe un pedido activo'
            ]);
        }
    }
}
