<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Nomina\CaFormaPagoModel;
use App\Models\Refacciones\TipoPedidoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\EnvioPedidoDetalleModel;
use App\Models\Refacciones\MaProductoPedidoModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Servicios\Core\ServicioDB;


class ServicioProductoPedidos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'producto pedidos';
        $this->modelo = new ProductosPedidosModel();
        $this->modeloProductos = new ProductosModel();
        $this->servicioProductos = new ServicioProductos();
        $this->serviciomaproductopedido = new ServicioMaProductoPedido();
    }

    public function getReglasGuardar()
    {
        return [
            ProductosModel::NO_IDENTIFICACION_FACTURA => 'nullable',
            ProductosPedidosModel::PRODUCTO_ID => 'nullable|exists:' . ProductosModel::getTableName() . ',id',
            ProductosPedidosModel::ESTATUS_ID => 'nullable',
            ProductosPedidosModel::CANTIDAD_SOLICITADA => 'required',
            ProductosPedidosModel::CANTIDAD_BACKORDER => 'nullable',
            ProductosPedidosModel::CANTIDAD_CARGADA => 'nullable',
            ProductosPedidosModel::PROVEEDOR_ID => 'nullable|exists:' . ProveedorRefacciones::getTableName() . ',id',
            ProductosPedidosModel::MA_PEDIDO_ID => 'nullable|exists:' . MaProductoPedidoModel::getTableName() . ',id'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ProductosPedidosModel::PRODUCTO_ID => 'nullable|exists:' . ProductosModel::getTableName() . ',id',
            ProductosPedidosModel::ESTATUS_ID => 'nullable',
            ProductosPedidosModel::CANTIDAD_SOLICITADA => 'nullable',
            ProductosPedidosModel::CANTIDAD_BACKORDER => 'nullable',
            ProductosPedidosModel::CANTIDAD_CARGADA => 'nullable',
            ProductosPedidosModel::PROVEEDOR_ID => 'nullable|exists:' . ProveedorRefacciones::getTableName() . ',id',
            ProductosPedidosModel::MA_PEDIDO_ID => 'nullable|exists:' . MaProductoPedidoModel::getTableName() . ',id'
        ];
    }


    function validarProducto($parametros)
    {
        $query =  $this->modeloProductos->select(
            ProductosModel::getTableName() . '.' . ProductosModel::ID . ' as producto_id',
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_SOLICITADA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ID . ' as producto_pedido_id'
        );

        $query->leftJoin(
            ProductosPedidosModel::getTableName(),
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID,
            ProductosModel::getTableName() . '.' . ProductosModel::ID
        );
        if (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]) && empty($parametros[ProductosPedidosModel::PRODUCTO_ID])) {
            $query->where(
                ProductosModel::NO_IDENTIFICACION_FACTURA,
                'ilike',
                $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA] . '%'
            );
        }

        if (isset($parametros[ProductosPedidosModel::PRODUCTO_ID])) {
            $query->where(
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                '=',
                $parametros[ProductosPedidosModel::PRODUCTO_ID]
            );
        }

        if (isset($parametros[ProductosPedidosModel::PROVEEDOR_ID])) {
            $query->where(function ($data) use ($parametros) {
                $data->where(
                    ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PROVEEDOR_ID,
                    $parametros[ProductosPedidosModel::PROVEEDOR_ID]
                );

                $data->orWhereNull(ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID);
            });
        }

        $query->where(function ($data) {
            $data->where(
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
                ProductosPedidosModel::ESTATUS_PROCESO
            );

            $data->orWhereNull(ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID);
        });
        // dd($query->dd());
        return $query->first();
    }

    public function crearQuery($parametros)
    {
        $query = $this->modelo->select(
            ProductosPedidosModel::getTableName() . '.*',
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::ID .' as id_enviodetalle',
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::VIA_ENVIO,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::DIA_ENVIO,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::RUTA,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::LINEAS,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::TOTAL_PIEZAS,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::PESO_TOTAL,
            EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::NUMERO_REMISION,
            TipoPedidoModel::getTableName() . '.' . TipoPedidoModel::NOMBRE . ' as tipo_pedido',
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_RFC,

            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_CALLE,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_NUMERO,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_COLONIA,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_ESTADO,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_PAIS,
            ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_LOCALIDAD,

            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
            ProductosModel::getTableName() . '.' . ProductosModel::UNIDAD,
            ProductosModel::getTableName() . '.' . ProductosModel::VALOR_UNITARIO,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            ProductosModel::getTableName() . '.' . ProductosModel::PESO,
            ProductosModel::getTableName() . '.' . ProductosModel::PREFIJO,
            ProductosModel::getTableName() . '.' . ProductosModel::SUFIJO,
            ProductosModel::getTableName() . '.' . ProductosModel::BASICO,
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::FINALIZADO,
            TipoFormaPagoModel::getTableName() . '.' . TipoFormaPagoModel::DESCRIPCION . ' as forma_pago',
            CatTipoPagoModel::getTableName() . '.' . CatTipoPagoModel::NOMBRE . ' as tipo_pago'
        )->join(
            ProductosModel::getTableName(),
            ProductosModel::getTableName() . '.' . ProductosModel::ID,
            ProductosPedidosModel::PRODUCTO_ID
        )
            ->join(
                MaProductoPedidoModel::getTableName(),
                MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID,
                ProductosPedidosModel::MA_PEDIDO_ID
            )
            ->join(
                TipoPedidoModel::getTableName(),
                TipoPedidoModel::getTableName() . '.' . TipoPedidoModel::ID,
                ProductosPedidosModel::ESTATUS_ID
            )
            ->leftJoin(
                EnvioPedidoDetalleModel::getTableName(),
                EnvioPedidoDetalleModel::getTableName() . '.' . EnvioPedidoDetalleModel::PEDIDO_ID,
                MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID
            )
            ->leftJoin(
                ProveedorRefacciones::getTableName(),
                ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::ID,
                ProductosPedidosModel::PROVEEDOR_ID
            )
            ->leftJoin(
                TipoFormaPagoModel::getTableName(),
                TipoFormaPagoModel::getTableName() . '.' . TipoFormaPagoModel::ID,
                ProveedorRefacciones::FORMA_PAGO_ID
            )->leftJoin(
                CatTipoPagoModel::getTableName(),
                CatTipoPagoModel::getTableName() . '.' . CatTipoPagoModel::ID,
                ProveedorRefacciones::METODO_PAGO_ID
            );


        if (isset($parametros[ProductosPedidosModel::ESTATUS_ID])) {
            $query->whereIn(
                ProductosPedidosModel::ESTATUS_ID,
                $parametros[ProductosPedidosModel::ESTATUS_ID]
            );
        }

        if (isset($parametros[ProductosPedidosModel::PROVEEDOR_ID])) {
            $query->where(
                ProductosPedidosModel::PROVEEDOR_ID,
                $parametros[ProductosPedidosModel::PROVEEDOR_ID]
            );
        }

        if (isset($parametros[ProductosPedidosModel::MA_PEDIDO_ID])) {
            $query->where(
                ProductosPedidosModel::MA_PEDIDO_ID,
                $parametros[ProductosPedidosModel::MA_PEDIDO_ID]
            );
        }

        if (isset($parametros['pedidos_activos'])) {
            $query->where(MaProductoPedidoModel::FINALIZADO, false);
        }

        return $query->get();
    }

    public function filterQuerys($parametros)
    {
        $validar = isset($parametros[ProductosPedidosModel::ESTATUS_ID])
            ? [$parametros[ProductosPedidosModel::ESTATUS_ID]]
            : [
                ProductosPedidosModel::ESTATUS_STOCK,
                ProductosPedidosModel::ESTATUS_BACKORDER,
                ProductosPedidosModel::ESTATUS_PROCESO
            ];
        // dd($parametros, $validar);
        return $this->crearQuery(array_merge($parametros, [ProductosPedidosModel::ESTATUS_ID => $validar]));
    }

    public function productospedido()
    {
        return $this->crearQuery(['pedidos_activos' => true]);
    }

    public function store($parametros)
    {
        if (empty($parametros[ProductosPedidosModel::MA_PEDIDO_ID])) {
            $pedidoactivo = $this->serviciomaproductopedido->createQuery([])->first();
            if (empty($pedidoactivo)) {
                $pedido = $this->serviciomaproductopedido->crear([]);
                $parametros[ProductosPedidosModel::MA_PEDIDO_ID] = $pedido->id;
            } else {
                $parametros[ProductosPedidosModel::MA_PEDIDO_ID] = $pedidoactivo->id;
            }
        }
        if (empty($parametros[ProductosPedidosModel::PRODUCTO_ID]) && empty($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            throw new ParametroHttpInvalidoException([
                'error' => 'El producto es requerido'
            ]);
        }
        $producto = $this->validarProducto($parametros);
        // dd($producto, $parametros);
        if (isset($producto) && isset($producto->producto_pedido_id)) {
            // $array_productos = [];
            // $array_productos[ProductosPedidosModel::CANTIDAD_SOLICITADA] = $producto->cantidad_solicitada + (int)$parametros[ProductosPedidosModel::CANTIDAD_SOLICITADA];

            return $this->massUpdateWhereId(
                ProductosPedidosModel::ID,
                $producto->producto_pedido_id,
                [
                    ProductosPedidosModel::CANTIDAD_SOLICITADA => $producto->cantidad_solicitada + (int)$parametros[ProductosPedidosModel::CANTIDAD_SOLICITADA],
                    ProductosPedidosModel::ESTATUS_ID => isset($parametros[ProductosPedidosModel::ESTATUS_ID]) ? $parametros[ProductosPedidosModel::ESTATUS_ID] : ProductosPedidosModel::ESTATUS_PROCESO,
                ]
            );
        } else if (isset($producto->producto_id)) {
            $producto_id = $producto->producto_id;

            return $this->crear(
                [
                    ProductosPedidosModel::PRODUCTO_ID => $producto_id,
                    ProductosPedidosModel::ESTATUS_ID => isset($parametros[ProductosPedidosModel::ESTATUS_ID]) ? $parametros[ProductosPedidosModel::ESTATUS_ID] : ProductosPedidosModel::ESTATUS_PROCESO,
                    ProductosPedidosModel::MA_PEDIDO_ID => $parametros[ProductosPedidosModel::MA_PEDIDO_ID],
                    ProductosPedidosModel::PROVEEDOR_ID => isset($parametros[ProductosPedidosModel::PROVEEDOR_ID]) ? $parametros[ProductosPedidosModel::PROVEEDOR_ID] : null,
                    ProductosPedidosModel::CANTIDAD_SOLICITADA => $parametros[ProductosPedidosModel::CANTIDAD_SOLICITADA]
                ]
            );
        } else if (is_null($producto) && (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]) || isset($parametros[ProductosPedidosModel::PRODUCTO_ID]))) {
            $data_parametro = [];
            if (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
                $data_parametro[ProductosModel::NO_IDENTIFICACION_FACTURA] = $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA];
            }

            if (isset($parametros[ProductosPedidosModel::PRODUCTO_ID])) {
                $data_parametro[ProductosPedidosModel::PRODUCTO_ID] = $parametros[ProductosPedidosModel::PRODUCTO_ID];
            }
            $buscar_producto = $this->servicioProductos->existeProducto($data_parametro)->first();

            return $this->crear(
                [
                    ProductosPedidosModel::PRODUCTO_ID => $buscar_producto->id,
                    ProductosPedidosModel::ESTATUS_ID => isset($parametros[ProductosPedidosModel::ESTATUS_ID]) ? $parametros[ProductosPedidosModel::ESTATUS_ID] : ProductosPedidosModel::ESTATUS_PROCESO,
                    ProductosPedidosModel::MA_PEDIDO_ID => $parametros[ProductosPedidosModel::MA_PEDIDO_ID],
                    ProductosPedidosModel::PROVEEDOR_ID => isset($parametros[ProductosPedidosModel::PROVEEDOR_ID]) ? $parametros[ProductosPedidosModel::PROVEEDOR_ID] : null,
                    ProductosPedidosModel::CANTIDAD_SOLICITADA => $parametros[ProductosPedidosModel::CANTIDAD_SOLICITADA]
                ]
            );
        } else {
            throw new ParametroHttpInvalidoException([
                'error' => 'No se encontro producto en la base de datos'
            ]);
        }
    }
}
