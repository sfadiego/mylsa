<?php

namespace App\Servicios\Refacciones;

use App\Models\Autos\CatModelosModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoMarcasModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\RecepcionUnidadesCostosModel;
use App\Models\Usuarios\User;

class ServicioRecepcionUnidades extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'recepcion unidades';
        $this->modelo = new UnidadesModel();
        $this->modeloCostos = new RecepcionUnidadesCostosModel();
        $this->ventasAutosModel = new VentasAutosModel();
        $this->servicioRecepcionCostos = new ServicioRecepcionUnidadadesCostos();
    }

    public function getReglasGuardar()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required|date',
            UnidadesModel::ID_ESTADO => 'required|numeric',
            UnidadesModel::MARCA_ID => 'required|numeric',
            UnidadesModel::MODELO_ID => 'required|numeric',
            UnidadesModel::ANIO_ID => 'required|numeric',
            UnidadesModel::COLOR_ID => 'required|numeric',
            UnidadesModel::VESTIDURA_ID => 'required|numeric',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::COMBUSTIBLE => 'required|numeric',
            UnidadesModel::N_MOTOR => 'required',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required',
            UnidadesModel::ID_UBICACION => 'required|numeric',
            UnidadesModel::ID_UBICACION_LLAVES => 'required|numeric',
            UnidadesModel::N_PRODUCCION => 'required',
            UnidadesModel::N_REMISION => 'required',
            UnidadesModel::N_FOLIO_REMISION => 'required',
            UnidadesModel::PDTO => 'required',
            UnidadesModel::ID_CUENTA => 'required',
            UnidadesModel::FOLIO_PEDIDO => 'required',

        ];
    }

    public function getReglasUpdate()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'nullable',
            UnidadesModel::FECHA_RECEPCION => 'nullable',
            UnidadesModel::ID_ESTADO => 'nullable',
            UnidadesModel::MARCA_ID => 'nullable',
            UnidadesModel::MODELO_ID => 'nullable',
            UnidadesModel::ANIO_ID => 'nullable',
            UnidadesModel::COLOR_ID => 'nullable',
            UnidadesModel::VESTIDURA_ID => 'nullable',
            UnidadesModel::NUMERO_PUERTAS => 'nullable',
            UnidadesModel::COMBUSTIBLE => 'nullable',
            UnidadesModel::N_MOTOR => 'nullable',
            UnidadesModel::MOTOR => 'nullable',
            UnidadesModel::TRANSMISION => 'nullable',
            UnidadesModel::NUMERO_CILINDROS => 'nullable',
            UnidadesModel::CAPACIDAD => 'nullable',
            UnidadesModel::VIN => 'nullable',
            UnidadesModel::SERIE_CORTA => 'nullable',
            UnidadesModel::NUMERO_ECONOMICO => 'nullable',
            UnidadesModel::ID_UBICACION => 'nullable',
            UnidadesModel::ID_UBICACION_LLAVES => 'nullable',
            UnidadesModel::N_PRODUCCION => 'nullable',
            UnidadesModel::N_REMISION => 'nullable',
            UnidadesModel::N_FOLIO_REMISION => 'nullable',
            UnidadesModel::PDTO => 'nullable',
            UnidadesModel::ID_CUENTA => 'nullable',
            UnidadesModel::FOLIO_PEDIDO => 'nullable',
        ];
    }

    public function reglasCombinadasCostos()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required|date',
            UnidadesModel::ID_ESTADO => 'required|numeric',
            UnidadesModel::MARCA_ID => 'required|numeric',
            UnidadesModel::MODELO_ID => 'required|numeric',
            UnidadesModel::ANIO_ID => 'required|numeric',
            UnidadesModel::COLOR_ID => 'required|numeric',
            UnidadesModel::VESTIDURA_ID => 'required|numeric',
            UnidadesModel::UNIDAD_DESCRIPCION => 'required',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::COMBUSTIBLE => 'required',
            UnidadesModel::N_MOTOR => 'required',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required',
            UnidadesModel::ID_UBICACION => 'required|numeric',
            UnidadesModel::ID_UBICACION_LLAVES => 'required|numeric',
            UnidadesModel::N_PRODUCCION => 'required',
            UnidadesModel::N_REMISION => 'required',
            UnidadesModel::N_FOLIO_REMISION => 'required',
            UnidadesModel::PDTO => 'required',
            UnidadesModel::FOLIO_PEDIDO => 'nullable',
            UnidadesModel::ID_CUENTA => 'required',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::OTROS_GASTOS => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_ACONDI => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_FONDOS_PUB => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_PROG_CIVIL => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_AMDA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_COPARMEX => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::IVA_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IVA_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_VENTA => 'required',
            RecepcionUnidadesCostosModel::UNIDAD_ID => 'nullable|exists:unidades,id'
        ];
    }


    public function reglasCombinadasCostosUpdate()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required|date',
            UnidadesModel::ID_ESTADO => 'required|numeric',
            UnidadesModel::MARCA_ID => 'required|numeric',
            UnidadesModel::MODELO_ID => 'required|numeric',
            UnidadesModel::ANIO_ID => 'required|numeric',
            UnidadesModel::UNIDAD_DESCRIPCION => 'required',
            UnidadesModel::COLOR_ID => 'required|numeric',
            UnidadesModel::VESTIDURA_ID => 'required|numeric',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::COMBUSTIBLE => 'required',
            UnidadesModel::N_MOTOR => 'required',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required',
            UnidadesModel::ID_UBICACION => 'required|numeric',
            UnidadesModel::ID_UBICACION_LLAVES => 'required|numeric',
            UnidadesModel::N_PRODUCCION => 'required',
            UnidadesModel::N_REMISION => 'required',
            UnidadesModel::N_FOLIO_REMISION => 'required',
            UnidadesModel::PDTO => 'required',
            UnidadesModel::ID_CUENTA => 'nullable',
            UnidadesModel::FOLIO_PEDIDO => 'nullable',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::OTROS_GASTOS => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_ACONDI => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_FONDOS_PUB => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_PROG_CIVIL => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_AMDA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_COPARMEX => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::IVA_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IVA_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_VENTA => 'required',
            RecepcionUnidadesCostosModel::UNIDAD_ID => 'required|exists:unidades,id'
        ];
    }

    public function guardarInfo($request)
    {
        return $this->crear([
            UnidadesModel::USUARIO_RECIBE => $request->usuario_recibe,
            UnidadesModel::UNIDAD_DESCRIPCION => $request->unidad_descripcion,
            UnidadesModel::FECHA_RECEPCION => $request->fecha_recepcion,
            UnidadesModel::ID_ESTADO => $request->id_estado,
            UnidadesModel::MARCA_ID => $request->marca_id,
            UnidadesModel::MODELO_ID => $request->modelo_id,
            UnidadesModel::ANIO_ID => $request->anio_id,
            UnidadesModel::COLOR_ID => $request->color_id,
            UnidadesModel::VESTIDURA_ID => $request->vestidura_id,
            UnidadesModel::NUMERO_PUERTAS => $request->numero_puertas,
            UnidadesModel::COMBUSTIBLE => $request->combustible,
            UnidadesModel::N_MOTOR => $request->n_motor,
            UnidadesModel::MOTOR => $request->motor,
            UnidadesModel::ID_CUENTA => $request->id_cuenta,
            UnidadesModel::TRANSMISION => $request->transmision,
            UnidadesModel::NUMERO_CILINDROS => $request->numero_cilindros,
            UnidadesModel::CAPACIDAD => $request->capacidad,
            UnidadesModel::VIN => $request->vin,
            UnidadesModel::NO_SERIE => isset($request->no_serie) ? $request->no_serie : '',
            UnidadesModel::SERIE_CORTA => $request->serie_corta,
            UnidadesModel::NUMERO_ECONOMICO => $request->numero_economico,
            UnidadesModel::ID_UBICACION => $request->id_ubicacion,
            UnidadesModel::ID_UBICACION_LLAVES => $request->id_ubicacion_llaves,
            UnidadesModel::N_PRODUCCION => $request->n_produccion,
            UnidadesModel::N_REMISION => $request->n_remision,
            UnidadesModel::N_FOLIO_REMISION => $request->n_folio_remision,
            UnidadesModel::PDTO => $request->pdto,
            UnidadesModel::FOLIO_PEDIDO => $request->folio_pedido,
            UnidadesModel::PRECIO_VENTA => $request->valor_unidad_venta,
            UnidadesModel::PRECIO_COSTO => $request->valor_unidad,
            UnidadesModel::KILOMETRAJE => 0,

        ]);
    }

    public function getByIdRecepcionUnidades($id)
    {
        return $this->getAll([
            'id' => $id,
            'id_tipo_auto' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getAllSeminuevos()
    {
        return $this->getAll([
            'id_tipo_auto' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function getAllnuevos()
    {
        return $this->getAll([
            'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getAll($parametros)
    {

        $tabla_cliente = ClientesModel::getTableName();
        $tabla_user = User::getTableName();
        $tabla_unidad = UnidadesModel::getTableName();

        $catalogo_marcas = CatalogoMarcasModel::getTableName();
        $catalogo_anio = CatalogoAnioModel::getTableName();
        $catalogo_modelos = CatModelosModel::getTableName();
        $catalogo_colores = CatalogoColoresModel::getTableName();
        $unidades_costos = RecepcionUnidadesCostosModel::getTableName();
        $estatus_venta = EstatusVentaModel::getTableName();

        $query = $this->modelo->select(
            $tabla_unidad . '.id AS id_unidad',
            $tabla_unidad . '.unidad_descripcion',
            $catalogo_anio . '.id AS anio_id',
            $catalogo_anio . '.nombre AS nombre_anio',
            $catalogo_marcas . '.id AS marca_id',
            $catalogo_marcas . '.nombre AS nombre_marca',
            $catalogo_modelos . '.id AS modelo_id',
            $catalogo_modelos . '.nombre AS nombre_modelo',
            $catalogo_colores . '.id AS color_id',
            $catalogo_colores . '.nombre AS nombre_color',
            $catalogo_colores . '.clave AS clave',
            // $tabla_cliente . '.nombre as nombre_cliente',
            // $tabla_cliente . '.apellido_paterno as apellido_paterno_cliente',
            // $tabla_cliente . '.apellido_materno as apellido_materno_cliente',

            // $estatus_venta . '.nombre as estatus_venta',

            // $tabla_user . '.nombre as nombre_vendedor',
            // $tabla_user . '.apellido_paterno as apellido_paterno_vendedor',
            // $tabla_user . '.apellido_materno as apellido_materno_vendedor',

            $unidades_costos . '.*',
            $tabla_unidad . '.motor',
            $tabla_unidad . '.transmision',
            $tabla_unidad . '.combustible',
            $tabla_unidad . '.vin',
            $tabla_unidad . '.precio_costo',
            $tabla_unidad . '.precio_venta',
            $tabla_unidad . '.id_estado',
            $tabla_unidad . '.kilometraje',
            $tabla_unidad . '.numero_cilindros',
            $tabla_unidad . '.capacidad',
            $tabla_unidad . '.numero_puertas',
            $tabla_unidad . '.serie_corta',
            $tabla_unidad . '.id_ubicacion',
            $tabla_unidad . '.id_ubicacion_llaves',
            $tabla_unidad . '.numero_economico',
            $tabla_unidad . '.usuario_recibe',
            $tabla_unidad . '.fecha_recepcion',
            $tabla_unidad . '.n_produccion',
            $tabla_unidad . '.n_remision',
            $tabla_unidad . '.n_folio_remision',
            $tabla_unidad . '.pdto',
            $tabla_unidad . '.folio_pedido',
            $tabla_unidad . '.vestidura_id',
            $tabla_unidad . '.n_motor'
        )->from($tabla_unidad);


        $query->join($catalogo_marcas, $catalogo_marcas . '.id', '=', $tabla_unidad . '.marca_id');
        $query->join($catalogo_anio, $catalogo_anio . '.id', '=', $tabla_unidad . '.anio_id');
        $query->join($catalogo_modelos, $catalogo_modelos . '.id', '=', $tabla_unidad . '.modelo_id');
        $query->join($catalogo_colores, $catalogo_colores . '.id', '=', $tabla_unidad . '.color_id');
        $query->join($unidades_costos, $unidades_costos . '.unidad_id', '=', $tabla_unidad . '.id');
        // $query->join($tabla_cliente, $tabla_cliente . '.id', '=', $preventa_unidad . '.id_cliente');
        // $query->join($tabla_user, $tabla_user . '.id', '=', $preventa_unidad . '.vendedor_id');
        // $query->join($estatus_venta, $estatus_venta . '.id', '=', $preventa_unidad . '.id_estatus');
        if (isset($parametros['id'])) {
            $query->where($tabla_unidad . '.id', '=', $parametros['id']);
        }

        //nuevo semi-nuevo
        if (isset($parametros['id_estado'])) {
            $query->where($tabla_unidad . '.id_estado', '=', $parametros['id_estado']);
        }

        return $query->get();
    }

    public function getAll_bk($parametros)
    {
        $preventa_unidad = VentasAutosModel::getTableName();
        $tabla_cliente = ClientesModel::getTableName();
        $tabla_user = User::getTableName();
        $tabla_unidad = UnidadesModel::getTableName();

        $catalogo_marcas = CatalogoMarcasModel::getTableName();
        $catalogo_anio = CatalogoAnioModel::getTableName();
        $catalogo_modelos = CatModelosModel::getTableName();
        $catalogo_colores = CatalogoColoresModel::getTableName();
        $unidades_costos = RecepcionUnidadesCostosModel::getTableName();

        $estatus_venta = EstatusVentaModel::getTableName();

        $query = $this->modelo->select(
            $preventa_unidad . '.id as id_salida_unidad',
            $preventa_unidad . '.id_tipo_auto',

            $tabla_unidad . '.id AS id_unidad',
            $tabla_unidad . '.unidad_descripcion',
            $catalogo_anio . '.id AS anio_id',
            $catalogo_anio . '.nombre AS nombre_anio',
            $catalogo_marcas . '.id AS marca_id',
            $catalogo_marcas . '.nombre AS nombre_marca',
            $catalogo_modelos . '.id AS modelo_id',
            $catalogo_modelos . '.nombre AS nombre_modelo',
            $catalogo_colores . '.id AS color_id',
            $catalogo_colores . '.nombre AS nombre_color',
            $catalogo_colores . '.clave AS clave',
            $tabla_cliente . '.nombre as nombre_cliente',
            $tabla_cliente . '.apellido_paterno as apellido_paterno_cliente',
            $tabla_cliente . '.apellido_materno as apellido_materno_cliente',

            $estatus_venta . '.nombre as estatus_venta',

            $tabla_user . '.nombre as nombre_vendedor',
            $tabla_user . '.apellido_paterno as apellido_paterno_vendedor',
            $tabla_user . '.apellido_materno as apellido_materno_vendedor',

            $unidades_costos . '.*',
            $tabla_unidad . '.motor',
            $tabla_unidad . '.transmision',
            $tabla_unidad . '.combustible',
            $tabla_unidad . '.vin',
            $tabla_unidad . '.precio_costo',
            $tabla_unidad . '.precio_venta',
            $tabla_unidad . '.id_estado',
            $tabla_unidad . '.kilometraje',
            $tabla_unidad . '.numero_cilindros',
            $tabla_unidad . '.capacidad',
            $tabla_unidad . '.numero_puertas',
            $tabla_unidad . '.serie_corta',
            $tabla_unidad . '.id_ubicacion',
            $tabla_unidad . '.id_ubicacion_llaves',
            $tabla_unidad . '.numero_economico',
            $tabla_unidad . '.usuario_recibe',
            $tabla_unidad . '.fecha_recepcion',
            $tabla_unidad . '.n_produccion',
            $tabla_unidad . '.n_remision',
            $tabla_unidad . '.n_folio_remision',
            $tabla_unidad . '.pdto',
            $tabla_unidad . '.folio_pedido',
            $tabla_unidad . '.vestidura_id',
            $tabla_unidad . '.n_motor'
        )->from($preventa_unidad);

        $query->join($tabla_unidad, $preventa_unidad . '.id_unidad', '=', $tabla_unidad . '.id');
        $query->join($catalogo_marcas, $catalogo_marcas . '.id', '=', $tabla_unidad . '.marca_id');
        $query->join($catalogo_anio, $catalogo_anio . '.id', '=', $tabla_unidad . '.anio_id');
        $query->join($catalogo_modelos, $catalogo_modelos . '.id', '=', $tabla_unidad . '.modelo_id');
        $query->join($catalogo_colores, $catalogo_colores . '.id', '=', $tabla_unidad . '.color_id');
        $query->join($unidades_costos, $unidades_costos . '.unidad_id', '=', $tabla_unidad . '.id');
        $query->join($tabla_cliente, $tabla_cliente . '.id', '=', $preventa_unidad . '.id_cliente');
        $query->join($tabla_user, $tabla_user . '.id', '=', $preventa_unidad . '.vendedor_id');
        $query->join($estatus_venta, $estatus_venta . '.id', '=', $preventa_unidad . '.id_estatus');

        if (isset($parametros['id'])) {
            $query->where($preventa_unidad . '.id', '=', $parametros['id']);
        }

        if (isset($parametros['id_tipo_auto'])) {
            $query->where($preventa_unidad . '.id_tipo_auto', '=', $parametros['id_tipo_auto']);
        }

        return $query->dd();
    }

    public function lastRecord()
    {
        return $this->modelo
            ->orderBy(UnidadesModel::ID, 'desc')
            ->first();
    }

    public function actualizar($request, $id)
    {
        $modelo = $this->modelo->findOrFail($id);
        $this->massUpdateWhereId(UnidadesModel::ID, $modelo->id, $request->only(
            [
                UnidadesModel::USUARIO_RECIBE,
                UnidadesModel::FECHA_RECEPCION,
                UnidadesModel::ID_ESTADO,
                UnidadesModel::MARCA_ID,
                UnidadesModel::UNIDAD_DESCRIPCION,
                UnidadesModel::MODELO_ID,
                UnidadesModel::ANIO_ID,
                UnidadesModel::COLOR_ID,
                UnidadesModel::VESTIDURA_ID,
                UnidadesModel::NUMERO_PUERTAS,
                UnidadesModel::COMBUSTIBLE,
                UnidadesModel::N_MOTOR,
                UnidadesModel::MOTOR,
                UnidadesModel::TRANSMISION,
                UnidadesModel::NUMERO_CILINDROS,
                UnidadesModel::CAPACIDAD,
                UnidadesModel::VIN,
                UnidadesModel::SERIE_CORTA,
                UnidadesModel::NUMERO_ECONOMICO,
                UnidadesModel::ID_UBICACION,
                UnidadesModel::ID_UBICACION_LLAVES,
                UnidadesModel::N_PRODUCCION,
                UnidadesModel::N_REMISION,
                UnidadesModel::N_FOLIO_REMISION,
                UnidadesModel::PDTO,
                UnidadesModel::FOLIO_PEDIDO
            ]
        ));

        $modelo_costos = $this->modeloCostos->findOrFail($id);
        return $this->servicioRecepcionCostos->massUpdateWhereId(
            RecepcionUnidadesCostosModel::ID,
            $modelo_costos->id,
            $request->only(
                [
                    RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO,
                    RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA,
                    RecepcionUnidadesCostosModel::OTROS_GASTOS,
                    RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO,
                    RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA,
                    RecepcionUnidadesCostosModel::GASTOS_ACONDI,
                    RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO,
                    RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA,
                    RecepcionUnidadesCostosModel::AP_FONDOS_PUB,
                    RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO,
                    RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA,
                    RecepcionUnidadesCostosModel::AP_PROG_CIVIL,
                    RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO,
                    RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA,
                    RecepcionUnidadesCostosModel::CUOTA_AMDA,
                    RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO,
                    RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA,
                    RecepcionUnidadesCostosModel::CUOTA_COPARMEX,
                    RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO,
                    RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA,
                    RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD,
                    RecepcionUnidadesCostosModel::BONIFICACION_COSTO,
                    RecepcionUnidadesCostosModel::BONIFICACION_VENTA,
                    RecepcionUnidadesCostosModel::SUBTOTAL_COSTO,
                    RecepcionUnidadesCostosModel::SUBTOTAL_VENTA,
                    RecepcionUnidadesCostosModel::IVA_COSTO,
                    RecepcionUnidadesCostosModel::IVA_VENTA,
                    RecepcionUnidadesCostosModel::TOTAL_COSTO,
                    RecepcionUnidadesCostosModel::TOTAL_VENTA,
                    RecepcionUnidadesCostosModel::UNIDAD_ID
                ]
            )
        );
    }
}
