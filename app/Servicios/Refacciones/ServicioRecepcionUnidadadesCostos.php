<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\RecepcionUnidadesCostosModel;
use App\Servicios\Core\ServicioDB;

class ServicioRecepcionUnidadadesCostos extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'recepcion unidades costos';
        $this->modelo = new RecepcionUnidadesCostosModel();
    }

    public function getReglasGuardar()
    {
        return [
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => 'required',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => 'required',
            RecepcionUnidadesCostosModel::OTROS_GASTOS => 'required',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO => 'required',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA => 'required',
            RecepcionUnidadesCostosModel::GASTOS_ACONDI => 'required',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO => 'required',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA => 'required',
            RecepcionUnidadesCostosModel::AP_FONDOS_PUB => 'required',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO => 'required',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA => 'required',
            RecepcionUnidadesCostosModel::AP_PROG_CIVIL => 'required',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO => 'required',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA => 'required',
            RecepcionUnidadesCostosModel::CUOTA_AMDA => 'required',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO => 'required',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA => 'required',
            RecepcionUnidadesCostosModel::CUOTA_COPARMEX => 'required',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO => 'required',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA => 'required',
            RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD => 'required',
            RecepcionUnidadesCostosModel::BONIFICACION_COSTO => 'required',
            RecepcionUnidadesCostosModel::BONIFICACION_VENTA => 'required',
            RecepcionUnidadesCostosModel::SUBTOTAL_COSTO => 'required',
            RecepcionUnidadesCostosModel::SUBTOTAL_VENTA => 'required',
            RecepcionUnidadesCostosModel::IVA_COSTO => 'required',
            RecepcionUnidadesCostosModel::IVA_VENTA => 'required',
            RecepcionUnidadesCostosModel::TOTAL_COSTO => 'required',
            RecepcionUnidadesCostosModel::TOTAL_VENTA => 'required',
            RecepcionUnidadesCostosModel::UNIDAD_ID => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => 'required',
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => 'required',
            RecepcionUnidadesCostosModel::OTROS_GASTOS => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_ACONDI => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_FONDOS_PUB => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::AP_PROG_CIVIL => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_AMDA => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_COPARMEX => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::BONIFICACION_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::SUBTOTAL_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::IVA_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::IVA_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_COSTO => 'nullable',
            RecepcionUnidadesCostosModel::TOTAL_VENTA => 'nullable',
            RecepcionUnidadesCostosModel::UNIDAD_ID => 'required'
        ];
    }

    public function guardar($id, $request)
    {
        return $this->crear([
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => isset($request->valor_unidad) ? $request->valor_unidad : null,
            RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => isset($request->valor_unidad_venta) ? $request->valor_unidad_venta : null,
            RecepcionUnidadesCostosModel::OTROS_GASTOS => isset($request->otros_gastos) ? $request->otros_gastos : null,
            RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO => isset($request->equipo_base_costo) ? $request->equipo_base_costo : null,
            RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA => isset($request->equipo_base_venta) ? $request->equipo_base_venta : null,
            RecepcionUnidadesCostosModel::GASTOS_ACONDI => isset($request->gastos_acondi) ? $request->gastos_acondi : null,
            RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO => isset($request->seg_traslado_costo) ? $request->seg_traslado_costo : null,
            RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA => isset($request->seg_traslado_venta) ? $request->seg_traslado_venta : null,
            RecepcionUnidadesCostosModel::AP_FONDOS_PUB => isset($request->ap_fondos_pub) ? $request->ap_fondos_pub : null,
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO => isset($request->impuesto_import_costo) ? $request->impuesto_import_costo : null,
            RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA => isset($request->impuesto_import_venta) ? $request->impuesto_import_venta : null,
            RecepcionUnidadesCostosModel::AP_PROG_CIVIL => isset($request->ap_prog_civil) ? $request->ap_prog_civil : null,
            RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO => isset($request->fletes_import_costo) ? $request->fletes_import_costo : null,
            RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA => isset($request->fletes_import_venta) ? $request->fletes_import_venta : null,
            RecepcionUnidadesCostosModel::CUOTA_AMDA => isset($request->cuota_amda) ? $request->cuota_amda : null,
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO => isset($request->gastos_traslado_costo) ? $request->gastos_traslado_costo : null,
            RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA => isset($request->gastos_traslado_venta) ? $request->gastos_traslado_venta : null,
            RecepcionUnidadesCostosModel::CUOTA_COPARMEX => isset($request->cuota_coparmex) ? $request->cuota_coparmex : null,
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO => isset($request->deduccion_ford_costo) ? $request->deduccion_ford_costo : null,
            RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA => isset($request->deduccion_ford_venta) ? $request->deduccion_ford_venta : null,
            RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD => isset($request->cuota_asociacion_ford) ? $request->cuota_asociacion_ford : null,
            RecepcionUnidadesCostosModel::BONIFICACION_COSTO => isset($request->bonificacion_costo) ? $request->bonificacion_costo : null,
            RecepcionUnidadesCostosModel::BONIFICACION_VENTA => isset($request->bonificacion_venta) ? $request->bonificacion_venta : null,
            RecepcionUnidadesCostosModel::SUBTOTAL_COSTO => isset($request->subtota_costo) ? $request->subtota_costo : null,
            RecepcionUnidadesCostosModel::SUBTOTAL_VENTA => isset($request->subtota_venta) ? $request->subtota_venta : null,
            RecepcionUnidadesCostosModel::IVA_COSTO => isset($request->iva_costo) ? $request->iva_costo : null,
            RecepcionUnidadesCostosModel::IVA_VENTA => isset($request->iva_venta) ? $request->iva_venta : null,
            RecepcionUnidadesCostosModel::TOTAL_COSTO => isset($request->total_costo) ? $request->total_costo : null,
            RecepcionUnidadesCostosModel::TOTAL_VENTA => isset($request->total_venta) ? $request->total_venta : null,
            RecepcionUnidadesCostosModel::UNIDAD_ID => $id
        ]);
    }
}
