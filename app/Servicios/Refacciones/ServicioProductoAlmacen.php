<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\ProductosModel;

class ServicioProductoAlmacen extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'producto_almacen';
        $this->modelo = new ProductoAlmacenModel();
    }

    public function getReglasGuardar()
    {
        return [
            ProductoAlmacenModel::ALMACEN_ID => 'required|exists:almacen,id',
            ProductoAlmacenModel::PRODUCTO_ID => 'required|exists:almacen,id',
            ProductoAlmacenModel::NO_IDENTIFICACION => 'nullable|exists:producto,no_identificacion',
            ProductoAlmacenModel::CANTIDAD => 'required|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ProductoAlmacenModel::ALMACEN_ID => 'required|exists:almacen,id',
            ProductoAlmacenModel::PRODUCTO_ID => 'required|exists:almacen,id',
            ProductoAlmacenModel::NO_IDENTIFICACION => 'nullable|exists:producto,no_identificacion',
            ProductoAlmacenModel::CANTIDAD => 'required|numeric',
        ];
    }

    public function getReglasCantidadProducto()
    {
        return [
            ProductoAlmacenModel::ALMACEN_ID => 'required|exists:almacen,id',
            ProductoAlmacenModel::PRODUCTO_ID => 'required|exists:producto,id',
        ];
    }


    public function getCantidadAlmByProducto(array $data)
    {
        return $this->modelo->with([
            ProductoAlmacenModel::REL_ALMACEN
        ])->where(ProductoAlmacenModel::ALMACEN_ID, $data[ProductoAlmacenModel::ALMACEN_ID])
            ->where(ProductoAlmacenModel::PRODUCTO_ID, $data[ProductoAlmacenModel::PRODUCTO_ID])->first();
    }

    public function descontarAlmacen(array $data)
    {
        $producto_id = $data['id'];
        $cantidad_descontar = $data[ProductoAlmacenModel::CANTIDAD];
        $producto = $this->modelo->where(ProductoAlmacenModel::PRODUCTO_ID, $producto_id)->first();
        if(isset($producto) && $producto){
            if ($cantidad_descontar > $producto->cantidad) {
                throw new ParametroHttpInvalidoException([
                    'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $producto_id])
                ]);
            } else {
                return $this->massUpdateWhereId(ProductoAlmacenModel::ID, $producto->id, [
                    ProductoAlmacenModel::CANTIDAD => $producto->cantidad - $cantidad_descontar
                ]);
            }
        } else{
            throw new ParametroHttpInvalidoException([
                'producto' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => $producto_id])
            ]);
        }

    }
    public function getByAlmacenProductoWhere($params)
    {
        return $this->modelo
        ->where(ProductoAlmacenModel::PRODUCTO_ID, $params['producto_id'])
        ->where(ProductoAlmacenModel::ALMACEN_ID, $params['almacen_id'])
        ->first();
    }
}
