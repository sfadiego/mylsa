<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\UbicacionLLavesModel;
use App\Servicios\Core\ServicioDB;

class ServicioUbicacionLLaves extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'ubicacion llaves';
        $this->modelo = new UbicacionLLavesModel();
    }

    public function getReglasGuardar()
    {
        return [
            UbicacionLLavesModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            UbicacionLLavesModel::NOMBRE => 'required'
        ];
    }
}
