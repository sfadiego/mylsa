<?php

namespace App\Servicios\CuentasPorPagar;

use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorPagar\AbonosPorPagarModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Models\Refacciones\FoliosModel;
use App\Servicios\Polizas\ServicioMovimientos;
use App\Servicios\Refacciones\ServicioCurl;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ServicioAbonoPorPagar extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'abonos';
        $this->modelo = new AbonosPorPagarModel();
        $this->modelo_cuentas_pagar = new CuentasPorPagarModel();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
        $this->servicioMovimientos = new ServicioMovimientos();
        $this->servicioCurl = new ServicioCurl();
    }

    public function getReglasGuardar()
    {
        return [
            AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AbonosPorPagarModel::TIPO_ABONO_ID => 'required|numeric',
            AbonosPorPagarModel::TIPO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            AbonosPorPagarModel::ESTATUS_ABONO_ID => 'required|exists:cat_estatus_abono,id',
            AbonosPorPagarModel::FECHA_VENCIMIENTO => 'required|date',
            AbonosPorPagarModel::TOTAL_ABONO => 'required|numeric',
            AbonosPorPagarModel::FECHA_PAGO => 'nullable|date',
            AbonosPorPagarModel::TOTAL_PAGO => 'nullable|numeric',
            AbonosPorPagarModel::CFDI_ID => 'required|exists:catalogo_cfdi,id',
            AbonosPorPagarModel::CAJA_ID => 'required|exists:catalogo_caja,id'

        ];
    }
    public function getReglasUpdate()
    {
        return [
            AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AbonosPorPagarModel::TIPO_ABONO_ID => 'required|numeric',
            AbonosPorPagarModel::TIPO_PAGO_ID => 'required|exists:cat_tipo_pago,id',
            AbonosPorPagarModel::ESTATUS_ABONO_ID => 'required|exists:cat_estatus_abono,id',
            AbonosPorPagarModel::FECHA_VENCIMIENTO => 'nullable|date',
            AbonosPorPagarModel::TOTAL_ABONO => 'nullable|numeric',
            AbonosPorPagarModel::FECHA_PAGO => 'required|date',
            AbonosPorPagarModel::TOTAL_PAGO => 'required|numeric',
            AbonosPorPagarModel::DIAS_MORATORIOS => 'nullable|numeric',
            AbonosPorPagarModel::MONTO_MORATORIO => 'nullable|numeric',
            AbonosPorPagarModel::CFDI_ID => 'required|exists:catalogo_cfdi,id',
            AbonosPorPagarModel::CAJA_ID => 'required|exists:catalogo_caja,id'
        ];
    }

    public function getAbonosPorPagar($params)
    {
        $query = DB::table(AbonosPorPagarModel::getTableName())
            ->select(
                'abonos_por_pagar.*',
                'cat_tipo_pago.nombre as tipo_pago',
                'cat_tipo_abono.nombre as tipo_abono',
                'catalogo_cfdi.clave as clave_cfdi',
                'catalogo_cfdi.descripcion as descripcion_cfdi',
                'cat_estatus_abono.nombre as estatus_abono'
            )
            ->leftJoin('cat_tipo_pago', 'cat_tipo_pago.id', '=', 'abonos_por_pagar.tipo_pago_id')
            ->leftJoin('cat_tipo_abono', 'cat_tipo_abono.id', '=', 'abonos_por_pagar.tipo_abono_id')
            ->leftjoin('catalogo_cfdi', 'catalogo_cfdi.id', '=', 'abonos_por_pagar.cfdi_id')
            ->leftJoin('cat_estatus_abono', 'cat_estatus_abono.id', '=', 'abonos_por_pagar.estatus_abono_id');
        if (isset($params['orden_entrada_id']) && $params['orden_entrada_id']) {
            $query->where(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID, $params['orden_entrada_id']);
        }
        if (isset($params['tipo_abono_id']) && $params['tipo_abono_id']) {
            $query->where(AbonosPorPagarModel::TIPO_ABONO_ID, $params['tipo_abono_id']);
        }
        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $query->where(AbonosPorPagarModel::ESTATUS_ABONO_ID, $params['estatus_abono_id']);
        }
        $query->orderBy(AbonosPorPagarModel::getTableName() . '.' . AbonosPorPagarModel::ID, 'ASC');
        return $query->get();
    }

    public function getById($id)
    {
        return $this->modelo
            ->select(
                'abonos_por_pagar.*',
                'cat_tipo_pago.nombre as tipo_pago',
                'cat_tipo_abono.nombre as tipo_abono',
                'catalogo_cfdi.clave as clave_cfdi',
                'catalogo_cfdi.descripcion as descripcion_cfdi',
                'cat_estatus_abono.nombre as estatus_abono'
            )
            ->leftJoin('cat_tipo_pago', 'cat_tipo_pago.id', '=', 'abonos_por_pagar.tipo_pago_id')
            ->leftJoin('cat_tipo_abono', 'cat_tipo_abono.id', '=', 'abonos_por_pagar.tipo_abono_id')
            ->leftjoin('catalogo_cfdi', 'catalogo_cfdi.id', '=', 'abonos_por_pagar.cfdi_id')
            ->leftJoin('cat_estatus_abono', 'cat_estatus_abono.id', '=', 'abonos_por_pagar.estatus_abono_id')
            ->find($id);
    }

    public function crearAbonosByCuentaId(int $cuentas_por_pagar_id)
    {
        $cuentas_pagar = $this->modelo_cuentas_pagar->find($cuentas_por_pagar_id);
        $tipo_forma_pago_id = $cuentas_pagar->tipo_forma_pago_id;

        if ($tipo_forma_pago_id == TipoFormaPagoModel::FORMA_CREDITO) {
            $enganche = $cuentas_pagar->enganche;
            $cantidad_abonos = $this->modelo_plazo_credito->find($cuentas_pagar->plazo_credito_id)->cantidad_mes;
            if ($enganche >= 1) {
                $pago_enganche = [
                    AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => $cuentas_pagar->id,
                    AbonosPorPagarModel::TIPO_ABONO_ID => CatTipoAbonoModel::ENGANCHE,
                    AbonosPorPagarModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
                    AbonosPorPagarModel::FECHA_VENCIMIENTO => $cuentas_pagar->fecha,
                    AbonosPorPagarModel::TOTAL_ABONO => $cuentas_pagar->enganche
                ];
                $this->modelo->create($pago_enganche);
            }

            for ($num_abono = 1; $num_abono <= $cantidad_abonos; $num_abono++) {
                $abono = ($cuentas_pagar->importe - $cuentas_pagar->enganche) / $cantidad_abonos;
                $interesAbono = ($abono * $cuentas_pagar->tasa_interes) / 100;
                $totalAbono = $abono + $interesAbono;
                $pago_abono = [
                    AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => $cuentas_pagar->id,
                    AbonosPorPagarModel::TIPO_ABONO_ID => CatTipoAbonoModel::ABONO,
                    AbonosPorPagarModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
                    AbonosPorPagarModel::FECHA_VENCIMIENTO => Carbon::createFromDate($cuentas_pagar->fecha)->addMonths($num_abono)->format('Y-m-d'),
                    AbonosPorPagarModel::TOTAL_ABONO => $totalAbono
                ];
                $abonos[] = $this->modelo->create($pago_abono);
            }
            return $abonos;
        } else {
            return $this->crear_abonos_contado($cuentas_pagar);
        }
    }

    private function crear_abonos_contado($cuentas_pagar)
    {
        $pago_contado = [
            AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => $cuentas_pagar->id,
            AbonosPorPagarModel::TIPO_ABONO_ID => CatTipoAbonoModel::UN_SOLO_PAGO,
            AbonosPorPagarModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
            AbonosPorPagarModel::FECHA_VENCIMIENTO => $cuentas_pagar->fecha,
            AbonosPorPagarModel::TOTAL_ABONO => $cuentas_pagar->total
        ];
        return $this->modelo->create($pago_contado);
    }

    public function actualiza_abonos($request, $id)
    {
        // $data_abono = parent::getById($id);
        // $dias_moratorios  = null;
        // $date_pago = new Carbon($request->get(AbonosPorPagarModel::FECHA_PAGO));
        // $date_vencimiento = new Carbon($data_abono->fecha_vencimiento);
        // if ($date_pago > $date_vencimiento) {
        //     $dias_moratorios = $date_vencimiento->diffInDays($date_pago);
        //     $request->merge([AbonosPorPagarModel::DIAS_MORATORIOS => $dias_moratorios]);
        // }
        // $request->merge([AbonosPorPagarModel::TOTAL_ABONO => $request->get(AbonosPorPagarModel::TOTAL_PAGO)]);
        $estatus_cuenta = false;
        $request->merge([AbonosPorPagarModel::TOTAL_ABONO => $request->get(AbonosPorPagarModel::TOTAL_PAGO)]);
        $update = $this->massUpdateWhereId(AbonosPorPagarModel::ID, $id, $request->all());

        if ($request->get(AbonosPorPagarModel::TIPO_ABONO_ID) != CatTipoAbonoModel::UN_SOLO_PAGO) {
            $cuentas_por_pagar_id = $request->get(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID);
            $cuentas_pagar = $this->modelo_cuentas_pagar->find($cuentas_por_pagar_id);
            $total_cuenta = $cuentas_pagar->total;
            $abonos_pendientes = $this->getAbonosPendientes($cuentas_por_pagar_id);

            $monto = $this->getMontoAbonadoByCuentaId($cuentas_por_pagar_id);
            $monto_pagado = isset($monto) ? $monto->monto_pagado : null;
            $saldo_actual = $total_cuenta - $monto_pagado;

            if ($abonos_pendientes && count($abonos_pendientes) > 0) {

                $contar_abonos_pendientes = count($abonos_pendientes); // Se obtiene el total de abonos pendientes menos el que se va realizar
                $interes_abono = ((($abonos_pendientes[0]->total_abono) * $cuentas_pagar->tasa_interes) / 100); // Obtenemos el interes que se aplica a los abonos
                $abono_sin_interes = ($abonos_pendientes[0]->total_abono - $interes_abono) * $contar_abonos_pendientes; // Restamos el interes a los abonos y lo multiplicamos por los abonos pendientes
                $liquidar_saldo = $abono_sin_interes +  $abonos_pendientes[0]->total_abono; // Suma de los abonos pendientes sin el interes + el abono mensual actual.

                if (round($request->get(AbonosPorPagarModel::TOTAL_PAGO), 2) >= round($liquidar_saldo, 2)) {
                    $estatus_cuenta = true;
                    $monto_abono_pendiente = 0;
                    $estatus_abono = CatEstatusAbonoModel::PAGADO;

                    $cuentas_pagar = $this->modelo_cuentas_pagar->find($cuentas_por_pagar_id);
                    $cuentas_pagar->total = $monto_pagado;
                    $cuentas_pagar->intereses = $monto_pagado - $cuentas_pagar->importe;
                    $cuentas_pagar->save();
                } else {
                    $monto_abono_pendiente = $saldo_actual / count($abonos_pendientes);
                    $estatus_abono = CatEstatusAbonoModel::PENDIENTE;
                }
            }

            if (round($saldo_actual, 2) == 0) {
                $estatus_cuenta = true;
                $monto_abono_pendiente = 0;
                $estatus_abono = CatEstatusAbonoModel::PAGADO;

                $cuentas_pagar = $this->modelo_cuentas_pagar->find($cuentas_por_pagar_id);
                $cuentas_pagar->total = $monto_pagado;
                $cuentas_pagar->intereses = $monto_pagado - $cuentas_pagar->importe;
                $cuentas_pagar->save();
            }

            foreach ($abonos_pendientes as $item) {
                parent::massUpdateWhereId('id', $item->id, [
                    AbonosPorPagarModel::TOTAL_ABONO => round($monto_abono_pendiente, 2),
                    AbonosPorPagarModel::ESTATUS_ABONO_ID => $estatus_abono
                ]);
            }

            $abonos_morosos = $this->modelo
                ->where(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID, $cuentas_por_pagar_id)
                ->where(AbonosPorPagarModel::TIPO_ABONO_ID, CatTipoAbonoModel::ABONO)
                ->where(AbonosPorPagarModel::ESTATUS_ABONO_ID, CatEstatusAbonoModel::MORATORIO)
                ->get();

            if ($abonos_morosos && count($abonos_morosos) >= 1) {
                $cuentas_pagar = $this->modelo_cuentas_cobrar->find($cuentas_por_pagar_id);
                $cuentas_pagar->estatus_cuenta_id = EstatusCuentaModel::ESTATUS_ATRASADO;
                $cuentas_pagar->save();
            } else {
                $cuentas_pagar = $this->modelo_cuentas_pagar->find($cuentas_por_pagar_id);
                $cuentas_pagar->estatus_cuenta_id = $estatus_cuenta == true ?  EstatusCuentaModel::ESTATUS_LIQUIDADO : EstatusCuentaModel::ESTATUS_PROCESO;
                $cuentas_pagar->save();
            }
        }

        return $update;
    }

    public function getAbonosPendientes($cuentas_por_pagar_id)
    {
        return $this->modelo
            ->where(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID, $cuentas_por_pagar_id)
            ->where(AbonosPorPagarModel::ESTATUS_ABONO_ID, CatEstatusAbonoModel::PENDIENTE)
            ->get();
    }

    public function getMontoAbonadoByCuentaId($cuentas_por_pagar_id)
    {
        $tabla_abonos = AbonosPorPagarModel::getTableName();
        return $this->modelo
            ->select(DB::raw('sum(' . $tabla_abonos . '.total_pago) as monto_pagado'))
            ->where(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID, $cuentas_por_pagar_id)
            ->first();
    }

    public function getVerificaAbonosPendientes($params)
    {
        $abonos_pendientes = $this->modelo
            ->where(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID, $params['cuenta_por_pagar_id'])
            ->where(AbonosPorPagarModel::TIPO_ABONO_ID, CatTipoAbonoModel::ABONO)
            ->whereNotIn(AbonosPorPagarModel::ESTATUS_ABONO_ID, [CatEstatusAbonoModel::PAGADO])
            ->get();

        $cuentas_pagar = false;
        foreach ($abonos_pendientes as $abono) {
            $date_pago = new Carbon($params[AbonosPorPagarModel::FECHA_ACTUAL]);
            $date_vencimiento = new Carbon($abono->fecha_vencimiento);
            if ($date_pago > $date_vencimiento) {
                $dias_moratorios = $date_vencimiento->diffInDays($date_pago);
                $porcentaje_interes_moratorio = 3;
                $monto_moratorio = (((($abono->total_abono * $porcentaje_interes_moratorio) / 100) / 30) * $dias_moratorios);

                $modelo = $this->modelo->find($abono->id);
                $modelo->estatus_abono_id = 2;
                $modelo->dias_moratorios = $dias_moratorios;
                $modelo->monto_moratorio = $monto_moratorio;
                $saveAbono = $modelo->save();

                if ($saveAbono) {
                    $cuentas_pagar = $this->modelo_cuentas_pagar->find($abono->cuenta_por_pagar_id);
                    $cuentas_pagar->estatus_cuenta_id = EstatusCuentaModel::ESTATUS_ATRASADO;
                    $cuentas_pagar->save();
                }
            }
        }
        return $cuentas_pagar;
    }

    public function getVerificaDiasParaPago($params)
    {

        $facturas_abonar = [];

        $abonos_proximos = $this->getAbonosProximos($params['cuenta_por_pagar_id']);
        foreach ($abonos_proximos as $abono) {
            $fecha_actual = new Carbon($params[AbonosPorPagarModel::FECHA_ACTUAL]);
            $date_vencimiento = new Carbon($abono->fecha_vencimiento);
            $dias = $date_vencimiento->diffInDays($fecha_actual);
            if ($dias == 7) {
                $abono['dias_faltantes'] = $dias;
                $facturas_abonar = $abono;
            } else if ($dias == 0) {
                $abono['dias_faltantes'] = $dias;
                $facturas_abonar = $abono;
            }
        }

        return $facturas_abonar;
    }

    private function getAbonosProximos($cuenta_por_pagar_id)
    {
        $tabla_cuentas = CuentasPorPagarModel::getTableName();
        $tabla_abonos = AbonosPorPagarModel::getTableName();
        $tabla_proveedores = ProveedorRefacciones::getTableName();
        $tabla_folios = FoliosModel::getTableName();

        return $this->modelo
            ->select(
                $tabla_abonos . '.' . AbonosPorPagarModel::TOTAL_ABONO,
                $tabla_abonos . '.' . AbonosPorPagarModel::FECHA_VENCIMIENTO,
                $tabla_cuentas . '.' . CuentasPorPagarModel::FECHA,
                $tabla_cuentas . '.' . CuentasPorPagarModel::TOTAL,
                $tabla_cuentas . '.' . CuentasPorPagarModel::CONCEPTO,
                $tabla_folios . '.' . FoliosModel::FOLIO,
                $tabla_proveedores . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE,
                $tabla_proveedores . '.' . ProveedorRefacciones::PROVEEDOR_NUMERO,
                $tabla_proveedores . '.' . ProveedorRefacciones::CORREO,
                $tabla_proveedores . '.' . ProveedorRefacciones::PROVEEDOR_CALLE,
                $tabla_proveedores . '.' . ProveedorRefacciones::PROVEEDOR_COLONIA,
                $tabla_proveedores . '.' . ProveedorRefacciones::PROVEEDOR_ESTADO
            )
            ->join($tabla_cuentas, $tabla_cuentas . '.' . CuentasPorPagarModel::ID, '=', $tabla_abonos . '.' . AbonosPorPagarModel::CUENTA_POR_PAGAR_ID)
            ->join($tabla_proveedores, $tabla_proveedores . '.' . ProveedorRefacciones::ID, '=', $tabla_cuentas . '.' . CuentasPorPagarModel::PROVEEDOR_ID)
            ->join($tabla_folios, $tabla_folios . '.' . FoliosModel::ID, '=', $tabla_cuentas . '.' . CuentasPorPagarModel::FOLIO_ID)

            ->where([
                AbonosPorPagarModel::CUENTA_POR_PAGAR_ID => $cuenta_por_pagar_id,
                AbonosPorPagarModel::TIPO_ABONO_ID => CatTipoAbonoModel::ABONO
            ])
            ->whereNotIn(AbonosPorPagarModel::ESTATUS_ABONO_ID, [CatEstatusAbonoModel::PAGADO])
            ->orderBy($tabla_abonos . '.' . AbonosPorPagarModel::ID, 'ASC')
            ->limit(2)

            ->get();
    }

    public function set_poliza()
    {

        $request = $this->servicioCurl->curlPost('https://planificadorempresarial.com/contabilidad_dms/index.php/api/crear_poliza', [
            'fecha' => date('Y-m-d'),
            'poliza' => 'San_Juan_' . date('Y-m-d'),
            'concepto' => 'DmsSanJuan',
            'tipo' => 1,
            'id_departamento' => 1,
        ], false);
        return json_decode($request);
    }

    public function set_movimiento($request)
    {

        $cuentas_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];      
        $respuesta['abono'] = $this->servicioCurl->curlPost('https://planificadorempresarial.com/contabilidad_dms/index.php/api/crear_asiento', [
            'id_id_poliza' => $request['poliza_id'],
            'nombre' => 'San Juan DMS: ' . date('Y-m-d') . ' cuentas por pagar',
            'referencia' => $request['fecha_pago'],
            'concepto' => isset($cuentas_por_pagar->concepto) ? $cuentas_por_pagar->concepto : '',
            'total' => $request['total_pago'],
            'cuenta' => isset($cuentas_por_pagar->id_movimiento) ? $cuentas_por_pagar->id_movimiento : '',
            'tipo' => CatalogoCuentasModel::ABONO


        ], false);

        $respuesta['cargo'] = $this->servicioCurl->curlPost('https://planificadorempresarial.com/contabilidad_dms/index.php/api/crear_asiento', [
            'id_id_poliza' => $request['poliza_id'],
            'nombre' => 'San Juan DMS: ' . date('Y-m-d') . ' cuentas por pagar',
            'referencia' => $request['fecha_pago'],
            'concepto' => isset($cuentas_por_pagar->concepto) ? $cuentas_por_pagar->concepto : '',
            'total' => $request['total_pago'],
            'cuenta' => CatalogoCuentasModel::ABONO_BANORTE,
            'tipo' => CatalogoCuentasModel::CARGO

        ], false);

        return $respuesta;
    }
}
