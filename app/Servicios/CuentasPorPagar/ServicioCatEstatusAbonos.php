<?php

namespace App\Servicios\CuentasPorPagar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;

class ServicioCatEstatusAbonos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus abono';
        $this->modelo = new CatEstatusAbonoModel();
    }

  
}
