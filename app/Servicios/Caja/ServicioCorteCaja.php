<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\CorteCajaModel;
use App\Models\Caja\MovimientoCajaModel;
use App\Models\Caja\CatalogoCajaModel;
use App\Models\Caja\DetalleCorteModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Usuarios\User;
use DB;

class ServicioCorteCaja extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'corte de caja';
        $this->modelo = new CorteCajaModel();
    }

    public function getReglasGuardar()
    {
        return [
            CorteCajaModel::CAJA_ID => 'required|numeric',
            CorteCajaModel::MOVIMIENTO_CAJA_ID => 'required|numeric',
            CorteCajaModel::FECHA_TRANSACCION => 'date|required',
            CorteCajaModel::USUARIO_REGISTRO => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CorteCajaModel::CAJA_ID => 'required|numeric',
            CorteCajaModel::MOVIMIENTO_CAJA_ID => 'required|numeric',
            CorteCajaModel::FECHA_TRANSACCION => 'date|required',
            CorteCajaModel::USUARIO_REGISTRO => 'required|numeric'
        ];
    }

    public function getByUsuarioFecha($params)
    {
        return $this->modelo
            ->select(
                CorteCajaModel::getTableName() . '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO
            )
            ->join(User::getTableName(), User::getTableName() . '.' . User::ID, CorteCajaModel::USUARIO_REGISTRO)
            ->where(CorteCajaModel::USUARIO_REGISTRO, $params[CorteCajaModel::USUARIO_REGISTRO])
            ->where(CorteCajaModel::FECHA_TRANSACCION, $params[CorteCajaModel::FECHA_TRANSACCION])
            ->first();
    }
    public function getPolizas($params)
    {
        $consulta =  $this->modelo
            ->select(
                CorteCajaModel::getTableName() . '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO,
                MovimientoCajaModel::getTableName() . '.' . MovimientoCajaModel::NOMBRE . ' as MovimientoNombre',
                CatalogoCajaModel::getTableName() . '.' . CatalogoCajaModel::NOMBRE . ' as CajaNombre',
                CatTipoPagoModel::getTableName() . '.' . CatTipoPagoModel::NOMBRE . ' as TipoPago',
                DetalleCorteModel::getTableName() . '.' . DetalleCorteModel::TOTAL_CANTIDAD_CAJA,
                DetalleCorteModel::getTableName() . '.' . DetalleCorteModel::TOTAL_CANTIDAD_REPORTADA,
                DetalleCorteModel::getTableName() . '.' . DetalleCorteModel::FALTANTES,
                DetalleCorteModel::getTableName() . '.' . DetalleCorteModel::SOBRANTES
            )
            ->join(User::getTableName(), User::getTableName() . '.' . User::ID, CorteCajaModel::USUARIO_REGISTRO)
            ->join(MovimientoCajaModel::getTableName(), MovimientoCajaModel::getTableName() . '.' . MovimientoCajaModel::ID, CorteCajaModel::MOVIMIENTO_CAJA_ID)
            ->join(CatalogoCajaModel::getTableName(), CatalogoCajaModel::getTableName() . '.' . CatalogoCajaModel::ID, CorteCajaModel::CAJA_ID)
            ->join(DetalleCorteModel::getTableName(), DetalleCorteModel::getTableName() . '.' . DetalleCorteModel::CORTE_CAJA_ID, CorteCajaModel::getTableName() . '.' . CorteCajaModel::ID)
            ->join(CatTipoPagoModel::getTableName(), CatTipoPagoModel::getTableName() . '.' . CatTipoPagoModel::ID, DetalleCorteModel::TIPO_PAGO_ID);

        if (isset($params['caja_id']) && $params['caja_id']) {
            $consulta->where( CorteCajaModel::getTableName() . '.' . CorteCajaModel::CAJA_ID, $params['caja_id']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->whereBetween(CorteCajaModel::getTableName(). '.' . CorteCajaModel::FECHA_TRANSACCION, [$params['fecha_inicio'] . ' 00:00:00',$params['fecha_fin'] . ' 23:59:59']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->whereDate(CorteCajaModel::getTableName(). '.' . CorteCajaModel::FECHA_TRANSACCION, '>=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->whereDate(CorteCajaModel::getTableName(). '.' . CorteCajaModel::FECHA_TRANSACCION, '<=', $params['fecha_fin']);
        }
        $consulta->orderBy(CorteCajaModel::getTableName(). '.' . CorteCajaModel::FECHA_TRANSACCION,'DESC');
        return $consulta->get();
    }
    public function getByUsuario($params)
    {
        return $this->modelo
            ->select(
                CorteCajaModel::getTableName() . '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO,
                MovimientoCajaModel::getTableName() . '.' . MovimientoCajaModel::NOMBRE . ' as MovimientoNombre',
                CatalogoCajaModel::getTableName() . '.' . CatalogoCajaModel::NOMBRE . ' as CajaNombre'
            )
            ->join(User::getTableName(), User::getTableName() . '.' . User::ID, CorteCajaModel::USUARIO_REGISTRO)
            ->join(MovimientoCajaModel::getTableName(), MovimientoCajaModel::getTableName() . '.' . MovimientoCajaModel::ID, CorteCajaModel::MOVIMIENTO_CAJA_ID)
            ->join(CatalogoCajaModel::getTableName(), CatalogoCajaModel::getTableName() . '.' . CatalogoCajaModel::ID, CorteCajaModel::CAJA_ID)
            ->where(CorteCajaModel::USUARIO_REGISTRO, $params[CorteCajaModel::USUARIO_REGISTRO])
            ->get();
    }
}
