<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\PolizaContabilidadDetalleModel;

class PolizaContabilidadDetalle extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'polizas detalle contabilidad ';
        $this->modelo = new PolizaContabilidadDetalleModel();
    }

    public function getReglasGuardar()
    {
        return [
            PolizaContabilidadDetalleModel::CONCEPTO => 'required',
            PolizaContabilidadDetalleModel::COSTO => 'required',
            PolizaContabilidadDetalleModel::ID_CUENTA => 'required',
            PolizaContabilidadDetalleModel::ID_SUBCUENTA => 'required',
            PolizaContabilidadDetalleModel::ID_POLIZA => 'required',
            
        ];
    }
    public function getReglasUpdate()
    {
        return [
            PolizaContabilidadDetalleModel::CONCEPTO => 'required',
            PolizaContabilidadDetalleModel::COSTO => 'required',
            PolizaContabilidadDetalleModel::ID_CUENTA => 'required',
            PolizaContabilidadDetalleModel::ID_SUBCUENTA => 'required',
            PolizaContabilidadDetalleModel::ID_POLIZA => 'required',
        ];
    }
}
