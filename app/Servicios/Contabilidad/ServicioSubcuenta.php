<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\Subcuenta;

class ServicioSubcuenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo subcuentas';
        $this->modelo = new Subcuenta();
    }

    public function getReglasGuardar()
    {
        return [
            Subcuenta::NOMBRE_CUENTA => 'required',
            Subcuenta::ID_CUENTA => 'required',
            Subcuenta::NO_CUENTA => 'required',
            
        ];
    }
    public function getReglasUpdate()
    {
        return [
            Subcuenta::NOMBRE_CUENTA => 'nullable',
            Subcuenta::ID_CUENTA => 'nullable',
            Subcuenta::NO_CUENTA => 'nullable',
        ];
    }
}
