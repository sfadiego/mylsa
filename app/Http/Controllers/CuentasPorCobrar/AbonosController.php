<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Mail\EmailAbonoRealizado;
use App\Models\CuentasPorCobrar\AbonosModel;
use Illuminate\Support\Facades\Mail;

class AbonosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAbono();
    }

    public function showByIdOrdenEntrada(Request $request)
    {
        try {

            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function listadoAbonosByOrdenEntrada(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getVerificaAbonosPendientes(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVerificaAbonosPendientes());
            $modelo = $this->servicio->getVerificaAbonosPendientes($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

            $poliza = $this->servicio->set_poliza();
            if ($poliza && isset($poliza->id_id)) {
                $request->merge([AbonosModel::POLIZA_ID =>  $poliza->id_id]);
                //$data['modelo'] = $this->servicio->actualiza_abonos($request, $id);
                $data['movimientos'] = $this->servicio->set_movimiento($request->toArray()); 
            }

            //Mail::to($modelo->correo_electronico)->send(new EmailAbonoRealizado($modelo));
            return Respuesta::json($data, 200, $mensaje);

        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
