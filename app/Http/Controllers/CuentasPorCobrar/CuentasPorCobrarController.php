<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\Refacciones\ServicioCurl;
use App\Models\Usuarios\User as UsuarioModel;
use Illuminate\Http\Request;
use App\Mail\MylsaEmail;
use Illuminate\Support\Facades\Mail;

class CuentasPorCobrarController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioCuentaPorCobrar();
		$this->servicioAbono = new ServicioAbono();
		$this->servicioCurl = new ServicioCurl();
		$this->modelo_usuarios = new UsuarioModel();

	}

	public function index()
	{
		return Respuesta::json($this->servicio->getAll(), 200);
	}

	public function show($id)
	{
		$modelo = $this->servicio->getByIdCuenta($id);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function listado(Request $request)
	{
		try {
			return Respuesta::json(array('data' => $this->servicio->getAll($request->all())), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getKardexPagos(Request $request)
	{
		try {
			return Respuesta::json(array('data' => $this->servicio->getKardexPagos($request->all())), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getCierreCaja(Request $request)
	{
		try {
			return Respuesta::json($this->servicio->getCierreCaja($request->all()), 200);
		} catch (\Throwable $e) {
			dd($e);
			return Respuesta::error($e);
		}
	}

	public function showByFolioId($id)
	{
		$modelo = $this->servicio->getByFolioId($id);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function getVerificaCuentasMorosas(Request $request)
	{
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
		$cuentas_por_cobrar = $this->servicio->getCuentasEnProcesoPago();
		$data = [];
		if ($cuentas_por_cobrar && !empty($cuentas_por_cobrar)) {
			foreach ($cuentas_por_cobrar as $cuenta) {
				$params = [
					'cuenta_por_cobrar_id' => $cuenta->id,
					'fecha_actual' => $fecha_actual
				];
				$verificando = $this->servicioAbono->getVerificaAbonosPendientes($params);
				if (!empty($verificando)) {
					$data[] = $verificando;
				}
			}
		}
		return Respuesta::json($data, 200);
	}

	public function envioAvisoPago(Request $request)
	{
		
		$cuentas_por_cobrar = $this->servicio->getCuentasEnProcesoPago();
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
		$cobradores = $this->modelo_usuarios
        ->select(UsuarioModel::TELEFONO)
        ->where(UsuarioModel::ROL_ID, UsuarioModel::ROL_COBRANZA)->get();
		$data = [];
		if ($cuentas_por_cobrar && count($cuentas_por_cobrar) >= 1) {
			foreach ($cuentas_por_cobrar as $cuenta) {
				$params = [
					'cuenta_por_cobrar_id' => $cuenta->id,
					'fecha_actual' => $fecha_actual
				];
				$abonosAviso = $this->servicioAbono->getVerificaDiasParaPago($params);
				if (!empty($abonosAviso)) {
					$this->enviarSmsTexto($abonosAviso);
					Mail::to($abonosAviso->correo_electronico)->send(new MylsaEmail($abonosAviso));
					$this->enviarNotificacion($abonosAviso, $cobradores);
					$data[] = $abonosAviso;
				}
			}
		}
		return Respuesta::json($data, 200);
	}

	public function enviarSmsTexto($data) {
		$link = "https://sohexdms.net/cs/companycars/dms_companycars/caja/entradas/imprime_estado_cuenta?cuenta_id=".base64_encode($data->cuenta_por_cobrar_id);
		$mensaje = $data->nombre. ' la factura con folio '. $data->folio .' vence el '. date('d/m/y', strtotime($data->fecha_vencimiento)).' '.$link;
		$this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje', [
			'celular' => $data->telefono,
			'mensaje' => $this->eliminar_tildes($mensaje)
		], false);
	}
	
	public function enviarNotificacion($data, $cobradores) {
      
        if (!empty($cobradores)) {
            foreach ($cobradores as $cobrador) {
				$mensaje =  'La factura con folio '. $data->folio .' del cliente '. $data->nombre. ' vence el '. date('d/m/y', strtotime($data->fecha_vencimiento));
                $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
                    'celular' => $cobrador->telefono,
                    'mensaje' => $this->eliminar_tildes($mensaje),
                    'sucursal' => 'M2137'
                ], false);
            }
        }
	}
	
	public function setNotificacion(Request $request) {
		
		ParametrosHttpValidador::validar($request, $this->servicio->getNotificacion());

		$dataNotificacion = $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
			'celular' => $request->get('telefono'),
			'mensaje' => $this->eliminar_tildes($request->get('mensaje')),
			'sucursal' => 'M2137'
		], false);

		return Respuesta::json($dataNotificacion, 200);
          
    }
	public function registrarCxcServicio(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			$modelo = $this->servicio->procesarCuentasPorCobrar($request->all());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	private function eliminar_tildes($cadena){

		$cadena = ($cadena);
	
		//Ahora reemplazamos las letras
		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);
	
		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena );
	
		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena );
	
		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena );
	
		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena );
	
		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);
	
		return $cadena;
	}
}
