<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioPlazoCredito;
use Illuminate\Http\Request;

class CatPlazoCreditoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioPlazoCredito();
    }
}
