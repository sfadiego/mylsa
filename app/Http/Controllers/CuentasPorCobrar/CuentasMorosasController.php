<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioCuentasMorosas;

class CuentasMorosasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCuentasMorosas();
    }
}
