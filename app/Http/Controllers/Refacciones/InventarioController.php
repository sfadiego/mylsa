<?php

namespace App\Http\Controllers\Refacciones;


use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\InventarioModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioInventario;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use Illuminate\Http\Request;

class InventarioController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioInventario();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
    } 
    
    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
			$mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->update($request, $id);
            $listaInventariados = [];
            if ($modelo && $modelo->estatus_inventario_id == 2) { //marcado como finalizado
                $inventarios_productos = $this->servicio->getProductosInventario($id);
                foreach($inventarios_productos as $inventario) {
                    $listaInventariados[] = $this->servicioDesgloseProductos->actualizaStockByProducto([
                        'producto_id' => $inventario->producto_id
                    ]);
                }
            }
			return Respuesta::json($modelo, 200, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

    public function ultimoidinventario($id)
    {
        return Respuesta::json($this->servicio->getWhere(InventarioModel::ID, $id), 200);
    }
}
