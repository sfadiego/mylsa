<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioReVentasEstatus;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use Throwable;

class ReVentasEstatus extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioReVentasEstatus();
    }

    public function store(Request $request)
    {
        try {
            
            $insert = $this->servicio->storeReVentasEstatus($request);   
            return Respuesta::json($insert, 201);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
