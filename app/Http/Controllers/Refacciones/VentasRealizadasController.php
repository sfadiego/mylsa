<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPermisoVenta;
use App\Servicios\Refacciones\ServicioVentasRealizadas;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Refacciones\ServicioVentaProducto;
use App\Servicios\Refacciones\ServicioVentasServicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentasRealizadasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentasRealizadas();
        $this->servicioVentasPermiso = new ServicioPermisoVenta();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioVentasServicios = new ServicioVentasServicios();
    }

    public function storeVenta(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarSinFolio());
            $data = $this->servicio->registrarVenta($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasByFolioId($folio_id)
    {
        try {
            $data = $this->servicio->ventasByFolioId($folio_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function totalventa($venta_id)
    {
        try {
            $venta_productos = $this->servicioVentaProducto->totalVentaById(['venta_id' => $venta_id]);
            $venta_servicios = $this->servicioVentasServicios->totalservicio($venta_id);
            $total = (float) $venta_servicios + (float)$venta_productos;
            return Respuesta::json($total, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function finalizarVenta(Request $request, $id)
    {
        try {
            //TODO: revisar venta total- VENTA TOTAL 
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());

            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }

            $data = $this->servicio->finalizarVenta($id, $request);
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        try {
            $data = $this->servicio->getAll();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function listaVentasByStatus(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasListaVentaByStatus());
            $data = $this->servicio->getVentasByStatus($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->servicio->getById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasFiltrarWithDetails(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusqueda());
            $data = $this->servicio->filtrarPorFechas($request->all());
            $array_final = $this->servicio->calcularTotal($data);
            return Respuesta::json($array_final, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentas(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentas($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllVentas(Request $request)
    {
        try {
            $data = $this->servicio->getAllVentas($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentasDevoluciones(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentasDevoluciones($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getRefaccionesVentanillaTaller($numero_orden)
    {
        try {
            $data = $this->servicio->listaProductosTaller($numero_orden);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventaMpm(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCrearVentaMpm());
            $data = $this->servicio->crearVentaMpm($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function detalleVentampmById($venta_id)
    {
        try {
            $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($venta_id)->toArray();
            $venta_servicios = $this->servicioVentasServicios->ventaservicio($venta_id)->toArray();
            $new_array = array_merge($productos_venta, $venta_servicios);
            return Respuesta::json($new_array);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function FinalizarVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->finalizarVentaServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }


    #TODO: revisar si al abrir de nuevo afecta a kardex y otro flujo
    public function abrirVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->abrirOrdenServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getVentasPorMesByProducto(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVentasAgrupadasMes());

            $data = $this->servicio->getVentasAgrupadasMes($request->all());
            $listado = [];
            $totalVentas = 0;
            foreach ($data as $val) {
                $totalVentas = $totalVentas + $val->total_ventas;
                $val->año = $request->get('year');
                array_push($listado, $val);
            }
            $cantidad = $request->get('cantidad_meses') ? $request->get('cantidad_meses') : 12;
            $promedio = $totalVentas / $cantidad;

            $totales[] = [
                'total' => $totalVentas,
                'promedio' => $promedio,
                'year' => $request->get('year'),
            ];


            return Respuesta::json([
                'lista_ventas' => $listado,
                'totales' => $totales
            ], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
