<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioDevolucionVentas;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Http\Request;

class DevolucionVentasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDevolucionVentas();
    }

    public function storeDevolucion(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->generarDevolucion($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function devolucionByServicio(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [
                VentasRealizadasModel::NUMERO_ORDEN => 'required',
                DevolucionVentasModel::OBSERVACIONES => 'required',
                DevolucionVentasModel::VENDEDOR_ID => 'required',
            ]);
            $data = $this->servicio->devolucionServicio($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->servicio->getByIdDevolucion($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    // TODO: filtro de ventas por cliente
    public function showBycliente($cliente_id)
    {
        try {
            $data = $this->servicio->getByClienteId($cliente_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }


    public function showByVentaID($venta_id)
    {
        try {
            $data = $this->servicio->getByIdVenta($venta_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getDevolucionMostradorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesDevolucionMostradorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
