<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCatalogoAnio;
use App\Servicios\Refacciones\ServicioCurl;
use Illuminate\Support\Facades\DB;

class CatalogoAnioController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoAnio();
        $this->servicioCurl = new ServicioCurl();
    }

    public function buscar_id(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaAnio());
            $modelo = $this->servicio->searchIdanio($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            $this->servicioCurl->curlPost('https://planificadorempresarial.com/servicios/sjr/anios/index', [
                'anio' => $request->get(CatalogoAnioModel::NOMBRE)
            ], false);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->update($request, $id);
            DB::commit();
            $this->servicioCurl->curlPost('https://planificadorempresarial.com/servicios/sjr/anios/index/' . $id, [
                'anio' => $request->get(CatalogoAnioModel::NOMBRE)
            ], false);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }
}
