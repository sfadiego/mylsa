<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\UnidadesModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioRecepcionUnidadadesCostos;
use App\Servicios\Refacciones\ServicioRecepcionUnidades;
use Illuminate\Http\Request;

class RecepcionUnidadesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRecepcionUnidades();
        $this->servicioRecepcionCostos = new ServicioRecepcionUnidadadesCostos();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasCombinadasCostos());
            $unidades = $this->servicio->guardarInfo($request);
            $this->servicioRecepcionCostos->guardar($unidades->id, $request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($unidades, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->getAllnuevos();
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllSeminuevos()
    {
        try {
            $modelo = $this->servicio->getAllSeminuevos();
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        $modelo = $this->servicio->getByIdRecepcionUnidades($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function update(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasCombinadasCostosUpdate());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->actualizar($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getLastRecord()
    {
        $modelo = $this->servicio->lastRecord();
        return Respuesta::json($modelo);
    }

}
