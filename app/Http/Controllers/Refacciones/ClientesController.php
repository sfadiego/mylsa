<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Models\Refacciones\ClientesModel;


class ClientesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioClientes();
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->getAll();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $modelo = $this->servicio->getOneCliente($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getLastRecord()
    {
        try {
            $modelo = $this->servicio->lastRecord();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    #TODO: REVISAR CONSULTAS - MEJORAR
    public function searchNombreCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaNombre());
            $modelo = $this->servicio->searchNombreCliente($request->all()); 
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getClienteByclave(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClaveCliente());
            $modelo = $this->servicio->clientePorClave($request->all()); 
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaCliente());
            $modelo = $this->servicio->searchCliente($request); 
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchNumeroCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCliente());
            $modelo = $this->servicio->searchNumeroCliente($request->all()); 
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }


}
