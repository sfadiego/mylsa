<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioProductos;

class RefaccionesController extends CrudController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->servicio = new ServicioProductos();
    }
}
