<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioTraspasoProducto;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioTraspasos;
use App\Models\Refacciones\Traspasos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TraspasoProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTraspasoProducto();
        $this->servicioTraspaso = new ServicioTraspasos();
        $this->servicioProducto = new ServicioProductos();
    }

    public function confirmarTraspaso(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasConfirmarTraspaso());
            $data = $this->servicio->handleTraspasos($request->all());
            DB::commit();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getbyidproducto($id)
    {
        try {
            $modelo = $this->servicio->getbyidproducto($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByIdTraspaso($id)
    {
        try {
            $modelo = $this->servicio->getByIdTraspaso($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleTraspasoAlmacen($id)
    {
        try {
            $modelo = $this->servicio->getDetalleTraspasoAlmacen($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->getAll();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function polizaVentaTotalByTraspasoId($id)
    {
        try {

            $array_data = $this->servicioTraspaso->searchDetails([Traspasos::ID => $id]);
            $total = $this->servicioTraspaso->calcularTotal($array_data);

            return Respuesta::json([
                'total' => $total
            ], 200);

        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
