<?php

namespace App\Http\Controllers\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioInventarioProductos;
use App\Models\Refacciones\InventarioProductosModel;
use Illuminate\Http\Request;

class InventarioProductosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioInventarioProductos();
    }

    public function inventarioProductos()
    {
        return Respuesta::json($this->servicio->inventarioProductos([]), 200);
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $stock = $request->get('cantidad_stock');
            $inventario = $request->get('cantidad_inventario');
            if ($stock >= $inventario) {
                $faltantes = $stock - $inventario;
                $request->merge(['faltantes' => $faltantes]);

            } else {
                $sobrantes = $inventario - $stock;
                $request->merge(['sobrantes' => $sobrantes]);
            }
            $residuo = 0;
            $total = 0;
            $residuo =  $inventario - $stock;
            $total = $request->get('valor_unitario') * $residuo;
            $request->merge(['valor_residuo' => $total]);
            $request->merge(['diferencia' => $residuo]);

            $inventario = $this->servicio
                ->validarExisteProductoInventariado([
                    InventarioProductosModel::PRODUCTO_ID => $request->get('producto_id'),
                    InventarioProductosModel::INVENTARIO_ID => $request->get('inventario_id'),
                ])
                ->first();
            if (isset($inventario)) {
                throw new ParametroHttpInvalidoException([
                    'msg' => "El producto ya se inventario."
                ]);
            } else {
                $modelo = $this->servicio->store($request);
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 201, $mensaje);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getInventarioByProductoAttr(Request $request)
    {
        return Respuesta::json($this->servicio->inventarioProductos(
            $request->all()
        ), 200);
    }

    public function validarInventarioCompleto($id_inventario)
    {
        try {
            if (count($this->servicio->validarInventarioCompleto($id_inventario)) > 0) {
                throw new ParametroHttpInvalidoException([
                    'msg' => 'Inventario incompleto'
                ]);
            }

            return Respuesta::noContent(204, 'inventario completo');
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
