<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioEstatusCompra;

class CatalogoEstatusCompraController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusCompra();
    }
}
