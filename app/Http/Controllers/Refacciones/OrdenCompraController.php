<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Facturas\ServicioFacturacion;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioOrdenCompra;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioReFacturaOrdenCompra;
use App\Servicios\Refacciones\ServicioReOrdenCompraEstatus;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\OrdenCompraModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdenCompraController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioOrdenCompra();
		$this->servicioProductos = new ServicioProductos();
		$this->servicioFacturas = new ServicioFacturacion();
		$this->servicioArchivos = new ServicioManejoArchivos();
		$this->servicioReFacturasOrdenCompra = new ServicioReFacturaOrdenCompra();
		$this->servicioReOrdenCompraEstatus = new ServicioReOrdenCompraEstatus();
		$this->servicioFolio = new ServicioFolios();
		$this->servicioDesgloseProductos = new ServicioDesgloseProductos();
	}

	public function listadoProductosCarrito($orden_compra)
	{
		try {
			$data = $this->servicio->listadoProductos($orden_compra);
			return Respuesta::json($data, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function update(Request $request, $id)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
			$mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
			$modelo = $this->servicio->actualizarOrdenCompra($request, $id);
			return Respuesta::json($modelo, 200, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function index()
	{
		try {
			$data = $this->servicio->listado();
			return Respuesta::json($data, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function show($id)
	{
		try {
			$data = $this->servicio->showById($id);
			return Respuesta::json($data, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function searchByDates(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
			$result = $this->servicio->getComprasByFechas($request);
			return Respuesta::json($result, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function totalesSearchByDates(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
			$result = $this->servicio->getTotalesComprasByFechas($request);
			return Respuesta::json($result, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function cancelarFactura(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicioReFacturasOrdenCompra->getReglasCancelarFactura());
			$result = $this->servicioReFacturasOrdenCompra->cancelarFactura($request->all());
			return Respuesta::json($result, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function facturasListado()
	{
		try {
			$data = $this->servicio->listaFacturaOrdenCompra();
			return Respuesta::json(['data' => $data], 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function store(Request $request)
	{
		DB::beginTransaction();

		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			//Generar folio para cuentas en caja
			$folio = $this->servicioFolio->generarFolio(CatalogoCuentasModel::CAT_COMPRAS);
			$request->merge([OrdenCompraModel::FOLIO_ID => $folio->id]);
			//Guardamos la orden compra con el folio id
			$orden_compra = $this->servicio->crearOrdenCompra($request->toArray());

			$request->merge(['orden_compra_id' => $orden_compra->id]);
			//Creamos el historico detalle orden compra
			if (isset($orden_compra) && $orden_compra->id) {
				$this->servicioReOrdenCompraEstatus->storeReOrdenCompraEstatus($request->only('orden_compra_id'));
			} else {
				//Si algo sale mal se restaura
				DB::rollback();
			}
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			DB::commit();
			//Se devuelve la respuesta satisfactoria de la orden compra
			return Respuesta::json($orden_compra, 200, $mensaje);
		} catch (\Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}


	public function facturaOrdenCompra(Request $request)
	{
		DB::beginTransaction();
		try {
			$error = false;
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasUploadxmlOrdenCompra());

			if (empty($request->file(Factura::ARCHIVO_FACTURA))) {
				$error = true;
			}

			$file = $request->file(Factura::ARCHIVO_FACTURA);
			$newFileName = $this->servicioFacturas->setFileName($file);
			$directorio = $this->servicioFacturas->setDirectory(ServicioFacturacion::DIRECTORIO_FACTURAS);
			$this->servicioArchivos->upload($file, $directorio, $newFileName);
			$path = DIRECTORY_SEPARATOR . $directorio . $newFileName;
			$factura = $this->servicioFacturas->handleDataXmlFactura($path);
			$request->merge(['factura_id' => $factura]);

			if ($factura) {
				$folio = $this->servicioFolio->generarFolio(CatalogoCuentasModel::CAT_COMPRAS);
				$request->merge([OrdenCompraModel::FOLIO_ID => $folio->id]);
				$orden_compra = $this->servicio->crearOrdenCompra($request->except(['archivo_factura']));
				$request->merge(['orden_compra_id' => $orden_compra->id]);
			}

			if (isset($orden_compra) && $orden_compra->id && $factura) {
				$this->servicioReFacturasOrdenCompra->storeReFacturaOrden($request->only('factura_id', 'orden_compra_id'));
				$this->servicioReOrdenCompraEstatus->storeReOrdenCompraEstatus($request->only('orden_compra_id'));
				$this->servicioProductos->setTipoFactura(ServicioFacturacion::FACTURA_ORDEN_COMPRA, $orden_compra->id);
				$this->servicioProductos->setFacturaId($factura);
				$conceptos = $this->servicioProductos->guardarDataFacturaProducto([
					'xml_path' => $path,
					'proveedor_id' => $request->get('proveedor_id')
				]);

				if (!$conceptos) {
					DB::rollback();
					$mensaje = __(static::$E0013_ERROR_REGISTERING_RESOURCE, ['recurso' => $this->servicio->getRecurso()]);
					return Respuesta::noContent(400, $mensaje);
				}
			} else {
				$error = true;
			}

			if ($error) {
				DB::rollback();
				$mensaje = __(static::$E0013_ERROR_REGISTERING_RESOURCE, ['recurso' => $this->servicio->getRecurso()]);
				return Respuesta::noContent(400, $mensaje);
			}
			DB::commit();
			return Respuesta::json($orden_compra, 200);
		} catch (\Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}

	public function getBusquedaOrdenCompra(Request $request)
	{
		try {
			$data = $this->servicio->getOrdenCompra($request);
			return Respuesta::json(['data' => $data], 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getBusquedaDevoluciones(Request $request)
	{
		try {
			$data = $this->servicio->getDevoluciones($request);
			return Respuesta::json(['data' => $data], 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
	public function actualizarInventarioByOrdenCompra(int $orden_compra_id)
	{
		try {
			$productos_orden_compra = $this->servicio->updateInventarioByOrdenCompra($orden_compra_id);
			$productos_inventario = [];
			if (!empty($productos_orden_compra)) {
				foreach ($productos_orden_compra as $key => $item) {
					$productos_inventario[] = $this->servicioDesgloseProductos->actualizaStockByProducto([
						'producto_id' => $item->producto_id
					]);
				}
			}
			return Respuesta::json($productos_inventario, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
