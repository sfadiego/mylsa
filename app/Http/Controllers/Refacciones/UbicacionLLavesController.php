<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioUbicacionLLaves;
use Illuminate\Http\Request;

class UbicacionLLavesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioUbicacionLLaves();
    }
}
