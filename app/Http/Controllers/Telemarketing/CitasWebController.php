<?php

namespace App\Http\Controllers\Telemarketing;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Telemarketing\ServicioCitasVentas;
use App\Servicios\Telemarketing\ServicioVentasWeb;
use App\Models\Telemarketing\VentasWebModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TelemarketingEmail;

class CitasWebController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCitasVentas();
        $this->servicio_ventas = new ServicioVentasWeb();
    }
    
    public function getCitaByIdVenta($id)
    {
        try {
            $modelo = $this->servicio->getByIdVenta($id);
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function horariosOCupados(Request $request)
    {
        try {
            $modelo = $this->servicio->getHorariosOcupadosAsesor($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function existeCita(Request $request)
    {
        try {
            $modelo = $this->servicio->validarCita($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function saveCita(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            //Actualizar en la tabla de ventas_web el id_asesor
            $this->servicio_ventas->massUpdateWhereId(VentasWebModel::ID, $request->get('id_venta'), [VentasWebModel::ID_ASESOR_WEB_ASIGNADO => $request->get('id_asesor')]);
            //Guardar la cita
            $modelo = $this->servicio->store($request);
            //enviar correo
            $datos_cita = $this->servicio_ventas->getAllVentaCita($request->get('id_venta'));
            Mail::to($datos_cita[0]->correo_electronico)->send(new TelemarketingEmail([$datos_cita[0]]));
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function datosCita(Request $request)
    {
        try {
            $modelo = $this->servicio->getDataCitas($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function cambiar_status_cita(Request $request)
    {
        try {
            $modelo = $this->servicio->updateStatusCita($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
