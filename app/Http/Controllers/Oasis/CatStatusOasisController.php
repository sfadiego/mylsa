<?php

namespace App\Http\Controllers\Oasis;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Oasis\ServicioEstatusOasis;
use Illuminate\Http\Request;

class CatStatusOasisController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusOasis();
    }
}
