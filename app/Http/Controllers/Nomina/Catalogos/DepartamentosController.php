<?php

namespace App\Http\Controllers\Nomina\Catalogos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Nomina\ServicioDepartamentos;
use Illuminate\Http\Request;

// Necesitaremos el modelo Fabricante para ciertas tareas.
///use App\Fabricante;

class DepartamentosController extends CrudController
{

	public function __construct()
	{
		$this->servicio = new ServicioDepartamentos();
	}


	public function getClave()
	{
		$data = ['clave' => $this->servicio->getClave()];
		return Respuesta::json($data, 200);
	}
}
