<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\UnidadesModel;
use App\Servicios\Autos\ServicioUnidades;
use App\Servicios\Core\Respuestas\Respuesta;

class UnidadesController extends CrudController
{

    public function __construct()
    {
        $this->servicio = new ServicioUnidades();
    }

    public function index()
    {
       
        return Respuesta::json($this->servicio->getunidadesNuevas(), 200);
    }

    
    public function show($id)
    {
        $modelo = $this->servicio->getUnidadById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function getTotalUnidadesnuevas()
    {
        return Respuesta::json($this->servicio->getTotalUnidadesnuevas(), 200);
    }

    
}
