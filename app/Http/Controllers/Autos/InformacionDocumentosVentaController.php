<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Servicios\Autos\ServicioInformacionDocumentosVenta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class InformacionDocumentosVentaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioInformacionDocumentosVenta();
    }

    public function store(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $existe = $this->servicio->getByIdunidad($request->get(InformacionDocumentosVentaModel::ID_VENTA_UNIDAD));
            if (isset($existe)) {
                $modelo = $this->servicio->massUpdateWhereId('id', $existe->id, $request->all());
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 200, $mensaje);
            }
            $modelo = $this->servicio->crear($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByIdunidad($id_venta_unidad)
    {
        return Respuesta::json($this->servicio->getByIdunidad($id_venta_unidad), 200);
    }
}
