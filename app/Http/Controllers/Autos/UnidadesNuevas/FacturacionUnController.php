<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioFacturacion;
use Illuminate\Http\Request;

class FacturacionUnController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioFacturacion();
    }
}
