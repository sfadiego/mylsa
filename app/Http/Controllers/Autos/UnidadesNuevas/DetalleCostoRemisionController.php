<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioDetalleCostosRemisiones;
use Illuminate\Http\Request;

class DetalleCostoRemisionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDetalleCostosRemisiones();
    }
}
