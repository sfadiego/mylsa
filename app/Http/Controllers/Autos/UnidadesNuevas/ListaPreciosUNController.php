<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioListaPreciosUn;
use Illuminate\Http\Request;

class ListaPreciosUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioListaPreciosUn();
    }
}
