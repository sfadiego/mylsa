<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioRemisiones;
use Illuminate\Http\Request;

class RemisionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRemisiones();
    }
}
