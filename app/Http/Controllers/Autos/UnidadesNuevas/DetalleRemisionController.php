<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioDetalleRemision;
use Illuminate\Http\Request;

class DetalleRemisionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDetalleRemision();
    }
}
