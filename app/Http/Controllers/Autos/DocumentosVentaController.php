<?php

namespace App\Http\Controllers\Autos;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\DocumentosVentaModel;
use App\Servicios\Autos\ServicioDocumentosVenta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Http\Request;

class DocumentosVentaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDocumentosVenta();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }

    public function store(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $existe = $this->servicio->validarArchivo([
                DocumentosVentaModel::ID_VENTA_UNIDAD => $request->get(DocumentosVentaModel::ID_VENTA_UNIDAD),
                DocumentosVentaModel::ID_TIPO_DOCUMENTO =>  $request->get(DocumentosVentaModel::ID_TIPO_DOCUMENTO)
            ]);

            if ($existe > 0) {

                throw new ParametroHttpInvalidoException([
                    'msg' => "El archivo ya se subio previamente"
                ]);
            }

            $file = $request->file(DocumentosVentaModel::NOMBRE_ARCHIVO);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(DocumentosVentaModel::DIRECTORIO_DOCUMENTOS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;

            $modelo = $this->servicio->crear([
                DocumentosVentaModel::NOMBRE_ARCHIVO =>  $newFileName,
                DocumentosVentaModel::ID_VENTA_UNIDAD => $request->get(DocumentosVentaModel::ID_VENTA_UNIDAD),
                DocumentosVentaModel::RUTA_ARCHIVO => $path,
                DocumentosVentaModel::ID_TIPO_DOCUMENTO =>  $request->get(DocumentosVentaModel::ID_TIPO_DOCUMENTO)
            ]);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updatedocumentoventa(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $get_id = $this->servicio->getIdRow($request->get(DocumentosVentaModel::ID_VENTA_UNIDAD), $request->get(DocumentosVentaModel::ID_TIPO_DOCUMENTO));
            
            $file = $request->file(DocumentosVentaModel::NOMBRE_ARCHIVO);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(DocumentosVentaModel::DIRECTORIO_DOCUMENTOS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;

            $modelo = $this->servicio->massUpdateWhereId('id', $get_id->id, [
                DocumentosVentaModel::NOMBRE_ARCHIVO =>  $newFileName,
                DocumentosVentaModel::ID_VENTA_UNIDAD => $request->get(DocumentosVentaModel::ID_VENTA_UNIDAD),
                DocumentosVentaModel::RUTA_ARCHIVO => $path,
                DocumentosVentaModel::ID_TIPO_DOCUMENTO =>  $request->get(DocumentosVentaModel::ID_TIPO_DOCUMENTO)
            ]);

            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getArchivosbyIdunidad($id)
    {
        $modelo = $this->servicio->getArchivosbyIdunidad($id);
        return Respuesta::json($modelo, 200);
    }

    public function downloadDocumento($file_name)
    {
        return response()->download(storage_path("app/unidades/documentos/{$file_name}"));
    }
}
