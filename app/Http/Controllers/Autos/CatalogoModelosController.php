<?php

namespace App\Http\Controllers\Autos;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\CatModelosModel;
use App\Servicios\Autos\ServicioCatModelos;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCurl;
use Illuminate\Support\Facades\DB;

class CatalogoModelosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatModelos();
        $this->servicioCurl = new ServicioCurl();
    }

    public function buscar_id(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaModelo());
            $modelo = $this->servicio->searchIdModelo($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            $store_parameters['modelo'] = $request->get(CatModelosModel::NOMBRE);
            $store_parameters['activo'] = 1;
            $store_parameters['categoria_id'] = 1;
            $store_parameters['tiempo_lavado'] = 0;
            if (!empty($request->get(CatModelosModel::TIEMPO_LAVADO))) {
                $store_parameters['tiempo_lavado'] = $request->get(CatModelosModel::TIEMPO_LAVADO);
            }
            
            $this->servicioCurl->curlPost(
                'https://planificadorempresarial.com/servicios/sjr/modelos/index',
                $store_parameters,
                false
            );
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->update($request, $id);
            DB::commit();
            $this->servicioCurl->curlPost('https://planificadorempresarial.com/servicios/sjr/modelos/index/' . $id, [
                'modelo' => $request->get(CatModelosModel::NOMBRE)
            ], false);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }
}
