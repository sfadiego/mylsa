<?php

namespace App\Http\Controllers\Bodyshop;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Bodyshop\ServicioCatProyecto;
use Illuminate\Http\Request;

class CatProyectosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatProyecto();
    }
}
