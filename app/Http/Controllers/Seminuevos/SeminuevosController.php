<?php

namespace App\Http\Controllers\Seminuevos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\UnidadesModel;
use App\Servicios\Autos\ServicioUnidades;
use App\Servicios\Seminuevos\ServicioSeminuevos;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class SeminuevosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSeminuevos();
        $this->servicioUnidades = new ServicioUnidades();
        
    }

    public function index()
    {

        return Respuesta::json($this->servicioUnidades->getunidadesSemiNuevas(), 200);
    }

    public function getTotalUnidadesSeminuevas()
    {
        return Respuesta::json($this->servicioUnidades->getTotalUnidadesSemiNuevas(), 200);
    }

    public function show($id)
    {
        $modelo = $this->servicioUnidades->getUnidadSeminuevaById(['id' => $id, 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO]);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
}
