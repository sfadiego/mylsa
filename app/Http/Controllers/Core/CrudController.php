<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Core\MainController;
use Illuminate\Http\Request;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Support\Facades\DB;
use Throwable;

abstract class CrudController extends MainController
{
    public function index()
    {
        return Respuesta::json($this->servicio->buscarTodos(), 200);
    }

    public function store(Request $request)
    {
        try {
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (Throwable $e) {
            //dd($e);
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        $modelo = $this->servicio->getById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->update($request, $id);
            DB::commit();
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function destroy(int $id)
    {
        $this->servicio->eliminar($id);
        return Respuesta::noContent(204);
    }
}
