<?php

namespace App\Http\Controllers;

use App\Http\Core\Controllers\Controller;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Reportes\Pdfservice;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Throwable;

class PdfGenerator extends Pdfservice
{
    public function __construct()
    {
        $this->servicioManejoArchivos = new ServicioManejoArchivos();
    }

    public function index(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->reglas());
            $data = base64_decode($request->view);
            $this->getInstance();
            $this->setContenidoPdf($data);
            return $this->generar();
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function reglas()
    {
        return ['view' => 'required'];
    }

    public function setContenidoPdf($data)
    {
        $this->mpdf->WriteHTML($data);
    }
}
