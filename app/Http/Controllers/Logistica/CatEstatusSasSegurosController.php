<?php

namespace App\Http\Controllers\Logistica;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Logistica\ServicioCatEstatusSasSeguros;
use Illuminate\Http\Request;

class CatEstatusSasSegurosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatEstatusSasSeguros();
    }
}
