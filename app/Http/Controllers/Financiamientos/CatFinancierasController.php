<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Financiamientos\ServicioCatFinancieras;
use Illuminate\Http\Request;

class CatFinancierasController extends CrudController
{
   public function __construct()
   {
       $this->servicio = new ServicioCatFinancieras();
   }
}
