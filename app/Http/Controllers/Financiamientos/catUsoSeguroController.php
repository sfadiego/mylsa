<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Financiamientos\ServicioCatUsoSeguro;
use Illuminate\Http\Request;

class catUsoSeguroController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatUsoSeguro();
    }
}
