<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO POLIZAS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'polizas/traspasos'], function () {
    // Route::get('/', 'Autos\UnidadesController@index');
});

Route::group(['prefix' => 'tipocuenta'], function () {
    Route::get('/test', 'Polizas\CatTipoPolizaController@test');
});

Route::resource('catalogo/tipo-poliza', 'Polizas\CatTipoPolizaController');

Route::group(['prefix' => 'polizas'], function () {
    Route::post('/traspasos/filtrar', 'Refacciones\TraspasosController@searchByDates');
    Route::post('/compras/listado', 'Refacciones\OrdenCompraController@searchByDates');
    Route::post('/compras/totales', 'Refacciones\OrdenCompraController@totalesSearchByDates');
    Route::post('/ventas/listado', 'Refacciones\VentasRealizadasController@searchByDates');
    Route::post('/ventas/totales', 'Refacciones\VentasRealizadasController@totalesSearchByDates');
    Route::post('/devolucion_ventas/listado', 'Refacciones\DevolucionVentasController@searchByDates');
    Route::post('/devolucion_ventas/totales', 'Refacciones\DevolucionVentasController@totalesSearchByDates');
    Route::post('/devolucion_proveedor/listado', 'Refacciones\DevolucionProveedorController@searchByDates');
    Route::post('/devolucion_proveedor/totales', 'Refacciones\DevolucionProveedorController@totalesSearchByDates');
});