<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::group(['prefix' => 'oasis/master/'], function () {
//     Route::post('/get-all', 'Oasis\OasisController@getAll');
// });

Route::resource('cat-combustible', 'Autos\UnidadesNuevas\CatCombustibleController');
Route::resource('cat-proveedores-un', 'Autos\UnidadesNuevas\ProveedoresUNController');
Route::resource('cat-lineas', 'Autos\UnidadesNuevas\CatLineasController');
Route::resource('precios-activos-un', 'Autos\UnidadesNuevas\PreciosActivosUNController');
Route::resource('lista-precios-un', 'Autos\UnidadesNuevas\ListaPreciosUNController');
Route::resource('remision', 'Autos\UnidadesNuevas\RemisionesController');
Route::resource('detalle-costos-remision', 'Autos\UnidadesNuevas\DetalleCostoRemisionController');
Route::resource('detalle-remision', 'Autos\UnidadesNuevas\DetalleRemisionController');
Route::resource('memo-compra', 'Autos\UnidadesNuevas\MemoCompraController');
Route::resource('facturacion-unidades-nuevas', 'Autos\UnidadesNuevas\FacturacionUnController');