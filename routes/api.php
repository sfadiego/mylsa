<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'usuarios'], function () {
    Route::post('/modulos', 'Usuarios\UsuariosController@storeUsuariosModulos');
    Route::get('/modulos-by-usuario', 'Usuarios\UsuariosController@usuariosModulos');
    Route::post('/login', 'Usuarios\UsuariosController@login');
    Route::post('/logout', 'Usuarios\UsuariosController@logout');
    Route::post('/registrar', 'Usuarios\UsuariosController@registro');
    Route::post('/usuario-by-rol', 'Usuarios\UsuariosController@getUsuariosByParametros');
});
Route::resource('usuarios', 'Usuarios\UsuariosController');

Route::group(['prefix' => 'menu'], function () {
    Route::get('/', 'Usuarios\Menu\MenuController@menu');
    Route::get('/modulos-userid/{user_id}', 'Usuarios\\Menu\ModulosMenuController@getModulosByUserID');
    Route::get('/modulos', 'Usuarios\Menu\ModulosMenuController@index');
    Route::post('/modulos', 'Usuarios\Menu\ModulosMenuController@store');
    Route::get('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@show');
    Route::put('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@update');
    Route::delete('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@destroy');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', 'Usuarios\RolesController@index');
    Route::post('/', 'Usuarios\RolesController@store');
    Route::post('/modulos', 'Usuarios\RolesController@storerolesModulos');
    Route::get('/modulos-by-rol', 'Usuarios\RolesController@rolesModulos');
    Route::get('/{id}', 'Usuarios\RolesController@show');
    Route::put('/{id}', 'Usuarios\RolesController@update');
    Route::delete('/{id}', 'Usuarios\RolesController@destroy');
});

Route::post('/pdf', 'PdfGenerator@index');
Route::post('/upload-factura', 'Facturacion\Facturacion@uploadXmlFactura');
Route::get('/getfileview', 'Core\ManejoArchivosController@returnFileView');
Route::get('/downloadFile', 'Core\ManejoArchivosController@downloadFile');


Route::group(['prefix' => 'facturas'], function () {
    Route::post('/upload', 'Facturacion\Facturacion@uploadXmlFactura');
    Route::get('/', 'Facturacion\Facturacion@index');
    Route::get('/{id}', 'Facturacion\Facturacion@getByIdFactura');
    Route::get('/descargar/{string}', 'Facturacion\Facturacion@downloadFactura');
});


Route::group(['prefix' => 'productos'], function () {
    Route::get('/', 'Refacciones\ProductosController@getAllProductos');
    Route::get('/stockActual', 'Refacciones\ProductosController@getAllStockProducto');
    Route::get('/stockByProductoId/{producto_id}', 'Refacciones\ProductosController@getStockByProductoId');
    Route::get('/listadoStock', 'Refacciones\ProductosController@listadoStockProductos');
    Route::get('/{id}', 'Refacciones\ProductosController@getProductoById');
    // Route::post('/', 'Refacciones\ProductosController@storeproductos');
    Route::post('/upload-factura-productos', 'Refacciones\ProductosController@uploadFacturaProductos');
    // Route::put('/{id}', 'Refacciones\ProductosController@update');
    Route::put('/ubicacion/{id}', 'Refacciones\ProductosController@updateProductoUbicacion');
    // Route::delete('/{id}', 'Refacciones\ProductosController@destroy');
    Route::post('/buscar-piezas', 'Refacciones\ProductosController@getByNumeroPiezaDescripcion');

    Route::post('/descontar-producto', 'Refacciones\ProductosController@descontarProductos');
    Route::post('/aumentar-producto', 'Refacciones\ProductosController@aumentarProductos');
    Route::post('/validar-descuento-producto', 'Refacciones\ProductosController@validarCantidadProducto');
});

Route::resource('productos', 'Refacciones\ProductosController');

Route::group(['prefix' => 'almacen'], function () {
    Route::get('/', 'Refacciones\AlmacenesController@index');
    Route::post('/', 'Refacciones\AlmacenesController@store');
    Route::get('/{id}', 'Refacciones\AlmacenesController@show');
    Route::put('/{id}', 'Refacciones\AlmacenesController@update');
    Route::delete('/{id}', 'Refacciones\AlmacenesController@destroy');
});

Route::group(['prefix' => 'menu-secciones'], function () {
    Route::get('/', 'Usuarios\Menu\MenuSeccionesController@getSeccionesByModulo');
    Route::post('/', 'Usuarios\Menu\MenuSeccionesController@store');
    Route::get('/{id}', 'Usuarios\Menu\MenuSeccionesController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuSeccionesController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuSeccionesController@destroy');
});

Route::group(['prefix' => 'menu-submenu'], function () {
    Route::get('/', 'Usuarios\Menu\MenuSubmenuController@getsubmenusBySeccion');
    Route::post('/', 'Usuarios\Menu\MenuSubmenuController@store');
    Route::get('/{id}', 'Usuarios\Menu\MenuSubmenuController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuSubmenuController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuSubmenuController@destroy');
});

Route::group(['prefix' => 'menu-vistas'], function () {
    Route::get('/', 'Usuarios\Menu\MenuVistasController@getVistasBySubmenu');
    // Route::post('/store-by-roles', 'Usuarios\Menu\MenuVistasController@saveVistasRoles');
    Route::get('/get-by-usuario', 'Usuarios\Menu\MenuVistasController@getUsuariosVistasByUserId');
    Route::post('/store-by-usuarios', 'Usuarios\Menu\MenuVistasController@saveVistasUsuarios');
    // Route::get('/get-by-rol', 'Usuarios\Menu\MenuVistasController@getRolesVistasByRolId');
    Route::post('/', 'Usuarios\Menu\MenuVistasController@store');
    Route::get('/{id}', 'Usuarios\Menu\MenuVistasController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuVistasController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuVistasController@destroy');
});

Route::group(['prefix' => 'requisiciones'], function () {
    Route::get('/', 'Refacciones\RequisicionesController@index');
    Route::post('/', 'Refacciones\RequisicionesController@store');
    Route::get('/{id}', 'Refacciones\RequisicionesController@getDetalleRequisicion');
    Route::put('/{id}', 'Refacciones\RequisicionesController@update');
    Route::delete('/{id}', 'Refacciones\RequisicionesController@destroy');
});

Route::group(['prefix' => 'clientes'], function () {
    Route::get('/ultimo-registro', 'Refacciones\ClientesController@getLastRecord');
    Route::get('/tipo-clave', 'Refacciones\ClientesController@getClienteByclave');
    Route::post('/buscar-cliente', 'Refacciones\ClientesController@searchNombreCliente');
    Route::post('/buscar-existencia', 'Refacciones\ClientesController@searchCliente');
    Route::post('/numero-cliente', 'Refacciones\ClientesController@searchNumeroCliente');
});

Route::resource('clientes', 'Refacciones\ClientesController');

Route::group(['prefix' => 'contactos'], function () {
    Route::get('/', 'Refacciones\ContactoController@index');
    Route::get('lista-cliente/{id}', 'Refacciones\ContactoController@getContactosByClienteId');
    Route::post('/', 'Refacciones\ContactoController@store');
    Route::get('/{id}', 'Refacciones\ContactoController@show');
    Route::put('/{id}', 'Refacciones\ContactoController@update');
    Route::delete('/{id}', 'Refacciones\ContactoController@destroy');
});

Route::group(['prefix' => 'vehiculos-clientes'], function () {
    Route::get('/', 'Refacciones\VehiculosClientesController@index');
    Route::get('lista-cliente/{id}', 'Refacciones\VehiculosClientesController@getVehiculosByClienteId');
    Route::post('/', 'Refacciones\VehiculosClientesController@store');
    Route::get('/{id}', 'Refacciones\VehiculosClientesController@show');
    Route::put('/{id}', 'Refacciones\VehiculosClientesController@update');
    Route::delete('/{id}', 'Refacciones\VehiculosClientesController@destroy');
    Route::post('/buscar-serie', 'Refacciones\VehiculosClientesController@searchSerieCliente');
});

Route::group(['prefix' => 'cfdi'], function () {
    Route::get('/', 'Refacciones\CfdiController@index');
    Route::post('/', 'Refacciones\CfdiController@store');
    Route::get('/{id}', 'Refacciones\CfdiController@show');
    Route::put('/{id}', 'Refacciones\CfdiController@update');
    Route::delete('/{id}', 'Refacciones\CfdiController@destroy');
});

Route::group(['prefix' => 'precios'], function () {
    Route::get('/', 'Refacciones\PreciosController@index');
    Route::post('/', 'Refacciones\PreciosController@store');
    Route::get('/{id}', 'Refacciones\PreciosController@show');
    Route::put('/{id}', 'Refacciones\PreciosController@update');
    Route::delete('/{id}', 'Refacciones\PreciosController@destroy');
});

Route::group(['prefix' => 'talleres'], function () {
    Route::get('/', 'Refacciones\TallerController@index');
    Route::post('/', 'Refacciones\TallerController@store');
    Route::get('/{id}', 'Refacciones\TallerController@show');
    Route::put('/{id}', 'Refacciones\TallerController@update');
    Route::delete('/{id}', 'Refacciones\TallerController@destroy');
});

Route::group(['prefix' => 'vendedor'], function () {
    Route::get('/', 'Refacciones\VendedorController@index');
    Route::post('/', 'Refacciones\VendedorController@store');
    Route::get('/{id}', 'Refacciones\VendedorController@show');
    Route::put('/{id}', 'Refacciones\VendedorController@update');
    Route::delete('/{id}', 'Refacciones\VendedorController@destroy');
});

Route::group(['prefix' => 'folio'], function () {
    Route::get('/', 'Refacciones\FoliosController@index');
    Route::get('/get-by-folio/{folio}', 'Refacciones\FoliosController@getByFolio');
    Route::post('/generar-folio', 'Refacciones\FoliosController@generarFolio');
    Route::post('/', 'Refacciones\FoliosController@store');
    Route::get('/{id}', 'Refacciones\FoliosController@show');
});

Route::group(['prefix' => 'venta-producto'], function () {
    Route::get('/', 'Refacciones\VentaProductoController@index');
    Route::get('/detalle-venta', 'Refacciones\VentaProductoController@detalleVenta');
    // Route::get('/detalle-venta-by-folio/{folio}', 'Refacciones\VentaProductoController@getDetalleVentaByFolio');
    Route::get('/detalle-by-id/{id}', 'Refacciones\VentaProductoController@getDetalleVentaId');
    Route::get('/total-venta-by-folio/{folio}', 'Refacciones\VentaProductoController@totalVentaByFolio');
    Route::get('/{id}', 'Refacciones\VentaProductoController@show');
    Route::post('/', 'Refacciones\VentaProductoController@store');
    
    Route::post('/autorizar', 'Refacciones\VentaProductoController@autorizarVenta');
    Route::delete('/{id}', 'Refacciones\VentaProductoController@destroy');
});

Route::group(['prefix' => 'ventas'], function () {
    Route::get('/taller/numero-orden/{numero_orden}', 'Refacciones\VentasRealizadasController@getRefaccionesVentanillaTaller');
    Route::get('/', 'Refacciones\VentasRealizadasController@index');
    Route::get('/venta-by-folio/{folio}', 'Refacciones\VentasRealizadasController@ventasByFolioId');
    Route::get('/busqueda-ventas', 'Refacciones\VentasRealizadasController@getBusquedaVentas');
    Route::get('/busqueda-all-ventas', 'Refacciones\VentasRealizadasController@getAllVentas');
    Route::get('/busqueda-devoluciones', 'Refacciones\VentasRealizadasController@getBusquedaVentasDevoluciones');
    Route::get('/ventas-agrupadas-producto', 'Refacciones\VentasRealizadasController@getVentasPorMesByProducto');
    Route::post('/', 'Refacciones\VentasRealizadasController@storeVenta');
    Route::post('/mpm', 'Refacciones\VentasRealizadasController@ventaMpm');
    Route::post('/mpm-finalizar', 'Refacciones\VentasRealizadasController@FinalizarVentaServicio');
    Route::put('/finalizar/{id}', 'Refacciones\VentasRealizadasController@finalizarVenta');
    Route::get('/detalle-mpm/{id}', 'Refacciones\VentasRealizadasController@detalleVentampmById');
    Route::get('/total/{id}', 'Refacciones\VentasRealizadasController@totalventa');
    Route::get('/{id}', 'Refacciones\VentasRealizadasController@show');
});

Route::group(['prefix' => 'producto-traspaso-almacen'], function () {
    Route::get('/', 'Refacciones\TraspasoProductoController@index');
    Route::get('/getby-idproducto/{id}', 'Refacciones\TraspasoProductoController@getbyidproducto');
    Route::post('/', 'Refacciones\TraspasoProductoController@store');
    Route::post('/confirmar-traspaso', 'Refacciones\TraspasoProductoController@confirmarTraspaso');
    Route::put('/{id}', 'Refacciones\TraspasoProductoController@update');
    Route::get('/get-by-id-traspaso/{id}', 'Refacciones\TraspasoProductoController@getByIdTraspaso');
    Route::get('/get-detalle-traspaso-by-id/{id}', 'Refacciones\TraspasoProductoController@getDetalleTraspasoAlmacen');
    Route::delete('/{id}', 'Refacciones\TraspasoProductoController@destroy');
});

Route::group(['prefix' => 'producto-almacen'], function () {
    Route::get('/', 'Refacciones\ProductoAlmacenController@index');
    Route::get('/{id}', 'Refacciones\ProductoAlmacenController@show');
    Route::post('/', 'Refacciones\ProductoAlmacenController@store');
    Route::put('/{id}', 'Refacciones\ProductoAlmacenController@update');
    Route::post('/get-cantidad-almacen-producto', 'Refacciones\ProductoAlmacenController@getCantidadAlmacenProducto');

    Route::post('/descontar', 'Refacciones\ProductoAlmacenController@descontar');
});

Route::group(['prefix' => 'traspasos'], function () {
    Route::get('/poliza/venta-total/{id}', 'Refacciones\TraspasoProductoController@polizaVentaTotalByTraspasoId');
    Route::get('/reporte/{traspaso_id}', 'Refacciones\TraspasosController@reportetraspaso');
});

Route::resource('traspasos', 'Refacciones\TraspasosController');

Route::group(['prefix' => 'producto-remplazo-almacen'], function () {
    Route::get('/', 'Refacciones\RemplazoProductoController@index');
    Route::post('/', 'Refacciones\RemplazoProductoController@actualizarProductoAlmacen');
    Route::put('/{id}', 'Refacciones\RemplazoProductoController@update');
    Route::get('/getby-idproducto/{id}', 'Refacciones\TraspasoProductoController@getbyidproducto');
});


Route::resource('catalogo-proveedor', 'Refacciones\ProveedorRefaccionesController');

Route::group(['prefix' => 'catalogo-tipo-cliente'], function () {
    Route::get('/', 'Refacciones\TipoClienteController@index');
    Route::get('/{id}', 'Refacciones\TipoClienteController@show');
    Route::post('/', 'Refacciones\TipoClienteController@store');
    Route::put('/{id}', 'Refacciones\TipoClienteController@update');
    Route::delete('/{id}', 'Refacciones\TipoClienteController@destroy');
});

Route::group(['prefix' => 'devoluciones'], function () {
    Route::get('/', 'Refacciones\DevolucionProveedorController@index');
    Route::get('/{id}', 'Refacciones\DevolucionProveedorController@show');
    Route::get('/get-by-compra_id/{id}', 'Refacciones\DevolucionProveedorController@showByOrdenCompraId');
    Route::post('/', 'Refacciones\DevolucionProveedorController@store');
    Route::put('/{id}', 'Refacciones\DevolucionProveedorController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionProveedorController@destroy');
});

Route::group(['prefix' => 'devolucion-venta'], function () {
    Route::get('/', 'Refacciones\DevolucionVentasController@index');
    Route::get('/{id}', 'Refacciones\DevolucionVentasController@show');
    Route::get('/get-by-venta-id/{id}', 'Refacciones\DevolucionVentasController@showByVentaID');
    Route::post('/', 'Refacciones\DevolucionVentasController@storeDevolucion');
    Route::post('/servicio', 'Refacciones\DevolucionVentasController@devolucionByServicio');
    Route::put('/{id}', 'Refacciones\DevolucionVentasController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionVentasController@destroy');
});


Route::group(['prefix' => 'orden-compra'], function () {
    Route::put('/update-inventario-por-orden/{orden_compra_id}', 'Refacciones\OrdenCompraController@actualizarInventarioByOrdenCompra');
    Route::get('/facturas-listado', 'Refacciones\OrdenCompraController@facturasListado');
    Route::get('/', 'Refacciones\OrdenCompraController@index');
    Route::post('/', 'Refacciones\OrdenCompraController@store');
    Route::put('/{id}', 'Refacciones\OrdenCompraController@update');
    Route::delete('/{id}', 'Refacciones\OrdenCompraController@destroy');
    Route::get('/lista-productos-carrito/{id}', 'Refacciones\OrdenCompraController@listadoProductosCarrito');
    Route::post('/factura', 'Refacciones\OrdenCompraController@facturaOrdenCompra');
    Route::post('/cancelar-factura', 'Refacciones\OrdenCompraController@cancelarFactura');
    Route::get('/busqueda-compras', 'Refacciones\OrdenCompraController@getBusquedaOrdenCompra');
    Route::get('/busqueda-devoluciones', 'Refacciones\OrdenCompraController@getBusquedaDevoluciones');
    Route::get('/{id}', 'Refacciones\OrdenCompraController@show');
});

Route::group(['prefix' => 'productos-orden-compra'], function () {
    Route::get('/', 'Refacciones\ListaProductosOrdenCompraController@index');
    Route::get('/{id}', 'Refacciones\ListaProductosOrdenCompraController@listadoProductosCarrito');
    Route::post('/', 'Refacciones\ListaProductosOrdenCompraController@store');
    Route::put('/{id}', 'Refacciones\ListaProductosOrdenCompraController@update');
    Route::delete('/{id}', 'Refacciones\ListaProductosOrdenCompraController@destroy');
    Route::get('/total-by-orden-compra/{id}', 'Refacciones\ListaProductosOrdenCompraController@totalByOrdenCompra');
});


Route::group(['prefix' => 'tipo-pago'], function () {
    Route::get('/', 'Refacciones\CatTipoPagoController@index');
    Route::get('/{id}', 'Refacciones\CatTipoPagoController@show');
    Route::post('/', 'Refacciones\CatTipoPagoController@store');
    Route::put('/{id}', 'Refacciones\CatTipoPagoController@update');
    Route::delete('/{id}', 'Refacciones\CatTipoPagoController@destroy');
});

Route::group(['prefix' => 're-estatus-compra'], function () {
    Route::get('/', 'Refacciones\ReOrdenCompraEstatus@index');
    Route::get('/{id}', 'Refacciones\ReOrdenCompraEstatus@show');
    Route::post('/', 'Refacciones\ReOrdenCompraEstatus@store');
    Route::put('/{id}', 'Refacciones\ReOrdenCompraEstatus@update');
    Route::delete('/{id}', 'Refacciones\ReOrdenCompraEstatus@destroy');
});

Route::group(['prefix' => 're-estatus-venta'], function () {
    Route::get('/', 'Refacciones\ReVentasEstatus@index');
    Route::get('/{id}', 'Refacciones\ReVentasEstatus@show');
    Route::post('/', 'Refacciones\ReVentasEstatus@store');
    Route::put('/{id}', 'Refacciones\ReVentasEstatus@update');
    Route::delete('/{id}', 'Refacciones\ReVentasEstatus@destroy');
});

// 
Route::group(['prefix' => 'catalogo-anio'], function () {
    Route::post('/buscar-id', 'Refacciones\CatalogoAnioController@buscar_id');
});
Route::resource('catalogo-anio', 'Refacciones\CatalogoAnioController');

Route::group(['prefix' => 'catalogo-colores'], function () {
    Route::post('/buscar-id', 'Refacciones\CatalogoColoresController@buscar_id');
});
Route::resource('catalogo-colores', 'Refacciones\CatalogoColoresController');

Route::group(['prefix' => 'catalogo-marcas'], function () {
    Route::post('/buscar-id', 'Refacciones\MarcasController@buscar_id');
});

Route::resource('catalogo-marcas', 'Refacciones\MarcasController');
Route::resource('catalogo-ubicacion', 'Refacciones\UbicacionController');

Route::group(['prefix' => 'catalogo-ubicacion-llaves'], function () {
    Route::get('/', 'Refacciones\UbicacionLLavesController@index');
    Route::get('/{id}', 'Refacciones\UbicacionLLavesController@show');
    Route::post('/', 'Refacciones\UbicacionLLavesController@store');
    Route::put('/{id}', 'Refacciones\UbicacionLLavesController@update');
    Route::delete('/{id}', 'Refacciones\UbicacionLLavesController@destroy');
});


Route::group(['prefix' => 'recepcion-unidades'], function () {
    Route::get('/ultimo-registro', 'Refacciones\RecepcionUnidadesController@getLastRecord');    
});

// dataformulariorecepcion
Route::resource('recepcion-unidades', 'Refacciones\RecepcionUnidadesController');
Route::resource('catalogo-estatus-ventas', 'Refacciones\CatalogoEstatusVentaController');
Route::resource('catalogo-estatus-compras', 'Refacciones\CatalogoEstatusCompraController');
Route::resource('catalogo-estatus-inventario', 'Refacciones\CatalogoEstatusInventarioController');

Route::group(['prefix' => 'desglose-producto'], function () {
    Route::get('/', 'Refacciones\DesgloseProductosController@index');
    Route::get('/actualizaStockByProducto', 'Refacciones\DesgloseProductosController@actualizaStockByProducto');
    Route::get('/getStockByProducto', 'Refacciones\DesgloseProductosController@getStockByProducto');
    Route::get('/{id}', 'Refacciones\DesgloseProductosController@show');
    Route::post('/', 'Refacciones\DesgloseProductosController@store');
    Route::put('/{id}', 'Refacciones\DesgloseProductosController@update');
    Route::delete('/{id}', 'Refacciones\DesgloseProductosController@destroy');
});

Route::group(['prefix' => 'devolucion-pieza'], function () {
    Route::get('/', 'Refacciones\DevolucionPiezaController@index');
    Route::get('/{id}', 'Refacciones\DevolucionPiezaController@show');
    Route::get('/por-orden-compra/{orden_compra_id}', 'Refacciones\DevolucionPiezaController@getByOrdenCompraId');
    Route::post('/', 'Refacciones\DevolucionPiezaController@store');
    Route::put('/{id}', 'Refacciones\DevolucionPiezaController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionPiezaController@destroy');
});


Route::resource('catalogo-clave-cliente', 'Refacciones\CatalogoClaveClienteController');

Route::group(['prefix' => 'inventario-producto'], function () {
    Route::get('/productos', 'Refacciones\InventarioProductosController@inventarioProductos');
    Route::get('/buscar-productos', 'Refacciones\InventarioProductosController@getInventarioByProductoAttr');
    Route::get('/inventario-completo/{id_inventario}', 'Refacciones\InventarioProductosController@validarInventarioCompleto');
});
Route::resource('inventario-producto', 'Refacciones\InventarioProductosController');

Route::group(['prefix' => 'inventario'], function () {
    Route::get('/validaractivo', 'Refacciones\InventarioController@validarInventarioActivo');
    Route::get('/validarid/{id}', 'Refacciones\InventarioController@ultimoidinventario'); 
});
Route::group(['prefix' => 'masterpedidoproducto'], function () {
    Route::post('/filtrar', 'Refacciones\MaProductoPedidoController@filtrarPedidos');
});
Route::resource('masterpedidoproducto', 'Refacciones\MaProductoPedidoController');

Route::group(['prefix' => 'pedidoproducto'], function () {
    Route::get('/listado', 'Refacciones\ProductoPedidosController@getpedidoproductos');
});
Route::resource('pedidoproducto', 'Refacciones\ProductoPedidosController');
Route::resource('inventario', 'Refacciones\InventarioController');
Route::resource('tipopedido', 'Refacciones\CatTipoPedidoController');
Route::resource('enviopedido', 'Refacciones\EnvioPedidoDetalleController');

Route::group(['prefix' => 'clave-cliente'], function () {
    Route::get('/', 'Refacciones\ClaveClienteController@index');
    Route::get('/{id}', 'Refacciones\ClaveClienteController@show');
    Route::post('/', 'Refacciones\ClaveClienteController@store');
    Route::put('/{id}', 'Refacciones\ClaveClienteController@update');
    Route::delete('/{id}', 'Refacciones\ClaveClienteController@destroy');
    Route::get('/cliente-lista/{id}', 'Refacciones\ClaveClienteController@searchClienteId');
});

Route::resource('catalogo-ubicacion-producto', 'Refacciones\UbicacionProductoController');
Route::post('/notificaciones', 'Sistemas\NotificacionesController@setNotificacion');
Route::get('/notificaciones', 'Sistemas\NotificacionesController@getNotificaciones');
Route::get('/chat', 'Sistemas\NotificacionesController@getChat');

//Bodyshop
Route::resource('catalogo-proyectos-bodyshop', 'Bodyshop\CatProyectosController');
