<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'unidades'], function () {
    Route::get('/resumen-contabilidad', 'Autos\UnidadesController@getTotalUnidadesnuevas');
});

Route::resource('unidades', 'Autos\UnidadesController');

Route::group(['prefix' => 'documentosventa'], function () {
    Route::get('/byidunidad/{id_unidad}', 'Autos\DocumentosVentaController@getArchivosbyIdunidad');
    Route::get('/descargar/{string}', 'Autos\DocumentosVentaController@downloadDocumento');
    Route::post('/actualizar', 'Autos\DocumentosVentaController@updatedocumentoventa');
});

Route::resource('tipodocumentoventa', 'Autos\TipoDocumentosController');

Route::group(['prefix' => 'informaciondocumentosventa'], function () {
    Route::get('/byidunidad/{id_unidad}', 'Autos\InformacionDocumentosVentaController@getByIdunidad');
});
Route::resource('informaciondocumentosventa', 'Autos\InformacionDocumentosVentaController');
Route::resource('documentosventa', 'Autos\DocumentosVentaController');
Route::resource('tipodocumentoventa', 'Autos\TipoDocumentosVentaController');

Route::resource('producto-servicio', 'Autos\HistoricoProductosController');

Route::group(['prefix' => 'seminuevos'], function () {
    Route::get('/ultimo-registro', 'Refacciones\RecepcionUnidadesController@getLastRecord');
    Route::get('/resumen-contabilidad', 'Seminuevos\SeminuevosController@getTotalUnidadesSeminuevas');
});

Route::group(['prefix' => 'seminuevos/pre-pedidos'], function () {
    Route::get('/', 'Autos\VentaAutosController@getPrepedidosSeminuevos');
    Route::get('/{prepedido_id}', 'Autos\VentaAutosController@getPrepedidosSeminuevosById'); //show by id pre pedido
});

Route::resource('seminuevos', 'Seminuevos\SeminuevosController');

Route::resource('seminuevos/control-documentos/documentacion', 'Seminuevos\DocumentacionController');

Route::group(['prefix' => 'catalogo-modelos'], function () {
    Route::post('/buscar-id', 'Autos\CatalogoModelosController@buscar_id');
});

Route::resource('catalogo-modelos', 'Autos\CatalogoModelosController');

Route::group(['prefix' => 'venta-unidades'], function () {
    Route::get('/formatosalida/{id_venta_unidad}', 'Autos\VentaAutosController@getdataformatosalida');
    Route::post('/storeformatosalida', 'Autos\VentaAutosController@guardarformatosalida');
});

Route::resource('venta-unidades', 'Autos\VentaAutosController');

Route::group(['prefix' => 'inventario-unidades'], function () {
    Route::post('/guardarformulario', 'Autos\Inventario\InventarioController@storeInventarioCompleto');
    Route::get('/getformulario/{id}', 'Autos\Inventario\InventarioController@getformulario');
});

Route::resource('inventario-unidades', 'Autos\Inventario\InventarioController');

Route::group(['prefix' => 'autos/pre-pedidos'], function () {
    Route::get('/preventa-equipo-opcional', 'Autos\EquipoOpcionalController@getVentaAutosEquipoOpcional'); //unidades pre pedidas con equipo adicional
    Route::get('/', 'Autos\VentaAutosController@getPrepedidos');
    Route::get('/{prepedido_id}', 'Autos\VentaAutosController@getPrepedidosById'); //show by id pre pedido
});

Route::resource('autos/equipo-opcional', 'Autos\EquipoOpcionalController');

//duplicado - catalogo modelos
Route::group(['prefix' => 'catalogo-autos'], function () {
    Route::get('/', 'Autos\CatalogoAutosController@index');
    Route::get('/{id}', 'Autos\CatalogoAutosController@show');
    Route::post('/', 'Autos\CatalogoAutosController@store');
    Route::put('/{id}', 'Autos\CatalogoAutosController@update');
    Route::delete('/{id}', 'Autos\CatalogoAutosController@destroy');
    Route::post('/buscar-id', 'Autos\CatalogoAutosController@buscar_id');
    Route::get('/buscar-catalogo/{id_catalogo}', 'Autos\CatalogoAutosController@buscar_elementos');
});

Route::group(['prefix' => 'catalogo-vestiduras'], function () {
    Route::get('buscar-id/{nombre}', 'Autos\CatalogoVestidurasController@buscar_id');
});

Route::resource('catalogo-vestiduras', 'Autos\CatalogoVestidurasController');
