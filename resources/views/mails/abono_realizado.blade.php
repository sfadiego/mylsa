@extends('mails.template')

@section('content')
<h3>Pago realizado</h3>
<p>
        Estimado cliente: {{ $demo->numero_cliente}} {{ $demo->nombre}}.<br/>
        Se efectuo un pago con número de folio <?php echo str_pad($demo->abono_id, 5, "0", STR_PAD_LEFT) ?> por un monto de <?php echo '$'.(number_format($demo->total_abono,2));?>  por el concepto de {{ $demo->concepto }} el día {{ date('d/m/y', strtotime($demo->fecha)) }} <br/>
        Clic en el enlace para descargar el comprobante de pago <a href="<?php echo 'https://sohex.net/new-dms-hexade230320.sohex.net/caja/entradas/imprime_comprobante?abono_id='.base64_encode($demo->abono_id);?>">Comprobante de pago</a>
</p>
@endsection