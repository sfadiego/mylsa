<?php

use App\Models\Refacciones\EstatusTraspasoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusTraspasoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                EstatusTraspasoModel::ID => 1,
                EstatusTraspasoModel::NOMBRE => 'REALIZADO'
            ],
            [
                EstatusTraspasoModel::ID => 2,
                EstatusTraspasoModel::NOMBRE => 'CANCELADO'
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(EstatusTraspasoModel::getTableName())->insert($items);
        }
    }
}
