<?php

use App\Models\Financiamientos\CatPropuestasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatPropuestasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatPropuestasModel::ID => 1,
                CatPropuestasModel::PROPUESTA => '444621'
            ],
            [
                CatPropuestasModel::ID => 2,
                CatPropuestasModel::PROPUESTA => '450314'
            ],
            [
                CatPropuestasModel::ID => 3,
                CatPropuestasModel::PROPUESTA => '482001'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatPropuestasModel::getTableName())->insert($value);
        }
    }
}
