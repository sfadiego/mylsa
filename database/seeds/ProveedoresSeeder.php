<?php

use App\Models\Refacciones\ProveedorRefacciones;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProveedoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proveedores = [
            [
                ProveedorRefacciones::ID => 1,
                ProveedorRefacciones::PROVEEDOR_NOMBRE => 'FORD',
                ProveedorRefacciones::PROVEEDOR_RFC=> 'SASFD901028JZ8',
                ProveedorRefacciones::PROVEEDOR_CALLE=> 'camino real',
                ProveedorRefacciones::PROVEEDOR_NUMERO=> '2032',
                ProveedorRefacciones::PROVEEDOR_COLONIA=> 'colima',
                ProveedorRefacciones::PROVEEDOR_ESTADO=> 'colima',
                ProveedorRefacciones::PROVEEDOR_PAIS=> 'mexico',
            ],
            [
                ProveedorRefacciones::ID => 1,
                ProveedorRefacciones::PROVEEDOR_NOMBRE => 'MICHELIN',
                ProveedorRefacciones::PROVEEDOR_RFC=> 'SOSFD901028JZ8',
                ProveedorRefacciones::PROVEEDOR_CALLE=> 'moctuezuma',
                ProveedorRefacciones::PROVEEDOR_NUMERO=> '2389',
                ProveedorRefacciones::PROVEEDOR_COLONIA=> 'centro',
                ProveedorRefacciones::PROVEEDOR_ESTADO=> 'colima',
                ProveedorRefacciones::PROVEEDOR_PAIS=> 'mexico',
            ],
            [
                ProveedorRefacciones::ID => 1,
                ProveedorRefacciones::PROVEEDOR_NOMBRE => 'GOODYEAR',
                ProveedorRefacciones::PROVEEDOR_RFC=> 'SOSFD901028JZ8',
                ProveedorRefacciones::PROVEEDOR_CALLE=> 'moctuezuma',
                ProveedorRefacciones::PROVEEDOR_NUMERO=> '4522',
                ProveedorRefacciones::PROVEEDOR_COLONIA=> 'centro',
                ProveedorRefacciones::PROVEEDOR_ESTADO=> 'colima',
                ProveedorRefacciones::PROVEEDOR_PAIS=> 'mexico',
            ],
        ];

        foreach ($proveedores as $key => $value) {
            DB::table(ProveedorRefacciones::getTableName())->insert([
                ProveedorRefacciones::PROVEEDOR_NOMBRE => $value[ProveedorRefacciones::PROVEEDOR_NOMBRE],
                ProveedorRefacciones::PROVEEDOR_RFC => $value[ProveedorRefacciones::PROVEEDOR_RFC],
                ProveedorRefacciones::PROVEEDOR_CALLE => $value[ProveedorRefacciones::PROVEEDOR_CALLE],
                ProveedorRefacciones::PROVEEDOR_NUMERO => $value[ProveedorRefacciones::PROVEEDOR_NUMERO],
                ProveedorRefacciones::PROVEEDOR_COLONIA => $value[ProveedorRefacciones::PROVEEDOR_COLONIA],
                ProveedorRefacciones::PROVEEDOR_ESTADO => $value[ProveedorRefacciones::PROVEEDOR_ESTADO],
                ProveedorRefacciones::PROVEEDOR_PAIS => $value[ProveedorRefacciones::PROVEEDOR_PAIS]
            ]);
        }
    }
}
