<?php

use App\Models\Autos\CatModelosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoModelosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [

            [
                CatModelosModel::NOMBRE => 'RANGER',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F350',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
        ];

        foreach ($data as $key => $items) {
            DB::table(CatModelosModel::getTableName())->insert($items);
        }
    }
}
