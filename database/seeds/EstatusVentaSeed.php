<?php

use App\Models\Refacciones\EstatusVentaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusVentaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                EstatusVentaModel::ID => 1,
                EstatusVentaModel::NOMBRE => 'PROCESO'
            ],
            [
                EstatusVentaModel::ID => 2,
                EstatusVentaModel::NOMBRE => 'VENDIDO'
            ],
            [
                EstatusVentaModel::ID => 3,
                EstatusVentaModel::NOMBRE => 'FACTURA PAGADA'
            ],
            [
                EstatusVentaModel::ID => 4,
                EstatusVentaModel::NOMBRE => 'CANCELADA'
            ],
            [
                EstatusVentaModel::ID => 5,
                EstatusVentaModel::NOMBRE => 'DEVOLUCION'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(EstatusVentaModel::getTableName())->insert($items);
        }
    }
}
