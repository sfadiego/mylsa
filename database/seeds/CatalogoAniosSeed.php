<?php

use App\Models\Refacciones\CatalogoAnioModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoAniosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoAnioModel::NOMBRE => 1995],
            [CatalogoAnioModel::NOMBRE => 1996],
            [CatalogoAnioModel::NOMBRE => 1997],
            [CatalogoAnioModel::NOMBRE => 1998],
            [CatalogoAnioModel::NOMBRE => 1999],
            [CatalogoAnioModel::NOMBRE => 2001],
            [CatalogoAnioModel::NOMBRE => 2002],
            [CatalogoAnioModel::NOMBRE => 2003],
            [CatalogoAnioModel::NOMBRE => 2004],
            [CatalogoAnioModel::NOMBRE => 2005],
            [CatalogoAnioModel::NOMBRE => 2006],
            [CatalogoAnioModel::NOMBRE => 2007],
            [CatalogoAnioModel::NOMBRE => 2008],
            [CatalogoAnioModel::NOMBRE => 2009],
            [CatalogoAnioModel::NOMBRE => 2010],
            [CatalogoAnioModel::NOMBRE => 2011],
            [CatalogoAnioModel::NOMBRE => 2012],
            [CatalogoAnioModel::NOMBRE => 2013],
            [CatalogoAnioModel::NOMBRE => 2014],
            [CatalogoAnioModel::NOMBRE => 2015],
            [CatalogoAnioModel::NOMBRE => 2016],
            [CatalogoAnioModel::NOMBRE => 2017],
            [CatalogoAnioModel::NOMBRE => 2018],
            [CatalogoAnioModel::NOMBRE => 2019],
            [CatalogoAnioModel::NOMBRE => 2020],
            [CatalogoAnioModel::NOMBRE => 2021],
            [CatalogoAnioModel::NOMBRE => 2022]
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoAnioModel::getTableName())->insert($items);
        }
    }
}
