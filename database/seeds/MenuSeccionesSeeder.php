<?php

use App\Models\Usuarios\MenuSeccionesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                MenuSeccionesModel::ID => 1,
                MenuSeccionesModel::NOMBRE => 'Movimientos',
                MenuSeccionesModel::MODULO_ID => 2
            ],
            [
                MenuSeccionesModel::ID => 2,
                MenuSeccionesModel::NOMBRE => 'Facturas',
                MenuSeccionesModel::MODULO_ID => 2
            ],
            [
                MenuSeccionesModel::ID => 3,
                MenuSeccionesModel::NOMBRE => 'Consultas',
                MenuSeccionesModel::MODULO_ID => 2
            ],
            [
                MenuSeccionesModel::ID => 4,
                MenuSeccionesModel::NOMBRE => 'Catalogos',
                MenuSeccionesModel::MODULO_ID => 3
            ],
            [
                MenuSeccionesModel::ID => 5,
                MenuSeccionesModel::NOMBRE => 'Inventario',
                MenuSeccionesModel::MODULO_ID => 3
            ],
            [
                MenuSeccionesModel::ID => 6,
                MenuSeccionesModel::NOMBRE => 'Ventas',
                MenuSeccionesModel::MODULO_ID => 3
            ],
            [
                MenuSeccionesModel::ID => 7,
                MenuSeccionesModel::NOMBRE => 'Reportes',
                MenuSeccionesModel::MODULO_ID => 3
            ],
            [
                MenuSeccionesModel::ID => 8,
                MenuSeccionesModel::NOMBRE => 'Roles de Usuario',
                MenuSeccionesModel::MODULO_ID => 10
            ],
            [
                MenuSeccionesModel::ID => 9,
                MenuSeccionesModel::NOMBRE => 'Catalogos',
                MenuSeccionesModel::MODULO_ID => 10
            ],
            [
                MenuSeccionesModel::ID => 12,
                MenuSeccionesModel::NOMBRE => 'Recepción',
                MenuSeccionesModel::MODULO_ID => 4
            ],
            [
                MenuSeccionesModel::ID => 13,
                MenuSeccionesModel::NOMBRE => 'Ord. Servicio',
                MenuSeccionesModel::MODULO_ID => 4
            ],
            [
                MenuSeccionesModel::ID => 14,
                MenuSeccionesModel::NOMBRE => 'Ventas',
                MenuSeccionesModel::MODULO_ID => 4
            ],
            [
                MenuSeccionesModel::ID => 15,
                MenuSeccionesModel::NOMBRE => 'Reportes',
                MenuSeccionesModel::MODULO_ID => 4
            ],
            [
                MenuSeccionesModel::ID => 16,
                MenuSeccionesModel::NOMBRE => 'Contabilidad',
                MenuSeccionesModel::MODULO_ID => 5
            ],
            [
                MenuSeccionesModel::ID => 18,
                MenuSeccionesModel::NOMBRE => 'Caja',
                MenuSeccionesModel::MODULO_ID => 6
            ],
            [
                MenuSeccionesModel::ID => 19,
                MenuSeccionesModel::NOMBRE => 'cxc',
                MenuSeccionesModel::MODULO_ID => 7
            ],
            [
                MenuSeccionesModel::ID => 20,
                MenuSeccionesModel::NOMBRE => 'cxp',
                MenuSeccionesModel::MODULO_ID => 8
            ],
            [
                MenuSeccionesModel::ID => 21,
                MenuSeccionesModel::NOMBRE => 'Monitoreo',
                MenuSeccionesModel::MODULO_ID => 9
            ],
            [
                MenuSeccionesModel::ID => 22,
                MenuSeccionesModel::NOMBRE => 'Inicio',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 23,
                MenuSeccionesModel::NOMBRE => 'Catalogos y consultas',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 25,
                MenuSeccionesModel::NOMBRE => 'Combustibles',
                MenuSeccionesModel::MODULO_ID => 17
            ],
            [
                MenuSeccionesModel::ID => 26,
                MenuSeccionesModel::NOMBRE => 'Servicios',
                MenuSeccionesModel::MODULO_ID => 14
            ],
            [
                MenuSeccionesModel::ID => 27,
                MenuSeccionesModel::NOMBRE => 'Acumulados',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 28,
                MenuSeccionesModel::NOMBRE => 'Fiscales',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 29,
                MenuSeccionesModel::NOMBRE => 'Reportes',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 30,
                MenuSeccionesModel::NOMBRE => 'Procesos de nómina',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 31,
                MenuSeccionesModel::NOMBRE => 'Herramientas',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 32,
                MenuSeccionesModel::NOMBRE => 'Configuración',
                MenuSeccionesModel::MODULO_ID => 11
            ],
            [
                MenuSeccionesModel::ID => 33,
                MenuSeccionesModel::NOMBRE => 'Ventas web',
                MenuSeccionesModel::MODULO_ID => 18
            ],
            [
                MenuSeccionesModel::ID => 34,
                MenuSeccionesModel::NOMBRE => 'FIS',
                MenuSeccionesModel::MODULO_ID => 19
            ],
            [
                MenuSeccionesModel::ID => 35,
                MenuSeccionesModel::NOMBRE => 'Logística',
                MenuSeccionesModel::MODULO_ID => 16
            ],
            [
                MenuSeccionesModel::ID => 36,
                MenuSeccionesModel::NOMBRE => 'Notificaciones',
                MenuSeccionesModel::MODULO_ID => 12
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(MenuSeccionesModel::getTableName())
                ->insert($items);
        }
    }
}
