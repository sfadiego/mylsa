<?php

use App\Models\Logistica\CatTipoSeveridadModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatTipoSeveridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatTipoSeveridadModel::ID => 1,
                CatTipoSeveridadModel::TIPO_SEVERIDAD => 'Tipo 1'
            ],
            [
                CatTipoSeveridadModel::ID => 2,
                CatTipoSeveridadModel::TIPO_SEVERIDAD => 'Tipo 2'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatTipoSeveridadModel::getTableName())->insert($items);
        }
    }
}
