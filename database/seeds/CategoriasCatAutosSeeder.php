<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Autos\CategoriaCatAutosModel;

class CategoriasCatAutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CategoriaCatAutosModel::NOMBRE => 'FIGO'],
            [CategoriaCatAutosModel::NOMBRE => 'FIESTA'],
            [CategoriaCatAutosModel::NOMBRE => 'FOCUS'],
            [CategoriaCatAutosModel::NOMBRE => 'FUSION'],
            [CategoriaCatAutosModel::NOMBRE => 'MUSTANG'],
            [CategoriaCatAutosModel::NOMBRE => 'ECOSPORT'],
            [CategoriaCatAutosModel::NOMBRE => 'ESCAPE'],
            [CategoriaCatAutosModel::NOMBRE => 'EDGE'],
            [CategoriaCatAutosModel::NOMBRE => 'EXPLORE'],
            [CategoriaCatAutosModel::NOMBRE => 'EXPEDITION'],
            [CategoriaCatAutosModel::NOMBRE => 'RANGER'],
            [CategoriaCatAutosModel::NOMBRE => 'F150'],
            [CategoriaCatAutosModel::NOMBRE => 'F250'],
            [CategoriaCatAutosModel::NOMBRE => 'F350'],
            [CategoriaCatAutosModel::NOMBRE => 'F450'],
            [CategoriaCatAutosModel::NOMBRE => 'F-550'],
            [CategoriaCatAutosModel::NOMBRE => 'TRANSIT'],
            [CategoriaCatAutosModel::NOMBRE => 'LOBO']
        ];

        foreach ($data as $key => $items) {
            DB::table(CategoriaCatAutosModel::getTableName())->insert($items);
        }
    }
}
