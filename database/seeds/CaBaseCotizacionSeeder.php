<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaBaseCotizacionModel as Model;
use Illuminate\Support\Facades\DB;

class CaBaseCotizacionSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Descripcion => 'Fija'],
            [Model::ID => 2, Model::Descripcion => 'Variable'],
            [Model::ID => 3, Model::Descripcion => 'Mixta']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
