<?php

use App\Models\Contabilidad\CatalogoCuentasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoCuentasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatalogoCuentasModel::ID => 1,
                CatalogoCuentasModel::NO_CUENTA => '1310 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Cuenta ventas',
                CatalogoCuentasModel::CVE_AUXILIAR => '00011',
                CatalogoCuentasModel::TIPO_CUENTA => '1',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 2,
                CatalogoCuentasModel::NO_CUENTA => '1310 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Cuenta Traspaso',
                CatalogoCuentasModel::CVE_AUXILIAR => '00011',
                CatalogoCuentasModel::TIPO_CUENTA => '2',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 3,
                CatalogoCuentasModel::NO_CUENTA => '1310 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Cuenta Compras',
                CatalogoCuentasModel::CVE_AUXILIAR => '00011',
                CatalogoCuentasModel::TIPO_CUENTA => '3',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 4,
                CatalogoCuentasModel::NO_CUENTA => '1310 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Devolucion proveedor',
                CatalogoCuentasModel::CVE_AUXILIAR => '00011',
                CatalogoCuentasModel::TIPO_CUENTA => '4',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 5,
                CatalogoCuentasModel::NO_CUENTA => '1210 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Devolucion venta',
                CatalogoCuentasModel::CVE_AUXILIAR => '00012',
                CatalogoCuentasModel::TIPO_CUENTA => '5',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 6,
                CatalogoCuentasModel::NO_CUENTA => '1210 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Venta de Autos nuevos',
                CatalogoCuentasModel::CVE_AUXILIAR => '00012',
                CatalogoCuentasModel::TIPO_CUENTA => '6',
                CatalogoCuentasModel::CODIGO => '123ABC456',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 7,
                CatalogoCuentasModel::NO_CUENTA => '1210 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Venta de Autos seminuevos',
                CatalogoCuentasModel::CVE_AUXILIAR => '00012',
                CatalogoCuentasModel::TIPO_CUENTA => '7',
                CatalogoCuentasModel::CODIGO => '123ABC488',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ],
            [
                CatalogoCuentasModel::ID => 8,
                CatalogoCuentasModel::NO_CUENTA => '1211 00',
                CatalogoCuentasModel::NOMBRE_CUENTA => 'Orden de servicio',
                CatalogoCuentasModel::CVE_AUXILIAR => '00012',
                CatalogoCuentasModel::TIPO_CUENTA => '8',
                CatalogoCuentasModel::CODIGO => '123ABC488',
                CatalogoCuentasModel::NATURALEZA => 'NATURALEZA',
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoCuentasModel::getTableName())->insert($items);
        }
    }
}
