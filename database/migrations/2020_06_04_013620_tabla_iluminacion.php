<?php

use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaIluminacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(IluminacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(IluminacionModel::ID);
            $table->string(IluminacionModel::ILUMINACION_AMBIENTAL)->nullable();
            $table->string(IluminacionModel::SISTEMA_ILUMINACION)->nullable();
            $table->string(IluminacionModel::FAROS_BIXENON)->nullable();
            $table->string(IluminacionModel::HID)->nullable();
            $table->string(IluminacionModel::LED)->nullable();
            $table->unsignedInteger(IluminacionModel::SALIDA_UNIDAD_ID);
            $table->foreign(IluminacionModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(IluminacionModel::getTableName());
    }
}
