<?php

use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VentasRealizadas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentasRealizadasModel::ID);
            $table->unsignedInteger(VentasRealizadasModel::FOLIO_ID)->nullable();
            $table->foreign(VentasRealizadasModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->unsignedInteger(VentasRealizadasModel::CLIENTE_ID)->nullable();
            $table->foreign(VentasRealizadasModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());

            $table->boolean(VentasRealizadasModel::ORDEN_CERRADA)->nullable();
            $table->float(VentasRealizadasModel::VENTA_TOTAL);
            $table->string(VentasRealizadasModel::NUMERO_ORDEN)->nullable();
            

            $table->integer(VentasRealizadasModel::TIPO_VENTA);
            $table->integer(VentasRealizadasModel::ALMACEN_ID)->nullable();

            $table->unsignedInteger(VentasRealizadasModel::PRECIO_ID);
            $table->foreign(VentasRealizadasModel::PRECIO_ID)->references(Precios::ID)->on(Precios::getTableName());

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentasRealizadasModel::getTableName());
    }
}
