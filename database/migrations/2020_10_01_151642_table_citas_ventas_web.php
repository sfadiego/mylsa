<?php

use App\Models\Telemarketing\CitasVentasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCitasVentasWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CitasVentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CitasVentasModel::ID);
            $table->integer(CitasVentasModel::ID_VENTA);
            $table->integer(CitasVentasModel::ID_ASESOR);
            $table->integer(CitasVentasModel::ID_STATUS);
            $table->integer(CitasVentasModel::ID_USUARIO_CREO);
            $table->date(CitasVentasModel::FECHA_CITA);
            $table->time(CitasVentasModel::HORARIO);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CitasVentasModel::getTableName());
    }
}
