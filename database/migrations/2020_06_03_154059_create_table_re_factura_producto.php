<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\ReFacturaProductoModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\EstatusFacturaModel;


class CreateTableReFacturaProducto extends Migration
{
    public function up()
    {
        Schema::create(ReFacturaProductoModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReFacturaProductoModel::ID);
            $table->unsignedInteger(ReFacturaProductoModel::FACTURA_ID);
            $table->unsignedInteger(ReFacturaProductoModel::PRODUCTO_ID);
            $table->unsignedInteger(ReFacturaProductoModel::ESTATUS_FACTURA_ID);
            $table->boolean(ReFacturaProductoModel::ACTIVO)->default(1);
            $table->unsignedInteger(ReFacturaProductoModel::USER_ID);
            $table->foreign(ReFacturaProductoModel::ESTATUS_FACTURA_ID)->references(EstatusFacturaModel::ID)->on(EstatusFacturaModel::getTableName());
            $table->foreign(ReFacturaProductoModel::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->foreign(ReFacturaProductoModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReFacturaProductoModel::getTableName());
    }
}
