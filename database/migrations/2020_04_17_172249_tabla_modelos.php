<?php

use App\Models\Autos\CatModelosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaModelos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(CatModelosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatModelosModel::ID);
            $table->string(CatModelosModel::NOMBRE);
            $table->integer(CatModelosModel::TIEMPO_LAVADO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatModelosModel::getTableName());
    }
}
