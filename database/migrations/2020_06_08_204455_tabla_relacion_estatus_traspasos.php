<?php

use App\Models\Refacciones\EstatusTraspasoModel;
use App\Models\Refacciones\ReEstatusTraspasoModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRelacionEstatusTraspasos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReEstatusTraspasoModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReEstatusTraspasoModel::ID);
            $table->unsignedInteger(ReEstatusTraspasoModel::TRASPASO_ID);
            $table->foreign(ReEstatusTraspasoModel::TRASPASO_ID)->references(Traspasos::ID)->on(Traspasos::getTableName());

            $table->unsignedInteger(ReEstatusTraspasoModel::ESTATUS_ID);
            $table->foreign(ReEstatusTraspasoModel::ESTATUS_ID)->references(EstatusTraspasoModel::ID)->on(EstatusTraspasoModel::getTableName());

            $table->unsignedInteger(ReEstatusTraspasoModel::USER_ID);
            $table->foreign(ReEstatusTraspasoModel::USER_ID)->references(User::ID)->on(User::getTableName());

            $table->boolean(ReEstatusTraspasoModel::ACTIVO)->default(1);
            //$table->integer(ReEstatusTraspasoModel::STOCK)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReEstatusTraspasoModel::getTableName());
    }
}
