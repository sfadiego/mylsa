<?php

use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\CatModelosModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Refacciones\CatalogoColoresModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSalidaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SalidaUnidadesModel::getTableName(), function (Blueprint $table) {
            $table->increments(SalidaUnidadesModel::ID);
            $table->string(SalidaUnidadesModel::NOMBRE_CLIENTE)->nullable();
            $table->string(SalidaUnidadesModel::NO_ECONOMICO)->nullable();
            $table->string(SalidaUnidadesModel::NO_SERIE)->nullable();
            
            $table->unsignedInteger(SalidaUnidadesModel::ID_CATALOGO);
            $table->foreign(SalidaUnidadesModel::ID_CATALOGO)->references(CatalogoAutosModel::ID)->on(CatalogoAutosModel::getTableName());


            $table->unsignedInteger(SalidaUnidadesModel::ID_MODELO);
            $table->foreign(SalidaUnidadesModel::ID_MODELO)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());

            $table->unsignedInteger(SalidaUnidadesModel::ID_COLOR);
            $table->foreign(SalidaUnidadesModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            
            $table->string(SalidaUnidadesModel::VEHICULO)->nullable();
            $table->string(SalidaUnidadesModel::NOMBRE_VENDEDOR)->nullable();
            $table->string(SalidaUnidadesModel::TIPO_VENTA)->nullable();
            $table->string(SalidaUnidadesModel::NO_ENCUESTA)->nullable();
            $table->string(SalidaUnidadesModel::NO_PEDIDO)->nullable();
            $table->string(SalidaUnidadesModel::FECHA_REGRESO)->nullable();
            $table->string(SalidaUnidadesModel::NOMBRE_ASESOR)->nullable();
            $table->string(SalidaUnidadesModel::URL_FIRMA_CLIENTE)->nullable();
            $table->string(SalidaUnidadesModel::URL_FIRMA_ASESOR)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SalidaUnidadesModel::getTableName());
    }
}
