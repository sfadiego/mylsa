<?php

use App\Models\Refacciones\MaProductoPedidoModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\ProveedorRefacciones;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaProductoPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductosPedidosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ProductosPedidosModel::ID);
            $table->integer(ProductosPedidosModel::ESTATUS_ID);
            $table->integer(ProductosPedidosModel::CANTIDAD_SOLICITADA);
            $table->integer(ProductosPedidosModel::CANTIDAD_CARGADA)->nullable();
            $table->integer(ProductosPedidosModel::CANTIDAD_BACKORDER)->nullable();

            $table->unsignedInteger(ProductosPedidosModel::PRODUCTO_ID);
            $table->foreign(ProductosPedidosModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());

            $table->unsignedInteger(ProductosPedidosModel::MA_PEDIDO_ID);
            $table->foreign(ProductosPedidosModel::MA_PEDIDO_ID)
                ->references(MaProductoPedidoModel::ID)
                ->on(MaProductoPedidoModel::getTableName());

            $table->unsignedInteger(ProductosPedidosModel::PROVEEDOR_ID)->nullable();
            $table->foreign(ProductosPedidosModel::PROVEEDOR_ID)
                ->references(ProveedorRefacciones::ID)
                ->on(ProveedorRefacciones::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ProductosPedidosModel::getTableName());
    }
}
