<?php

use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaAhorroCombustible extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AhorroCombustibleModel::getTableName(), function (Blueprint $table) {
            $table->increments(AhorroCombustibleModel::ID);
            $table->string(AhorroCombustibleModel::IVCT)->nullable();
            $table->string(AhorroCombustibleModel::TIVCT)->nullable();
            $table->unsignedInteger(AhorroCombustibleModel::SALIDA_UNIDAD_ID);
            $table->foreign(AhorroCombustibleModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AhorroCombustibleModel::getTableName());
    }
}
