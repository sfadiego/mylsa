<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\ReComprasEstatusModel;
use App\Models\Refacciones\EstatusCompra as EstatusCompraModel;
use App\Models\Refacciones\OrdenCompraModel;


class CreateTableReComprasEstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReComprasEstatusModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReComprasEstatusModel::ID);
            $table->unsignedInteger(ReComprasEstatusModel::COMPRA_ID);
            $table->unsignedInteger(ReComprasEstatusModel::ESTATUS_COMPRA_ID);
            //$table->unsignedInteger(ReComprasEstatusModel::STOCK);
            $table->boolean(ReComprasEstatusModel::ACTIVO)->default(1);
            $table->unsignedInteger(ReComprasEstatusModel::USER_ID);
            $table->foreign(ReComprasEstatusModel::ESTATUS_COMPRA_ID)->references(EstatusCompraModel::ID)->on(EstatusCompraModel::getTableName());
            $table->foreign(ReComprasEstatusModel::COMPRA_ID)->references(OrdenCompraModel::ID)->on(OrdenCompraModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReComprasEstatusModel::getTableName());
    }
}
