<?php

use App\Models\Refacciones\CatCfdiModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create(ClientesModel::getTableName(), function (Blueprint $table) {
            $table->increments(ClientesModel::ID);
            $table->string(ClientesModel::NUMERO_CLIENTE)->nullable();
            $table->string(ClientesModel::TIPO_REGISTRO)->nullable();
            $table->string(ClientesModel::NOMBRE)->nullable();
            $table->string(ClientesModel::APELLIDO_MATERNO)->nullable();
            $table->string(ClientesModel::APELLIDO_PATERNO)->nullable();
            $table->string(ClientesModel::REGIMEN_FISCAL)->nullable();
            $table->string(ClientesModel::NOMBRE_EMPRESA)->nullable();
            $table->string(ClientesModel::RFC)->nullable();
            $table->string(ClientesModel::DIRECCION)->nullable();
            $table->string(ClientesModel::NUMERO_INT)->nullable();
            $table->string(ClientesModel::NUMERO_EXT)->nullable();
            $table->string(ClientesModel::COLONIA)->nullable();
            $table->string(ClientesModel::MUNICIPIO)->nullable();
            $table->string(ClientesModel::ESTADO)->nullable();
            $table->string(ClientesModel::PAIS)->nullable();
            $table->string(ClientesModel::CODIGO_POSTAL)->nullable();
            $table->string(ClientesModel::TELEFONO)->nullable();
            $table->string(ClientesModel::TELEFONO_2)->nullable();
            $table->string(ClientesModel::TELEFONO_3)->nullable();
            $table->integer(ClientesModel::FLOTILLERO)->nullable();
            $table->string(ClientesModel::CORREO_ELECTRONICO)->nullable();
            $table->string(ClientesModel::CORREO_ELECTRONICO_2)->nullable();
            $table->date(ClientesModel::FECHA_NACIMIENTO)->nullable();
            $table->unsignedInteger(ClientesModel::CFDI_ID)->nullable();
            $table->foreign(ClientesModel::CFDI_ID)->references(CatCfdiModel::ID)->on(CatCfdiModel::getTableName());
            $table->unsignedInteger(ClientesModel::METODO_PAGO_ID)->nullable();
            $table->foreign(ClientesModel::METODO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->string(ClientesModel::FORMA_PAGO)->nullable();
            $table->string(ClientesModel::SALDO)->nullable();
            $table->string(ClientesModel::LIMITE_CREDITO)->nullable();
            $table->text(ClientesModel::NOTAS)->nullable();
            $table->boolean(ClientesModel::ES_CLIENTE)->nullable()->default(1); // 1 ya es cliente 0 puede ser un cliente prospecto
            $table->boolean(ClientesModel::APLICA_CREDITO)->nullable()->default(0); // 1 ya es cliente 0 puede ser un cliente prospecto
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ClientesModel::getTableName());
    }
}
