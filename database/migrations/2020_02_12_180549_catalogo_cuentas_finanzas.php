<?php

use App\Models\Contabilidad\CatalogoCuentasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoCuentasFinanzas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoCuentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoCuentasModel::ID);
            $table->string(CatalogoCuentasModel::NO_CUENTA);
            $table->string(CatalogoCuentasModel::NOMBRE_CUENTA);
            $table->string(CatalogoCuentasModel::CVE_AUXILIAR);
            $table->string(CatalogoCuentasModel::TIPO_CUENTA);
            $table->string(CatalogoCuentasModel::CODIGO);
            $table->string(CatalogoCuentasModel::NATURALEZA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoCuentasModel::getTableName());
    }
}
