<?php

use App\Models\Refacciones\MaProductoPedidoModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaMasterPedidoPiezas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MaProductoPedidoModel::getTableName(), function (Blueprint $table) {
            $table->increments(MaProductoPedidoModel::ID);
            $table->boolean(MaProductoPedidoModel::AUTORIZADO)->nullable()->default(0);
            $table->boolean(MaProductoPedidoModel::FINALIZADO)->nullable()->default(0);
            $table->unsignedInteger(MaProductoPedidoModel::ID_USUARIO_AUTORIZO)->nullable();
            $table->foreign(MaProductoPedidoModel::ID_USUARIO_AUTORIZO)
                ->references(User::ID)
                ->on(User::getTableName());
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MaProductoPedidoModel::getTableName());
    }
}
