<?php

use App\Models\Refacciones\Cardex\MovimientosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaMovimientosCardex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MovimientosModel::getTableName(), function (Blueprint $table) {
            $table->increments(MovimientosModel::ID);
            $table->string(MovimientosModel::NOMBRE);
            $table->string(MovimientosModel::CLAVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MovimientosModel::getTableName());
    }
}
