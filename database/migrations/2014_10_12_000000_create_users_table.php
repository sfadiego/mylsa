<?php

use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::getTableName(), function (Blueprint $table) {
            $table->increments(User::ID);
            $table->string(User::NOMBRE);
            $table->string(User::APELLIDO_PATERNO);
            $table->string(User::APELLIDO_MATERNO)->nullable();
            $table->string(User::TELEFONO)->nullable();
            $table->string(User::USUARIO)->unique();
            $table->string(User::EMAIL)->unique();
            $table->string(User::PASSWORD);
            $table->integer(User::ESTATUS_ID)->default(1);
            $table->string(User::RFC)->nullable()->unique();
            $table->boolean(User::ACTIVO)->default(1);
            $table->date(User::ULTIMO_ACCESO)->nullable();
            $table->timestamp(User::EMAIL_VERIFIED_AT)->nullable();
            $table->text(User::API_TOKEN)->nullable();
            $table->string(User::IP_ADRESS)->nullable();
            $table->boolean(User::SESION_ACTIVA)->default(0);
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(User::getTableName());
    }
}
