<?php

use App\Models\Logistica\CatEstatusSasSegurosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusSasSeguros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusSasSegurosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusSasSegurosModel::ID);
            $table->string(CatEstatusSasSegurosModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusSasSegurosModel::getTableName());
    }
}
