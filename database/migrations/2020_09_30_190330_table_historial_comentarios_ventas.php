<?php

use App\Models\Telemarketing\HistorialComentariosVentasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableHistorialComentariosVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistorialComentariosVentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistorialComentariosVentasModel::ID);
            $table->integer(HistorialComentariosVentasModel::ID_VENTA_WEB);
            $table->text(HistorialComentariosVentasModel::COMENTARIO);
            $table->integer(HistorialComentariosVentasModel::ID_USUARIO);
            $table->timestamp(HistorialComentariosVentasModel::FECHA_NOTIFICACION)->nullable();
            $table->integer(HistorialComentariosVentasModel::ID_TIPO_COMENTARIO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(HistorialComentariosVentasModel::getTableName());
    }
}
