<?php

use App\Models\Usuarios\RolModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarRolesusuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(User::ROL_ID)->default(2);
            $table->foreign(User::ROL_ID)->references(RolModel::ID)->on(RolModel::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::getTableName(), function (Blueprint $table) {
            $table->dropForeign(User::ROL_ID);
            $table->dropColumn(User::ROL_ID);
        });
    }
}
