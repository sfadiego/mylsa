<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\MenuSeccionesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaMenuSecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuSeccionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(MenuSeccionesModel::ID);
            $table->string(MenuSeccionesModel::NOMBRE);
            $table->unsignedInteger(MenuSeccionesModel::MODULO_ID);
            $table->foreign(MenuSeccionesModel::MODULO_ID)
                ->references(ModulosModel::ID)
                ->on(ModulosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MenuSeccionesModel::getTableName());
    }
}
