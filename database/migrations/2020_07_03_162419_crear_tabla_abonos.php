<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\CatCfdiModel;

class CrearTablaAbonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AbonosModel::getTableName(), function (Blueprint $table) {
            $table->increments(AbonosModel::ID);
            $table->unsignedInteger(AbonosModel::CUENTA_POR_COBRAR_ID);
            $table->foreign(AbonosModel::CUENTA_POR_COBRAR_ID)->references(CuentasPorCobrarModel::ID)->on(CuentasPorCobrarModel::getTableName());
            $table->unsignedInteger(AbonosModel::TIPO_ABONO_ID);
            $table->foreign(AbonosModel::TIPO_ABONO_ID)->references(CatTipoAbonoModel::ID)->on(CatTipoAbonoModel::getTableName());
            $table->unsignedInteger(AbonosModel::TIPO_PAGO_ID)->nullable();
            $table->foreign(AbonosModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->unsignedInteger(AbonosModel::ESTATUS_ABONO_ID)->nullable();
            $table->foreign(AbonosModel::ESTATUS_ABONO_ID)->references(CatEstatusAbonoModel::ID)->on(CatEstatusAbonoModel::getTableName());
            $table->unsignedInteger(AbonosModel::CFDI_ID)->nullable();
            $table->foreign(AbonosModel::CFDI_ID)->references(CatCfdiModel::ID)->on(CatCfdiModel::getTableName());
            $table->float(AbonosModel::TOTAL_ABONO);
            $table->date(AbonosModel::FECHA_VENCIMIENTO);
            $table->float(AbonosModel::TOTAL_PAGO)->nullable();
            $table->date(AbonosModel::FECHA_PAGO)->nullable();
            $table->unsignedInteger(AbonosModel::DIAS_MORATORIOS)->nullable();
            $table->float(AbonosModel::MONTO_MORATORIO)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AbonosModel::getTableName());

    }
}
