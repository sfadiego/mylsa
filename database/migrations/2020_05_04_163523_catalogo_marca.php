<?php

use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoMarca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoMarcasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoMarcasModel::ID);
            $table->string(CatalogoMarcasModel::NOMBRE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoMarcasModel::getTableName());
    }
}
