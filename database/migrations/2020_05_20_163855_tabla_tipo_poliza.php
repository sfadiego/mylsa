<?php

use App\Models\Polizas\CatTipoPolizasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaTipoPoliza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoPolizasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoPolizasModel::ID);
            $table->string(CatTipoPolizasModel::NOMBRE)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoPolizasModel::getTableName());
    }
}
