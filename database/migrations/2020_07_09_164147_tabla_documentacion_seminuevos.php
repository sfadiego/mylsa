<?php

use App\Models\Autos\UnidadesModel;
use App\Models\Seminuevos\DocumentacionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDocumentacionSeminuevos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DocumentacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(DocumentacionModel::ID);
            $table->string(DocumentacionModel::USUARIO_RECIBE);
            $table->date(DocumentacionModel::FECHA_RECEPCION);

            $table->unsignedInteger(DocumentacionModel::ID_UNIDAD);
            $table->foreign(DocumentacionModel::ID_UNIDAD)
                 ->references(UnidadesModel::ID)->on(UnidadesModel::getTableName());

            $table->string(DocumentacionModel::NUMERO_ECONOMICO);
            $table->string(DocumentacionModel::NUMERO_CLIENTE);
            $table->string(DocumentacionModel::REGIMEN_FISCAL);
            $table->string(DocumentacionModel::NOMBRE);
            $table->string(DocumentacionModel::APELLIDO_PATERNO);
            $table->string(DocumentacionModel::APELLIDO_MATERNO);
            $table->string(DocumentacionModel::NOMBRE_EMPRESA);
            $table->string(DocumentacionModel::COPIA_FACTURA_ORIGEN)->nullable();
            $table->string(DocumentacionModel::REFACTURACION_MYLSA)->nullable();
            $table->string(DocumentacionModel::FACTURA_ORIGINAL_ENDOSADA)->nullable();
            $table->string(DocumentacionModel::TENENCIAS_PAGADAS)->nullable();
            $table->string(DocumentacionModel::COPIA_TARJETA_CIRCULACION)->nullable();
            $table->string(DocumentacionModel::CERTIFICADO_VERIFICACION_VIGENTE)->nullable();
            $table->string(DocumentacionModel::MANUAL_PROPIETARIO)->nullable();
            $table->string(DocumentacionModel::POLIZA_GARANTIA)->nullable();
            $table->string(DocumentacionModel::CANCELACION_POLIZA_VIGENTE)->nullable();
            $table->string(DocumentacionModel::DUPLICADO_LLAVES)->nullable();
            $table->string(DocumentacionModel::CODIGO_RADIO)->nullable();
            $table->string(DocumentacionModel::BRILLO_SEGURIDAD)->nullable();
            $table->string(DocumentacionModel::CONTROL)->nullable();
            $table->string(DocumentacionModel::AUDIFONOS)->nullable();
            $table->string(DocumentacionModel::INE_FRONTAL)->nullable();
            $table->string(DocumentacionModel::INE_TRASERA)->nullable();
            $table->string(DocumentacionModel::CURP)->nullable();
            $table->string(DocumentacionModel::RFC_SAT)->nullable();
            $table->string(DocumentacionModel::COMPROBANTE_DOMICILIO)->nullable();
            $table->string(DocumentacionModel::CONSTANCIA_SITUACION_FISCAL)->nullable();
            $table->string(DocumentacionModel::ACTA_CONSTITUTIVA)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DocumentacionModel::getTableName());
    }
}
