<?php

use App\Models\Contabilidad\PolizasContabilidadModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaPolizaContabilidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PolizasContabilidadModel::getTableName(), function (Blueprint $table) {
            $table->increments(PolizasContabilidadModel::ID);
            //$table->string(PolizasContabilidadModel::NO_CUENTA);
            $table->string(PolizasContabilidadModel::POLIZA);
            $table->string(PolizasContabilidadModel::CONCEPTO);
            $table->string(PolizasContabilidadModel::FORMULO);
            $table->unsignedInteger(PolizasContabilidadModel::CUENTA);
            $table->unsignedInteger(PolizasContabilidadModel::SUBCUENTA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PolizasContabilidadModel::getTableName());
    }
}
