<?php

use App\Models\CuentasPorCobrar\SolicitudCreditoModel;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSolicitudCredito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SolicitudCreditoModel::getTableName(), function (Blueprint $table) {
            $table->increments(SolicitudCreditoModel::ID);
            $table->string(SolicitudCreditoModel::REFERENCIA_NOMBRE);
            $table->string(SolicitudCreditoModel::REFERENCIA_NOMBRE_2);
            $table->text(SolicitudCreditoModel::REFERENCIA_TELEFONO);
            $table->text(SolicitudCreditoModel::REFERENCIA_TELEFONOO_2);
            
            $table->unsignedInteger(SolicitudCreditoModel::CLIENTE_ID);
            $table->foreign(SolicitudCreditoModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->boolean(SolicitudCreditoModel::BURO_CREDITO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SolicitudCreditoModel::getTableName());
    }
}
